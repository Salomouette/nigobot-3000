const diceControllerService = require('../../Service/Dice/diceControllerService');

const diceData = require('../../Data/diceData');
const frenchTranslation = require('../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();

const validationImageBasePath = '/src/Assets/nigobot-3000-images/Dice/Validation/';
module.exports = {
  formatUserIPing: function(userID) {
    return '<@' + userID + '>';
  },
  generateDiceThrowDescription: function(callNumber, userID) {
    const numberOfDice = parseInt(callNumber.split('d')[0]);
    const diceFace = parseInt(callNumber.split('d')[1]);
    let description = null;

    if (!isNaN(numberOfDice) &&
            numberOfDice > 0 &&
            !isNaN(diceFace) &&
            diceFace > 0
    ) {
      if (numberOfDice === 1) {
        const diceResult = Math.floor(Math.random() * diceFace) + 1;
        description = this.formatUserIPing(userID) + tr.dice.youHaveMade + diceResult + '! \n';
        const isSpecialThrow = this.isSpecialThrow(diceResult, diceFace);
        if (isSpecialThrow !== undefined) {
          description = description + this.isSpecialThrow(diceResult, diceFace);
        }
      } else {
        description = this.formatUserIPing(userID) + tr.dice.youHaveMade + ':\n';
        for (let i = 0; i < numberOfDice; i++) {
          const diceResult = Math.floor(Math.random() * diceFace) + 1;
          description = description + '- ' + diceResult + '\n';
          const isSpecialThrow = this.isSpecialThrow(diceResult, diceFace);
          if (isSpecialThrow !== undefined) {
            description = description + this.isSpecialThrow(diceResult, diceFace);
          }
        }
      }
    } else {
      description = tr.error.errorInvalidArguments;
    }

    return description;
  },
  generateDiceSumDescription: function(callNumber, userID) {
    const numberOfDice = parseInt(callNumber.split('d')[0]);
    const diceFace = parseInt(callNumber.split('d')[1]);
    let description = null;

    if (!isNaN(numberOfDice) &&
            numberOfDice > 0 &&
            !isNaN(diceFace) &&
            diceFace > 0
    ) {
      if (numberOfDice === 1) {
        const diceResult = Math.floor(Math.random() * diceFace) + 1;
        description = this.formatUserIPing(userID) + tr.dice.youHaveMade + diceResult + '! \n';
        const isSpecialThrow = this.isSpecialThrow(diceResult, diceFace);
        if (isSpecialThrow !== undefined) {
          description = description + this.isSpecialThrow(diceResult, diceFace);
        }
      } else {
        description = this.formatUserIPing(userID) + tr.dice.youHaveMade + ':\n';
        let diceThrowSum = null;
        for (let i = 0; i < numberOfDice; i++) {
          const diceResult = Math.floor(Math.random() * diceFace) + 1;
          description = description + '- ' + diceResult + '\n';
          const isSpecialThrow = this.isSpecialThrow(diceResult, diceFace);
          if (isSpecialThrow !== undefined) {
            description = description + this.isSpecialThrow(diceResult, diceFace);
          }
          diceThrowSum = diceThrowSum + diceResult;
        }
        description = description + tr.dice.diceSum + diceThrowSum / numberOfDice + '.';
      }
    } else {
      description = tr.error.errorInvalidArguments;
    }

    return description;
  },
  generateDiceThrowValidateDescription: function(
      characteristic,
      skill,
      malus,
      userID,
  ) {
    const characteristicPoint = parseInt(characteristic);
    const skillPoint = skill !== undefined ? parseInt(skill) : null;
    const malusValue = malus !== undefined ? parseInt(malus) : null;

    let description = null;

    if (!isNaN(characteristicPoint) && characteristicPoint >= 0 && characteristicPoint <= 6) {
      const diceResult = Math.floor(Math.random() * 100) + 1;
      description = this.formatUserIPing(userID) + tr.dice.youHaveMade + diceResult + '! \n';

      // Generate Fail/Success message
      const isSpecialThrow = this.isSpecialThrow(diceResult, 100);
      if (isSpecialThrow !== undefined) {
        description = description + this.isSpecialThrow(diceResult, 100);
      }

      // Generate Validation
      const isValidThrow = this.validateDiceThrow(diceResult, characteristicPoint, skillPoint, malusValue);
      if (isValidThrow === true) {
        description = description + tr.dice.validThrow;
      } else {
        description = description + tr.dice.invalidThrow;
      }

      // Throw commentary
      const throwCommentary = this.generateCommentaryThrow(
          diceResult,
          this.getMaxValidThrowValue(characteristicPoint, skillPoint, malusValue),
      );
      if (throwCommentary !== null) {
        description = description + throwCommentary;
      }

      // Validation range
      description = description +
                tr.dice.valueRange +
                this.getMaxValidThrowValue(characteristicPoint, skillPoint, malusValue) +
                '.\n';

      // Save throw values
      diceControllerService.diceSaveUserThrow(diceResult, description, userID);
    } else {
      description = tr.error.errorInvalidArguments;
    }

    return description;
  },
  isSpecialThrow: function(diceThrow, diceFace) {
    if (diceFace >= 100) {
      if (diceThrow > diceFace * 0.99) {
        return tr.dice.specialThrow.absoluteFail;
      }
      if (diceThrow <= diceFace * 0.01) {
        return tr.dice.specialThrow.absoluteSuccess;
      }
      if (diceThrow >= diceFace * 0.96) {
        return tr.dice.specialThrow.criticFail;
      }
      if (diceThrow <= diceFace * 0.05) {
        return tr.dice.specialThrow.criticSuccess;
      }
    }
  },
  validateDiceThrow: function(
      diceThrow,
      characteristicPoint,
      skillPoint,
      malusValue,
  ) {
    const maxValidThrowValue = this.getMaxValidThrowValue(characteristicPoint, skillPoint, malusValue);
    return diceThrow <= maxValidThrowValue;
  },
  getMaxValidThrowValue: function(
      characteristicPoint,
      skillPoint,
      malusValue,
  ) {
    let maxThrowValue = diceData.getCharacteristicConstraintByLevel(characteristicPoint) +
            diceData.getSkillConstraintByLevel(skillPoint);
    maxThrowValue = maxThrowValue + malusValue;

    maxThrowValue = maxThrowValue >= 95 ? 95 : maxThrowValue;
    maxThrowValue = maxThrowValue <= 5 ? 5 : maxThrowValue;

    return maxThrowValue;
  },
  generateCommentaryThrow: function(
      throwValue,
      maxValidThrowValue,
  ) {
    const throwMarging = maxValidThrowValue - throwValue;
    let commentary = null;

    if (throwMarging >= 0) {
      if (throwMarging <= 2) {
        commentary = tr.dice.throwCommentary.reallyShort;
      } else if (throwMarging <= 5) {
        commentary = tr.dice.throwCommentary.short;
      } else if (throwMarging <= 30) {
        commentary = tr.dice.throwCommentary.average;
      } else if (throwMarging >= 30) {
        commentary = tr.dice.throwCommentary.large;
      }
    }

    return commentary;
  },
  generateInputsThrowResult: function(description) {
    const inputResults = {};

    if (description.includes('Votre jet est valide!')) {
      if (description.includes('absolu')) {
        inputResults.imagePath = validationImageBasePath + 'SuccesAbsolu.png';
        inputResults.colorEmbed = process.env.COLOR_DICE_ABSOLUTE_SUCCES_QUERY;
        inputResults.embedAuthorIcon = process.env.SET_AUTHOR_DICE_ABSOLUTE_SUCCES_ICON;
      } else if (description.includes('critique')) {
        inputResults.imagePath = validationImageBasePath + 'SuccesCritique.png';
        inputResults.colorEmbed = process.env.COLOR_DICE_CRITIC_SUCCES_QUERY;
        inputResults.embedAuthorIcon = process.env.SET_AUTHOR_DICE_CRITIC_SUCCES_ICON;
      } else {
        inputResults.imagePath = validationImageBasePath + 'Succes.png';
        inputResults.colorEmbed = process.env.COLOR_DICE_SUCCES_QUERY;
        inputResults.embedAuthorIcon = process.env.SET_AUTHOR_DICE_SUCCES_ICON;
      }
    }
    if (description.includes('Votre jet n\'est pas valide!')) {
      if (description.includes('absolu')) {
        inputResults.imagePath = validationImageBasePath + 'EchecAbsolu.png';
        inputResults.colorEmbed = process.env.COLOR_DICE_ABSOLUTE_FAIL_QUERY;
        inputResults.embedAuthorIcon = process.env.SET_AUTHOR_DICE_ABSOLUTE_FAIL_ICON;
      } else if (description.includes('critique')) {
        inputResults.imagePath = validationImageBasePath + 'EchecCritique.png';
        inputResults.colorEmbed = process.env.COLOR_DICE_CRITIC_FAIL_QUERY;
        inputResults.embedAuthorIcon = process.env.SET_AUTHOR_DICE_CRITIC_FAIL_ICON;
      } else {
        inputResults.imagePath = validationImageBasePath + 'Echec.png';
        inputResults.colorEmbed = process.env.COLOR_DICE_FAIL_QUERY;
        inputResults.embedAuthorIcon = process.env.SET_AUTHOR_DICE_FAIL_ICON;
      }
    }
    return inputResults;
  },
};
