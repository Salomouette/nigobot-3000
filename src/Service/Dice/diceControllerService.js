const botService = require('../botService');

const playerData = require('../../Data/playerData');
const systemData = require('../../Data/systemData');
module.exports = {
  diceSaveUserThrow: function(diceResult, throwDescription, userID) {
    const throwValidation = this.getTypeOfThrow(throwDescription);
    const players = playerData.getAllPlayers();

    for (let i = 0; i < players.players.length; i++) {
      if (players.players[i] != null) {
        // Check if user added
        if (players.players[i].id === userID) {
          if (!players.players[i].throws) {
            players.players[i].throws = [];
          }
          players.players[i].throws.push(
              {
                diceValue: diceResult,
                timePeriod: systemData.getSystemProperty('timePeriod'),
                throwValidation: throwValidation,
              },
          );
        }
      }
    }
    botService.jsonWriteFile(players, 'players');
  },
  getTypeOfThrow: function(diceThrowMessage) {
    diceThrowMessage = JSON.stringify(diceThrowMessage);
    if (diceThrowMessage.includes('Votre jet est valide!')) {
      if (diceThrowMessage.includes('réussite critique')) {
        return 'critic success';
      }
      if (diceThrowMessage.includes('réussite absolue')) {
        return 'absolute success';
      } else {
        return 'success';
      }
    }
    if (diceThrowMessage.includes('Votre jet n\'est pas valide!')) {
      if (diceThrowMessage.includes('échec critique')) {
        return 'critic fail';
      }
      if (diceThrowMessage.includes('échec absolu')) {
        return 'absolute fail';
      } else {
        return 'fail';
      }
    }
    return 'error';
  },
};
