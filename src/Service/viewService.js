module.exports = {
  formatUnderscoreAndBoldText: function(stringTitle) {
    return '**__' + stringTitle.toString() + '__**';
  },
  formatUnderscoreText: function(stringTitle) {
    return '__' + stringTitle.toString() + '__';
  },
  formatBoldText: function(stringTitle) {
    return '**' + stringTitle.toString() + '**';
  },
  formatToString: function(value) {
    value = value.toString();
    return value.length > 0 ? value : 'Vide';
  },
};
