const frenchTranslation = require('../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  generateDiscoveredRecipeFieldValue: function(recipes) {
    let output = '';
    if (recipes.length > 0) {
      let groupModule = recipes[0].group;
      output = '- ' + recipes[0].group + ' :';
      for (let i = 0; i < recipes.length; i++) {
        if (groupModule === recipes[i].group) {
          output = output + ' ' + recipes[i].name + ',';
        } else {
          groupModule = recipes[i].group;
          output = output.slice(0, -1);
          output = output + '.\n';
          output = output + '- ' + recipes[i].group + ' : ' + recipes[i].name + ',';
        }
      }
      output = output.slice(0, -1);
      output = output + '.\n';
    }
    return output;
  },
  sortDiscoveredRecipe: function(recipes) {
    const jsonOutput = [];
    for (let i = 0; i < recipes.length; i++) {
      jsonOutput.push(
          {
            group: recipes[i].group,
            name: recipes[i].name,
          },
      );
    }
    return jsonOutput.sort();
  },
  recipeComponentFormat: function(foundRecipeComponent) {
    let returnValue = '';
    for (let i = 0; i < foundRecipeComponent.length; i++) {
      returnValue = returnValue + '- ' + foundRecipeComponent[i] + '\n';
    }
    return returnValue;
  },
  craftMedecineCreationResultFields: function(foundRecipe, itemData) {
    const numberCrafted = foundRecipe.numberMinimalMade > 1 ? foundRecipe.numberMinimalMade : 1;

    const returnValue = {
      head: {
        name: tr.craft.craftCreationMedecineResultTemplate.youHaveCreated +
            ' ' +
            numberCrafted +
            ' ' +
            foundRecipe.name +
            '!',
        value: tr.craft.craftCreationMedecineResultTemplate.aMedecineObjet,
      },
      description: {
        name: tr.craft.craftCreationMedecineResultTemplate.description,
        value: itemData.description,
      },
      components: {
        name: tr.craft.craftCreationMedecineResultTemplate.components,
        value: this.recipeComponentFormat(foundRecipe.components),
      },
      type: {
        name: tr.craft.craftCreationMedecineResultTemplate.type,
        value: foundRecipe.group.toString(),
        inline: true,
      },
      sellPrice: {
        name: tr.craft.craftCreationMedecineResultTemplate.sellPrice,
        value: itemData.basePrice.toString(),
        inline: true,
      },
      illegal: {
        name: tr.craft.craftCreationModuleResultTemplate.illegal,
        value: foundRecipe.illegal ? 'Oui' : 'Non',
        inline: true,
      },
    };

    if (foundRecipe.group === 'Appât') {
      returnValue.head.value = tr.craft.craftCreationMedecineResultTemplate.aLure;
    }
    if (foundRecipe.group === 'Matériaux de fabrication') {
      returnValue.head.value = tr.craft.craftCreationMedecineResultTemplate.aCraftMaterial;
    }
    if (foundRecipe.group === 'Pokéball') {
      returnValue.head.value = tr.craft.craftCreationMedecineResultTemplate.aBall;
    }

    return returnValue;
  },
  craftMealCreationResultFields: function(foundRecipe) {
    return {
      head: {
        name: tr.craft.craftMealCreationResultFields.youHaveCreated + foundRecipe.name + '!',
        value: tr.craft.craftMealCreationResultFields.aMealObject,
      },
      description: {
        name: tr.craft.craftMealCreationResultFields.description,
        value: foundRecipe.description,
      },
      components: {
        name: tr.craft.craftMealCreationResultFields.components,
        value: this.recipeComponentFormat(foundRecipe.components),
      },
      sellPrice: {
        name: tr.craft.craftMealCreationResultFields.sellPrice,
        value: foundRecipe.sellPrice.toString(),
      },
    };
  },
  craftModuleCreationResultFields: function(foundRecipe) {
    return {
      head: {
        name: tr.craft.craftCreationModuleResultTemplate.youHaveCreated + foundRecipe.name + '!',
        value: tr.craft.craftCreationModuleResultTemplate.anElectronicObjet,
      },
      description: {
        name: tr.craft.craftCreationModuleResultTemplate.description,
        value: foundRecipe.description,
      },
      components: {
        name: tr.craft.craftCreationModuleResultTemplate.components,
        value: this.recipeComponentFormat(foundRecipe.components),
      },
      level: {
        name: tr.craft.craftCreationModuleResultTemplate.level,
        value: foundRecipe.level.toString(),
        inline: true,
      },
      sellPrice: {
        name: tr.craft.craftCreationModuleResultTemplate.sellPrice,
        value: foundRecipe.sellPrice.toString(),
        inline: true,
      },
      illegal: {
        name: tr.craft.craftCreationModuleResultTemplate.illegal,
        value: foundRecipe.illegal ? 'Oui' : 'Non',
        inline: true,
      },
    };
  },
  craftModuleRecipeShowFields: function(foundRecipe) {
    return {
      description: {
        name: foundRecipe.name,
        value: foundRecipe.description,
      },
      group: {
        name: tr.craft.craftCreationModuleResultTemplate.group,
        value: foundRecipe.group,
      },
      components: {
        name: tr.craft.craftCreationModuleResultTemplate.components,
        value: this.recipeComponentFormat(foundRecipe.components),
      },
      level: {
        name: tr.craft.craftCreationModuleResultTemplate.level,
        value: foundRecipe.level.toString(),
        inline: true,
      },
      sellPrice: {
        name: tr.craft.craftCreationModuleResultTemplate.sellPrice,
        value: foundRecipe.sellPrice.toString(),
        inline: true,
      },
      illegal: {
        name: tr.craft.craftCreationModuleResultTemplate.illegal,
        value: foundRecipe.illegal ? 'Oui' : 'Non',
        inline: true,
      },
    };
  },
  craftRecipeShowFields: function(foundRecipe, itemData) {
    return {
      description: {
        name: foundRecipe.name,
        value: itemData.description,
      },
      group: {
        name: tr.craft.craftCreationModuleResultTemplate.group,
        value: foundRecipe.group,
      },
      components: {
        name: tr.craft.craftCreationModuleResultTemplate.components,
        value: this.recipeComponentFormat(foundRecipe.components),
      },
      level: {
        name: tr.craft.craftCreationModuleResultTemplate.level,
        value: foundRecipe.level.toString(),
        inline: true,
      },
      sellPrice: {
        name: tr.craft.craftCreationModuleResultTemplate.sellPrice,
        value: itemData.basePrice.toString(),
        inline: true,
      },
      illegal: {
        name: tr.craft.craftCreationModuleResultTemplate.illegal,
        value: itemData.illegal ? 'Oui' : 'Non',
        inline: true,
      },
    };
  },
};
