const lodash = require('lodash');

const botService = require('../../Service/botService');
const messageService = require('../../Service/messageService');

const recipesData = require('../../Data/craftRecipesData');

const craftCreationModuleResultTemplate =
    require('../../Assets/EmbedMessage/Craft/ResultViews/craftCreationModuleResultTemplate');
const craftCreationMealResultTemplate =
    require('../../Assets/EmbedMessage/Craft/ResultViews/craftCreationMealResultTemplate');
const craftCreationMedecineResultTemplate =
    require('../../Assets/EmbedMessage/Craft/ResultViews/craftCreationChemistryResultTemplate');
const craftCreationBallResultTemplate =
    require('../../Assets/EmbedMessage/Craft/ResultViews/craftCreationBallResultTemplate');
const craftInvalidRecipeErrorTemplate =
    require('../../Assets/EmbedMessage/Craft/ErrorViews/craftInvalidRecipeErrorTemplate');
const craftinvalidThrowErrorTemplate =
    require('../../Assets/EmbedMessage/Craft/ErrorViews/craftinvalidThrowErrorTemplate');
module.exports = {
  formatCraftCommandInputCreate: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    switch (inputCommandPlaceNumber) {
      case 1:
        formatedInput = callInput.split('--Composant1=')[1];
        formatedInput = formatedInput.split('--Composant2=')[0];
        break;
      case 2:
        formatedInput = callInput.split('--Composant2=')[1];
        formatedInput = formatedInput.split('--Composant3=')[0];
        break;
      case 3:
        formatedInput = callInput.split('--Composant3=')[1];
        formatedInput = formatedInput.split('--Composant4=')[0];
        break;
      case 4:
        formatedInput = callInput.split('--Composant4=')[1];
        formatedInput = formatedInput.split('--NivCaractéristique=')[0];
        break;
      case 5:
        if (callInput.includes('NivCompétence')) {
          formatedInput = callInput.split('--NivCaractéristique=')[1];
          formatedInput = formatedInput.split('--NivCompétence=')[0];
        } else if (callInput.includes('Modificateur')) {
          formatedInput = callInput.split('--NivCaractéristique=')[1];
          formatedInput = formatedInput.split('--Modificateur=')[0];
        } else {
          formatedInput = callInput.split('--NivCaractéristique=')[1];
        }
        break;
      case 6:
        if (callInput.includes('NivCompétence')) {
          if (callInput.includes('Modificateur')) {
            formatedInput = callInput.split('--NivCompétence=')[1];
            formatedInput = formatedInput.split('--Modificateur=')[0];
          } else {
            formatedInput = callInput.split('--NivCompétence=')[1];
          }
        }
        break;
      case 7:
        if (callInput.includes('Modificateur')) {
          formatedInput = callInput.split('--Modificateur=')[1];
        } else {
          formatedInput = 0;
        }
        break;
      default:
        formatedInput = 'error';
        break;
    }

    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/(\r\n|\n|\r)/gm, '') : formatedInput;
    }
    return null;
  },
  findCompatibleRecipe: function(
      componentInput1,
      componentInput2,
      componentInput3,
      componentInput4,
      isValidThrow,
      skillValue,
  ) {
    const recipes = recipesData.getAllCraftRecipes();
    if (isValidThrow) {
      const wantedRecipeSorted = [];
      if (componentInput1.length > 0) {
        wantedRecipeSorted.push((botService.removeDiacritics(componentInput1)).toLowerCase());
      }
      if (componentInput2.length > 0) {
        wantedRecipeSorted.push((botService.removeDiacritics(componentInput2)).toLowerCase());
      }
      if (componentInput3.length > 0) {
        wantedRecipeSorted.push((botService.removeDiacritics(componentInput3)).toLowerCase());
      }
      if (componentInput4.length > 0) {
        wantedRecipeSorted.push((botService.removeDiacritics(componentInput4)).toLowerCase());
      }
      wantedRecipeSorted.sort();

      for (let i = 0; i < recipes.recipes.length; i++) {
        if (recipes.recipes[i] != null) {
          skillValue = parseInt(skillValue);
          const recipeComponentsSorted = [];
          for (let j = 0; j < recipes.recipes[i].components.length; j++) {
            recipeComponentsSorted.push(
                (botService.removeDiacritics(recipes.recipes[i].components[j])).toLowerCase(),
            );
          }
          recipeComponentsSorted.sort();
          // Check exact recipe
          if (lodash.isEqual(recipeComponentsSorted, wantedRecipeSorted)) {
            // Check if skill level is enough
            if (skillValue >= (parseInt(recipes.recipes[i].level))) {
              recipesData.setCraftRecipeDiscovered(i);
              return recipes.recipes[i];
            }
          }
        }
      }
      return 'invalidRecipe';
    }
    return 'invalidThrow';
  },
  switchCraftResultView: function(foundRecipe, message) {
    let embed = '';

    if (foundRecipe === 'invalidThrow') {
      embed = craftinvalidThrowErrorTemplate.craftinvalidThrowErrorTemplate();
      messageService.sendEmbedMessageWithImages(message.channel, embed.embedMessage, true);
    } else if (foundRecipe === 'invalidRecipe') {
      embed = craftInvalidRecipeErrorTemplate.craftInvalidRecipeErrorTemplate();
      messageService.sendEmbedMessageWithImages(message.channel, embed.embedMessage, true);
    } else {
      switch (foundRecipe.type) {
        case 'Module':
          embed = craftCreationModuleResultTemplate.craftCreationModuleResultTemplate(foundRecipe);
        case 'Meal':
          embed = craftCreationMealResultTemplate.craftCreationMealResultTemplate(foundRecipe);
        case 'Chemical':
          embed = craftCreationMedecineResultTemplate.craftCreationMedecineResultTemplate(foundRecipe);
        case 'Ball':
          embed = craftCreationBallResultTemplate.craftCreationBallResultTemplate(foundRecipe);
      }
      messageService.sendEmbedMessageWithImages(message.channel, embed.embedMessage, embed.images, true);
    }
    return embed;
  },
};
