const itemsData = require('../../Data/itemsData');
const frenchTranslation = require('../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  formatCommandInput: function(callInput) {
    return callInput.charAt(0).toUpperCase() + callInput.slice(1);
  },
  formatSubCategoryView: function(callInput) {
    const subCategoryItemsNames = itemsData.getAllItemsNamePerSubCategory(callInput);
    let returnValue = '';
    if (subCategoryItemsNames !== null) {
      returnValue = tr.item.listSubCategories +
          itemsData.findSubCategoryNameFromRequestedSubCategory(callInput) + ' :\n';

      for (let i = 0; i < subCategoryItemsNames.length; i++) {
        returnValue = returnValue + '- ' + subCategoryItemsNames[i] + '\n';
      }
    } else {
      returnValue = tr.error.returnValue;
    }

    return returnValue;
  },
  formatSubCategoriesView: function(callInput) {
    const subCategories = itemsData.getAllSubCategoriesPerCategory(callInput);
    let returnValue = '';

    if (subCategories !== null) {
      returnValue = tr.item.listSubCategories + itemsData.findCategoryNameFromRequestedCategory(callInput) + ' :\n';

      for (let i = 0; i < subCategories.length; i++) {
        returnValue = returnValue + '- ' + subCategories[i] + '\n';
      }
    } else {
      returnValue = tr.error.returnValue;
    }

    return returnValue;
  },
  formatCategoriesView: function(callInput) {
    const categories = itemsData.getAllCategoriesPerCategory(callInput);
    let returnValue = '';

    if (categories !== null) {
      returnValue = tr.item.listCategories + ' :\n';

      for (let i = 0; i < categories.length; i++) {
        returnValue = returnValue + '- ' + categories[i] + '\n';
      }
    } else {
      returnValue = tr.error.returnValue;
    }

    return returnValue;
  },
};
