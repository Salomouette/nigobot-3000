const craftViewService = require('../../Service/Craft/craftViewService');
const viewService = require('../../Service/viewService');
const botService = require('../../Service/botService');
const emojiService = require('../../Service/emojiService');

const biomeCategoriesData = require('../../Data/biomeCategoriesData');
const playersData = require('../../Data/playerData');

const frenchTranslation = require('../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  itemViewRecipeFormat: function(recipes) {
    const returnValue = [];
    for (let i = 0; i < recipes.length; i++) {
      returnValue.push(
          {
            inline: true,
            name: viewService.formatUnderscoreAndBoldText(tr.item.itemViewTemplate.recipeX.replace('{X}', i + 1)),
            value: tr.item.itemViewTemplate.recipeLevel.replace('{X}', recipes[i].level) +
                tr.item.itemViewTemplate.recipeComponents +
                craftViewService.recipeComponentFormat(recipes[i].components),
          },
      );
    }

    return returnValue;
  },
  savePlayerFoundItem: function(playerId, foundItem) {
    const allPlayersData = playersData.getAllPlayers();
    for (let j = 0; j < allPlayersData.players.length; j++) {
      if (allPlayersData.players[j].id === playerId) {
        allPlayersData.players[j].inventory.foundItems.push(foundItem);
      }
    }
    botService.jsonWriteFile(allPlayersData, 'players');
  },
  itemViewFoundableLocationFormat: function(foundableLocation, client) {
    const returnValue = tr.item.itemViewTemplate.foundableItem.foundableItemLocationBase;
    let coLocation = '';
    let inhaLocation = '';
    let uncoLocation = '';
    let rareLocation = '';
    let rariLocation = '';
    let specialLocation = '';

    for (let i = 0; i < foundableLocation.length; i++) {
      const biomeCategory = biomeCategoriesData.getBiomeCategoryByID(foundableLocation[i].biomeCategoryId);

      if (foundableLocation[i].rarity === 'co') {
        coLocation = coLocation.length === 0 ?
            tr.item.itemViewTemplate.foundableItem.foundableItemLocationRarity
                .replace('{RARITY}', 'commun')
                .replace('{EMOJI}', emojiService.getFoundItemRarityEmojiPerNameOrNumber('co', client)) :
            coLocation + ', ';
        coLocation = coLocation + biomeCategory.name;
      } else if (foundableLocation[i].rarity === 'unco') {
        uncoLocation = uncoLocation.length === 0 ?
            tr.item.itemViewTemplate.foundableItem.foundableItemLocationRarity
                .replace('{RARITY}', 'peu commune')
                .replace('{EMOJI}', emojiService.getFoundItemRarityEmojiPerNameOrNumber('unco', client)) :
            uncoLocation + ', ';
        uncoLocation = uncoLocation + biomeCategory.name;
      } else if (foundableLocation[i].rarity === 'inha') {
        inhaLocation = inhaLocation.length === 0 ?
            tr.item.itemViewTemplate.foundableItem.foundableItemLocationRarity
                .replace('{RARITY}', 'inhabituelle')
                .replace('{EMOJI}', emojiService.getFoundItemRarityEmojiPerNameOrNumber('inha', client)) :
            inhaLocation + ', ';
        inhaLocation = inhaLocation + biomeCategory.name;
      } else if (foundableLocation[i].rarity === 'rare') {
        rareLocation = rareLocation.length === 0 ?
            tr.item.itemViewTemplate.foundableItem.foundableItemLocationRarity
                .replace('{RARITY}', 'rare')
                .replace('{EMOJI}', emojiService.getFoundItemRarityEmojiPerNameOrNumber('rare', client)) :
            rareLocation + ', ';
        rareLocation = rareLocation + biomeCategory.name;
      } else if (foundableLocation[i].rarity === 'rari') {
        rariLocation = rariLocation.length === 0 ?
            tr.item.itemViewTemplate.foundableItem.foundableItemLocationRarity
                .replace('{RARITY}', 'rarissime')
                .replace('{EMOJI}', emojiService.getFoundItemRarityEmojiPerNameOrNumber('rari', client)) :
            rariLocation + ', ';
        rariLocation = rariLocation + biomeCategory.name;
      } else if (foundableLocation[i].rarity === 'special') {
        specialLocation = tr.item.itemViewTemplate.foundableItem.foundableItemLocationSpecial
            .replace('{EMOJI}', emojiService.getFoundItemRarityEmojiPerNameOrNumber('special', client));
      }
    }

    return returnValue + coLocation + uncoLocation + inhaLocation + rareLocation + rariLocation + specialLocation;
  },
};
