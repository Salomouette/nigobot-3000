const moment = require('moment');
const Discord = require('discord.js');

const messageService = require('../Service/messageService');

const frenchTranslation = require('../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  sendLogMessage: function(guild, command, username, addInformation, embedMessage) {
    const channel = guild.channels.cache.get(process.env.DASHBOARD_LOGS_GENERAL_CHANNEL);

    embedMessage = this.generateMessage(command, username, addInformation, embedMessage);

    messageService.sendEmbedMessage(channel, embedMessage);
  },
  sendFoundItemLogMessage: function(guild, embedMessage) {
    const channel = guild.channels.cache.get(process.env.DASHBOARD_LOGS_FOUND_ITEM_CHANNEL);

    messageService.sendEmbedMessage(channel, embedMessage);
  },
  generateAdditionalInformationString: function(addInformation) {
    let returnValue = '';
    for (let i = 0; i < addInformation.length; i++) {
      if (addInformation[i].value instanceof Array) {
        for (let j = 0; j < addInformation[i].value.length; j++) {
          returnValue =
                returnValue + '- ' + addInformation[i].fieldName + ' n°' + (j+1) +
                ' : ' + addInformation[i].value[j] + '\n';
        }
      } else {
        returnValue =
                returnValue + '- ' + addInformation[i].fieldName + ' : ' + addInformation[i].value + '\n';
      }
    }
    return returnValue;
  },
  formatUsernameLogs: function(username) {
    return '<**' + username + '**> ';
  },
  generateMessage: function(command, username, addInformation, embedMessage) {
    let message = new Discord.MessageEmbed()
        .setColor(process.env.COLOR_COG_QUERY)
        .setFooter({
          text: moment().format('MMMM Do YYYY, LTS'),
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        })
        .setAuthor({
          name: tr.author.logs,
          iconURL: process.env.SET_AUTHOR_COG_ICON,
        });

    switch (command) {
      // FUN PART
      case 'fun:slur':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.fun.slur);
        break;
        // BOT PART
      case 'bot:ping':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.bot.ping);
        break;
      case 'bot:lore':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.bot.lore);
        break;
      case 'bot:help':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.bot.help);
        break;
        // BIOME PART
      case 'biome:help':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.biome.help);
        break;
      case 'biome:biomes':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.biome.showAll);
        break;
      case 'biome:biome':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.biome.showOne +
                    tr.logs.commandComplement +
                    this.generateAdditionalInformationString(addInformation),
        );
        break;
        // DICE PART
      case 'dice:help':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.dice.help);
        break;
        // POKEMON PART
      case 'pokémon:help':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.pokémon.help);
        break;
      case 'pokémon:trainersHelp':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.pokémon.trainersHelp);
        break;
      case 'pokémon:trainersHelpAdmin':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.pokémon.trainersHelpAdmin);
        break;
      case 'pokémon:seePokémon':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.pokémon.seePokemon +
            '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'pokémon:givePokémonToPlayer':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.pokémon.givePokémonToPlayer +
            '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'pokémon:changeMove':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.pokémon.givePokémonToPlayer +
            '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'pokémon:changeNickname':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.pokémon.givePokémonToPlayer +
            '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'pokémon:evolveOneInto':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.pokémon.evolveOneInto +
            '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'pokémon:seePokémonPlayer':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.pokémon.seePokémonPlayer);
        break;
      case 'pokémon:view':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.pokémon.pokémonInfo +
                    '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'pokémon:changeNotes':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.pokémon.pokémonNotes+
          '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'pokémon:addLevel':
      case 'pokémon:addFriendship':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.pokémon.pokemonLevelorFriendship +
                    '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
        // BERRY PART
      case 'berry:help':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.berry.help);
        break;
      case 'berry:mixTemplate':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.berry.mixTemplate);
        break;
      case 'berry:view':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.berry.berryInfo +
                    '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
        // SYSTEM PART
      case 'system:help':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.system.help);
        break;
      case 'system:addPlayer':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.system.addPlayer);
        break;
      case 'system:removePlayer':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.system.removePlayer);
        break;
      case 'system:initializeMoney':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.system.initializeMoney +
                    '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'system:dump':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.system.dump);
        break;
      case 'system:startDatabases':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.system.startDatabases);
        break;
      case 'system:removeAllPlayers':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.system.removeAllPlayers);
        break;
      case 'system:addLogChannelId':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.system.addLogChannelId +
                    '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'system:setupEmojiServer1':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.system.setupEmojiServer1);
        break;
      case 'system:setupEmojiServer2':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.system.setupEmojiServer2);
        break;
      case 'system:setupEmojiServer3':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.system.setupEmojiServer3);
        break;
      case 'system:setupEmojiServer4':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.system.setupEmojiServer4);
        break;
      case 'system:setupEmojiServer5':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.system.setupEmojiServer5);
        break;
      case 'system:setupEmojiServer6':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.system.setupEmojiServer6);
        break;
      case 'system:deleteAllEmojiInServer':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.system.deleteAllEmojiInServer);
        break;
        // PLAYER PART
        // MONEY PART
      case 'player:help':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.player.help);
        break;
      case 'player:throwStats':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.player.throwStats);
        break;
      case 'player:seeMoney':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.player.money.seeMoney);
        break;
      case 'player:addPlayerMoney':
      case 'player:addAllPlayersMoney':
      case 'player:removePlayerMoneyPlayer':
      case 'player:removePlayerMoneyAdmin':
      case 'player:removeAllPlayersMoney':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.player.money.changeMoney +
                    '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
        // INVENTORY PART
      case 'inventory:help':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.inventory.help);
        break;
      case 'inventory:seeFoundItem':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.inventory.seeFoundItem);
        break;
      case 'inventory:keepItem':
      case 'inventory:keepSubCategory':
      case 'inventory:keepCategory':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.inventory.keepItem +
            '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'inventory:sellFoundItem':
      case 'inventory:sellFoundItemSubCategory':
      case 'inventory:sellFoundItemCategory':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.inventory.keepItem +
            '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'system:sellAllFoundItems':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.inventory.sellAllItem);
        break;
      case 'system:estimateAllFoundItems':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.inventory.estimateAllItem);
        break;
        // CRAFT PART
      case 'craft:help':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.craft.help);
        break;
      case 'craft:creationTemplate':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.craft.creationTemplate);
        break;
      case 'craft:discoveredRecipes':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.craft.discoveredRecipes);
        break;
        // CALENDAR PART
      case 'calendar:help':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.calendar.help);
        break;
      case 'calendar:modifyDateTemplate':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.calendar.modifyDateTemplate +
            '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'calendar:viewMonth':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.calendar.viewMonth +
            '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'calendar:viewDate':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.calendar.viewDate +
            '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'calendar:modifyOrderTemplate':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.calendar.modifyOrderTemplate +
            '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'calendar:modifyOrder':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.calendar.modifyOrder +
            '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'calendar:viewActualDay':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.calendar.viewActualDay);
        break;
        // Item part
      case 'item:help':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.item.help);
        break;
      case 'item:categories':
        message = message.setDescription(this.formatUsernameLogs(username) + tr.logs.item.categories);
        break;
      case 'item:view':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.item.itemInfo +
            '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'item:subCategory':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.item.subCategoryInfo +
            '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'item:category':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.item.categoryInfo +
            '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
        // TRAINERS PART
      case 'trainers:generate':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.trainers.generate +
              '\n' + this.generateAdditionalInformationString(addInformation),
        );
        break;
      case 'trainers:help':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.trainers.help,
        );
        break;
      case 'trainers:helpPast':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.trainers.helpPast,
        );
        break;
      case 'trainers:list':
        message = message.setDescription(
            this.formatUsernameLogs(username) + tr.logs.trainers.list,
        );
        break;
        // Case for all cases with embed to show
      case 'pokémon:generation':
      case 'dice:throw':
      case 'dice:average':
      case 'dice:validate':
      case 'dice:private':
      case 'berry:mix':
      case 'craft:creation':
      case 'craft:viewRecipe':
      case 'calendar:defineDay':
      case 'calendar:changeDateType':
      case 'calendar:modifyEvent':
      case 'calendar:addEvent':
      case 'calendar:addPeriod':
      case 'calendar:addEventPeriod':
        message = embedMessage
            .setColor(process.env.COLOR_COG_QUERY)
            .setFooter({
              text: moment().format('MMMM Do YYYY, LTS'),
              iconURL: process.env.NIGOBOT_AVATAR_LINK,
            })
            .setAuthor({
              name: tr.author.logs,
              iconURL: process.env.SET_AUTHOR_COG_ICON,
            })
            .addFields(
                {
                  name: tr.logs.informationLog,
                  value:
                      tr.logs.requestOf +
                      this.formatUsernameLogs(username) +
                      tr.logs.commandComplement +
                      this.generateAdditionalInformationString(addInformation) +
                      moment().format('MMMM Do YYYY, LTS'),
                },
            );

        break;
    }
    return message;
  },
};
