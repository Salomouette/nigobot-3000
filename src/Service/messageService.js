const privateMessageAlertTemplate = require('../Assets/EmbedMessage/privateMessageAlertTemplate');
module.exports = {
  sendPrivateMessageWithId: function(client, id, embed) {
    if (embed && embed.length > 0) {
      client.users.send(id, {embeds: [embed]});
    }
  },
  sendTextMessage: function(msg, text) {
    msg.channel.sendTyping();
    if (text && text.length > 0) {
      msg.reply({content: text});
    }
  },
  sendEmbedMessage: function(channel, embed, sendTyping) {
    if (sendTyping) {
      channel.sendTyping();
    }
    if (embed && embed.length > 0) {
      channel.send({embeds: [embed]});
    }
  },
  replyEmbedMessage: function(message, embed) {
    if (embed && embed.length > 0) {
      message.reply({embeds: [embed]});
    }
  },
  sendEmbedMessageWithImages: function(channel, embed, images, sendTyping) {
    if (sendTyping) {
      channel.sendTyping();
    }
    if (embed && embed.length > 0 && images && images.length > 0) {
      channel.send({
        embeds: [embed],
        files: images,
      });
    } else if (embed && embed.length > 0) {
      channel.send({
        embeds: [embed],
      });
    }
  },
  replyEmbedMessageWithImages: function(channel, embed, images) {
    if (embed && embed.length > 0 && images && images.length > 0) {
      channel.reply({
        embeds: [embed],
        files: images,
      });
    } else if (embed && embed.length > 0) {
      channel.reply({
        embeds: [embed],
      });
    }
  },
  sendPrivateMessageAlert: function(message) {
    const embedAlert = privateMessageAlertTemplate.privateMessageAlertTemplate();
    if (message.channel.type !== 'DM') {
      this.sendEmbedMessage(message.channel, embedAlert);
    }
  },
  sendPrivateMessage: function(message, embed, alert) {
    if (alert) {
      this.sendPrivateMessageAlert(message);
    }
    this.sendEmbedMessage(message.author, embed, false);
  },
  sendMessageRPGChannel: function(guild, embedMessage) {
    const channel = guild.channels.cache.get(process.env.MAIN_RPG_CHANNEL);

    this.sendEmbedMessage(channel, embedMessage);
  },
};
