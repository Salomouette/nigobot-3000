const biomeData = require('../../Data/biomeData');
const pokémonData = require('../../Data/pokémonData');
module.exports = {
  biomeActionHandler: function(action, callInput) {
    let returnValue = '';
    const biomeId = biomeData.getBiomeIDFromName(callInput);

    if (callInput.includes('d\'arguments')) {
      returnValue = callInput;
    }

    const isEmptyPokémonBiome = pokémonData.getAllPokémonFromBiome(callInput);

    if (isEmptyPokémonBiome['pokémon'][0] !== undefined) {
      returnValue = action;
      if (action === 'view') {
        returnValue = 'les Pokémon trouvables dans le biome ' + callInput + ' sont : \n';

        const commonList = this.generateDisplayPokémon(biomeId, 'commun');
        const uncommonList = this.generateDisplayPokémon(biomeId, 'unco');
        const rareList = this.generateDisplayPokémon(biomeId, 'rare');
        const eventList = this.generateDisplayPokémon(biomeId, 'event');

        if (commonList !== '') {
          returnValue = returnValue +
              '- Pokémon communs: ' +
              this.generateDisplayPokémon(biomeId, 'commun') +
              '\n\n';
        }
        if (uncommonList !== '') {
          returnValue = returnValue +
              '- Pokémon peu communs: ' +
              this.generateDisplayPokémon(biomeId, 'unco') +
              '\n\n';
        }
        if (rareList !== '') {
          returnValue = returnValue +
              '- Pokémon rares: ' +
              this.generateDisplayPokémon(biomeId, 'rare') +
              '\n\n';
        }
        if (eventList !== '') {
          returnValue = returnValue +
              '- Pokémon d\'évènement(s): ' +
              this.generateDisplayPokémon(biomeId, 'event') +
              '\n';
        }
      }
    } else {
      returnValue = 'Biome invalide ou qui n\'existe pas.';
    }

    return returnValue;
  },
  displayBiomes: function() {
    let displayBiomes = 'Les biomes dont je suis capable de dire les Pokémon sont les suivants: \n\n';
    displayBiomes = displayBiomes + '- Biomes forêts :deciduous_tree: : ';
    displayBiomes = displayBiomes + this.generateDisplayBiomes(biomeData.getAllBiomePerType('Forêt'));
    displayBiomes = displayBiomes + '\n';
    displayBiomes = displayBiomes + '- Biomes montagneux :mountain_snow: : ';
    displayBiomes = displayBiomes + this.generateDisplayBiomes(biomeData.getAllBiomePerType('Montagne'));
    displayBiomes = displayBiomes + '\n';
    displayBiomes = displayBiomes + '- Biomes de grottes :mountain: : ';
    displayBiomes = displayBiomes + this.generateDisplayBiomes(biomeData.getAllBiomePerType('Caverne'));
    displayBiomes = displayBiomes + '\n';
    displayBiomes = displayBiomes + '- Biomes de plaines :seedling: : ';
    displayBiomes = displayBiomes + this.generateDisplayBiomes(biomeData.getAllBiomePerType('Plaine'));
    displayBiomes = displayBiomes + '\n';
    displayBiomes = displayBiomes + '- Biomes de déserts :desert: : ';
    displayBiomes = displayBiomes + this.generateDisplayBiomes(biomeData.getAllBiomePerType('Désert'));
    displayBiomes = displayBiomes + '\n';
    displayBiomes = displayBiomes + '- Biomes d\'eau :ocean: : ';
    displayBiomes = displayBiomes + this.generateDisplayBiomes(biomeData.getAllBiomePerType('Eau'));
    displayBiomes = displayBiomes + '\n';
    displayBiomes = displayBiomes + '- Biomes urbain :cityscape: : ';
    displayBiomes = displayBiomes + this.generateDisplayBiomes(biomeData.getAllBiomePerType('Urbain'));
    displayBiomes = displayBiomes + '\n';
    displayBiomes = displayBiomes + '- Biomes Paradoxes :sparkler: : ';
    displayBiomes = displayBiomes + this.generateDisplayBiomes(biomeData.getAllBiomePerType('Paradoxe'));
    displayBiomes = displayBiomes + '\n';
    displayBiomes = displayBiomes + '- Autres biomes :interrobang: : ';
    displayBiomes = displayBiomes + this.generateDisplayBiomes(biomeData.getAllBiomePerType('Inconnu'));
    displayBiomes = displayBiomes + '\n';
    displayBiomes = displayBiomes + 'Cela fait **' + (biomeData.getAllBiomeData()).biomes.length + '** biomes en tout!';

    return displayBiomes;
  },
  generateDisplayBiomes: function(biome) {
    let displayBiomes = '';
    for (let i = 0; i < biome.length; i++) {
      if (i === biome.length - 1) {
        displayBiomes = displayBiomes + '**' + biome[i].name + '**.';
      } else {
        displayBiomes = displayBiomes + '**' + biome[i].name + '**, ';
      }
    }
    displayBiomes = displayBiomes + '\n';

    return displayBiomes;
  },
  generateDisplayPokémon: function(biomeId, requestRarity) {
    let displayPokémonNames = '';
    const data = pokémonData.getAllPokémonFromBiomeAndRarity(biomeId, requestRarity);

    for (let i = 0; i < data['pokémon'].length; i++) {
      if (i === data['pokémon'].length - 1) {
        displayPokémonNames = displayPokémonNames + '**' + data['pokémon'][i].name + '**.';
      } else {
        displayPokémonNames = displayPokémonNames + '**' + data['pokémon'][i].name + '**, ';
      }
    }

    return displayPokémonNames;
  },
};
