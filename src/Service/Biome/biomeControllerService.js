const frenchTranslation = require('../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  getCallInputBiomeView: function(msg) {
    switch (msg.content.split(' ').length) {
      case 1:
      case 2:
        return tr.error.invalidOrBadArgument;
      case 3:
        return msg.content.split(' ')[1];
      case 4:
        return msg.content.split(' ')[1] + ' ' + msg.content.split(' ')[2];
      default:
        return tr.error.tooManyArgumentsOrBadSyntax;
    }
  },
  formatGenerateByBiomeCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 1:
        formatedInput = callInput.split('--Biome=')[1];
        formatedInput = formatedInput.split('--Rareté=')[0];
        break;
      case 2:
        if (callInput.includes('Charme Chroma')) {
          formatedInput = callInput.split('--Rareté=')[1];
          formatedInput = formatedInput.split('--Charme Chroma=')[0];
        } else {
          formatedInput = callInput.split('--Rareté=')[1];
        }
        break;
      case 3:
        if (callInput.includes('Charme Alpha')) {
          formatedInput = callInput.split('--Charme Chroma=')[1];
          formatedInput = formatedInput.split('--Charme Alpha=')[0];
        } else {
          formatedInput = callInput.split('--Charme Chroma=')[1];
        }
        break;
      case 4:
        if (callInput.includes('Charme Taille')) {
          formatedInput = callInput.split('--Charme Alpha=')[1];
          formatedInput = formatedInput.split('--Charme Taille=')[0];
        } else {
          formatedInput = callInput.split('--Charme Alpha ?=')[1];
        }
        break;
      case 5:
        formatedInput = callInput.split('--Charme Taille=')[1];
        break;
      case 6:
        if (callInput.includes('Charme Chroma') && !callInput.includes('Forme')) {
          formatedInput = callInput.split('--Pokémon=')[1];
          formatedInput = formatedInput.split('--Charme Chroma=')[0];
        } else if (callInput.includes('Forme')) {
          formatedInput = callInput.split('--Pokémon=')[1];
          formatedInput = formatedInput.split('--Forme=')[0];
        } else {
          formatedInput = callInput.split('--Pokémon=')[1];
        }
        break;
      case 7:
        if (callInput.includes('Forme')) {
          if (callInput.includes('Charme Chroma')) {
            formatedInput = callInput.split('--Forme=')[1];
            formatedInput = formatedInput.split('--Charme Chroma=')[0];
          } else {
            formatedInput = callInput.split('--Forme=')[1];
          }
        }
        break;
      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/(\r\n|\n|\r)/gm, '') : formatedInput;
    }
    return null;
  },
};
