const playersData = require('../../Data/playerData');

const botService = require('../../Service/botService');
module.exports = {
  savePlayerFoundItemsAndMoney: function(playerFoundItems, addedMoney, playerID) {
    const allPlayersData = playersData.getAllPlayers();

    console.log(playerID);
    for (let j = 0; j < allPlayersData.players.length; j++) {
      if (allPlayersData.players[j].id === playerID) {
        allPlayersData.players[j].inventory.foundItems = playerFoundItems;
        allPlayersData.players[j].money += addedMoney;
      }
    }

    botService.jsonWriteFile(allPlayersData, 'players');
  },
  savePlayerFoundItems: function(playerFoundItems, playerID) {
    const allPlayersData = playersData.getAllPlayers();

    for (let j = 0; j < allPlayersData.players.length; j++) {
      if (allPlayersData.players[j].id === playerID) {
        allPlayersData.players[j].inventory.foundItems = playerFoundItems;
      }
    }

    botService.jsonWriteFile(allPlayersData, 'players');
  },
};
