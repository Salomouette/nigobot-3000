const lodash = require('lodash');

const playerControllerService = require('./playerControllerService');
const botService = require('../../Service/botService');

const systemData = require('../../Data/systemData');
const itemsData = require('../../Data/itemsData');

const frenchTranslation = require('../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  getArrayNumberFoundItems: function(pokémonFoundItem) {
    pokémonFoundItem = lodash.orderBy(pokémonFoundItem, 'id', 'asc');
    const foundItemQtyArray = [];

    for (let i = 0; i < pokémonFoundItem.length; i++) {
      let addItem = false;

      if (foundItemQtyArray.length > 0) {
        for (let j = 0; j < foundItemQtyArray.length; j++) {
          if (foundItemQtyArray[j].id === pokémonFoundItem[i].id) {
            foundItemQtyArray[j].quantity++;
            addItem = true;
          }
        }
      }

      if (!addItem) {
        const itemData = itemsData.getItemFromId(pokémonFoundItem[i].id);

        foundItemQtyArray.push(
            {
              name: pokémonFoundItem[i].name,
              id: pokémonFoundItem[i].id,
              emojiName: itemData.foundableData.buddyFoundableEmoji,
              quantity: 1,
            },
        );
      }
    }

    return lodash.orderBy(foundItemQtyArray, 'emojiName', 'asc');
  },
  keepFoundItemsCategory: function(pokémonFoundItems, playerID, categoryName) {
    const playerFoundItems = [];
    let returnValue = tr.player.keepFoundItemDescriptionNotFound;

    for (let i = 0; i < pokémonFoundItems.length; i++) {
      const itemData = itemsData.getItemFromId(pokémonFoundItems[i].id);

      categoryName = (botService.removeDiacritics(categoryName)).toLowerCase();
      const categoryDataName = (botService.removeDiacritics(itemData.category)).toLowerCase();
      if (categoryName == categoryDataName) {
        pokémonFoundItems[i].keep = true;

        returnValue = tr.player.keepFoundItemCategoryDescription.replace('{CATEGORYNAME}', itemData.category);
      }
      playerFoundItems.push(pokémonFoundItems[i]);
    }
    playerControllerService.savePlayerFoundItems(playerFoundItems, playerID);

    return returnValue;
  },
  keepFoundItemsSubCategory: function(pokémonFoundItems, playerID, subCategoryName) {
    const playerFoundItems = [];
    let returnValue = tr.player.keepFoundItemDescriptionNotFound;

    for (let i = 0; i < pokémonFoundItems.length; i++) {
      const itemData = itemsData.getItemFromId(pokémonFoundItems[i].id);

      subCategoryName = (botService.removeDiacritics(subCategoryName)).toLowerCase();
      const subCategoryDataName = (botService.removeDiacritics(itemData.subCategory)).toLowerCase();
      if (subCategoryName == subCategoryDataName) {
        pokémonFoundItems[i].keep = true;

        returnValue = tr.player.keepFoundItemSubCategoryDescription.replace('{SUBCATEGORYNAME}', itemData.subCategory);
      }
      playerFoundItems.push(pokémonFoundItems[i]);
    }
    playerControllerService.savePlayerFoundItems(playerFoundItems, playerID);

    return returnValue;
  },
  keepFoundItems: function(pokémonFoundItems, playerID, itemName) {
    const playerFoundItems = [];
    let returnValue = tr.player.keepFoundItemDescriptionNotFound;

    for (let i = 0; i < pokémonFoundItems.length; i++) {
      const itemData = itemsData.getItemFromId(pokémonFoundItems[i].id);

      itemName = (botService.removeDiacritics(itemName)).toLowerCase();
      const itemDataName = (botService.removeDiacritics(itemData.name)).toLowerCase();
      if (itemDataName == itemName) {
        pokémonFoundItems[i].keep = true;

        returnValue = tr.player.keepFoundItemDescription.replace('{ITEMNAME}', itemData.name);
      }
      playerFoundItems.push(pokémonFoundItems[i]);
    }
    playerControllerService.savePlayerFoundItems(playerFoundItems, playerID);

    return returnValue;
  },
  // If foundItem return -1  it's unautorized item sub category
  soldFoundItemSubCategory: function(pokémonFoundItems, playerID, subCategoryName) {
    let playerFoundItems = [];
    let returnPrice = 0;

    subCategoryName = (botService.removeDiacritics(subCategoryName)).toLowerCase();
    const bouquetCategory = (botService.removeDiacritics('Bouquet')).toLowerCase();
    const leafCategory = (botService.removeDiacritics('Feuilles')).toLowerCase();
    if (subCategoryName == bouquetCategory || subCategoryName == leafCategory) {
      returnPrice = -1;
      playerFoundItems = pokémonFoundItems;
    } else {
      for (let i = 0; i < pokémonFoundItems.length; i++) {
        const itemData = itemsData.getItemFromId(pokémonFoundItems[i].id);

        if (
          itemData.name != 'Feuille Dorée' &&
            itemData.name != 'Feuille Argentée' &&
            itemData.name != 'Petit Bouquet'
        ) {
          const itemDataSubCategory = (botService.removeDiacritics(itemData.subCategory)).toLowerCase();
          const priceRates = systemData.getSystemProperty('priceRates');

          if ((itemDataSubCategory == subCategoryName) && pokémonFoundItems[i].keep === undefined) {
            returnPrice += Math.round(itemData.basePrice * priceRates.sellRate);
          } else {
            playerFoundItems.push(pokémonFoundItems[i]);
          }
        } else {
          pokémonFoundItems[i].keep = true;
          playerFoundItems.push(pokémonFoundItems[i]);
        }
      }
    }
    playerControllerService.savePlayerFoundItemsAndMoney(playerFoundItems, returnPrice, playerID);

    return returnPrice;
  },
  // If foundItem return -1  it's unautorized item category
  soldFoundItemCategory: function(pokémonFoundItems, playerID, categoryName) {
    let playerFoundItems = [];
    let returnPrice = 0;

    categoryName = (botService.removeDiacritics(categoryName)).toLowerCase();
    const giftCategory = (botService.removeDiacritics('Cadeau Pokémon')).toLowerCase();
    if (categoryName == giftCategory) {
      returnPrice = -1;
      playerFoundItems = pokémonFoundItems;
    } else {
      for (let i = 0; i < pokémonFoundItems.length; i++) {
        const itemData = itemsData.getItemFromId(pokémonFoundItems[i].id);

        if (
          itemData.name != 'Feuille Dorée' &&
            itemData.name != 'Feuille Argentée' &&
            itemData.name != 'Petit Bouquet'
        ) {
          const itemDataCategory = (botService.removeDiacritics(itemData.category)).toLowerCase();
          const priceRates = systemData.getSystemProperty('priceRates');

          if ((itemDataCategory == categoryName) && pokémonFoundItems[i].keep === undefined) {
            returnPrice += Math.round(itemData.basePrice * priceRates.sellRate);
          } else {
            playerFoundItems.push(pokémonFoundItems[i]);
          }
        } else {
          pokémonFoundItems[i].keep = true;
          playerFoundItems.push(pokémonFoundItems[i]);
        }
      }
    }
    playerControllerService.savePlayerFoundItemsAndMoney(playerFoundItems, returnPrice, playerID);

    return returnPrice;
  },
  // If foundItem return -1  it's unautorized item
  soldFoundItem: function(pokémonFoundItems, playerID, itemName) {
    let playerFoundItems = [];
    let returnPrice = 0;

    itemName = (botService.removeDiacritics(itemName)).toLowerCase();
    const littleBouquet = (botService.removeDiacritics('Petit Bouquet')).toLowerCase();
    const goldLeaf = (botService.removeDiacritics('Feuille Dorée')).toLowerCase();
    const silverLeaf = (botService.removeDiacritics('Feuille Argentée')).toLowerCase();

    if (itemName == goldLeaf || itemName == silverLeaf || itemName == littleBouquet) {
      returnPrice = -1;
      playerFoundItems = pokémonFoundItems;
    } else {
      for (let i = 0; i < pokémonFoundItems.length; i++) {
        const itemData = itemsData.getItemFromId(pokémonFoundItems[i].id);

        const itemDataName = (botService.removeDiacritics(itemData.name)).toLowerCase();
        const priceRates = systemData.getSystemProperty('priceRates');

        if (
          (itemDataName == itemName) &&
            pokémonFoundItems[i].keep === undefined
        ) {
          returnPrice += Math.round(itemData.basePrice * priceRates.sellRate);
        } else {
          playerFoundItems.push(pokémonFoundItems[i]);
        }
      }
    }
    playerControllerService.savePlayerFoundItemsAndMoney(playerFoundItems, returnPrice, playerID);

    return returnPrice;
  },
  getValueFoundItems: function(pokémonFoundItems) {
    let returnPrice = 0;
    const priceRates = systemData.getSystemProperty('priceRates');

    for (let i = 0; i < pokémonFoundItems.length; i++) {
      const itemData = itemsData.getItemFromId(pokémonFoundItems[i].id);

      if (
        (itemData.name != 'Feuille Dorée' && itemData.name != 'Feuille Argentée' && itemData.name != 'Petit Bouquet') &&
          pokémonFoundItems[i].keep === undefined
      ) {
        returnPrice += Math.round(itemData.basePrice * priceRates.sellRate);
      }
    }

    return returnPrice;
  },
  soldAllFoundItems: function(pokémonFoundItems, playerID) {
    let returnPrice = 0;
    const priceRates = systemData.getSystemProperty('priceRates');
    const playerFoundItems = [];

    for (let i = 0; i < pokémonFoundItems.length; i++) {
      const itemData = itemsData.getItemFromId(pokémonFoundItems[i].id);

      if (
        (itemData.name != 'Feuille Dorée' && itemData.name != 'Feuille Argentée' && itemData.name != 'Petit Bouquet') &&
          pokémonFoundItems[i].keep === undefined
      ) {
        returnPrice += Math.round(itemData.basePrice * priceRates.sellRate);
      } else {
        pokémonFoundItems[i].keep = true;
        playerFoundItems.push(pokémonFoundItems[i]);
      }
    }
    playerControllerService.savePlayerFoundItemsAndMoney(playerFoundItems, returnPrice, playerID);

    return returnPrice;
  },
};
