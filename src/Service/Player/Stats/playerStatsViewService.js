const lodash = require('lodash');
const fs = require('fs');
const vega = require('vega');
const sharp = require('sharp');

const playerData = require('../../../Data/playerData');
const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();

const tempFilePath = 'src/Assets/Temp';
module.exports = {
  getDiceThrowsForGroupTimePeriod: function(userID) {
    const userThrowData = playerData.getDiceThrowsForPlayerTimePeriod(userID);
    const groupThrowData = playerData.getDiceThrowsForGroupTimePeriod(userID);
    const returnJson = {periods: []};

    if (groupThrowData !== null && userThrowData !== null) {
      for (let i = 0; i < groupThrowData.periods.length; i++) {
        const userKey = lodash.findKey(userThrowData.periods, {periodName: groupThrowData.periods[i].periodName});
        returnJson.periods.push(
            {
              periodName: groupThrowData.periods[i].periodName,
              userThrows: '',
              groupThrows: '',
            },
        );
        returnJson.periods[returnJson.periods.length - 1].groupThrows = groupThrowData.periods[i].throws;
        if (userKey !== undefined) {
          returnJson.periods[returnJson.periods.length - 1].userThrows = userThrowData.periods[userKey].throws;
        }
      }
    }

    return returnJson;
  },
  generateThrowStatsPlayerFields: function(userID) {
    const throwsData = this.getDiceThrowsForGroupTimePeriod(userID);
    const returnFields = [];

    if (throwsData.periods.length > 0) {
      for (let i = 0; i < throwsData.periods.length; i++) {
        if (throwsData.periods[i].userThrows.length > 0) {
          returnFields.push(
              {
                name: throwsData.periods[i].periodName,
                value: tr.player.stats.meanDicePart1 +
                    lodash.meanBy(throwsData.periods[i].userThrows, 'diceValue').toFixed(2) +
                    tr.player.stats.meanDicePart2 +
                    lodash.meanBy(throwsData.periods[i].groupThrows, 'diceValue').toFixed(2) + '.',
              },
          );
        }
      }

      returnFields.push(
          {
            name: tr.player.stats.graphicName,
            value: tr.player.stats.graphicValue,
          },
      );
    }

    return returnFields;
  },
  generateThrowDiagramPlayerImage: function(userId) {
    const spec = {
      $schema: 'https://vega.github.io/schema/vega/v5.json',
      description: 'A basic stacked bar chart example.',
      width: 1200,
      height: 400,
      padding: 50,
      background: 'white',
      data: [
        {
          name: 'table',
          values: [
            {
              x: 'Réussite A joueur',
              y: playerData.getDiceThrowsForPlayerFilterThrowValidation(userId, 'absolute success').length,
              c: 0,
            },
            {
              x: 'Réussite A groupe',
              y: playerData.getDiceThrowsForGroupMinusPlayerFilterThrowValidation(userId, 'absolute success').length,
              c: 1,
            },
            {
              x: 'Réussite C joueur',
              y: playerData.getDiceThrowsForPlayerFilterThrowValidation(userId, 'critic success').length,
              c: 0,
            },
            {
              x: 'Réussite C groupe',
              y: playerData.getDiceThrowsForGroupMinusPlayerFilterThrowValidation(userId, 'critic success').length,
              c: 1,
            },
            {
              x: 'Réussite joueur',
              y: playerData.getDiceThrowsForPlayerFilterThrowValidation(userId, 'success').length,
              c: 0,
            },
            {
              x: 'Réussite groupe',
              y: playerData.getDiceThrowsForGroupMinusPlayerFilterThrowValidation(userId, 'success').length,
              c: 1,
            },
            {
              x: 'Échec joueur',
              y: playerData.getDiceThrowsForPlayerFilterThrowValidation(userId, 'fail').length,
              c: 0,
            },
            {
              x: 'Échec groupe',
              y: playerData.getDiceThrowsForGroupMinusPlayerFilterThrowValidation(userId, 'fail').length,
              c: 1,
            },
            {
              x: 'Échec C joueur',
              y: playerData.getDiceThrowsForPlayerFilterThrowValidation(userId, 'critic fail').length,
              c: 0,
            },
            {
              x: 'Échec C groupe',
              y: playerData.getDiceThrowsForGroupMinusPlayerFilterThrowValidation(userId, 'critic fail').length,
              c: 1,
            },
            {
              x: 'Échec A joueur',
              y: playerData.getDiceThrowsForPlayerFilterThrowValidation(userId, 'absolute fail').length,
              c: 0,
            },
            {
              x: 'Échec A groupe',
              y: playerData.getDiceThrowsForGroupMinusPlayerFilterThrowValidation(userId, 'absolute fail').length,
              c: 1,
            },
          ],
          transform: [
            {
              type: 'stack',
              groupby: [
                'x',
              ],
              sort: {
                field: 'c',
              },
              field: 'y',
            },
          ],
        },
      ],
      scales: [
        {
          name: 'x',
          type: 'band',
          range: 'width',
          domain: {
            data: 'table',
            field: 'x',
          },
        },
        {
          name: 'y',
          type: 'linear',
          range: 'height',
          nice: true,
          zero: true,
          domain: {
            data: 'table',
            field: 'y1',
          },
        },
        {
          name: 'color',
          type: 'ordinal',
          range: 'category',
          domain: {
            data: 'table',
            field: 'c',
          },
        },
      ],
      axes: [
        {
          orient: 'bottom',
          scale: 'x',
          zindex: 1,
          encode: {
            labels: {
              update: {
                fontSize: {
                  value: 10,
                },
              },
            },
          },
        },
        {
          orient: 'left',
          scale: 'y',
          zindex: 1,
          encode: {
            labels: {
              update: {
                fontSize: {
                  value: 10,
                },
              },
            },
          },
        },
      ],
      marks: [
        {
          type: 'rect',
          from: {
            data: 'table',
          },
          encode: {
            enter: {
              x: {
                scale: 'x',
                field: 'x',
              },
              width: {
                scale: 'x',
                band: 1,
                offset: -1,
              },
              y: {
                scale: 'y',
                field: 'y0',
              },
              y2: {
                scale: 'y',
                field: 'y1',
              },
              fill: {
                scale: 'color',
                field: 'c',
              },
            },
            update: {
              fillOpacity: {
                value: 1,
              },
            },
            hover: {
              fillOpacity: {
                value: 0.5,
              },
            },
          },
        },
      ],
    };

    const view = new vega.View(vega.parse(spec), {renderer: 'none'});

    if (!fs.existsSync(tempFilePath)) {
      fs.mkdirSync(tempFilePath);
    } else {
      fs.unlink(tempFilePath + '/throwDiagramPlayer.png', function() {});
    }

    view.toSVG().then(async function(svg) {
      await sharp(Buffer.from(svg))
          .resize(12000, 4000)
          .toFormat('png')
          .toFile(tempFilePath + '/throwDiagramPlayer.png');
    });
  },
};
