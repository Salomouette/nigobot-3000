const Discord = require('discord.js');
const lodash = require('lodash');

const botService = require('../../Service/botService');
const messageService = require('../../Service/messageService');

const pokémonData = require('../../Data/pokémonData');
const playerData = require('../../Data/playerData');
const generatedPokémonData = require('../../Data/generatedPokémonData');
const frenchTranslation = require('../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  // returnJsonEmbeds can be a JSON Array or an Embed
  sendMessagesLevelAndFriendshipUpdates: function(client, returnJsonEmbeds, msg) {
    if (returnJsonEmbeds.fields && returnJsonEmbeds.fields.length > 0) {
      msg.channel.send(returnJsonEmbeds);
    } else if (typeof returnJsonEmbeds === 'object') {
      messageService.sendPrivateMessageAlert(msg);
      for (let i = 0; i < returnJsonEmbeds.length; i++) {
        messageService.sendPrivateMessageWithId(client, returnJsonEmbeds[i].trainerId, returnJsonEmbeds[i].embed);
        messageService.sendPrivateMessageWithId(client, process.env.ADMIN_ID, returnJsonEmbeds[i].embed);
      }
    } else {
      msg.channel.send(
          new Discord.MessageEmbed()
              .setTitle(tr.error.errorTitle)
              .setDescription(tr.error.errorUnknown)
              .setColor(process.env.COLOR_POKEMON_QUERY)
              .setAuthor({
                name: tr.author.pokémon,
                iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
              })
              .setTimestamp()
              .setFooter({
                text: process.env.NIGOBOT_NAME,
                iconURL: process.env.NIGOBOT_AVATAR_LINK,
              }),
      );
    }
  },
  getArrayOfPokémonBiomePerRarity: function(pokémonBiomeData, rarity) {
    const jsonOutput = [];

    for (let i = 0; i < pokémonBiomeData.length; i++) {
      if (pokémonBiomeData[i].rarity === rarity) {
        jsonOutput.push(pokémonBiomeData[i]);
      }
    }

    return jsonOutput;
  },
  formatPokedexCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 1:
        if (callInput.includes('Forme')) {
          formatedInput = callInput.split('--Pokémon=')[1];
          formatedInput = formatedInput.split('--Forme=')[0];
        } else {
          formatedInput = callInput.split('--Pokémon=')[1];
        }
        break;
      case 2:
        if (callInput.includes('Forme2')) {
          formatedInput = callInput.split('--Forme=')[1];
          formatedInput = formatedInput.split('--Forme2=')[0];
        } else {
          formatedInput = callInput.split('--Forme=')[1];
        }
        break;
      case 3:
        formatedInput = callInput.split('--Forme2=')[1];
        break;
      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/(\r\n|\n|\r)/gm, '') : formatedInput;
    }
    return null;
  },
  formatGenerateCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 1:
        formatedInput = callInput.split('--Biome=')[1];
        formatedInput = formatedInput.split('--Rareté=')[0];
        break;
      case 2:
        if (callInput.includes('Charme Chroma')) {
          formatedInput = callInput.split('--Rareté=')[1];
          formatedInput = formatedInput.split('--Charme Chroma=')[0];
        } else {
          formatedInput = callInput.split('--Rareté=')[1];
        }
        break;
      case 3:
        if (callInput.includes('Charme Alpha')) {
          formatedInput = callInput.split('--Charme Chroma=')[1];
          formatedInput = formatedInput.split('--Charme Alpha=')[0];
        } else {
          formatedInput = callInput.split('--Charme Chroma=')[1];
        }
        break;
      case 4:
        if (callInput.includes('Charme Taille')) {
          formatedInput = callInput.split('--Charme Alpha=')[1];
          formatedInput = formatedInput.split('--Charme Taille=')[0];
        } else {
          formatedInput = callInput.split('--Charme Alpha=')[1];
        }
        break;
      case 5:
        if (callInput.includes('Charme Tera')) {
          formatedInput = callInput.split('--Charme Taille=')[1];
          formatedInput = formatedInput.split('--Charme Tera=')[0];
        } else {
          formatedInput = callInput.split('--Charme Taille=')[1];
        }
        break;
      case 6:
        if (callInput.includes('Charme Chroma') && !callInput.includes('Forme')) {
          formatedInput = callInput.split('--Pokémon=')[1];
          formatedInput = formatedInput.split('--Charme Chroma=')[0];
        } else if (callInput.includes('Forme')) {
          formatedInput = callInput.split('--Pokémon=')[1];
          formatedInput = formatedInput.split('--Forme=')[0];
        } else {
          formatedInput = callInput.split('--Pokémon=')[1];
        }
        break;
      case 7:
        if (callInput.includes('Forme')) {
          if (callInput.includes('Charme Chroma')) {
            formatedInput = callInput.split('--Forme=')[1];
            formatedInput = formatedInput.split('--Charme Chroma=')[0];
          } else {
            formatedInput = callInput.split('--Forme=')[1];
          }
        }
        break;
      case 8:
        if (callInput.includes('Charme Titan')) {
          formatedInput = callInput.split('--Charme Tera=')[1];
          formatedInput = formatedInput.split('--Charme Titan=')[0];
        } else {
          formatedInput = callInput.split('--Charme Tera=')[1];
        }
        break;
      case 9:
        if (callInput.includes('Charme Antique')) {
          formatedInput = callInput.split('--Charme Titan=')[1];
          formatedInput = formatedInput.split('--Charme Antique=')[0];
        } else {
          formatedInput = callInput.split('--Charme Titan=')[1];
        }
        break;
      case 10:
        formatedInput = callInput.split('--Charme Antique=')[1];
        break;


      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ?
          formatedInput.replace(/(\r\n|\n|\r)/gm, '') :
          formatedInput;
    }
    return null;
  },
  formatLevelCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 0:
        formatedInput = callInput.split('--Niv20=')[1];
        formatedInput = formatedInput.split('--Niv40=')[0];
        break;
      case 1:
        formatedInput = callInput.split('--Niv40=')[1];
        formatedInput = formatedInput.split('--Niv60=')[0];
        break;
      case 2:
        formatedInput = callInput.split('--Niv60=')[1];
        formatedInput = formatedInput.split('--Niv80=')[0];
        break;
      case 3:
        formatedInput = callInput.split('--Niv80=')[1];
        formatedInput = formatedInput.split('--Niv100=')[0];
        break;
      case 4:
        formatedInput = callInput.split('--Niv100=')[1];
        break;

      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? parseInt(formatedInput.replace(/(\r\n|\n|\r)/gm, '')) : formatedInput;
    }
    return 0;
  },
  formatCatchCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 0:
        formatedInput = callInput.split('--IDJoueur=')[1];
        formatedInput = formatedInput.split('--IDPokémon=')[0];
        break;
      case 1:
        if (callInput.includes('--Level')) {
          formatedInput = callInput.split('--IDPokémon=')[1];
          formatedInput = formatedInput.split('--Level=')[0];
        } else {
          formatedInput = callInput.split('--IDPokémon=')[1];
        }
        break;
      case 2:
        if (callInput.includes('--Ball')) {
          formatedInput = callInput.split('--Level=')[1];
          formatedInput = formatedInput.split('--Ball=')[0];
        } else {
          formatedInput = callInput.split('--Level=')[1];
        }
        break;
      case 3:
        if (callInput.includes('--Place')) {
          formatedInput = callInput.split('--Ball=')[1];
          formatedInput = formatedInput.split('--Place=')[0];
        } else {
          formatedInput = callInput.split('--Ball=')[1];
        }
        break;
      case 4:
        formatedInput = callInput.split('--Place=')[1];
        break;
      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/(\r\n|\n|\r)/gm, '') : formatedInput;
    }
    return 0;
  },
  formatMoveCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 0:
        formatedInput = callInput.split('--IDPokémon=')[1];
        formatedInput = formatedInput.split('--Capacité1=')[0];
        break;
      case 1:
        formatedInput = callInput.split('--Capacité1=')[1];
        formatedInput = formatedInput.split('--Capacité2=')[0];
        break;
      case 2:
        formatedInput = callInput.split('--Capacité2=')[1];
        formatedInput = formatedInput.split('--Capacité3=')[0];
        break;
      case 3:
        formatedInput = callInput.split('--Capacité3=')[1];
        formatedInput = formatedInput.split('--Capacité4=')[0];
        break;
      case 4:
        formatedInput = callInput.split('--Capacité4=')[1];
        break;
      case 5:
        formatedInput = callInput.split('--IDJoueur=')[1];
        formatedInput = formatedInput.split('--IDPokémon=')[0];
        break;
      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/(\r\n|\n|\r)/gm, '') : formatedInput;
    }
    return 0;
  },
  formatNameCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 0:
        formatedInput = callInput.split('--IDPokémon=')[1];
        formatedInput = formatedInput.split('--Surnom=')[0];
        break;
      case 1:
        formatedInput = callInput.split('--Surnom=')[1];
        break;
      case 2:
        formatedInput = callInput.split('--IDJoueur=')[1];
        formatedInput = formatedInput.split('--IDPokémon=')[0];
        break;
      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/(\r\n|\n|\r)/gm, '') : formatedInput;
    }
    return 0;
  },
  formatEvolutionCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 0:
        formatedInput = callInput.split('--IDPokémon=')[1];
        formatedInput = formatedInput.split('--Devient=')[0];
        break;
      case 1:
        if (!callInput.includes('--Forme=')) {
          formatedInput = callInput.split('--Devient=')[1];
        } else {
          formatedInput = callInput.split('--Devient=')[1];
          formatedInput = formatedInput.split('--Forme=')[0];
        }
        break;
      case 2:
        formatedInput = callInput.split('--IDJoueur=')[1];
        formatedInput = formatedInput.split('--IDPokémon=')[0];
        break;
      case 3:
        formatedInput = callInput.split('--Forme=')[1];
        break;
      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/(\r\n|\n|\r)/gm, '') : formatedInput;
    }
    return 0;
  },
  formatNotesCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 1:
        formatedInput = callInput.split('--Pokémon=')[1];
        formatedInput = formatedInput.split('--Note1=')[0];
        break;
      case 2:
        formatedInput = callInput.split('--Note1=')[1];
        formatedInput = formatedInput.split('--Note2=')[0];
        break;
      case 3:
        formatedInput = callInput.split('--Note2=')[1];
        formatedInput = formatedInput.split('--Note3=')[0];
        break;
      case 4:
        formatedInput = callInput.split('--Note3=')[1];
        formatedInput = formatedInput.split('--Note4=')[0];
        break;
      case 5:
        formatedInput = callInput.split('--Note4=')[1];
        formatedInput = formatedInput.split('--Note5=')[0];
        break;
      case 6:
        formatedInput = callInput.split('--Note5=')[1];
        formatedInput = formatedInput.split('--Note6=')[0];
        break;
      case 7:
        formatedInput = callInput.split('--Note6=')[1];
        formatedInput = formatedInput.split('--Note7=')[0];
        break;
      case 8:
        if (!callInput.includes('--Note8=')) {
          formatedInput = callInput.split('--Note7=')[1];
        } else {
          formatedInput = callInput.split('--Note7=')[1];
          formatedInput = formatedInput.split('--Note8=')[0];
        }
        break;
      case 9:
        if (!callInput.includes('--Note9=')) {
          formatedInput = callInput.split('--Note8=')[1];
        } else {
          formatedInput = callInput.split('--Note8=')[1];
          formatedInput = formatedInput.split('--Note9=')[0];
        }
        break;
      case 10:
        if (callInput.includes('--Note9=')) {
          formatedInput = callInput.split('--Note9=')[1];
        } else {
          formatedInput = '';
        }
        break;
      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/(\r\n|\n|\r)/gm, '') : formatedInput;
    }
    return 0;
  },
  formatNoteCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 0:
        formatedInput = callInput.split('--IDPokémon=')[1];
        formatedInput = formatedInput.split('--Notes=')[0];
        break;
      case 1:
        formatedInput = callInput.split('--Notes=')[1];
        break;
      case 2:
        formatedInput = callInput.split('--IDJoueur=')[1];
        formatedInput = formatedInput.split('--IDPokémon=')[0];
        break;
      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/(\r\n|\n|\r)/gm, '') : formatedInput;
    }
    return 0;
  },
  generatePokémon: function(requestBiome, requestRarity) {
    const data = pokémonData.getAllPokémonFromBiomeAndRarity(requestBiome, requestRarity);

    const randNumber = Math.floor(Math.random() * data['pokémon'].length);
    return data['pokémon'][randNumber];
  },
  generatePokémonSex: function(pokémon) {
    const randNumber = Math.random();
    let gender = '';
    switch (pokémon.genderRate) {
      case -2:
        gender = 'Inconnu';
        break;

      case -1:
        gender = 'Asexué';
        break;

      case 0:
        gender = 'Mâle';
        break;

      case 1:
        if (randNumber < 0.875) gender = 'Mâle';
        else gender = 'Femelle';
        break;

      case 2:
        if (randNumber < 0.75) gender = 'Mâle';
        else gender = 'Femelle';
        break;

      case 4:
        if (randNumber < 0.50) gender = 'Mâle';
        else gender = 'Femelle';
        break;

      case 6:
        if (randNumber < 0.25) gender = 'Mâle';
        else gender = 'Femelle';
        break;

      case 7:
        if (randNumber < 0.125) gender = 'Mâle';
        else gender = 'Femelle';
        break;

      case 8:
        gender = 'Femelle';
        break;
    }

    return gender;
  },
  generatePokémonAbility: function(id, abilities, sex, image) {
    const allAbilities = pokémonData.getAllAbilities();

    let j = 0;

    switch (id) {
      case 550:
        const blue = image.includes('Bleu.png') === true;
        const red = image.includes('Rouge.png') === true;
        const white = image.includes('Blanc.png') === true;

        for (let i = 0; i < abilities.length; i++) {
          if (
            (abilities[i].forBlue === false && blue) ||
              (abilities[i].forRed === false && red) ||
              (abilities[i].forWhite === false && white)
          ) {
            abilities.splice(i, 1);
          }
        }
        break;

      case 678:
      case 876:
      case 902:
        const female = image.includes('Femelle.png') === true;
        const male = image.includes('Male.png') === true;

        for (let i = 0; i < abilities.length; i++) {
          if ((!abilities[i].forFemale && female) || (!abilities[i].forMale && male)) {
            abilities.splice(i, 1);
          }
        }
        break;

      case 849:
        const low = image.includes('Grave.png') === true;
        const amp = image.includes('Aigue.png') === true;

        for (let i = 0; i < abilities.length; i++) {
          if ((!abilities[i].forLowKey && low) || (!abilities[i].forAmped && amp)) {
            abilities.splice(i, 1);
          }
        }

        break;
      default:
        break;
    }
    let ability = abilities[Math.floor(Math.random() * abilities.length)];
    while (ability.isHidden && j < 1) {
      ability = abilities[Math.floor(Math.random() * abilities.length)];
      j++;
    }
    let returnAbility = '';

    for (let i = 0; i < allAbilities.abilities.length; i++) {
      if (allAbilities.abilities[i].id === ability.id) {
        returnAbility = allAbilities.abilities[i];
        break;
      }
    }

    return returnAbility;
  },
  generatePokémonTeraType: function(pokémon, tera) {
    let teraType = '';

    if (tera) {
      let possibleTypes = Array.from(pokémonData.types);

      possibleTypes = lodash.remove(possibleTypes, function(n) {
        return n !== pokémon.type.first && n !== pokémon.type.second;
      });

      teraType = possibleTypes[Math.floor(Math.random()*possibleTypes.length)];
    } else {
      const chooseBetweenTwo = pokémon.type.second === undefined ? 0 : Math.floor(Math.random()*2);
      teraType = chooseBetweenTwo === 0 ? pokémon.type.first : pokémon.type.second;
    }
    return teraType;
  },
  generatePokémonFavouriteBloc: function() {
    let pokebloc = '';

    const random = Math.floor(Math.random() * 4);

    switch (random) {
      case 0:
      case 1:
      case 2:
        const randomBloc = Math.floor(Math.random() * 6);
        switch (randomBloc) {
          case 0:
            pokebloc = 'rainbow';
            break;
          case 1:
            pokebloc = 'red';
            break;
          case 2:
            pokebloc = 'blue';
            break;
          case 3:
            pokebloc = 'pink';
            break;
          case 4:
            pokebloc = 'green';
            break;
          case 5:
            pokebloc = 'yellow';
            break;
        }
        break;

      case 3:
        const randomRare = Math.floor(Math.random() * 3);
        switch (randomRare) {
          case 0:
            pokebloc = 'indigo';
            break;
          case 1:
            pokebloc = 'orange';
            break;
          case 2:
            pokebloc = 'purple';
            break;
        }
        break;
    }

    return pokebloc;
  },
  generatePokémonTraits: function() {
    const allTraits = pokémonData.getAllTraits();
    const totalWeight = [0, 0, 0, 0];
    for (let i = 0; i < allTraits.traits.commun.length; i++) {
      totalWeight[0] += allTraits.traits.commun[i].weight;
    }
    for (let i = 0; i < allTraits.traits.uncommun.length; i++) {
      totalWeight[1] += allTraits.traits.uncommun[i].weight;
    }
    for (let i = 0; i < allTraits.traits.rare.length; i++) {
      totalWeight[2] += allTraits.traits.rare[i].weight;
    }
    for (let i = 0; i < allTraits.traits.veryRare.length; i++) {
      totalWeight[3] += allTraits.traits.veryRare[i].weight;
    }

    const possibleTypes = allTraits.parameters.type;
    const possibleWeathers = allTraits.parameters.weather;
    const possibleColors = allTraits.parameters.color;
    const possibleClasses = allTraits.parameters.classes;

    const numberOfTrait = Math.random() * 100 > 80 ? 3 : 2;

    const returnValue = [];

    for (let i = 0; i < numberOfTrait; i++) {
      const random = Math.floor(Math.random() * 100)+1;
      let traitToGenerate = '';
      let type = 0;
      if (random <= 50) {
        traitToGenerate = allTraits.traits.commun;
        type = 0;
      } else if (random > 50 && random <= 90) {
        traitToGenerate = allTraits.traits.uncommun;
        type = 1;
      } else if (random > 90 && random <= 99) {
        traitToGenerate = allTraits.traits.rare;
        type = 2;
      } else {
        traitToGenerate = allTraits.traits.veryRare;
        type = 3;
      }

      const targetWeight = Math.ceil(Math.random() * totalWeight[type]);
      let cursor = 0;
      for (let j = 0; j < traitToGenerate.length; j++) {
        cursor += traitToGenerate[j].weight;
        if (cursor >= targetWeight) {
          let txt = '';
          if (traitToGenerate[j].trait.includes('[TYPE]')) {
            const type = possibleTypes.splice(Math.floor(Math.random() * possibleTypes.length), 1);
            txt = traitToGenerate[j].trait.replace('[TYPE]', type[0]);
          } else if (traitToGenerate[j].trait.includes('[WEATHER]')) {
            const weather = possibleWeathers.splice(Math.floor(Math.random() * possibleWeathers.length), 1);
            txt = traitToGenerate[j].trait.replace('[WEATHER]', weather[0]);
          } else if (traitToGenerate[j].trait.includes('[COLOR]')) {
            const color = possibleColors.splice(Math.floor(Math.random() * possibleColors.length), 1);
            txt = traitToGenerate[j].trait.replace('[COLOR]', color[0]);
          } else if (traitToGenerate[j].trait.includes('[NUMBER]')) {
            const number = Math.floor(Math.random() * 3) + 2;
            txt = traitToGenerate[j].trait.replace('[NUMBER]', number.toString());
          } else if (traitToGenerate[j].trait.includes('[FEEL]')) {
            const number = Math.floor(Math.random() * traitToGenerate[j].feel.length);
            txt = traitToGenerate[j].trait.replace('[FEEL]', traitToGenerate[j].feel[number]);
          } else if (traitToGenerate[j].trait.includes('[CLASS]')) {
            const number = possibleClasses.splice(Math.floor(Math.random() * possibleClasses.length), 1);
            txt = traitToGenerate[j].trait.replace('[CLASS]', number[0]);
          } else {
            txt = traitToGenerate[j].trait;
          }

          if (!traitToGenerate[j].trait.includes('[TYPE]') &&
          !traitToGenerate[j].trait.includes('[WEATHER]') &&
          !traitToGenerate[j].trait.includes('[COLOR]')) {
            totalWeight[type] -= traitToGenerate[j].weight;
            traitToGenerate[j].weight = 0;
          }
          returnValue.push(
              {
                txt: txt,
              },
          );
          break;
        }
      }
    }

    return returnValue;
  },
  generatePokémonNotes: function(traitsLength) {
    const returnValue = [];
    for (i = 0; i < 10-traitsLength; i++) {
      returnValue.push(
          {
            txt: '',
          },
      );
    }
    return returnValue;
  },
  generatePokémonCondition: function() {
    const contest = {
      beauty: 1,
      cuteness: 1,
      cleverness: 1,
      toughness: 1,
      coolness: 1,
      liveliness: 1,
      atypical: 1,
      natural: 1,
    };
    const switcher = [
      'beauty',
      'cuteness',
      'cleverness',
      'toughness',
      'coolness',
      'liveliness',
      'atypical',
      'natural',
    ];
    const randNumber = (Math.random() * 100);
    let value = 0;
    if (randNumber < 10) value = 1;
    else if (randNumber >= 10 && randNumber < 50) value = 2;
    else if (randNumber >= 50 && randNumber < 90) value = 3;
    else value = 4;

    for (let i = 0; i < value; i++) {
      const randNumber2 = (Math.random() * 100);
      let levelOfCondition = 1;
      if (randNumber2 < 50) levelOfCondition = 2;
      else if (randNumber2 >= 50 && randNumber2 < 75) levelOfCondition = 3;
      else if (randNumber2 >= 75 && randNumber2 < 93.75) levelOfCondition = 4;
      else levelOfCondition = 5;

      const switching = switcher[Math.floor(Math.random() * switcher.length)];
      switch (switching) {
        case 'beauty':
          contest.beauty = levelOfCondition;
          break;
        case 'cuteness':
          contest.cuteness = levelOfCondition;
          break;
        case 'cleverness':
          contest.cleverness = levelOfCondition;
          break;
        case 'toughness':
          contest.toughness = levelOfCondition;
          break;
        case 'coolness':
          contest.coolness = levelOfCondition;
          break;
        case 'liveliness':
          contest.liveliness = levelOfCondition;
          break;
        case 'atypical':
          contest.atypical = levelOfCondition;
          break;
        case 'natural':
          contest.natural = levelOfCondition;
          break;
      }
      for (let j = 0; j < switcher.length; j++) {
        if (switcher[j] === switching) {
          switcher.splice(j, 1);
        }
      }
    }

    return contest;
  },
  addLevelPokémon: function(under20, under40, under60, under80, under100) {
    const allPlayersPokémon = pokémonData.getAllPlayersPokémon();
    const returnJSON = [];

    if (allPlayersPokémon.playersPokémon.length !== 0) {
      for (let i = 0; i < allPlayersPokémon.playersPokémon.length; i++) {
        let pokémonLine = '';
        returnJSON.push(
            {
              trainerId: allPlayersPokémon.playersPokémon[i].id,
              trainerLine: tr.playersPokémon.allTrainerPokémon.trainer.replace(
                  '{PLAYER}',
                  allPlayersPokémon.playersPokémon[i].username,
              ),
              pokémonLines: [],
            },
        );

        if (allPlayersPokémon.playersPokémon[i].playerPokémon.length !== 0) {
          for (let j = 0; j < allPlayersPokémon.playersPokémon[i].playerPokémon.length; j++) {
            if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].level < 100) {
              pokémonLine = allPlayersPokémon.playersPokémon[i].playerPokémon[j].surname !== '' ?
                  tr.playersPokémon.levelAndFriendship.levelUp.replace(
                      '{POKÉMON}',
                      allPlayersPokémon.playersPokémon[i].playerPokémon[j].surname,
                  ) :
                  tr.playersPokémon.levelAndFriendship.levelUp.replace(
                      '{POKÉMON}',
                      allPlayersPokémon.playersPokémon[i].playerPokémon[j].name,
                  );

              pokémonLine = pokémonLine.replace(
                  '{OLDLEVEL}',
                  allPlayersPokémon.playersPokémon[i].playerPokémon[j].level,
              );

              const intLevel = parseInt(allPlayersPokémon.playersPokémon[i].playerPokémon[j].level);
              switch (true) {
                case (intLevel < 20):
                  allPlayersPokémon.playersPokémon[i].playerPokémon[j].level = intLevel + parseInt(under20);
                  break;
                case (intLevel < 40):
                  allPlayersPokémon.playersPokémon[i].playerPokémon[j].level = intLevel + parseInt(under40);
                  break;
                case (intLevel < 60):
                  allPlayersPokémon.playersPokémon[i].playerPokémon[j].level = intLevel + parseInt(under60);
                  break;
                case (intLevel < 80):
                  allPlayersPokémon.playersPokémon[i].playerPokémon[j].level = intLevel + parseInt(under80);
                  break;
                case (intLevel < 100):
                  allPlayersPokémon.playersPokémon[i].playerPokémon[j].level = intLevel + parseInt(under100);
                  break;
              }

              // Check if upper 100 level
              if (parseInt(allPlayersPokémon.playersPokémon[i].playerPokémon[j].level) > 100) {
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].level = 100;
              }

              pokémonLine = pokémonLine.replace(
                  '{NEWLEVEL}',
                  allPlayersPokémon.playersPokémon[i].playerPokémon[j].level,
              );
            } else {
              pokémonLine = allPlayersPokémon.playersPokémon[i].playerPokémon[j].surname !== '' ?
                  tr.playersPokémon.levelAndFriendship.maxLevel.replace(
                      '{POKÉMON}',
                      allPlayersPokémon.playersPokémon[i].playerPokémon[j].surname,
                  ) :
                  tr.playersPokémon.levelAndFriendship.maxLevel.replace(
                      '{POKÉMON}',
                      allPlayersPokémon.playersPokémon[i].playerPokémon[j].name,
                  );
            }
            returnJSON[i].pokémonLines.push(pokémonLine);
          }
        } else {
          returnJSON[i].pokémonLines.push(tr.playersPokémon.allTrainerPokémon.none);
        }
      }
    }

    botService.jsonWriteFile(allPlayersPokémon, 'playersPokemon');
    return returnJSON;
  },
  addFriendshipPokémon: function() {
    const allPlayersPokémon = pokémonData.getAllPlayersPokémon();
    const returnJSON = [];
    if (allPlayersPokémon.playersPokémon.length !== 0) {
      for (let i = 0; i < allPlayersPokémon.playersPokémon.length; i++) {
        let pokémonLine = '';
        returnJSON.push(
            {
              trainerId: allPlayersPokémon.playersPokémon[i].id,
              trainerLine: tr.playersPokémon.allTrainerPokémon.trainer.replace(
                  '{PLAYER}',
                  allPlayersPokémon.playersPokémon[i].username,
              ),
              pokémonLines: [],
            },
        );

        if (allPlayersPokémon.playersPokémon[i].playerPokémon.length !== 0) {
          for (let j = 0; j < allPlayersPokémon.playersPokémon[i].playerPokémon.length; j++) {
            const conditionString = this.addConditionPokémon(allPlayersPokémon.playersPokémon[i].playerPokémon[j]);
            if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].friendship < 15) {
              pokémonLine = allPlayersPokémon.playersPokémon[i].playerPokémon[j].surname !== '' ?
                  tr.playersPokémon.levelAndFriendship.friendship.replace(
                      '{POKÉMON}',
                      allPlayersPokémon.playersPokémon[i].playerPokémon[j].surname,
                  ) :
                  tr.playersPokémon.levelAndFriendship.friendship.replace(
                      '{POKÉMON}',
                      allPlayersPokémon.playersPokémon[i].playerPokémon[j].name,
                  );


              const random = Math.floor(Math.random() * 6);

              switch (random) {
                case 0:
                case 1:
                case 2:
                  pokémonLine = pokémonLine.replace('{CHANGE}', tr.playersPokémon.levelAndFriendship.change[0]);
                  break;
                case 3:
                case 4:
                  if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].ball === 11) {
                    allPlayersPokémon.playersPokémon[i].playerPokémon[j].friendship += 2;
                    pokémonLine = pokémonLine.replace('{CHANGE}', tr.playersPokémon.levelAndFriendship.change[3]);
                  } else {
                    allPlayersPokémon.playersPokémon[i].playerPokémon[j].friendship += 1;
                    pokémonLine = pokémonLine.replace('{CHANGE}', tr.playersPokémon.levelAndFriendship.change[1]);
                  }
                  break;
                case 5:
                  if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].ball === 11) {
                    allPlayersPokémon.playersPokémon[i].playerPokémon[j].friendship += 4;
                    pokémonLine = pokémonLine.replace(
                        '{CHANGE}',
                        tr.playersPokémon.levelAndFriendship.change[4],
                    );
                  } else {
                    allPlayersPokémon.playersPokémon[i].playerPokémon[j].friendship += 2;
                    pokémonLine = pokémonLine.replace(
                        '{CHANGE}',
                        tr.playersPokémon.levelAndFriendship.change[2],
                    );
                  }

                  break;
              }

              if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].friendship > 15) {
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].friendship = 15;
              }
            } else {
              pokémonLine = allPlayersPokémon.playersPokémon[i].playerPokémon[j].surname !== '' ?
                  tr.playersPokémon.levelAndFriendship.maxFriendship.replace(
                      '{POKÉMON}',
                      allPlayersPokémon.playersPokémon[i].playerPokémon[j].surname,
                  ) :
                  tr.playersPokémon.levelAndFriendship.maxFriendship.replace(
                      '{POKÉMON}',
                      allPlayersPokémon.playersPokémon[i].playerPokémon[j].name,
                  );
            }
            pokémonLine = pokémonLine + conditionString + '\n';
            returnJSON[i].pokémonLines.push(pokémonLine);
          }
        } else {
          returnJSON[i].pokémonLines.push(tr.playersPokémon.allTrainerPokémon.none);
        }
      }
    }

    botService.jsonWriteFile(allPlayersPokémon, 'playersPokemon');
    return returnJSON;
  },
  addConditionPokémon: function(playerPokemon) {
    const randomAdd = Math.floor(Math.random() * 3);
    let returnString = '';

    if (randomAdd === 1) {
      const randomCondition = Math.floor(Math.random() * 8);

      switch (randomCondition) {
        case 1:
          playerPokemon.contest.beauty = playerPokemon.contest.beauty + 1;
          returnString = tr.pokémon.addConditionPokémon.beauty;
          break;
        case 2:
          playerPokemon.contest.cuteness = playerPokemon.contest.cuteness + 1;
          returnString = tr.pokémon.addConditionPokémon.cuteness;
          break;
        case 3:
          playerPokemon.contest.cleverness = playerPokemon.contest.cleverness + 1;
          returnString = tr.pokémon.addConditionPokémon.cleverness;
          break;
        case 4:
          playerPokemon.contest.toughness = playerPokemon.contest.toughness + 1;
          returnString = tr.pokémon.addConditionPokémon.toughness;
          break;
        case 5:
          playerPokemon.contest.coolness = playerPokemon.contest.coolness + 1;
          returnString = tr.pokémon.addConditionPokémon.coolness;
          break;
        case 6:
          playerPokemon.contest.liveliness = playerPokemon.contest.liveliness + 1;
          returnString = tr.pokémon.addConditionPokémon.liveliness;
          break;
        case 7:
          playerPokemon.contest.atypical = playerPokemon.contest.atypical + 1;
          returnString = tr.pokémon.addConditionPokémon.atypical;
          break;
        case 8:
          playerPokemon.contest.natural = playerPokemon.contest.natural + 1;
          returnString = tr.pokémon.addConditionPokémon.natural;
          break;
      }
    }

    return returnString;
  },
  addNewPokémon: function(idTrainerInput, pokemonIdInput, level, pokeball, place) {
    const players = pokémonData.getAllPlayersPokémon();
    const generatedPokémon = generatedPokémonData.getAllGeneratedPokémon();
    const ball = pokémonData.getAllBalls();
    let ballFound = false;
    level = level > 100 ? 100 : level;

    pokeball = botService.removeDiacritics(botService.removeSpaceAndEnterKey(pokeball.toLowerCase()));

    let pokémon = '';
    let playerName = '';

    let k = 0;
    while (k < ball.balls.length && ballFound == false) {
      const ballName = botService.removeDiacritics(botService.removeSpaceAndEnterKey(ball.balls[k].name.toLowerCase()));

      if (ballName === pokeball) {
        ballFound = true;
        pokeball = ball.balls[k].id;
      }
      k++;
    }

    if (ballFound == false) {
      pokeball = 27;
    }
    for (let h = 0; h < generatedPokémon.generatedPokémon.length; h++) {
      if (generatedPokémon.generatedPokémon[h].genId.toString().trim() === pokemonIdInput.toString().trim()) {
        // if (generatedPokémon.generatedPokémon[h].given === false) {
        generatedPokémon.generatedPokémon[h].given = true;
        botService.jsonWriteFile(generatedPokémon, 'generatedPokémon');
        pokémon = generatedPokémon.generatedPokémon[h];
        break;
        // }
        // else return 'alreadyGiven'
      }
    }

    if (pokémon !== '') {
      for (let i = 0; i < players.playersPokémon.length; i++) {
        if (players.playersPokémon[i].id.toString().trim() === idTrainerInput.toString().trim()) {
          if (level !== 0) {
            pokémon.level = level;
            pokémon.catchLevel = level;
          }
          if (pokeball !== 0) pokémon.ball = pokeball;
          if (pokeball == 22) pokémon.friendship = 8;

          if (place !== 0) pokémon.place = place;
          pokémon.genId = players.playersPokémon[i].playerPokémon.length + 1;
          players.playersPokémon[i].playerPokémon.push(pokémon);
          playerName = players.playersPokémon[i].username;
          break;
        }
      }

      if (playerName !== '') {
        botService.jsonWriteFile(players, 'playersPokemon');
        return {pokémon: pokémon, trainer: playerName};
      } else {
        return null;
      }
    } else {
      return null;
    }
  },
  // TODO Refacto
  changeMoveOfAPokémon: function(idTrainer, idPokémon, move1, move2, move3, move4) {
    idTrainer = this.formatIntInput(idTrainer);
    idPokémon = this.formatIntInput(idPokémon);

    const allPlayersPokémon = pokémonData.getAllPlayersPokémon();
    const translation = tr.playersPokémon.moveChange;
    let title = '';
    let returnValue = '';

    for (let i = 0; i < allPlayersPokémon.playersPokémon.length; i++) {
      if (idTrainer === this.formatIntInput(allPlayersPokémon.playersPokémon[i].id)) {
        for (let j = 0; j < allPlayersPokémon.playersPokémon[i].playerPokémon.length; j++) {
          if (idPokémon === this.formatIntInput(allPlayersPokémon.playersPokémon[i].playerPokémon[j].genId)) {
            if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].surname !== '') {
              // eslint-disable-next-line max-len
              title = translation.title.replace('{POKÉMON}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].surname);
            } else {
              // eslint-disable-next-line max-len
              title = translation.title.replace('{POKÉMON}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].name);
            }

            title = title.replace('{NAME}', allPlayersPokémon.playersPokémon[i].username);

            if (move1 === 'Oublier' || move1 === 'oublier') {
              if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move1 !== '') {
                // eslint-disable-next-line max-len
                returnValue = returnValue + translation.remove.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move1);
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move1 = '';
              }
            } else if (move1 === '') {
              // eslint-disable-next-line max-len
              if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move1 !== '') {} returnValue = returnValue + translation.inchanged.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move1);
            } else {
              let changing = '';
              // eslint-disable-next-line max-len
              if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move1 !== '' && allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move1 !== move1) {
                // eslint-disable-next-line max-len
                changing = translation.changed.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move1);
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move1 = move1;
                // eslint-disable-next-line max-len
                changing = changing.replace('{NEWMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move1);
                // eslint-disable-next-line max-len
              } else if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move1 !== '' && allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move1 === move1) {
                // eslint-disable-next-line max-len
                changing = translation.inchanged.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move1);
              } else {
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move1 = move1;
                // eslint-disable-next-line max-len
                changing = translation.added.replace('{NEWMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move1);
              }
              returnValue = returnValue + changing;
            }

            if (move2 === 'Oublier' || move2 === 'oublier') {
              if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move2 !== '') {
                // eslint-disable-next-line max-len
                returnValue = returnValue + translation.remove.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move2);
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move2 = '';
              }
            } else if (move2 === '') {
              // eslint-disable-next-line max-len
              if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move2 !== '') returnValue = returnValue + translation.inchanged.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move2);
            } else {
              let changing = '';
              // eslint-disable-next-line max-len
              if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move2 !== '' && allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move2 !== move2) {
                // eslint-disable-next-line max-len
                changing = translation.changed.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move2);
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move2 = move2;
                // eslint-disable-next-line max-len
                changing = changing.replace('{NEWMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move2);
                // eslint-disable-next-line max-len
              } else if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move2 !== '' && allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move2 === move2) {
                // eslint-disable-next-line max-len
                changing = translation.inchanged.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move2);
              } else {
                // eslint-disable-next-line max-len
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move2 = move2;
                // eslint-disable-next-line max-len
                changing = translation.added.replace('{NEWMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move2);
              }
              returnValue = returnValue + changing;
            }

            if (move3 === 'Oublier' || move3 === 'oublier') {
              if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move3 !== '') {
                // eslint-disable-next-line max-len
                returnValue = returnValue + translation.remove.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move3);
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move3 = '';
              }
            } else if (move3 === '') {
              // eslint-disable-next-line max-len
              if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move3 !== '') returnValue = returnValue + translation.inchanged.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move3);
            } else {
              let changing = '';
              // eslint-disable-next-line max-len
              if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move3 !== '' && allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move3 !== move3) {
                // eslint-disable-next-line max-len
                changing = translation.changed.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move3);
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move3 = move3;
                // eslint-disable-next-line max-len
                changing = changing.replace('{NEWMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move3);
                // eslint-disable-next-line max-len
              } else if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move3 !== '' && allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move3 === move3) {
                // eslint-disable-next-line max-len
                changing = translation.inchanged.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move3);
              } else {
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move3 = move3;
                // eslint-disable-next-line max-len
                changing = translation.added.replace('{NEWMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move3);
              }
              returnValue = returnValue + changing;
            }

            if (move4 === 'Oublier' || move4 === 'oublier') {
              if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move4 !== '') {
                // eslint-disable-next-line max-len
                returnValue = returnValue + translation.remove.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move4);
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move4 = '';
              }
            } else if (move4 === '') {
              // eslint-disable-next-line max-len
              if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move4 !== '') returnValue = returnValue + translation.inchanged.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move4);
            } else {
              let changing = '';
              // eslint-disable-next-line max-len
              if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move4 !== '' && allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move4 !== move4) {
                // eslint-disable-next-line max-len
                changing = translation.changed.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move4);
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move4 = move4;
                // eslint-disable-next-line max-len
                changing = changing.replace('{NEWMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move4);
                // eslint-disable-next-line max-len
              } else if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move4 !== '' && allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move4 === move4) {
                // eslint-disable-next-line max-len
                changing = translation.inchanged.replace('{OLDMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move4);
              } else {
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move4 = move4;
                // eslint-disable-next-line max-len
                changing = translation.added.replace('{NEWMOVE}', allPlayersPokémon.playersPokémon[i].playerPokémon[j].moves.move4);
              }
              returnValue = returnValue + changing;
            }
            botService.jsonWriteFile(allPlayersPokémon, 'playersPokemon');
          }
        }
      }
    }
    if (returnValue === '') {
      title = 'Erreur';
      returnValue = 'Une erreur est survenue. Soit ce Dresseur ne possède pas de Pokémon, ' +
          'soit l\'ID du Pokémon est invalide, soit ce joueur n\'existe pas.';
    }
    return {title, returnValue};
  },
  formatIntInput: function(intValue) {
    return intValue.toString().trim();
  },
  changeNameOfAPokémon: function(idTrainer, idPokémon, nickname) {
    idTrainer = this.formatIntInput(idTrainer);
    idPokémon = this.formatIntInput(idPokémon);

    nickname = nickname.slice(0, 15);

    const allPlayersPokémon = pokémonData.getAllPlayersPokémon();
    const trad = tr.playersPokémon.nicknameChange;
    let title = trad.title;
    let returnValue = trad.return;
    const changed = trad.changed;
    const same = trad.same;
    const remove = trad.remove;

    for (let i = 0; i < allPlayersPokémon.playersPokémon.length; i++) {
      if (idTrainer === this.formatIntInput(allPlayersPokémon.playersPokémon[i].id)) {
        for (let j = 0; j < allPlayersPokémon.playersPokémon[i].playerPokémon.length; j++) {
          if (idPokémon === this.formatIntInput(allPlayersPokémon.playersPokémon[i].playerPokémon[j].genId)) {
            const pokémon = allPlayersPokémon.playersPokémon[i].playerPokémon[j];
            if (pokémon.surname === '') {
              title = title.replace('{POKEMON}', pokémon.name);
              if (nickname === pokémon.name || nickname === undefined) {
                returnValue = remove;
                title = title.replace('{SURNAME}', pokémon.name);
              } else {
                returnValue = changed;
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].surname = nickname;
                title = title.replace('{SURNAME}', nickname);
              }
            } else {
              title = title.replace('{POKEMON}', pokémon.surname);
              if (nickname === pokémon.surname || nickname === undefined) {
                returnValue = same;
                title = title.replace('{SURNAME}', pokémon.surname);
              } else if (nickname === pokémon.name || nickname === 'RetirerSurnom') {
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].surname = '';
                title = title.replace('{SURNAME}', pokémon.name);
                returnValue = remove;
              } else {
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].surname = nickname;
                title = title.replace('{SURNAME}', nickname);
                returnValue = changed;
              }
            }

            botService.jsonWriteFile(allPlayersPokémon, 'playersPokemon');
          }
        }
      }
    }

    return {title: title, field: returnValue};
  },
  evolveAPokémon: function(idTrainer, idPokémon, evolution, forme) {
    idTrainer = this.formatIntInput(idTrainer);
    idPokémon = this.formatIntInput(idPokémon);
    if (forme == 0) forme = null;

    const allTrainersPokémon = pokémonData.getAllPlayersPokémon();
    const pokemonEvolution = pokémonData.getPokémonFromPokémonNameandForm(
        evolution,
        forme,
        null);

    if (pokemonEvolution != '') {
      let oldPokemon = '';
      let oldPokemonSurname = '';
      let trainer = '';
      let imagePath = '';
      const newPokemon = pokemonEvolution['name'];

      for (let i = 0; i < allTrainersPokémon.playersPokémon.length; i++) {
        if (idTrainer === this.formatIntInput(allTrainersPokémon.playersPokémon[i].id)) {
          trainer = allTrainersPokémon.playersPokémon[i].username;

          for (let j = 0; j < allTrainersPokémon.playersPokémon[i].playerPokémon.length; j++) {
            if (idPokémon === this.formatIntInput(allTrainersPokémon.playersPokémon[i].playerPokémon[j].genId)) {
              const pokemon = allTrainersPokémon.playersPokémon[i].playerPokémon[j];
              oldPokemon = pokemon['name'];
              oldPokemonSurname = pokemon['surname'];

              const pokemonBase = pokémonData.getPokémonFromPokémonNameandForm(
                  pokemon['name'],
                  null,
                  null);


              /* CHANGEMENT DU NOM D'ESPECE*/ {
                pokemon['name'] = pokemonEvolution['name'];

                // CHANGEMENT D'IMAGE
                // Cas spécifique pour Flabébé, Floette et Florges
                if (pokemonEvolution.id === 670 || pokemonEvolution.id === 671) {
                  pokemon.imagePath = pokemon.imagePath.replace(pokemonBase.id, pokemonEvolution.id);
                } else { // Par défaut
                  pokemon.imagePath = botService.generatePathPokémonImages(pokemonEvolution, pokemon.sex);
                }

                imagePath = pokemon.imagePath;
              }

              /* CHANGEMENT DE TALENT*/ {
                switch (pokemonEvolution.id) {
                  case 916:
                  case 678: /* Cas des Pokémon avec dimorphismes sexuels qui inflluencent les talents */
                  case 876: /* Fragrouin, Wimessir, Mistigrix, Paragruel*/
                  case 902:
                    const sex = pokemon.sex == 'Mâle' ? 'forMale' : 'forFemale';
                    pokemonEvolution.abilities = pokemonEvolution.abilities.filter(function(ability) {
                      return ability[sex] !== false;
                    });
                    break;

                  case 849: /* Cas de Salarsen*/
                    const formToxtricity = imagePath.includes('Grave') ? 'forLowKey' : 'forAmped';
                    pokemonEvolution.abilities = pokemonEvolution.abilities.filter(function(ability) {
                      return ability[formToxtricity] == true;
                    });
                    break;
                }
                let positionTalent = 0;
                for (let k = 0; k < pokemonBase.abilities.length; k++) {
                  if (pokemonBase.abilities[k].id == pokemon.ability) {
                    positionTalent = k;
                  }
                }
                if (pokemonBase.abilities.length > pokemonEvolution.abilities.length) {
                  if (pokemonEvolution.abilities.length == 1) {
                    positionTalent = 0;
                  } else if (pokemonEvolution.abilities.length == 2) {
                    if (positionTalent == 1) {
                      positionTalent = 0;
                    } else if (positionTalent == 2) {
                      positionTalent = 1;
                    }
                  }
                }
                pokemon.ability = pokemonEvolution.abilities[positionTalent].id;
              }
              botService.jsonWriteFile(allTrainersPokémon, 'playersPokemon');
              return {error: 0,
                oldPokemon: oldPokemon,
                oldPokemonSurname: oldPokemonSurname,
                newPokemon: newPokemon,
                imagePath: imagePath,
                trainer: trainer};
            }
          }
          return {error: 2, trainer: trainer};
        }
      }

      return {error: 1};
    } else {
      return {error: 3};
    }
  },
  changeNoteTemplate(idTrainer, idPokémon) {
    let needToUpdate = false;
    const allPlayersPokémon = pokémonData.getAllPlayersPokémon();
    let pokémonFound = false;
    let trainerFound = false;
    let returnValue = 'nb!pokémon modifierNotes \n--Pokémon={IDPKMN}\n';
    for (let i = 0; i < allPlayersPokémon.playersPokémon.length; i++) {
      if (idTrainer === this.formatIntInput(allPlayersPokémon.playersPokémon[i].id)) {
        trainerFound = true;
        for (let j = 0; j < allPlayersPokémon.playersPokémon[i].playerPokémon.length; j++) {
          if (idPokémon === this.formatIntInput(allPlayersPokémon.playersPokémon[i].playerPokémon[j].genId)) {
            const pokemon = allPlayersPokémon.playersPokémon[i].playerPokémon[j];
            pokémonFound = true;
            returnValue = returnValue.replace('{IDPKMN}', pokemon.genId);

            if (pokemon.editableNotes == undefined) {
              pokemon.editableNotes = this.generatePokémonNotes(pokemon.notes.length);
              needToUpdate = true;
            }

            for (k = 0; k < pokemon.editableNotes.length; k++) {
              returnValue = returnValue + '--Note' + (k+1) + '=' + pokemon.editableNotes[k].txt;
              if (k != pokemon.editableNotes.length -1) {
                returnValue += '\n';
              }
            }

            break;
          }
        }
      }
    }
    if (needToUpdate) {
      botService.jsonWriteFile(allPlayersPokémon, 'playersPokemon');
    };
    if (trainerFound) {
      if (pokémonFound) {
        return tr.playersPokémon.noteChange.template.templateData.replace('{COMMAND}', returnValue);
      } else {
        return tr.playersPokémon.noteChange.template.noPokemon;
      }
    } else {
      return tr.playersPokémon.noteChange.template.noTrainer;
    }
  },
  changeNote(idTrainer, idPokémon, notesToAdd) {
    const maxLenght = 60;
    let returnText = '';
    let trainerFound = false;
    const allPlayersPokémon = pokémonData.getAllPlayersPokémon();
    let pokémonFound = false;
    for (let i = 0; i < allPlayersPokémon.playersPokémon.length; i++) {
      if (idTrainer === this.formatIntInput(allPlayersPokémon.playersPokémon[i].id)) {
        trainerFound = true;
        for (let j = 0; j < allPlayersPokémon.playersPokémon[i].playerPokémon.length; j++) {
          if (idPokémon === this.formatIntInput(allPlayersPokémon.playersPokémon[i].playerPokémon[j].genId)) {
            const pokemon = allPlayersPokémon.playersPokémon[i].playerPokémon[j];
            pokémonFound = true;

            if (pokemon.editableNotes == undefined) {
              pokemon.editableNotes = this.generatePokémonNotes(pokemon.notes.length);
            }

            for (k = 0; k < pokemon.editableNotes.length; k++) {
              if (notesToAdd[k].length > maxLenght) {
                returnText += tr.playersPokémon.noteChange.normal.information.replace('{ID}', k+1) +
                tr.playersPokémon.noteChange.normal.reason.tooLong.replace('{NB}', maxLenght);
              } else {
                if (pokemon.editableNotes[k].txt != notesToAdd[k]) {
                  if (notesToAdd[k] != '') {
                    returnText += tr.playersPokémon.noteChange.normal.information.replace('{ID}', k+1) +
                    tr.playersPokémon.noteChange.normal.reason.add;
                  } else {
                    returnText += tr.playersPokémon.noteChange.normal.information.replace('{ID}', k+1) +
                    tr.playersPokémon.noteChange.normal.reason.delete;
                  }

                  if (pokemon.editableNotes[k].txt != '') {
                    returnText += tr.playersPokémon.noteChange.normal.reason.last.replace(
                        '{NOTE}', pokemon.editableNotes[k].txt);
                  }
                  returnText += '\n';
                  pokemon.editableNotes[k].txt = notesToAdd[k];
                }
              }
            }

            break;
          }
        }
      }
    }
    if (trainerFound) {
      if (pokémonFound) {
        botService.jsonWriteFile(allPlayersPokémon, 'playersPokemon');
        return tr.playersPokémon.noteChange.normal.data + returnText;
      } else {
        return tr.playersPokémon.noteChange.normal.noID;
      }
    } else {
      return tr.playersPokémon.noteChange.normal.noTrainer;
    }
  },
  pokémonAddNote: function(idTrainer, idPokémon, notes) {
    const allPlayersPokémon = pokémonData.getAllPlayersPokémon();
    let returnValue = '';
    let returnError = '';
    for (let i = 0; i < allPlayersPokémon.playersPokémon.length; i++) {
      if (idTrainer === this.formatIntInput(allPlayersPokémon.playersPokémon[i].id)) {
        for (let j = 0; j < allPlayersPokémon.playersPokémon[i].playerPokémon.length; j++) {
          if (idPokémon === this.formatIntInput(allPlayersPokémon.playersPokémon[i].playerPokémon[j].genId)) {
            if (notes.length > 100) {
              returnError = notes.length;
              returnValue = -2;
              break;
            } else {
              if (allPlayersPokémon.playersPokémon[i].playerPokémon[j].notes.length > 9) {
                returnError = allPlayersPokémon.playersPokémon[i].playerPokémon[j].notes[0].txt;
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].notes.shift();
              }
              allPlayersPokémon.playersPokémon[i].playerPokémon[j].notes.push({
                txt: notes,
              },
              );
            }
            botService.jsonWriteFile(allPlayersPokémon, 'playersPokemon');
            returnValue = allPlayersPokémon.playersPokémon[i].playerPokémon[j];
            break;
          } else returnValue = 0;
        }
      } else returnValue - 1;
    }

    return {returnValue, returnError};
  },
  modifyNotesCommand: function(idPlayer, pokémonData, admin) {
    let returnString = '';
    // Check if date exist and is valid
    if (pokémonData === 0) return 'Ce joueur n\'a pas de Pokémon à l\'ID indiqué';
    else if (pokémonData === -1) return 'Ce joueur n\'est pas dans les données du Robot';
    else {
      if (admin) {
        returnString = 'nb!pokémon modifyNotes \n';
        returnString = returnString + '--Joueur=' + idPlayer + '\n';
      } else {
        returnString = 'nb!pokémon modifierNotes \n';
      }
      returnString = returnString + '--Pokémon=' + pokémonData.genId + '\n';

      for (let i = 0; i < pokémonData.notes.length; i++) {
        returnString = returnString + '--Note' + (i + 1) + '=' + pokémonData.notes[i].txt + '\n';
      }
    }

    return returnString;
  },
  getDataAndModifyDateCommand: function(input, idPlayer, admin) {
    const allPlayersPokémon = pokémonData.getAllPlayersPokémon();

    let returnID = 'Pokémon invalide';
    let playerClass = false;

    let returnError = '';

    if (admin) idPlayer = botService.removeSpaceAndEnterKey((input.split('--Joueur=')[1]).split('--Pokémon=')[0]);

    if (input.includes('Note')) {
      const pokémonID = botService.removeSpaceAndEnterKey((input.split('--Pokémon=')[1]).split('--Note1=')[0]);

      if (allPlayersPokémon.playersPokémon.length !== 0) {
        for (let i = 0; i < allPlayersPokémon.playersPokémon.length; i++) {
          if (idPlayer === allPlayersPokémon.playersPokémon[i].id.toString()) {
            if (allPlayersPokémon.playersPokémon[i].class === 'Coordinator') playerClass = true;
            for (let j = 0; j < allPlayersPokémon.playersPokémon[i].playerPokémon.length; j++) {
              if (pokémonID === allPlayersPokémon.playersPokémon[i].playerPokémon[j].genId.toString()) {
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].notes = [];

                let eventsLoop = true;
                let loopCount = 1;
                let maxCount = 11;
                while (eventsLoop) {
                  if (input.includes('--Note' + (loopCount + 1))) {
                    const txt = botService.removeEnterKey(
                        (input.split('--Note' + loopCount + '=')[1]).split('--Note' + (loopCount + 1) + '=')[0],
                    );
                    if (txt.length > 100) {
                      returnError = returnError +
                          '--Note' +
                          loopCount +
                          ' **- (Trop long, 100 caractères max. (' +
                          txt.length +
                          ')\n**';
                      maxCount++;
                    } else if (loopCount >= maxCount) {
                      returnError = returnError +
                          '--Note' +
                          loopCount +
                          ' **- (Pas plus de 10 notes possibles).**\n';
                    } else {
                      allPlayersPokémon.playersPokémon[i].playerPokémon[j].notes.push(
                          {
                            txt: txt,
                          },
                      );
                    }
                    loopCount++;
                  } else {
                    const txt = botService.removeEnterKey(input.split('--Note' + loopCount + '=')[1]);
                    if (txt.length > 100) {
                      returnError = returnError +
                          '--Note' + loopCount +
                          ' **- (Trop long, 100 caractères max. (' +
                          txt.length +
                          ')\n**';
                    } else if (loopCount >= maxCount) {
                      returnError = returnError +
                          '--Note' +
                          loopCount +
                          ' **- (Pas plus de 10 notes possibles).**\n';
                    } else {
                      allPlayersPokémon.playersPokémon[i].playerPokémon[j].notes.push(
                          {
                            txt: txt,
                          },
                      );
                    }
                    eventsLoop = false;
                  }
                }
                returnID = allPlayersPokémon.playersPokémon[i].playerPokémon[j];
              } else returnID = 0;
            }
          } else returnID = -1;
        }
      } else {
        returnID = -1;
      }
    } else {
      const pokémonID = input.split('--Pokémon=')[1].replace(/\s+/g, '');

      if (allPlayersPokémon.playersPokémon.length !== 0) {
        for (let i = 0; i < allPlayersPokémon.playersPokémon.length; i++) {
          if (idPlayer === allPlayersPokémon.playersPokémon[i].id.toString()) {
            if (allPlayersPokémon.playersPokémon[i].class === 'Coordinator') playerClass = true;
            for (let j = 0; j < allPlayersPokémon.playersPokémon[i].playerPokémon.length; j++) {
              if (pokémonID === allPlayersPokémon.playersPokémon[i].playerPokémon[j].genId.toString()) {
                allPlayersPokémon.playersPokémon[i].playerPokémon[j].notes = [];
                returnID = allPlayersPokémon.playersPokémon[i].playerPokémon[j];
              } else returnID = 0;
            }
          } else returnID = -1;
        }
      } else {
        returnID = -1;
      }
    }

    botService.jsonWriteFile(allPlayersPokémon, 'playersPokemon');
    return {returnID, playerClass, returnError};
  },
  getPlayerPokémonByID: function(playerId, callInput) {
    callInput = callInput.toString();
    playerId = playerId.toString();
    const allPlayersPokémon = pokémonData.getAllPlayersPokémon();
    let returnValue = '';
    if (allPlayersPokémon.playersPokémon.length !== 0) {
      for (let i = 0; i < allPlayersPokémon.playersPokémon.length; i++) {
        if (playerId === allPlayersPokémon.playersPokémon[i].id.toString()) {
          for (let j = 0; j < allPlayersPokémon.playersPokémon[i].playerPokémon.length; j++) {
            if (callInput === allPlayersPokémon.playersPokémon[i].playerPokémon[j].genId.toString()) {
              returnValue = allPlayersPokémon.playersPokémon[i].playerPokémon[j];
              break;
            } else returnValue = 0;
          }
        }
      }
    } else {
      returnValue = -1;
    }
    return returnValue;
  },
  getPlayerPokémonFoundItemsByIds: function(playerId, callInput) {
    callInput = callInput.toString();
    playerId = playerId.toString();
    const allPlayers = playerData.getAllPlayers();
    const jsonOutput = [];

    for (let i = 0; i < allPlayers.players.length; i++) {
      if (allPlayers.players[i] != null) {
        if (allPlayers.players[i].id === playerId) {
          for (let j = 0; j < allPlayers.players[i].inventory.foundItems.length; j++) {
            if (allPlayers.players[i].inventory.foundItems[j].pokemonId == callInput) {
              jsonOutput.push(allPlayers.players[i].inventory.foundItems[j].id);
            }
          }
        }
      }
    }

    return jsonOutput;
  },
};
