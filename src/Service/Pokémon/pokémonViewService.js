const lodash = require('lodash');

const pokémonControllerService = require('./pokémonControllerService');
const emojiService = require('../../Service/emojiService');

const pokémonData = require('../../Data/pokémonData');
const biomeData = require('../../Data/biomeData');
const itemsData = require('../../Data/itemsData');

const frenchTranslation = require('../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {

  checkIfUserAlreadyAdded: function(id) {
    const playersPokémon = pokémonData.getAllPlayersPokémon();
    for (let i = 0; i < playersPokémon.playersPokémon.length; i++) {
      if (playersPokémon.playersPokémon[i] != null) {
        if (playersPokémon.playersPokémon[i].id === id) {
          return true;
        }
      }
    }
    return false;
  },
  generateDangerousnessDescription: function(dangerousness, client) {
    const emoji = emojiService.getDangerousnessEmojiFromDangerousnessInt(
        dangerousness,
        client,
    );
    let returnValue = ' ';
    switch (dangerousness) {
      case 0:
        returnValue = tr.pokémon.pokémonPokédexTemplate.dangerousnessDescriptions.case0
            .replace('{EMOJI}', emoji);
        break;
      case 1:
        returnValue = tr.pokémon.pokémonPokédexTemplate.dangerousnessDescriptions.case1
            .replace('{EMOJI}', emoji);
        break;
      case 2:
        returnValue = tr.pokémon.pokémonPokédexTemplate.dangerousnessDescriptions.case2
            .replace('{EMOJI}', emoji);
        break;
      case 3:
        returnValue = tr.pokémon.pokémonPokédexTemplate.dangerousnessDescriptions.case3
            .replace('{EMOJI}', emoji);
        break;
      case 4:
        returnValue = tr.pokémon.pokémonPokédexTemplate.dangerousnessDescriptions.case4
            .replace('{EMOJI}', emoji);
        break;
      case 5:
        returnValue = tr.pokémon.pokémonPokédexTemplate.dangerousnessDescriptions.case5
            .replace('{EMOJI}', emoji);
        break;
      case 6:
        returnValue = tr.pokémon.pokémonPokédexTemplate.dangerousnessDescriptions.case6
            .replace('{EMOJI}', emoji);
        break;
      case 7:
        returnValue = tr.pokémon.pokémonPokédexTemplate.dangerousnessDescriptions.case7
            .replace('{EMOJI}', emoji);
        break;
      case 8:
        returnValue = tr.pokémon.pokémonPokédexTemplate.dangerousnessDescriptions.case8
            .replace('{EMOJI}', emoji);
        break;
      default:
        returnValue = tr.pokémon.pokémonPokédexTemplate.dangerousnessDescriptions.case0
            .replace('{EMOJI}', emoji);
        break;
    }

    return returnValue;
  },
  generateFieldSexRepartition: function(pokémonData, client) {
    const emoji = emojiService.getSexDiagramEmojiFromSexInt(pokémonData.genderRate, client);

    let returnValue = '';
    switch (pokémonData.genderRate) {
      case -2:
        returnValue = emoji + tr.pokémon.pokémonPokédexTemplate.sexDescriptions.caseM2;
        break;
      case -1:
        returnValue = emoji + tr.pokémon.pokémonPokédexTemplate.sexDescriptions.caseM1;
        break;
      case 0:
        returnValue = emoji + tr.pokémon.pokémonPokédexTemplate.sexDescriptions.case0;
        break;
      case 1:
        returnValue = emoji + tr.pokémon.pokémonPokédexTemplate.sexDescriptions.case1;
        break;
      case 2:
        returnValue = emoji + tr.pokémon.pokémonPokédexTemplate.sexDescriptions.case2;
        break;
      case 4:
        returnValue = emoji + tr.pokémon.pokémonPokédexTemplate.sexDescriptions.case4;
        break;
      case 6:
        returnValue = emoji + tr.pokémon.pokémonPokédexTemplate.sexDescriptions.case6;
        break;
      case 7:
        returnValue = emoji + tr.pokémon.pokémonPokédexTemplate.sexDescriptions.case7;
        break;
      case 8:
        returnValue = emoji + tr.pokémon.pokémonPokédexTemplate.sexDescriptions.case8;
        break;
    }

    return returnValue;
  },
  generateFieldFavoriteFood: function(pokémonData, client) {
    let returnValue = tr.pokémon.pokémonPokédexTemplate.favoriteFoodValue;
    for (let i = 0; i < pokémonData.favoriteFood.length; i++) {
      returnValue = returnValue + emojiService.getPokemonFavoriteFoodEmojiPerFoodName(
          pokémonData.favoriteFood[i],
          client,
      );
    }
    return returnValue;
  },
  generateDisplayWeightFieldValue: function(pokémonData, client) {
    if (pokémonData.giga) {
      return pokémonData.weight + 'kg. \n';
    } else {
      return tr.pokémon.pokémonPokédexTemplate.normalField +
        tr.pokémon.pokémonPokédexTemplate.weightVariations.min +
        emojiService.getHeightWeightEmojiPerName('XS', client) +
        ' ' + (pokémonData.weight * process.env.MINI_NORMAL_HEIGHT_WEIGHT_RATIO).toFixed(1) + 'kg. \n' +
        tr.pokémon.pokémonPokédexTemplate.weightVariations.average +
        emojiService.getHeightWeightEmojiPerName('N', client) +
        ' ' + pokémonData.weight + 'kg. \n' +
        tr.pokémon.pokémonPokédexTemplate.weightVariations.max +
        emojiService.getHeightWeightEmojiPerName('XL', client) +
        ' ' + (pokémonData.weight * process.env.MAX_NORMAL_HEIGHT_WEIGHT_RATIO).toFixed(1) + 'kg. \n' +

        tr.pokémon.pokémonPokédexTemplate.alphaField +
          emojiService.getCharacteristicEmojiPerName('Alpha', client) + '\n' +
        tr.pokémon.pokémonPokédexTemplate.weightVariations.min +
        emojiService.getHeightWeightEmojiPerName('XS', client) +
        ' ' + ((pokémonData.weight * process.env.ALPHA_POKEMON_WEIGHT_RATIO) *
              process.env.MINI_ALPHA_HEIGHT_WEIGHT_RATIO).toFixed(1) + 'kg. \n' +
        tr.pokémon.pokémonPokédexTemplate.weightVariations.average +
        emojiService.getHeightWeightEmojiPerName('N', client) +
        ' ' + (pokémonData.weight * process.env.ALPHA_POKEMON_WEIGHT_RATIO).toFixed(1) + 'kg. \n' +
        tr.pokémon.pokémonPokédexTemplate.weightVariations.max +
        emojiService.getHeightWeightEmojiPerName('XL', client) +
        ' ' + ((pokémonData.weight * process.env.ALPHA_POKEMON_WEIGHT_RATIO) *
              process.env.MAX_ALPHA_HEIGHT_WEIGHT_RATIO).toFixed(1) + 'kg. \n'+

          tr.pokémon.pokémonPokédexTemplate.titanField +
          emojiService.getTitanEmoji(false, client) + '\n' +
          tr.pokémon.pokémonPokédexTemplate.weightVariations.min +
          emojiService.getHeightWeightEmojiPerName('XS', client) +
          ' ' + ((pokémonData.weight * process.env.TITAN_POKEMON_WEIGHT_RATIO) *
              process.env.MINI_TITAN_HEIGHT_WEIGHT_RATIO).toFixed(1) + 'kg. \n' +
          tr.pokémon.pokémonPokédexTemplate.weightVariations.average +
          emojiService.getHeightWeightEmojiPerName('N', client) +
          ' ' + (pokémonData.weight * process.env.TITAN_POKEMON_WEIGHT_RATIO).toFixed(1) + 'kg. \n' +
          tr.pokémon.pokémonPokédexTemplate.weightVariations.max +
          emojiService.getHeightWeightEmojiPerName('XL', client) +
          ' ' + ((pokémonData.weight * process.env.TITAN_POKEMON_WEIGHT_RATIO) *
              process.env.MAX_TITAN_HEIGHT_WEIGHT_RATIO).toFixed(1) + 'kg. \n';
    }
  },
  generateDisplayHeightFieldValue: function(pokémonData, client) {
    if (pokémonData.giga) {
      return pokémonData.height + 'm +. \n';
    } else {
      return tr.pokémon.pokémonPokédexTemplate.normalField +
        tr.pokémon.pokémonPokédexTemplate.heightVariations.min +
        emojiService.getHeightWeightEmojiPerName('XS', client) +
        ' ' + (pokémonData.height * process.env.MINI_NORMAL_HEIGHT_WEIGHT_RATIO).toFixed(1) + 'm. \n' +
        tr.pokémon.pokémonPokédexTemplate.heightVariations.average +
        emojiService.getHeightWeightEmojiPerName('N', client) +
        ' ' + pokémonData.height + 'm. \n' +
        tr.pokémon.pokémonPokédexTemplate.heightVariations.max +
        emojiService.getHeightWeightEmojiPerName('XL', client) +
        ' ' + (pokémonData.height * process.env.MAX_NORMAL_HEIGHT_WEIGHT_RATIO).toFixed(1) + 'm. \n' +

        tr.pokémon.pokémonPokédexTemplate.alphaField +
          emojiService.getCharacteristicEmojiPerName('Alpha', client) + '\n' +
        tr.pokémon.pokémonPokédexTemplate.heightVariations.min +
        emojiService.getHeightWeightEmojiPerName('XS', client) +
        ' ' + ((pokémonData.height * process.env.ALPHA_POKEMON_HEIGHT_RATIO) *
              process.env.MINI_ALPHA_HEIGHT_WEIGHT_RATIO).toFixed(1) + 'm. \n' +
        tr.pokémon.pokémonPokédexTemplate.heightVariations.average +
        emojiService.getHeightWeightEmojiPerName('N', client) +
        ' ' + (pokémonData.height * process.env.ALPHA_POKEMON_HEIGHT_RATIO).toFixed(1) + 'm. \n' +
        tr.pokémon.pokémonPokédexTemplate.heightVariations.max +
        emojiService.getHeightWeightEmojiPerName('XL', client) +
        ' ' + ((pokémonData.height * process.env.ALPHA_POKEMON_HEIGHT_RATIO) *
              process.env.MAX_ALPHA_HEIGHT_WEIGHT_RATIO).toFixed(1) + 'm. \n' +

      tr.pokémon.pokémonPokédexTemplate.titanField +
      emojiService.getTitanEmoji(false, client) + '\n' +
      tr.pokémon.pokémonPokédexTemplate.heightVariations.min +
      emojiService.getHeightWeightEmojiPerName('XS', client) +
      ' ' + ((pokémonData.height * process.env.TITAN_POKEMON_HEIGHT_RATIO) *
              process.env.MINI_TITAN_HEIGHT_WEIGHT_RATIO).toFixed(1) + 'm. \n' +
      tr.pokémon.pokémonPokédexTemplate.heightVariations.average +
      emojiService.getHeightWeightEmojiPerName('N', client) +
      ' ' + (pokémonData.height * process.env.TITAN_POKEMON_HEIGHT_RATIO).toFixed(1) + 'm. \n' +
      tr.pokémon.pokémonPokédexTemplate.heightVariations.max +
      emojiService.getHeightWeightEmojiPerName('XL', client) +
      ' ' + ((pokémonData.height * process.env.TITAN_POKEMON_HEIGHT_RATIO) *
              process.env.MAX_TITAN_HEIGHT_WEIGHT_RATIO).toFixed(1) + 'm. \n';
    }
  },
  generateLivingBiomePokémonPokédexData: function(pokémonData) {
    if (pokémonData.biome !== undefined) {
      const commonArray = pokémonControllerService.getArrayOfPokémonBiomePerRarity(
          pokémonData.biome,
          'commun',
      );
      const uncoArray = pokémonControllerService.getArrayOfPokémonBiomePerRarity(
          pokémonData.biome,
          'unco',
      );
      const rareArray = pokémonControllerService.getArrayOfPokémonBiomePerRarity(
          pokémonData.biome,
          'rare',
      );
      const eventArray = pokémonControllerService.getArrayOfPokémonBiomePerRarity(
          pokémonData.biome,
          'event',
      );

      let output = pokémonData.name + tr.pokémon.canBeFindIn;
      output = commonArray.length > 0 ?
        output + this.generateDisplayPokémonBiomeRarity(commonArray, 'commun') :
        output;
      output = uncoArray.length > 0 ?
        output + this.generateDisplayPokémonBiomeRarity(uncoArray, 'unco') :
        output;
      output = rareArray.length > 0 ?
        output + this.generateDisplayPokémonBiomeRarity(rareArray, 'rare') :
        output;
      output = output + '\n';
      output = eventArray.length > 0 ?
        output + this.generateDisplayPokémonBiomeRarity(eventArray, 'event') :
        output;

      return output;
    }
  },
  generateTypesPokémonPokédexData: function(pokémonData, client) {
    if (pokémonData.type.special !== undefined) {
      return pokémonData.name +
          tr.pokémon.haveTypes +
          pokémonData.type.first +
          ' ' + emojiService.getTypeIconEmojiPerType(pokémonData.type.first, client) +
          ' et ' +
          pokémonData.type.second +
          ' ' + emojiService.getTypeIconEmojiPerType(pokémonData.type.second, client) + '.' +
          '\n' +
          tr.pokémon.someHaveTypes +
          pokémonData.type.special +
          ' ' + emojiService.getTypeIconEmojiPerType(pokémonData.type.special, client) + '.';
    }
    if (pokémonData.type.second === undefined) {
      return pokémonData.name +
          tr.pokémon.haveType +
          pokémonData.type.first +
          ' ' + emojiService.getTypeIconEmojiPerType(pokémonData.type.first, client) + '.';
    }
    if (pokémonData.type.second !== undefined) {
      return pokémonData.name +
          tr.pokémon.haveTypes +
          pokémonData.type.first +
          ' ' + emojiService.getTypeIconEmojiPerType(pokémonData.type.first, client) +
          ' et ' +
          pokémonData.type.second +
          ' ' + emojiService.getTypeIconEmojiPerType(pokémonData.type.second, client) + '.';
    }
  },
  generateFieldMovesPokémonPokédexData: function(pokémonData, client) {
    let returnValue = '';
    for (let i = 0; i < pokémonData.fieldMove.length; i++) {
      returnValue = returnValue +
          '- ' +
          pokémonData.fieldMove[i].name + ' ' +
          (pokémonData.fieldMove[i].strength === undefined ? '' : pokémonData.fieldMove[i].strength) +
          ' ' + this.generateFieldMoveEmojiFromNameAndStrength(
          pokémonData.fieldMove[i].name,
          pokémonData.fieldMove[i].strength,
          client,
      ) +
          '\n';
    }
    return returnValue;
  },
  generateFieldMoveEmojiFromNameAndStrength: function(name, strength, client) {
    name = name.replace('-', '');
    name = name.replace(' ', '');
    name = name.replace('d\'', '');
    return emojiService.getFieldMoveEmojiPerNameAndStrenght(name, strength, client);
  },
  generateDisplayPokémonBiomeRarity: function(pokémonBiomeRarityData, rarity) {
    let displayPokémonBiome;

    if (pokémonBiomeRarityData.length > 0) {
      if (rarity !== 'event') {
        displayPokémonBiome = tr.pokémon.inBiome;

        switch (rarity) {
          case 'commun':
            displayPokémonBiome = displayPokémonBiome + tr.pokémon.biomeRarity.co;
            break;
          case 'unco':
            displayPokémonBiome = displayPokémonBiome + tr.pokémon.biomeRarity.unco;
            break;
          case 'rare':
            displayPokémonBiome = displayPokémonBiome + tr.pokémon.biomeRarity.rare;
            break;
        }

        for (let i = 0; i < pokémonBiomeRarityData.length; i++) {
          if (i === pokémonBiomeRarityData.length - 1) {
            displayPokémonBiome = displayPokémonBiome + '**' +
                biomeData.getBiomeNameFromID(pokémonBiomeRarityData[i].id) + '**.';
          } else {
            displayPokémonBiome = displayPokémonBiome + '**' +
                biomeData.getBiomeNameFromID(pokémonBiomeRarityData[i].id) + '**, ';
          }
        }
        displayPokémonBiome = displayPokémonBiome + '\n';
      }
      if (rarity === 'event') {
        displayPokémonBiome = tr.pokémon.biomeRarity.event;
        for (let i = 0; i < pokémonBiomeRarityData.length; i++) {
          displayPokémonBiome = displayPokémonBiome +
              '- ' + '**' + biomeData.getBiomeNameFromID(pokémonBiomeRarityData[i].id) + '** : ' +
              pokémonBiomeRarityData[i].why + '\n';
        }
      }
    }

    return displayPokémonBiome;
  },
  pokémonGeneratorHandler: function(callInput, rarity) {
    let returnValue = '';
    rarity = rarity !== undefined ? rarity : 'commun';
    if (callInput !== null) callInput = callInput.charAt(0).toUpperCase() + callInput.slice(1).toLowerCase();
    if (callInput.includes('d\'arguments')) {
      returnValue = callInput;
    }

    const isEmptyPokémonBiome = pokémonData.getAllPokémonFromBiome(callInput);

    if (isEmptyPokémonBiome['pokémon'][0] !== undefined) {
      if (callInput.includes('d\'arguments')) {
        returnValue = callInput;
      } else {
        returnValue = pokémonControllerService.generatePokémon(callInput, rarity);
      }
    } else {
      returnValue = 'Biome invalide ou qui n\'existe pas.';
    }

    return returnValue;
  },
  generateDisplayPokémon: function(requestBiome, requestRarity) {
    let displayPokémonNames = '';
    const data = pokémonData.getAllPokémonFromBiomeAndRarity(requestBiome, requestRarity);

    for (let i = 0; i < data['pokémon'].length; i++) {
      if (i === data['pokémon'].length - 1) {
        displayPokémonNames = displayPokémonNames + '**' + data['pokémon'][i].name + '**.';
      } else {
        displayPokémonNames = displayPokémonNames + '**' + data['pokémon'][i].name + '**, ';
      }
    }

    return displayPokémonNames;
  },
  isPokémonChroma: function(shine) {
    let shiny = process.env.SHINY_CHANCE;
    switch (shine) {
      case 'OUI':
        shiny = shiny/3;
        break;

      case 'CONCOURS':
        shiny = shiny/18;
        break;

      case 'GARANTI':
        shiny = 0;
        break;
    }
    return Math.floor(Math.random() * shiny) === 0;
  },
  isPokémonTitan: function(titanCharm) {
    let titan = process.env.TITAN_CHANCE;
    switch (titanCharm) {
      case 'OUI':
        titan = titan/3;
        break;

      case 'GARANTI':
        titan = 0;
        break;
    }

    return Math.floor(Math.random()*titan) === 0;
  },
  isPokémonAlpha: function(alphaCharm) {
    let alpha = process.env.ALPHA_CHANCE;
    switch (alphaCharm) {
      case 'OUI':
        alpha = alpha/10;
        break;

      case 'GARANTI':
        alpha = 0;
    }
    return Math.floor(Math.random() * alpha) === 0;
  },
  isPokémonAntique: function(antique, modifier) {
    let yesItIs = false;

    if (antique !== undefined) {
      modifier = modifier == null ? 'non' : modifier.toLowerCase();
      let weightModifier = 1;
      if (modifier == 'oui') {
        weightModifier = 3;
      } else if (modifier == 'garanti') {
        weightModifier = 255;
      }
      yesItIs = Math.floor(Math.random()*100) <= antique * weightModifier;
    }
    return yesItIs;
  },
  doesPokémonHasTera: function(teraModifier) {
    let tera = process.env.TERA_CHANCE;
    switch (teraModifier) {
      case 'OUI':
        tera = tera/3;
        break;

      case 'GARANTI':
      case 'BOSS':
        tera = 0;
        break;
    }
    const randNumber = Math.floor(Math.random() * tera);

    return randNumber === 0 ? true : false;
  },
  isTeraPokémonABoss: function(teraCharme) {
    const boss = teraCharme.includes('BOSS') ? 0 : 4;
    const randNumber = Math.floor(Math.random()*boss);
    return randNumber === 0 ? true : false;
  },
  generateFactorPokémon: function(size) {
    let u = 0; let v = 0;
    while (u === 0) u = Math.random();
    while (v === 0) v = Math.random();
    let num = Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);

    switch (size) {
      case 'MINI':
        num = 0;
        break;

      case 'XS':
        num = num / 10.0 + 0.1;
        num < 0 ? num = 0 : num;
        break;

      case 'S':
        num = num / 10.0 + 0.25;
        break;

      case 'L':
        num = num / 10.0 + 0.75;
        break;

      case 'XL':
        num = num / 10.0 + 0.9;
        num > 1 ? num = 1 : num;
        break;

      case 'MAXI':
        num = 1;
        break;

      default:
        num = num / 10.0 + 0.5;
        break;
    }
    num = Math.pow(num, 1);

    return num;
  },
  generateHeightPokémon: function(height, isAlpha, isTitan, num) {
    if (isAlpha === true) {
      height = parseFloat(height) * process.env.ALPHA_POKEMON_HEIGHT_RATIO;
    } else if (isTitan == true) {
      height = parseFloat(height) * process.env.TITAN_POKEMON_HEIGHT_RATIO;
    } else {
      height = parseFloat(height);
    }

    let min = 0;
    let max = 0;
    if (isTitan === true) {
      min = height * process.env.MINI_TITAN_HEIGHT_WEIGHT_RATIO;
      max = height * process.env.MAX_TITAN_HEIGHT_WEIGHT_RATIO;
    } else if (isAlpha === true) {
      min = height * process.env.MINI_ALPHA_HEIGHT_WEIGHT_RATIO;
      max = height * process.env.MAX_ALPHA_HEIGHT_WEIGHT_RATIO;
    } else {
      min = height * process.env.MINI_NORMAL_HEIGHT_WEIGHT_RATIO;
      max = height * process.env.MAX_NORMAL_HEIGHT_WEIGHT_RATIO;
    }

    num *= max - min;
    num += min;

    if (isAlpha === true) {
      if (num < height * process.env.MINI_ALPHA_HEIGHT_WEIGHT_RATIO) {
        return height * process.env.MINI_ALPHA_HEIGHT_WEIGHT_RATIO;
      }
      if (num > height * process.env.MAX_ALPHA_HEIGHT_WEIGHT_RATIO) {
        return height * process.env.MAX_ALPHA_HEIGHT_WEIGHT_RATIO;
      }
    } else if (isTitan == true) {
      if (num < height * process.env.MINI_TITAN_HEIGHT_WEIGHT_RATIO) {
        return height * process.env.MINI_TITAN_HEIGHT_WEIGHT_RATIO;
      }
      if (num > height * process.env.MAX_TITAN_HEIGHT_WEIGHT_RATIO) {
        return height * process.env.MAX_TITAN_HEIGHT_WEIGHT_RATIO;
      }
    } else {
      if (num < height * process.env.MINI_NORMAL_HEIGHT_WEIGHT_RATIO) {
        return height * process.env.MINI_NORMAL_HEIGHT_WEIGHT_RATIO;
      }
      if (num > height * process.env.MAX_NORMAL_HEIGHT_WEIGHT_RATIO) {
        return height * process.env.MAX_NORMAL_HEIGHT_WEIGHT_RATIO;
      }
    }

    return num;
  },
  generateWeightPokémon: function(obtainHeight, normalHeight, normalWeight, isAlpha, isTitan, num) {
    obtainHeight = parseFloat(obtainHeight);

    if (isAlpha) {
      normalHeight = parseFloat(normalHeight) * process.env.ALPHA_POKEMON_HEIGHT_RATIO;
      normalWeight = parseFloat(normalWeight) * process.env.ALPHA_POKEMON_WEIGHT_RATIO;
    } else if (isTitan) {
      normalHeight = parseFloat(normalHeight) * process.env.TITAN_POKEMON_HEIGHT_RATIO;
      normalWeight = parseFloat(normalWeight) * process.env.TITAN_POKEMON_WEIGHT_RATIO;
    } else {
      normalHeight = parseFloat(normalHeight);
      normalWeight = parseFloat(normalWeight);
    }

    let coeffWeight = (obtainHeight * 100) / normalHeight;
    coeffWeight = (coeffWeight.toFixed(0)) * 0.01;
    const adapaptiveWeight = normalWeight * coeffWeight;

    const min = adapaptiveWeight - (adapaptiveWeight * 0.4);
    const max = adapaptiveWeight + (adapaptiveWeight * 0.4);

    num *= max - min;
    num += min;
    return num;
  },
  generateHeightWeightEmoji: function(num, client) {
    let size = 'N';
    if (num >= 0.9) size = 'XL';
    else if (num < 0.9 && num >= 0.7) size = 'L';
    else if (num <= 0.3 && num > 0.1) size = 'S';
    else if (num <= 0.1) size = 'XS';

    return {emoji: emojiService.getHeightWeightEmojiPerName(size, client), name: size};
  },
  // If isHeight is false, it's for weight
  viewHeightWeight: function(isHeight, multiplicator, specie) {
    let returnValue = 0;
    const pokedex = pokémonData.getAllPokemon();
    for (let i = 0; i < pokedex.pokédex.length; i++) {
      if (pokedex.pokédex[i].name == specie) {
        if (isHeight) {
          returnValue = pokedex.pokédex[i].height * multiplicator;
        } else {
          returnValue = pokedex.pokédex[i].weight * multiplicator;
        }
      }
    }
    return returnValue;
  },
  getPokeball: function(pokeballID, client) {
    const allBalls = pokémonData.getAllBalls();
    let founded = false;
    let actualBall = '';
    let i = 0;
    while (i < allBalls.balls.length && founded == false) {
      if (allBalls.balls[i].id == pokeballID || allBalls.balls[i].emoji == 'Strange') {
        actualBall = allBalls.balls[i];
        if (allBalls.balls[i].id == pokeballID) founded = true;
      }
      i++;
    }

    const emoji = actualBall.special ? emojiService.getRPStatusEmoji(actualBall.emoji, client) :
    emojiService.getBallEmoji(actualBall.emoji, client);

    return emoji + ' ' + actualBall.name;
  },

  geStatusOrBall: function(pokeballID) {
    const allBalls = pokémonData.getAllBalls();

    let founded = false;
    let actualBall = '';
    let i = 0;
    while (i < allBalls.balls.length && founded == false) {
      if (allBalls.balls[i].id == pokeballID || allBalls.balls[i].emoji == 'Strange') {
        actualBall = allBalls.balls[i];
        if (allBalls.balls[i].id == pokeballID) founded = true;
      }
      i++;
    }

    return actualBall.special;
  },
  getAllAbilities: function(abilities) {
    const allAb = pokémonData.getAllAbilities();
    let returnValue = '';
    for (let i = 0; i < abilities.length; i++) {
      for (let j = 0; j < allAb.abilities.length; j++) {
        if (abilities[i].id === allAb.abilities[j].id) {
          returnValue = returnValue + ('**' + allAb.abilities[j].name + '**');

          if (abilities[i].forRed && !abilities[i].forBlue && !abilities[i].forWhite) {
            returnValue = returnValue + tr.pokémon.pokémonPokédexTemplate.talents.basculinR;
          }
          if (!abilities[i].forRed && abilities[i].forBlue && !abilities[i].forWhite) {
            returnValue = returnValue + tr.pokémon.pokémonPokédexTemplate.talents.basculinB;
          }
          if (!abilities[i].forRed && !abilities[i].forBlue && abilities[i].forWhite) {
            returnValue = returnValue + tr.pokémon.pokémonPokédexTemplate.talents.basculinW;
          }
          if (abilities[i].forMale && !abilities[i].forFemale) {
            returnValue = returnValue + tr.pokémon.pokémonPokédexTemplate.talents.maleOnly;
          }
          if (!abilities[i].forMale && abilities[i].forFemale) {
            returnValue = returnValue + tr.pokémon.pokémonPokédexTemplate.talents.femaleOnly;
          }
          if (abilities[i].forAmped && !abilities[i].forLowKey) {
            returnValue = returnValue + tr.pokémon.pokémonPokédexTemplate.talents.toxtricityA;
          }
          if (!abilities[i].forAmped && abilities[i].forLowKey) {
            returnValue = returnValue + tr.pokémon.pokémonPokédexTemplate.talents.toxtricityL;
          }
          if (abilities[i].isHidden) {
            returnValue = returnValue + tr.pokémon.pokémonPokédexTemplate.talents.hidden;
          }
          returnValue = returnValue + ' : ' + allAb.abilities[j].desc + '\n\n';
        }
      }
    }
    return returnValue;
  },
  getPlayerPokémonAbility: function(id) {
    const allAb = pokémonData.getAllAbilities();
    let returnValue = '';

    for (let i = 0; i < allAb.abilities.length; i++) {
      if (id === allAb.abilities[i].id) {
        returnValue = '**' + allAb.abilities[i].name + '** : ' + allAb.abilities[i].desc;
        break;
      }
    }
    return returnValue;
  },
  getTrainersAllPokémon: function(msg) {
    const idMessage = msg.author.id.toString();
    const allTrainersPokémon = pokémonData.getAllPlayersPokémon();
    let returnValue = '';

    if (allTrainersPokémon.playersPokémon.length !== 0) {
      for (let i = 0; i < allTrainersPokémon.playersPokémon.length; i++) {
        if (idMessage === allTrainersPokémon.playersPokémon[i].id.toString()) {
          if (allTrainersPokémon.playersPokémon[i].playerPokémon.length !== 0) {
            for (let j = 0; j < allTrainersPokémon.playersPokémon[i].playerPokémon.length; j++) {
              returnValue = returnValue + allTrainersPokémon.playersPokémon[i].playerPokémon[j].name;

              if (allTrainersPokémon.playersPokémon[i].playerPokémon[j].surname !== '') {
                returnValue = returnValue + tr.playersPokémon.allTrainerPokémon.surname.replace(
                    '{SURNAME}',
                    allTrainersPokémon.playersPokémon[i].playerPokémon[j].surname,
                );
              }

              returnValue = returnValue + tr.playersPokémon.allTrainerPokémon.id.replace(
                  '{IDPOKE}',
                  allTrainersPokémon.playersPokémon[i].playerPokémon[j].genId,
              );
            }
          } else {
            returnValue = tr.playersPokémon.allTrainerPokémon.none;
          }
        }
      }
    } else {
      returnValue = 0;
    }
    return returnValue;
  },
  getPlayerPokémonMoves: function(moves) {
    let movesList = '';
    movesList = this.displayPlayerPokémonMoves(movesList, moves.move1);
    movesList = this.displayPlayerPokémonMoves(movesList, moves.move2);
    movesList = this.displayPlayerPokémonMoves(movesList, moves.move3);
    movesList = this.displayPlayerPokémonMoves(movesList, moves.move4);

    if (movesList.length === 0) {
      movesList += tr.playersPokémon.pokémonSheetTemplate.noMoves;
    }

    return movesList;
  },
  displayPlayerPokémonMoves: function(movesList, move) {
    if (move.length > 0) {
      movesList += '- ' + move + '\n';
    }
    return movesList;
  },
  getPlayerPokémonTraits: function(traits, notes) {
    let returnTraits = '';
    if (traits.length > 0 || (notes != undefined && notes.length > 0)) {
      if (traits.length > 0) {
        for (let i = 0; i < traits.length; i++) {
          returnTraits = returnTraits + '- __' + traits[i].txt + '__\n';
        }
      }
      if (notes != undefined && notes.length > 0) {
        for (let j = 0; j < notes.length; j++) {
          if (notes[j].txt != '') {
            returnTraits = returnTraits + '- ' + notes[j].txt + '\n';
          }
        }
      }
    } else {
      returnTraits = tr.playersPokémon.pokémonSheetTemplate.noNotes;
    }

    return returnTraits;
  },
  getPlayerPokémonFriendship: function(friendshipLevel) {
    let returnValue = '';
    for (let i = 0; i < 15; i++) {
      if (i < friendshipLevel) returnValue = returnValue + ':green_square: ';
      else returnValue = returnValue + ' :white_small_square: ';
    }
    return returnValue;
  },
  getPlayerPokémonNames: function(name, surname, alpha, shiny, sex, titan, antique, client) {
    let returnValue = '';
    const vowelRegex = '^[aieouyéAIEOUYÉ].*';
    const translateSurname = tr.playersPokémon.pokémonSheetTemplate.surname;
    const matched = name.match(vowelRegex);

    if (surname !== '') {
      if (matched) {
        returnValue = translateSurname.vowel.replace('{SURNAME}', surname).replace('{POKÉMON}', name);
      } else if (sex === 'Femelle') {
        returnValue = translateSurname.female.replace('{SURNAME}', surname).replace('{POKÉMON}', name);
      } else {
        returnValue = translateSurname.male.replace('{SURNAME}', surname).replace('{POKÉMON}', name);
      }
    } else returnValue = name;

    if (shiny) returnValue = returnValue + ' ' + emojiService.getCharacteristicEmojiPerName('Shiny', client);
    if (alpha) returnValue = returnValue + ' ' + emojiService.getCharacteristicEmojiPerName('Alpha', client);
    if (titan) returnValue = returnValue + ' ' + emojiService.getCharacteristicEmojiPerName('Titan', client);
    if (antique) returnValue = returnValue + ' ' + emojiService.getCharacteristicEmojiPerName('Antique', client);

    return returnValue;
  },
  getTrainerPokémonFood: function(color) {
    switch (color) {
      case 'rainbow':
        return tr.playersPokémon.pokémonSheetTemplate.pokebloc.color[0];
      case 'red':
        return tr.playersPokémon.pokémonSheetTemplate.pokebloc.color[1];
      case 'blue':
        return tr.playersPokémon.pokémonSheetTemplate.pokebloc.color[2];
      case 'pink':
        return tr.playersPokémon.pokémonSheetTemplate.pokebloc.color[3];
      case 'green':
        return tr.playersPokémon.pokémonSheetTemplate.pokebloc.color[4];
      case 'yellow':
        return tr.playersPokémon.pokémonSheetTemplate.pokebloc.color[5];
      case 'indigo':
        return tr.playersPokémon.pokémonSheetTemplate.pokebloc.color[6];
      case 'orange':
        return tr.playersPokémon.pokémonSheetTemplate.pokebloc.color[7];
      case 'purple':
        return tr.playersPokémon.pokémonSheetTemplate.pokebloc.color[8];
    }
  },
  getPlayerPokémonLastFoundItems: function(pokémonFoundItem, client) {
    let lastItemsLine = '';
    let lastItemsPokémon = pokémonFoundItem.slice(-15);
    lastItemsPokémon = lodash.reverse(lastItemsPokémon);

    for (let i = 0; i < lastItemsPokémon.length; i++) {
      const itemData = itemsData.getItemFromId(lastItemsPokémon[i]);

      lastItemsLine += emojiService.getFoundItemEmojiPerName(
          itemData.foundableData.buddyFoundableEmoji,
          client,
      ) +
          ' ';
    }

    return lastItemsLine;
  },
  getNumberSpecialFoundItems: function(pokémonFoundItem) {
    let nbBouquetFound = 0;
    let nbSilverLeafFound = 0;
    let nbGoldLeafFound = 0;

    for (let i = 0; i < pokémonFoundItem.length; i++) {
      const itemData = itemsData.getItemFromId(pokémonFoundItem[i]);

      if (itemData.name === 'Feuille Dorée') {
        nbGoldLeafFound++;
      } else if (itemData.name === 'Feuille Argentée') {
        nbSilverLeafFound++;
      } else if (itemData.name === 'Petit Bouquet') {
        nbBouquetFound++;
      }
    }

    return {
      nbBouquetFound: nbBouquetFound,
      nbSilverLeafFound: nbSilverLeafFound,
      nbGoldLeafFound: nbGoldLeafFound,
    };
  },
};
