module.exports = {
  formatTrainerCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 1:
        formatedInput = callInput.split('--Type=')[1];
        formatedInput = formatedInput.split('--Pokémon=')[0];
        break;
      case 2:
        formatedInput = callInput.split('--Pokémon=')[1];
        formatedInput = formatedInput.split('--Classe=')[0];
        break;
      case 3:
        formatedInput = callInput.split('--Classe=')[1];
        formatedInput = formatedInput.split('--Avancée')[0];
        break;

      case 4:
        formatedInput = callInput.split('--Avancée=')[1];
        formatedInput = formatedInput.split('--Catégorie=')[0];
        break;
      case 5:
        formatedInput = callInput.split('--Catégorie=')[1];
        break;
      case 6:
        formatedInput = callInput.split('--Biome=')[1];
        formatedInput = formatedInput.split('--Pokémon=')[0];
        break;
      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/(\r\n|\n|\r)/gm, '') : formatedInput;
    }
    return null;
  },
};
