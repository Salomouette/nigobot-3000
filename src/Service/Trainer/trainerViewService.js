const {faker} = require('@faker-js/faker');

const pokémonData = require('../../Data/pokémonData.js');
module.exports = {
  getTrainerGender: function(male, female, tr) {
    let gender = '';
    const neutral = Math.floor(Math.random() * 100);

    if (male && !female) {
      if (neutral >= 15) {
        gender = tr.male;
      } else {
        gender = tr.neutral;
      }
    }

    if (!male && female) {
      if (neutral >= 15) {
        gender = tr.female;
      } else {
        gender = tr.neutral;
      }
    }

    if (male && female) {
      if (neutral <= 45) {
        gender = tr.male;
      } else if (neutral >= 55) {
        gender = tr.female;
      } else {
        gender = tr.neutral;
      }
    }

    return gender;
  },
  getTrainerAppearance: function(gender, maleApp, femaleApp, tr) {
    let path = '';
    if (gender === tr.male) {
      path = maleApp;
    }

    if (gender === tr.female) {
      path = femaleApp;
    }

    if (gender === tr.neutral) {
      const neutral = Math.floor(Math.random());
      if (neutral === 0) {
        path = maleApp;
      } else if (neutral === 1) {
        path = femaleApp;
      }
    }
    return path;
  },
  giveTrainerName: function(gender, tr) {
    if (gender === tr.male) gender = 'male';
    if (gender === tr.female) gender = 'female';
    if (gender === tr.neutral) gender = '';

    const name = faker.name.firstName(gender);
    return name;
  },
  setClass: function(classe, gender, trainer, tr) {
    const photoEquiv = [
      'photo',
      'photographe',
      'p',
      'photographer',
      'Photo',
      'Photographe',
      'P',
      'Photographer',
    ];
    const coordEquiv = [
      'c',
      'coordinateur',
      'coordinatrice',
      'Coordinateur',
      'Coordinatrice',
      'Coord',
      'coord',
      'Coordinator',
      'coordinator',
    ];

    if (photoEquiv.includes(classe)) {
      classe = tr.class.photographer;
    } else if (coordEquiv.includes(classe)) {
      if (gender === tr.gender.male) classe = tr.class.coordinator.male;
      if (gender === tr.gender.female) classe = tr.class.coordinator.female;
      if (gender === tr.gender.neutral) classe = tr.class.coordinator.neutral;
    } else {
      if (gender === tr.gender.male) classe = tr.class.trainer.male;
      if (gender === tr.gender.female) classe = tr.class.trainer.female;
      if (gender === tr.gender.neutral) classe = tr.class.trainer.neutral;
    }

    if (trainer === tr.class.ranger) {
      classe = tr.class.ranger;
    }

    return classe;
  },
  setCollectibles: function(classe, tr) {
    let typeCollectible = '';
    if (classe === tr.class.photographer) {
      typeCollectible = tr.collectible.photographer;
    } else if (
      classe === tr.class.coordinator.male ||
        classe === tr.class.coordinator.female ||
        classe === tr.class.coordinator.neutral
    ) {
      typeCollectible = tr.collectible.coordinator;
    } else {
      typeCollectible = tr.collectible.trainer;
    }

    if (classe === tr.class.ranger) {
      typeCollectible = tr.collectible.ranger;
    }
    return typeCollectible;
  },
  setTitle: function(randomName, trainer, gender, tr) {
    const vowelRegex = '^[aieouyéAIEOUYÉ].*';
    const matched = trainer.match(vowelRegex);
    let setUpGender = '';
    if (matched) {
      setUpGender = randomName + ' ' + tr.pronoun.vowel + ' ' + trainer;
    } else {
      if (gender === 'Femme') {
        setUpGender = randomName + ' ' + tr.pronoun.female + ' ' + trainer;
      }
      if (gender === 'Homme') {
        setUpGender = randomName + ' ' + tr.pronoun.male + ' ' + trainer;
      }
      if (gender === 'Neutre') {
        setUpGender = randomName + ' ' + tr.pronoun.neutral + ' ' + trainer;
      }
    }
    const title = setUpGender + tr.title;
    return title;
  },
  getTrainerMonData: function(trainerType, classe, nbPokémon, badges, categorie) {
    if (nbPokémon > 6) {
      nbPokémon = 6;
    }
    if (nbPokémon > 3 && classe === 'Photographe') {
      nbPokémon = 3;
    }
    if (nbPokémon == null || nbPokémon < 1) {
      nbPokémon = 1;
    }

    // TODO Fix trainer levels
    let minLevel = '';
    let maxLevel = '';
    if (categorie === 'Master') {
      minLevel = badges * 3 + 82;
      maxLevel = badges * 2 + 92;

      minLevel = minLevel >= 100 ? minLevel = 100 : minLevel;
      maxLevel = maxLevel >= 100 ? maxLevel = 100 : maxLevel;
    } else {
      minLevel = badges * 10 + 2;
      maxLevel = Math.floor(badges * 9.7 + 14);
    }
    const pokémon = pokémonData.getAllPokemon();
    const output = [];
    const team = [];
    const niv = [];
    if (trainerType[0].name !== 'Aucun') {
      for (let h = 0; h < trainerType.length; h++) {
        for (let i = 0; i < pokémon.pokédex.length; i++) {
          if (
            (
              pokémon.pokédex[i].type.first === trainerType[h].name ||
                  pokémon.pokédex[i].type.second === trainerType[h].name
            ) &&
              pokémon.pokédex[i].minLevel !== 0
          ) {
            output.push(pokémon.pokédex[i]);
          }
        }
      }
      for (let i = 0; i < nbPokémon; i++) {
        const levelFloor = Math.floor(Math.random() * (maxLevel - minLevel + 1)) + minLevel;
        const randomness = Math.floor(Math.random() * 4) + 1;
        if (randomness === 4) {
          let number = Math.floor(Math.random() * (pokémon.pokédex.length));
          while (pokémon.pokédex[number].minLevel === 0 || pokémon.pokédex[number].minLevel > minLevel) {
            number = Math.floor(Math.random() * (pokémon.pokédex.length));
          }
          team.push(pokémon.pokédex[number].name);
          niv.push(levelFloor);
        } else {
          let number = Math.floor(Math.random() * (output.length));
          while (output[number].minLevel === 0 || output[number].minLevel > minLevel) {
            number = Math.floor(Math.random() * (output.length));
          }
          team.push(output[number].name);
          niv.push(levelFloor);
        }
      }
    } else {
      for (let i = 0; i < nbPokémon; i++) {
        const levelFloor = Math.floor(Math.random() * (maxLevel - minLevel + 1)) + minLevel;
        let number = Math.floor(Math.random() * (pokémon.pokédex.length));
        while (pokémon.pokédex[number].minLevel === 0 || pokémon.pokédex[number].minLevel > minLevel) {
          number = Math.floor(Math.random() * (pokémon.pokédex.length));
        }
        team.push(pokémon.pokédex[number].name);
        niv.push(levelFloor);
      }
    }

    return {team, niv};
  },
};
