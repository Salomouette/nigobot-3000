const pastTrainersData = require('../../Data/pastTrainersData.js');

module.exports = {
  getPastTrainerById: function(id) {
    const data = pastTrainersData.getPastTrainerData();
    let output = '';

    for (let i = 0; i < data.pastTrainers.length; i++) {
      if (data.pastTrainers[i].id === id) {
        output = data.pastTrainers[i];
      }
    }
    return output;
  },
};
