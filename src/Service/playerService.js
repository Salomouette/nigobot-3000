const botService = require('../Service/botService');

const playerData = require('../Data/playerData');
const frenchTranslation = require('../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  addSlurPlayer: function(msg) {
    const players = playerData.getAllPlayers();
    for (let i = 0; i < players.players.length; i++) {
      if (players.players[i] != null) {
        // Check if user added
        if (players.players[i].id === msg.author.id) {
          if (!players.players[i].slur) {
            players.players[i].slur = 1;
          } else {
            players.players[i].slur = parseInt(players.players[i].slur) + 1;
          }
        }
      }
    }
    botService.jsonWriteFile(players, 'players');
  },

  checkIfUserAlreadyAdded: function(id) {
    const players = playerData.getAllPlayers();
    for (let i = 0; i < players.players.length; i++) {
      if (players.players[i] != null) {
        if (players.players[i].id === id) {
          return true;
        }
      }
    }
    return false;
  },

  checkIfAdminUser: function(id) {
    return id === process.env.ADMIN_ID;
  },

  addAllPlayersMoney: function(msg) {
    let returnValue = '';
    let numberAdded = 0;
    let moneyAdded = 0;
    let players = '';

    if (this.checkIfAdminUser(msg.author.id)) {
      switch (msg.content.split(' ').length) {
        case 1:
        case 2:
          return tr.error.invalidOrBadArgument;
        case 3:
          moneyAdded = parseInt(msg.content.split(' ')[2]);
          players = playerData.getAllPlayers();
          for (let i = 0; i < players.players.length; i++) {
            if (players.players[i] != null) {
              if (players.players[i].money) {
                players.players[i].money = players.players[i].money + moneyAdded;
                numberAdded++;
              }
            }
          }
          botService.jsonWriteFile(players, 'players');
          returnValue = {
            text: tr.player.money.moneyModificationStart + numberAdded + tr.player.money.moneyModificationEnd,
            moneyAdded: moneyAdded,
          };
          break;
        default:
          returnValue = {
            text: tr.error.tooManyArgumentsOrBadSyntax,
          };
      }
    } else {
      returnValue = {
        text: tr.error.notAdminUser,
      };
    }

    return returnValue;
  },

  removeAllPlayersMoney: function(msg) {
    let returnValue = '';
    let numberAdded = 0;
    let moneyRemove = 0;
    let players = '';

    if (this.checkIfAdminUser(msg.author.id)) {
      switch (msg.content.split(' ').length) {
        case 1:
        case 2:
          return tr.error.invalidOrBadArgument;
        case 3:
          moneyRemove = parseInt(msg.content.split(' ')[2]);
          players = playerData.getAllPlayers();
          for (let i = 0; i < players.players.length; i++) {
            if (players.players[i] != null) {
              if (players.players[i].money) {
                players.players[i].money = players.players[i].money - moneyRemove;
                numberAdded++;
              }
            }
          }
          botService.jsonWriteFile(players, 'players');
          returnValue = {
            text: tr.player.money.moneyModificationStart + numberAdded + tr.player.money.moneyModificationEnd,
            moneyRemove: moneyRemove,
          };
          break;
        default:
          returnValue = {
            text: tr.error.tooManyArgumentsOrBadSyntax,
          };
      }
    } else {
      returnValue = {
        text: tr.error.notAdminUser,
      };
    }

    return returnValue;
  },

  addPlayerMoney: function(msg) {
    let returnValue = '';

    if (this.checkIfAdminUser(msg.author.id)) {
      let idUser = 0;
      let moneyAdded = 0;
      let players = 0;

      switch (msg.content.split(' ').length) {
        case 1:
        case 2:
        case 3:
          return tr.error.invalidOrBadArgument;
        case 4:
          idUser = msg.content.split(' ')[2];
          moneyAdded = parseInt(msg.content.split(' ')[3]);
          players = playerData.getAllPlayers();

          for (let i = 0; i < players.players.length; i++) {
            // Check if json not null
            if (players.players[i] != null) {
              // Check if user added
              if (players.players[i].id === idUser) {
                // Check if money initialized
                if (players.players[i].money) {
                  const initialMoney = players.players[i].money;
                  players.players[i].money = players.players[i].money + moneyAdded;

                  botService.jsonWriteFile(players, 'players');

                  return returnValue = {
                    text: players.players[i].username +
                        tr.player.money.obtain +
                        moneyAdded +
                        tr.player.money.pokedollarEnd,
                    initialMoney: initialMoney,
                    moneyAdded: moneyAdded,
                    money: players.players[i].money,
                    username: players.players[i].username,
                    idUser: idUser,
                  };
                }
              } else {
                returnValue = {
                  text: tr.error.userMoneyNotInitialize,
                };
              }
            } else {
              returnValue = {
                text: tr.error.userNotInitialize,
              };
            }
          }
          break;
        default:
          returnValue = {
            text: tr.error.tooManyArgumentsOrBadSyntax,
          };
      }
    } else {
      returnValue = {
        text: tr.error.notAdminUser,
      };
    }

    return returnValue;
  },

  removePlayerMoneyAdmin: function(msg) {
    let returnValue = '';

    if (this.checkIfAdminUser(msg.author.id)) {
      let idUser = 0;
      let moneyRemove = 0;
      let players = 0;
      switch (msg.content.split(' ').length) {
        case 1:
        case 2:
        case 3:
          return tr.error.invalidOrBadArgument;
        case 4:
          idUser = msg.content.split(' ')[2];
          moneyRemove = parseInt(msg.content.split(' ')[3]);
          players = playerData.getAllPlayers();

          for (let i = 0; i < players.players.length; i++) {
            // Check if json not null
            if (players.players[i] != null) {
              // Check if user added
              if (players.players[i].id === idUser) {
                // Check if money initialized
                if (players.players[i].money) {
                  const initialMoney = players.players[i].money;
                  players.players[i].money = players.players[i].money - moneyRemove;

                  botService.jsonWriteFile(players, 'players');

                  return returnValue = {
                    text: players.players[i].username +
                        tr.player.money.remove +
                        moneyRemove +
                        tr.player.money.pokedollarEnd,
                    initialMoney: initialMoney,
                    moneyRemove: moneyRemove,
                    money: players.players[i].money,
                    username: players.players[i].username,
                    idUser: idUser,
                  };
                }
              } else {
                returnValue = {
                  text: tr.error.userMoneyNotInitialize,
                };
              }
            } else {
              returnValue = {
                text: tr.error.userNotInitialize,
              };
            }
          }
          break;
        default:
          returnValue = {
            text: tr.error.tooManyArgumentsOrBadSyntax,
          };
      }
    } else {
      returnValue = {
        text: tr.error.notAdminUser,
      };
    }

    return returnValue;
  },

  // PLayers commands
  viewPlayerMoneyPlayer: function(msg) {
    const players = playerData.getAllPlayers();
    let returnValue = '';

    for (let i = 0; i < players.players.length; i++) {
      // Check if json not null
      if (players.players[i] != null) {
        // Check if user added
        if (players.players[i].id === msg.author.id) {
          // Check if money initialized
          if (players.players[i].money) {
            return returnValue = {
              text:
                                tr.player.money.moneyTotal +
                                players.players[i].money +
                                tr.player.money.pokedollarEnd,
            };
          } else {
            return returnValue = {
              text: tr.error.userMoneyNotInitialize,
            };
          }
        } else {
          returnValue = {
            text: tr.error.userNotInitialize,
          };
        }
      }
    }

    return returnValue;
  },

  removePlayerMoneyPlayer: function(msg) {
    let returnValue = '';
    let moneyUsed = 0;
    let players = 0;

    switch (msg.content.split(' ').length) {
      case 1:
      case 2:
        return tr.error.invalidOrBadArgument;
      case 3:
        moneyUsed = parseInt(msg.content.split(' ')[2]);
        players = playerData.getAllPlayers();
        for (let i = 0; i < players.players.length; i++) {
          // Check if json not null
          if (players.players[i] != null) {
            // Check if user added
            if (players.players[i].id === msg.author.id) {
              // Check if money initialized
              if (players.players[i].money) {
                // Check if user have enough money
                if (players.players[i].money >= moneyUsed) {
                  const initialMoney = players.players[i].money;
                  players.players[i].money = players.players[i].money - moneyUsed;

                  botService.jsonWriteFile(players, 'players');

                  return returnValue = {
                    text: tr.player.money.youSpend + moneyUsed + '.',
                    initialMoney: initialMoney,
                    moneyRemoved: moneyUsed,
                    money: players.players[i].money,
                    username: players.players[i].username,
                    idUser: players.players[i].id,
                  };
                } else {
                  returnValue = {
                    text: tr.error.notEnoughMoney,
                  };
                }
              } else {
                returnValue = {
                  text: tr.error.userMoneyNotInitialize,
                };
              }
            } else {
              returnValue = {
                text: tr.error.userNotInitialize,
              };
            }
          }
        }
        break;
      default:
        returnValue = {
          text: tr.error.tooManyArgumentsOrBadSyntax,
        };
    }

    return returnValue;
  },

};
