const emojiData = require('../Data/emojiData');

const botService = require('../Service/botService');

module.exports = {
  getSexDiagramEmojiFromSexInt: function(int, client) {
    int = (int.toString()).replace('-', 'M');
    const existingEmoji = emojiData.getEmojiFromName('NB_Sex_' + int);
    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    }
    return null;
  },
  getDangerousnessEmojiFromDangerousnessInt: function(int, client) {
    int = parseInt(int);
    const existingEmoji = emojiData.getEmojiFromName('NB_Dangerousness_' + int);
    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    }
    return null;
  },
  getCalendarEmojiPerInt: function(int, client) {
    const existingEmoji = emojiData.getEmojiFromName('NB_Calendar_' + int);

    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    } else {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Calendar_0');
      return `${emoji}`;
    }
  },
  getFoundItemEmojiPerName: function(name, client) {
    const existingEmoji = emojiData.getEmojiFromName('NB_BFI_' + name);

    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    }
    return null;
  },
  getCriticalityEmojiPerLevel: function(level, client) {
    const existingEmoji = emojiData.getEmojiFromName('NB_Criticality_Level_' + level);

    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    }
    return null;
  },
  getFieldMoveEmojiPerNameAndStrenght: function(name, strength, client) {
    let emojiName = 'NB_FM_Aucune';

    switch (name) {
      case 'Vol': {
        switch (strength) {
          case 1: {
            emojiName = 'NB_FM_Vol1';
            break;
          }
          case 2: {
            emojiName = 'NB_FM_Vol2';
            break;
          }
        }
        break;
      }
      case 'Téléport': {
        switch (strength) {
          case 1: {
            emojiName = 'NB_FM_Teleport1';
            break;
          }
          case 2: {
            emojiName = 'NB_FM_Teleport2';
            break;
          }
        }
        break;
      }
      case 'Survoltage': {
        switch (strength) {
          case 1: {
            emojiName = 'NB_FM_Survoltage1';
            break;
          }
          case 2: {
            emojiName = 'NB_FM_Survoltage2';
            break;
          }
          case 3: {
            emojiName = 'NB_FM_Survoltage3';
            break;
          }
          case 4: {
            emojiName = 'NB_FM_Survoltage4';
            break;
          }
        }
        break;
      }
      case 'Charge': {
        switch (strength) {
          case 1: {
            emojiName = 'NB_FM_Charge1';
            break;
          }
          case 2: {
            emojiName = 'NB_FM_Charge2';
            break;
          }
          case 3: {
            emojiName = 'NB_FM_Charge3';
            break;
          }
          case 4: {
            emojiName = 'NB_FM_Charge4';
            break;
          }
        }
        break;
      }
      case 'Recharge': {
        switch (strength) {
          case 1: {
            emojiName = 'NB_FM_Recharge1';
            break;
          }
          case 2: {
            emojiName = 'NB_FM_Recharge2';
            break;
          }
          case 3: {
            emojiName = 'NB_FM_Recharge3';
            break;
          }
          case 4: {
            emojiName = 'NB_FM_Recharge4';
            break;
          }
        }
        break;
      }
      case 'JetEau': {
        switch (strength) {
          case 1: {
            emojiName = 'NB_FM_JetDEau1';
            break;
          }
          case 2: {
            emojiName = 'NB_FM_JetDEau2';
            break;
          }
          case 3: {
            emojiName = 'NB_FM_JetDEau3';
            break;
          }
        }
        break;
      }
      case 'Hâte': {
        switch (strength) {
          case 1: {
            emojiName = 'NB_FM_Hate1';
            break;
          }
          case 2: {
            emojiName = 'NB_FM_Hate2';
            break;
          }
        }
        break;
      }
      case 'AntiBrume': {
        switch (strength) {
          case 1: {
            emojiName = 'NB_FM_AntiBrume1';
            break;
          }
          case 2: {
            emojiName = 'NB_FM_AntiBrume2';
            break;
          }
        }
        break;
      }
      case 'Foreuse': {
        switch (strength) {
          case 1: {
            emojiName = 'NB_FM_Foreuse1';
            break;
          }
          case 2: {
            emojiName = 'NB_FM_Foreuse2';
            break;
          }
        }
        break;
      }
      case 'ForcePsy': {
        switch (strength) {
          case 1: {
            emojiName = 'NB_FM_ForcePsy1';
            break;
          }
          case 2: {
            emojiName = 'NB_FM_ForcePsy2';
            break;
          }
          case 3: {
            emojiName = 'NB_FM_ForcePsy3';
            break;
          }
        }
        break;
      }
      case 'Flash': {
        switch (strength) {
          case 1: {
            emojiName = 'NB_FM_Flash1';
            break;
          }
          case 2: {
            emojiName = 'NB_FM_Flash2';
            break;
          }
        }
        break;
      }
      case 'Coupe': {
        switch (strength) {
          case 1: {
            emojiName = 'NB_FM_Coupe1';
            break;
          }
          case 2: {
            emojiName = 'NB_FM_Coupe2';
            break;
          }
          case 3: {
            emojiName = 'NB_FM_Coupe3';
            break;
          }
          case 4: {
            emojiName = 'NB_FM_Coupe4';
            break;
          }
        }
        break;
      }
      case 'Brûlure': {
        switch (strength) {
          case 1: {
            emojiName = 'NB_FM_Brulure1';
            break;
          }
          case 2: {
            emojiName = 'NB_FM_Brulure2';
            break;
          }
          case 3: {
            emojiName = 'NB_FM_Brulure3';
            break;
          }
        }
        break;
      }
      case 'Broyage': {
        switch (strength) {
          case 1: {
            emojiName = 'NB_FM_Broyage1';
            break;
          }
          case 2: {
            emojiName = 'NB_FM_Broyage2';
            break;
          }
          case 3: {
            emojiName = 'NB_FM_Broyage3';
            break;
          }
          case 4: {
            emojiName = 'NB_FM_Broyage4';
            break;
          }
        }
        break;
      }
      case 'AquaRadeau': {
        switch (strength) {
          case 1: {
            emojiName = 'NB_FM_AquaRadeau1';
            break;
          }
          case 2: {
            emojiName = 'NB_FM_AquaRadeau2';
            break;
          }
        }
        break;
      }
      case 'MagmaRadeau': {
        emojiName = 'NB_FM_MagmaRadeau';
        break;
      }
      case 'Vigie': {
        emojiName = 'NB_FM_Vigie';
        break;
      }
      case 'Traverse': {
        emojiName = 'NB_FM_Traverse';
        break;
      }
      case 'SousMarin': {
        emojiName = 'NB_FM_SousMarin';
        break;
      }
      case 'Ensablement': {
        emojiName = 'NB_FM_Ensablement';
        break;
      }
      case 'DansePluie': {
        emojiName = 'NB_FM_DansePluie';
        break;
      }
      case 'Dépollution': {
        emojiName = 'NB_FM_Depollution';
        break;
      }
    }

    const existingEmoji = emojiData.getEmojiFromName(emojiName);

    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    }
    return null;
  },
  getPokémonAssistIconEmojiPerType: function(typeName, client) {
    typeName = botService.removeDiacritics(typeName);
    const existingEmoji = emojiData.getEmojiFromName('NB_PA_' + typeName);

    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    }
    return null;
  },
  getTypeIconEmojiPerType: function(typeName, client) {
    typeName = botService.removeDiacritics(typeName);
    const existingEmoji = emojiData.getEmojiFromName('NB_Type_' + typeName);

    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    }
    return null;
  },
  getTeraIconEmojiPerType: function(typeName, client) {
    typeName = botService.removeDiacritics(typeName);
    const existingEmoji = emojiData.getEmojiFromName('NB_Tera_' + typeName);

    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    }
    return null;
  },
  getHeightWeightEmojiPerName: function(hwType, client) {
    const existingEmoji = emojiData.getEmojiFromName('NB_HW_' + hwType);

    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    }
    return null;
  },
  getTitanEmoji: function(isAnimated, client) {
    const existingEmoji =
        isAnimated ?
            emojiData.getEmojiFromName('NB_CH_A_Titan') :
            emojiData.getEmojiFromName('NB_CH_Titan');

    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    }
    return null;
  },
  getCharacteristicEmojiPerName: function(chType, client) {
    const existingEmoji = emojiData.getEmojiFromName('NB_CH_' + chType);

    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    }
    return null;
  },
  getBerryPotencyStarEmojiPerPotencyValue: function(int, client) {
    const existingEmoji = emojiData.getEmojiFromName('NB_Berry_Star_' + int);

    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    }
    return null;
  },
  getBotPartEmojiPerName: function(name, client) {
    const existingEmoji = emojiData.getEmojiFromName('NB_Part_' + name);

    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    }
    return null;
  },
  getBallEmoji: function(name, client) {
    const existingEmoji = emojiData.getEmojiFromName('NB_Ball_'+ name);

    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    }
    return null;
  },
  getRPStatusEmoji: function(name, client) {
    const existingEmoji = emojiData.getEmojiFromName('NB_RPStatus_'+ name);

    if (existingEmoji !== null) {
      const emoji = client.emojis.cache.find((emoji) => emoji.name === existingEmoji.name);
      return `${emoji}`;
    }
    return null;
  },
  getPokemonFavoriteFoodEmojiPerFoodName: function(foodName, client) {
    let returnEmoji = '';
    let emoji = null;

    switch (foodName) {
      case 'Miel Kibrille':
        emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Food_Honey');
        returnEmoji = `${emoji}`;
        break;
      case 'Riz Zélé':
        emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Food_Rice');
        returnEmoji = `${emoji}`;
        break;
      case 'Fève Dodue':
        emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Food_Beans');
        returnEmoji = `${emoji}`;
        break;
      case 'Champi Toumou':
        emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Food_Mushroom');
        returnEmoji = `${emoji}`;
        break;
      case 'Sel Rochedur':
        emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Food_Salt');
        returnEmoji = `${emoji}`;
        break;
      case 'Feuille Émeraude':
        emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Food_Leaf');
        returnEmoji = `${emoji}`;
        break;
      case 'CD Brisé':
        emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Food_CD');
        returnEmoji = `${emoji}`;
        break;
      case 'Noigrume Pourpre':
        emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Food_Apicorn');
        returnEmoji = `${emoji}`;
        break;
      case 'Baie':
        emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Food_Berry');
        returnEmoji = `${emoji}`;
        break;
    }

    return returnEmoji;
  },
  getFoundItemRarityEmojiPerNameOrNumber: function(rarity, client) {
    let returnEmoji = '';
    let emoji = null;

    switch (rarity) {
      case 'special':
      case 0:
        emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_BFIR_0');
        returnEmoji = `${emoji}`;
        break;
      case 'co':
      case 1:
        emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_BFIR_1');
        returnEmoji = `${emoji}`;
        break;
      case 'unco':
      case 2:
        emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_BFIR_2');
        returnEmoji = `${emoji}`;
        break;
      case 'inha':
      case 3:
        emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_BFIR_3');
        returnEmoji = `${emoji}`;
        break;
      case 'rare':
      case 4:
        emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_BFIR_4');
        returnEmoji = `${emoji}`;
        break;
      case 'rari':
      case 5:
        emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_BFIR_5');
        returnEmoji = `${emoji}`;
        break;
    }

    return returnEmoji;
  },
  getPokeblocEmojiPerColorAndLevelValue: function(color, level, client) {
    let returnEmoji = '';
    let emoji = null;

    switch (color) {
      case 'blue':
        switch (level) {
          case 0:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Bleu');
            returnEmoji = `${emoji}`;
            break;
          case 1:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Bleu_Plus');
            returnEmoji = `${emoji}`;
            break;
        }
        break;
      case 'red':
        switch (level) {
          case 0:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Rouge');
            returnEmoji = `${emoji}`;
            break;
          case 1:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Rouge_Plus');
            returnEmoji = `${emoji}`;
            break;
        }
        break;
      case 'pink':
        switch (level) {
          case 0:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Rose');
            returnEmoji = `${emoji}`;
            break;
          case 1:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Rose_Plus');
            returnEmoji = `${emoji}`;
            break;
        }
        break;
      case 'green':
        switch (level) {
          case 0:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Vert');
            returnEmoji = `${emoji}`;
            break;
          case 1:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Vert_Plus');
            returnEmoji = `${emoji}`;
            break;
        }
        break;
      case 'yellow':
        switch (level) {
          case 0:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Jaune');
            returnEmoji = `${emoji}`;
            break;
          case 1:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Jaune_Plus');
            returnEmoji = `${emoji}`;
            break;
        }
        break;
      case 'orange':
        switch (level) {
          case 0:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Orange');
            returnEmoji = `${emoji}`;
            break;
          case 1:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Orange_Plus');
            returnEmoji = `${emoji}`;
            break;
        }
        break;
      case 'indigo':
        switch (level) {
          case 0:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Indigo');
            returnEmoji = `${emoji}`;
            break;
          case 1:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Indigo_Plus');
            returnEmoji = `${emoji}`;
            break;
        }
        break;
      case 'purple':
        switch (level) {
          case 0:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Violet');
            returnEmoji = `${emoji}`;
            break;
          case 1:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Violet_Plus');
            returnEmoji = `${emoji}`;
            break;
        }
        break;
      case 'rainbow':
        switch (level) {
          case 0:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Arcenciel');
            returnEmoji = `${emoji}`;
            break;
          case 1:
            emoji = client.emojis.cache.find((emoji) => emoji.name === 'NB_Pokebloc_Arcenciel_Plus');
            returnEmoji = `${emoji}`;
            break;
        }
        break;
    }

    return returnEmoji;
  },
};
