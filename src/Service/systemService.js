const moment = require('moment');
const fs = require('fs');
// const Pokedex = require('pokedex-promise-v2');
// const P = new Pokedex();

const playerService = require('../Service/playerService');
const pokémonViewService = require('./Pokémon/pokémonViewService');

const emojisData = require('../Data/emojiData');
const playerData = require('../Data/playerData');
const pokémonData = require('../Data/pokémonData');
const generatedPokémonData = require('../Data/generatedPokémonData');
const craftRecipesData = require('../Data/craftRecipesData');
const biomeCategoriesData = require('../Data/biomeCategoriesData');
const systemData = require('../Data/systemData');
const calendarData = require('../Data/calendarData');
const pastTrainersData = require('../Data/pastTrainersData');
const frenchTranslation = require('../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();

const dumpPath = 'src/Data/jsons/dumps';
const dynamicDataPath = 'src/Data/jsons/dynamicData';
module.exports = {
  setPeriod: function(stringPeriod) {
    systemData.setSystemProperty('timePeriod', stringPeriod);
    return 'Période sauvegardée';
  },
  setBiomeCategory: function(inputBiomeCategory) {
    inputBiomeCategory = biomeCategoriesData.getBiomeCategoryByName(inputBiomeCategory);

    if (inputBiomeCategory !== null) {
      systemData.setSystemProperty('actualBiomeCategory', inputBiomeCategory.id);
      return 'Biome sauvegardé';
    } else {
      return 'Catégorie de biome inconnue';
    }
  },
  removeAllPlayers: function(msg) {
    if (playerService.checkIfAdminUser(msg.author.id)) {
      const players = {players: []};

      if (!fs.existsSync(dumpPath + '/players')) {
        fs.mkdirSync(dumpPath + '/players');
      }
      fs.writeFile(dumpPath + '/players/' + moment().unix() + '.json', JSON.stringify(players), function() {});
      fs.writeFile(process.env.PLAYER_DATA, JSON.stringify(players), function() {});
      return tr.system.dataDumpAndReset;
    } else {
      return tr.error.notAdminUser;
    }
  },
  startDatabases: function(msg) {
    if (playerService.checkIfAdminUser(msg.author.id)) {
      if (!fs.existsSync(dynamicDataPath)) {
        fs.mkdirSync(dynamicDataPath);
      } else {
        this.dumpData(msg);
      }

      if (!fs.existsSync(dynamicDataPath + '/players.json')) {
        const players = {players: []};
        fs.writeFile(dynamicDataPath + '/players.json', JSON.stringify(players), function() {});
      }
      if (!fs.existsSync(dynamicDataPath + '/craftRecipes.json')) {
        const recipes = {recipes: []};
        fs.writeFile(dynamicDataPath + '/craftRecipes.json', JSON.stringify(recipes), function() {});
      }
      if (!fs.existsSync(dynamicDataPath + '/generatedPokemon.json')) {
        const generatedPokémon = {generatedPokémon: []};
        fs.writeFile(dynamicDataPath + '/generatedPokemon.json', JSON.stringify(generatedPokémon), function() {});
      }
      if (!fs.existsSync(dynamicDataPath + '/calendar.json')) {
        const calendar = {calendar: []};
        fs.writeFile(dynamicDataPath + '/calendar.json', JSON.stringify(calendar), function() {});
      }
      if (!fs.existsSync(dynamicDataPath + '/system.json')) {
        const timePeriod = {
          timePeriod: 'Séance 0',
          actualBiomeCategory: 3,
        };
        fs.writeFile(dynamicDataPath + '/system.json', JSON.stringify(timePeriod), function() {});
      }
      if (!fs.existsSync(dynamicDataPath + '/pastTrainers.json')) {
        const pastTrainers = {pastTrainers: []};
        fs.writeFile(dynamicDataPath + '/pastTrainers.json', JSON.stringify(pastTrainers), function() {});
      }
      if (!fs.existsSync(dynamicDataPath + '/playersPokémon.json')) {
        const playersPokémon = {playersPokémon: []};
        fs.writeFile(dynamicDataPath + '/playersPokémon.json', JSON.stringify(playersPokémon), function() {});
      }
      return tr.system.startDatabases;
    } else {
      return tr.error.notAdminUser;
    }
  },
  dumpData: function(msg) {
    if (playerService.checkIfAdminUser(msg.author.id)) {
      const players = playerData.getAllPlayers();
      if (!fs.existsSync(dumpPath)) {
        fs.mkdirSync(dumpPath);
      }
      // Dump players
      if (!fs.existsSync(dumpPath + '/players')) {
        fs.mkdirSync(dumpPath + '/players');
      }
      fs.writeFile(dumpPath + '/players/' + moment().unix() + '.json', JSON.stringify(players), function() {});
      // Dump craft recipes
      const recipes = craftRecipesData.getAllCraftRecipes();
      if (!fs.existsSync(dumpPath + '/craftRecipes')) {
        fs.mkdirSync(dumpPath + '/craftRecipes');
      }
      fs.writeFile(dumpPath + '/craftRecipes/' + moment().unix() + '.json', JSON.stringify(recipes), function() {});
      // Dump generatedPokémon
      const generatedPokémon = generatedPokémonData.getAllGeneratedPokémon();
      if (!fs.existsSync(dumpPath + '/generatedPokémon')) {
        fs.mkdirSync(dumpPath + '/generatedPokémon');
      }
      fs.writeFile(
          dumpPath + '/generatedPokémon/' + moment().unix() + '.json',
          JSON.stringify(generatedPokémon),
          function() {},
      );
      // Dump calendar
      const calendar = calendarData.getAllCalendarData();
      if (!fs.existsSync(dumpPath + '/calendar')) {
        fs.mkdirSync(dumpPath + '/calendar');
      }
      fs.writeFile(dumpPath + '/calendar/' + moment().unix() + '.json', JSON.stringify(calendar), function() {});
      // Dump pastTrainers
      const pastTrainers = pastTrainersData.getPastTrainerData();
      if (!fs.existsSync(dumpPath + '/pastTrainers')) {
        fs.mkdirSync(dumpPath + '/pastTrainers');
      }
      fs.writeFile(
          dumpPath + '/pastTrainers/' + moment().unix() + '.json',
          JSON.stringify(pastTrainers),
          function() {},
      );
      return tr.system.dataDump;
    } else {
      return tr.error.notAdminUser;
    }
  },
  // Return Json with only text if too much/little parameters or player already added
  // Return Json with text, id and username if success
  removePlayer: function(msg, client) {
    if (playerService.checkIfAdminUser(msg.author.id)) {
      let id = '';
      let username = '';
      switch (msg.content.split(' ').length) {
        case 1:
        case 2:
          return {
            text: tr.error.invalidOrBadArgument,
          };
        case 3:
          id = msg.content.split(' ')[2];
          username = client.users.cache.get(id).username;
          if (playerService.checkIfUserAlreadyAdded(id)) {
            const players = playerData.getAllPlayers();
            for (let i = 0; i < players.players.length; i++) {
              if (players.players[i] != null) {
                if (players.players[i].id === id) {
                  delete players.players[i];
                }
              }
            }
            fs.writeFile(process.env.PLAYER_DATA, JSON.stringify(players), function() {});
            return {
              text: tr.system.deleteOf + username + tr.system.doneEnd,
              username: username,
              id: id,
            };
          }
          return {
            text: tr.system.userStart + username + tr.system.userNotFoundEnd,
          };
        default:
          return {
            text: tr.error.tooManyArgumentsOrBadSyntax,
          };
      }
    } else {
      return {
        text: tr.error.notAdminUser,
      };
    }
  },

  // Return Json with only text if too much/little parameters or player already added
  // Return Json with text, id and username if success
  addPlayer: function(msg, client) {
    if (playerService.checkIfAdminUser(msg.author.id)) {
      let id = '';
      let username = 'TEST';
      switch (msg.content.split(' ').length) {
        case 1:
        case 2:
          return {
            text: tr.error.invalidOrBadArgument,
          };
        case 3:
          id = msg.content.split(' ')[2];
          username = client.users.cache.get(id).username;
          if (!playerService.checkIfUserAlreadyAdded(id)) {
            const players = playerData.getAllPlayers();
            players.players.push(
                {
                  id: id,
                  username: username,
                },
            );
            fs.writeFile(process.env.PLAYER_DATA, JSON.stringify(players), function() {});
            return {
              text: tr.system.addOfStart + username + tr.system.doneEnd,
              username: username,
              id: id,
            };
          }

          if (!pokémonViewService.checkIfUserAlreadyAdded(id)) {
            const playersPokémon = pokémonData.getAllPlayersPokémon();
            playersPokémon.playersPokémon.push(
                {
                  id: id,
                  username: username,
                  class: 'Trainer',
                  playerPokémon: [],
                },
            );
            fs.writeFile(process.env.PLAYER_POKEMON_DATA, JSON.stringify(playersPokémon), function() {});
            return {
              text: tr.system.addOfStart + username + tr.system.doneEnd,
              username: username,
              id: id,
            };
          }
          return {
            text: tr.system.userStart + username + tr.system.userAlreadyAddedEnd,
          };
        default:
          return {
            text: tr.error.tooManyArgumentsOrBadSyntax,
          };
      }
    } else {
      return {
        text: tr.error.notAdminUser,
      };
    }
  },

  addLogChannelId: function(msg) {
    if (playerService.checkIfAdminUser(msg.author.id)) {
      let id = '';
      let logChannelId = '';
      let username = '';
      switch (msg.content.split(' ').length) {
        case 1:
        case 2:
        case 3:
          return {
            text: tr.error.invalidOrBadArgument,
          };
        case 4:
          id = msg.content.split(' ')[2];
          logChannelId = msg.content.split(' ')[3];
          if (playerService.checkIfUserAlreadyAdded(id)) {
            const players = playerData.getAllPlayers();
            for (let i = 0; i < players.players.length; i++) {
              if (players.players[i].id === id) {
                players.players[i].logChannelId = logChannelId;
                username = players.players[i].username;
              }
            }
            fs.writeFile(process.env.PLAYER_DATA, JSON.stringify(players), function() {});
            return {
              text: tr.system.addLogChannel + username + tr.system.doneEnd,
              username: username,
              id: id,
              logChannelId: logChannelId,
            };
          }
          return {
            text: tr.system.userStart + username + tr.system.userAlreadyAddedEnd,
          };
        default:
          return {
            text: tr.error.tooManyArgumentsOrBadSyntax,
          };
      }
    } else {
      return {
        text: tr.error.notAdminUser,
      };
    }
  },

  deleteAllEmojisInGuild: function(msg) {
    if (playerService.checkIfAdminUser(msg.author.id)) {
      msg.guild.emojis.cache.map((e) => e.delete().then((emoji) => console.log('Emoji deleted: ' + emoji.name)));
      return 'Done';
    } else {
      return tr.error.notAdminUser;
    }
  },

  // If endInt = -1, endInt is the length of emojiData
  uploadEmojiInDiscordServer: function(msg, startInt, endInt) {
    const allEmojis = emojisData.getAllEmojisData();
    endInt = endInt === -1 ? allEmojis.emojis.length : endInt;
    if (playerService.checkIfAdminUser(msg.author.id)) {
      for (let i = startInt; i < endInt; i++) {
        if (allEmojis.emojis[i] !== undefined) {
          msg.guild.emojis.create(allEmojis.emojis[i].path, allEmojis.emojis[i].name)
              .then((emoji) => console.log('Emoji uploaded: ' + emoji.name));
        }
      }
      return 'Done';
    } else {
      return tr.error.notAdminUser;
    }
  },

  // Used to send message in specified channel in log server
  sendMessageToLogServer: function(id, guild, templateResult) {
    const players = playerData.getAllPlayers();
    for (let i = 0; i < players.players.length; i++) {
      if (players.players[i] != null) {
        if (players.players[i].id === id) {
          const channel = guild.channels.cache.get(players.players[i].logChannelId);
          channel.send({
            embeds: [templateResult.embedMessage],
            files: templateResult.images,
          });
        }
      }
    }
    return false;
  },
};
