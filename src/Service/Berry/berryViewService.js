const itemsData = require('../../Data/itemsData');

const berryControllerService = require('./berryControllerService');
const emojiService = require('../../Service/emojiService');

module.exports = {
  generateBerryPotencyEmoji: function(berryPotency, client) {
    let returnValue = '';
    berryPotency = parseInt(berryPotency);
    if (berryPotency > 0) {
      for (let i = 0; i < berryPotency; i++) {
        returnValue = returnValue + emojiService.getBerryPotencyStarEmojiPerPotencyValue(berryPotency, client);
      }
    } else {
      returnValue = 'Non présente';
    }
    return returnValue;
  },
  generateBerryMixDescription: function(
      berry1,
      berry2,
      berry3,
      berry4,
      diceThrowMessageResult,
      client,
  ) {
    let description = 'Avec les baies ' +
            berry1 + ', ' +
            berry2 + ', ' +
            berry3 + ' et ' +
            berry4 + '. \n';

    const sortPotency = berryControllerService.sortPokeblocPotencyAndSplitRarity(
        berryControllerService.calculateTotalPotencyBerries(
            itemsData.getItemFromName(berry1),
            itemsData.getItemFromName(berry2),
            itemsData.getItemFromName(berry3),
            itemsData.getItemFromName(berry4),
        ),
    );

    const communPotency = sortPotency.commun;
    const rarePotency = sortPotency.rare;
    const modificators = berryControllerService.getModificators(diceThrowMessageResult);
    const slots = modificators.slots;
    let rarityOrder = 0;

    if (slots !== 0) {
      description = description + 'Vous avez trouvé dans le bac à Pokébloc:\n';
      const pokeblocMultiplicator = modificators.multiplicator;

      let communPokéblocArray = '';
      let rarePokéblocArray = '';

      // Check which rare flavors and commun flavors are superior in number or equal
      if (
        berryControllerService.calculateTotalPotencyValue(communPotency) >
          berryControllerService.calculateTotalPotencyValue(rarePotency)
      ) {
        rarityOrder = 0;
        const numberOfCommunPokeblocFlavor =
            berryControllerService.calculateTotalPotencyValue(rarePotency) !== 0 ?
              slots - 1 :
              slots;

        const numberOfRarePokeblocFlavor =
            berryControllerService.calculateTotalPotencyValue(rarePotency) !== 0 ? 1 : 0;

        communPokéblocArray = berryControllerService.getPokéblocArray(
            numberOfCommunPokeblocFlavor,
            communPotency,
            'commun',
        );

        rarePokéblocArray = berryControllerService.getPokéblocArray(
            numberOfRarePokeblocFlavor,
            rarePotency,
            'rare',
        );
      }
      if (
        berryControllerService.calculateTotalPotencyValue(communPotency) <
          berryControllerService.calculateTotalPotencyValue(rarePotency)
      ) {
        rarityOrder = 1;
        const numberOfRarePokeblocFlavor =
            berryControllerService.calculateTotalPotencyValue(rarePotency) !== 0 ?
              slots - 1 :
              slots;

        const numberOfCommunPokeblocFlavor =
            berryControllerService.calculateTotalPotencyValue(rarePotency) !== 0 ? 1 : 0;

        communPokéblocArray = berryControllerService.getPokéblocArray(
            numberOfCommunPokeblocFlavor,
            communPotency,
            'commun',
        );

        rarePokéblocArray = berryControllerService.getPokéblocArray(
            numberOfRarePokeblocFlavor,
            rarePotency,
            'rare',
        );
      }
      if (
        berryControllerService.calculateTotalPotencyValue(communPotency) ===
          berryControllerService.calculateTotalPotencyValue(rarePotency)
      ) {
        rarityOrder = 0;
        const numberOfRarePokeblocFlavor = 2;
        const numberOfCommunPokeblocFlavor =
            berryControllerService.calculateTotalPotencyValue(rarePotency) !== 0 ?
              slots - 2 :
              slots;

        communPokéblocArray = berryControllerService.getPokéblocArray(
            numberOfCommunPokeblocFlavor,
            communPotency,
            'commun',
        );

        rarePokéblocArray = berryControllerService.getPokéblocArray(
            numberOfRarePokeblocFlavor,
            rarePotency,
            'rare',
        );
      }

      const multiplicatorRepartition = berryControllerService.getRepartitionOfMultiplicators(
          communPokéblocArray,
          rarePokéblocArray,
          pokeblocMultiplicator,
          slots,
      );

      let increment = 0;

      if (rarityOrder === 0) {
        if (communPokéblocArray.length !== undefined) {
          for (let i = 0; i < communPokéblocArray.length; i++) {
            description =
                description +
                '- %' + increment +
                ' ' + emojiService.getPokeblocEmojiPerColorAndLevelValue(
                    communPokéblocArray[i].color,
                    communPokéblocArray[i].level,
                    client,
                ) +
                ' (' + berryControllerService.formatPokéblocNameFromColorAndLevel(
                    communPokéblocArray[i].color,
                    communPokéblocArray[i].level,
                ) +
                ') !\n';
            increment++;
          }
        } else {
          description = description +
              '- %' + increment +
              ' ' + emojiService.getPokeblocEmojiPerColorAndLevelValue(
              communPokéblocArray.color,
              communPokéblocArray.level,
              client,
          ) +
              ' (' + berryControllerService.formatPokéblocNameFromColorAndLevel(
              communPokéblocArray.color,
              communPokéblocArray.level,
          ) +
              ') !\n';
          increment++;
        }

        if (rarePokéblocArray.length !== undefined) {
          for (let i = 0; i < rarePokéblocArray.length; i++) {
            description = description +
                '- %' + increment +
                ' ' + emojiService.getPokeblocEmojiPerColorAndLevelValue(
                rarePokéblocArray[i].color,
                rarePokéblocArray[i].level,
                client,
            ) +
                ' (' + berryControllerService.formatPokéblocNameFromColorAndLevel(
                rarePokéblocArray[i].color,
                rarePokéblocArray[i].level,
            ) +
                ') !\n';
            increment++;
          }
        } else {
          description = description +
              '- %' + increment +
              ' ' + emojiService.getPokeblocEmojiPerColorAndLevelValue(
              rarePokéblocArray.color,
              rarePokéblocArray.level,
              client,
          ) +
              ' (' + berryControllerService.formatPokéblocNameFromColorAndLevel(
              rarePokéblocArray.color,
              rarePokéblocArray.level,
          ) +
              ') !\n';
          increment++;
        }
      }
      if (rarityOrder === 1) {
        if (rarePokéblocArray.length !== undefined) {
          for (let i = 0; i < rarePokéblocArray.length; i++) {
            description = description +
                '- %' + increment +
                ' ' + emojiService.getPokeblocEmojiPerColorAndLevelValue(
                rarePokéblocArray[i].color,
                rarePokéblocArray[i].level,
                client,
            ) +
                ' (' + berryControllerService.formatPokéblocNameFromColorAndLevel(
                rarePokéblocArray[i].color,
                rarePokéblocArray[i].level,
            ) +
                ') !\n';
            increment++;
          }
        } else {
          description = description +
              '- %' + increment +
              ' ' + emojiService.getPokeblocEmojiPerColorAndLevelValue(
              rarePokéblocArray.color,
              rarePokéblocArray.level,
              client,
          ) +
              ' (' + berryControllerService.formatPokéblocNameFromColorAndLevel(
              rarePokéblocArray.color,
              rarePokéblocArray.level,
          ) +
              ') !\n';
          increment++;
        }

        if (communPokéblocArray.length !== undefined) {
          for (let i = 0; i < communPokéblocArray.length; i++) {
            description = description +
                '- %' + increment +
                ' ' + emojiService.getPokeblocEmojiPerColorAndLevelValue(
                communPokéblocArray[i].color,
                communPokéblocArray[i].level,
                client,
            ) +
                ' (' + berryControllerService.formatPokéblocNameFromColorAndLevel(
                communPokéblocArray[i].color,
                communPokéblocArray[i].level,
            ) +
                ') !\n';
            increment++;
          }
        } else {
          description = description +
              '- %' + increment +
              ' ' + emojiService.getPokeblocEmojiPerColorAndLevelValue(
              communPokéblocArray.color,
              communPokéblocArray.level,
              client,
          ) +
              ' (' + berryControllerService.formatPokéblocNameFromColorAndLevel(
              communPokéblocArray.color,
              communPokéblocArray.level,
          ) +
              ') !\n';
          increment++;
        }
      }

      for (let i = 0; i < increment; i++) {
        description = description.replace('%' + i, multiplicatorRepartition[i]);
      }
    } else {
      description = description + 'Vous ne trouvez rien dans le bac à Pokébloc :c\n';
    }

    return description;
  },
  generateBerryTableDescription: function(intPage) {
    const paginatedBerries = berryControllerService.paginateBerryData(intPage);
    let description = 'Pour naviguer dans ce tableau merci d\'indiquer un chiffre compris ' +
        'entre 1 et 8 après cette commande.\n';
    description = description + this.insertTopTable();

    for (let i = 0; i <= paginatedBerries.length - 1; i++) {
      description = description + this.insertBerryLineTable(paginatedBerries[i]);
      if (i !== paginatedBerries.length - 1) {
        description = description + this.insertBetweenLineTable();
      }
    }
    description = description + this.insertBottomTable();

    return description;
  },
  insertTopTable: function() {
    return '```' +
        '┌─────────┬──┬──┬──┬──┬──┬──┬──┬──┐\n' +
        '│   Nom   │Ép│Se│Su│Ac│Am│Sa│Um│As│\n' +
        '├─────────┼──┼──┼──┼──┼──┼──┼──┼──┤\n';
  },
  insertBottomTable: function() {
    return '└─────────┴──┴──┴──┴──┴──┴──┴──┴──┘\n' +
        '```';
  },
  insertBetweenLineTable: function() {
    return '├─────────┼──┼──┼──┼──┼──┼──┼──┼──┤\n';
  },
  insertBerryLineTable: function(berry) {
    return '│' + this.formatBerryNameForTable(berry.name.replace('Baie ', '')) +
        '│ ' + berry.berryData.flavors[0].potency +
        '│ ' + berry.berryData.flavors[1].potency +
        '│ ' + berry.berryData.flavors[2].potency +
        '│ ' + berry.berryData.flavors[4].potency +
        '│ ' + berry.berryData.flavors[3].potency +
        '│ ' + berry.berryData.flavors[6].potency +
        '│ ' + berry.berryData.flavors[5].potency +
        '│ ' + berry.berryData.flavors[7].potency + '│\n';
  },
  formatBerryNameForTable: function(berryName) {
    while (berryName.length <= 8) {
      berryName = berryName + ' ';
    }
    return berryName;
  },
  generateBerrySortedTableDescription: function(potencyName, intPage) {
    const paginatedBerries = berryControllerService.paginateBerryDataPerPotencyAttribute(intPage, potencyName);
    let description = 'Pour naviguer dans ce tableau merci d\'indiquer un chiffre compris ' +
        'entre 1 et 8 après cette commande.\n';
    description = description + this.insertTopTable();

    for (let i = 0; i <= paginatedBerries.length - 1; i++) {
      description = description + this.insertBerryLineTable(paginatedBerries[i]);
      if (i !== paginatedBerries.length - 1) {
        description = description + this.insertBetweenLineTable();
      }
    }
    description = description + this.insertBottomTable();

    return description;
  },
};
