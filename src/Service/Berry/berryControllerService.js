const itemsData = require('../../Data/itemsData');

module.exports = {
  formatBerryNameInput: function(callInput) {
    return callInput.charAt(0).toUpperCase() + callInput.slice(1);
  },
  formatBerryCommandInputMix: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    switch (inputCommandPlaceNumber) {
      case 1:
        formatedInput = callInput.split('--Baie1=')[1];
        formatedInput = formatedInput.split('--Baie2=')[0];
        formatedInput = this.formatBerryNameInput(formatedInput);
        break;
      case 2:
        formatedInput = callInput.split('--Baie2=')[1];
        formatedInput = formatedInput.split('--Baie3=')[0];
        formatedInput = this.formatBerryNameInput(formatedInput);
        break;
      case 3:
        formatedInput = callInput.split('--Baie3=')[1];
        formatedInput = formatedInput.split('--Baie4=')[0];
        formatedInput = this.formatBerryNameInput(formatedInput);
        break;
      case 4:
        formatedInput = callInput.split('--Baie4=')[1];
        formatedInput = formatedInput.split('--NivCaractéristique=')[0];
        formatedInput = this.formatBerryNameInput(formatedInput);
        break;
      case 5:
        if (callInput.includes('NivCompétence')) {
          formatedInput = callInput.split('--NivCaractéristique=')[1];
          formatedInput = formatedInput.split('--NivCompétence=')[0];
        } else if (callInput.includes('Modificateur')) {
          formatedInput = callInput.split('--NivCaractéristique=')[1];
          formatedInput = formatedInput.split('--Modificateur=')[0];
        } else {
          formatedInput = callInput.split('--NivCaractéristique=')[1];
        }
        break;
      case 6:
        if (callInput.includes('NivCompétence')) {
          if (callInput.includes('Modificateur')) {
            formatedInput = callInput.split('--NivCompétence=')[1];
            formatedInput = formatedInput.split('--Modificateur=')[0];
          } else {
            formatedInput = callInput.split('--NivCompétence=')[1];
          }
        }
        break;
      case 7:
        if (callInput.includes('Modificateur')) {
          formatedInput = callInput.split('--Modificateur=')[1];
        } else {
          formatedInput = 0;
        }
        break;
      default:
        formatedInput = 'error';
        break;
    }

    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/\s/g, '') : formatedInput;
    }
    return null;
  },
  calculateTotalPotencyBerries: function(
      berry1,
      berry2,
      berry3,
      berry4,
  ) {
    return [
      {
        name: berry1.berryData.flavors[0].name,
        rarity: 'commun',
        value: berry1.berryData.flavors[0].potency +
                    berry2.berryData.flavors[0].potency +
                    berry3.berryData.flavors[0].potency +
                    berry4.berryData.flavors[0].potency,
      },
      {
        name: berry1.berryData.flavors[1].name,
        rarity: 'commun',
        value: berry1.berryData.flavors[1].potency +
                    berry2.berryData.flavors[1].potency +
                    berry3.berryData.flavors[1].potency +
                    berry4.berryData.flavors[1].potency,
      },
      {
        name: berry1.berryData.flavors[2].name,
        rarity: 'commun',
        value: berry1.berryData.flavors[2].potency +
                    berry2.berryData.flavors[2].potency +
                    berry3.berryData.flavors[2].potency +
                    berry4.berryData.flavors[2].potency,
      },
      {
        name: berry1.berryData.flavors[3].name,
        rarity: 'commun',
        value: berry1.berryData.flavors[3].potency +
                    berry2.berryData.flavors[3].potency +
                    berry3.berryData.flavors[3].potency +
                    berry4.berryData.flavors[3].potency,
      },
      {
        name: berry1.berryData.flavors[4].name,
        rarity: 'commun',
        value: berry1.berryData.flavors[4].potency +
                    berry2.berryData.flavors[4].potency +
                    berry3.berryData.flavors[4].potency +
                    berry4.berryData.flavors[4].potency,
      },
      {
        name: berry1.berryData.flavors[5].name,
        rarity: 'rare',
        value: berry1.berryData.flavors[5].potency +
                    berry2.berryData.flavors[5].potency +
                    berry3.berryData.flavors[5].potency +
                    berry4.berryData.flavors[5].potency,
      },
      {
        name: berry1.berryData.flavors[6].name,
        rarity: 'rare',
        value: berry1.berryData.flavors[6].potency +
                    berry2.berryData.flavors[6].potency +
                    berry3.berryData.flavors[6].potency +
                    berry4.berryData.flavors[6].potency,
      },
      {
        name: berry1.berryData.flavors[7].name,
        rarity: 'rare',
        value: berry1.berryData.flavors[7].potency +
                    berry2.berryData.flavors[7].potency +
                    berry3.berryData.flavors[7].potency +
                    berry4.berryData.flavors[7].potency,
      },
    ];
  },
  sortPokeblocPotencyAndSplitRarity: function(totalPotency) {
    const communFlavors = totalPotency.slice(0, 5);
    const rareFlavors = totalPotency.slice(5);

    communFlavors.sort(
        function compareNumbers(a, b) {
          return b.value - a.value;
        },
    );
    rareFlavors.sort(
        function compareNumbers(a, b) {
          return b.value - a.value;
        },
    );

    return {
      commun: communFlavors,
      rare: rareFlavors,
    };
  },
  getModificators: function(diceThrowMessage) {
    diceThrowMessage = JSON.stringify(diceThrowMessage);
    if (diceThrowMessage.includes('Votre jet est valide!')) {
      if (
        diceThrowMessage.includes('C\'est une réussite critique!')
      ) {
        return {
          slots: 5,
          multiplicator: this.randomizeMultiplicator(2),
        };
      }
      if (
        diceThrowMessage.includes('C\'est une réussite absolue!')
      ) {
        return {
          slots: 5,
          multiplicator: this.randomizeMultiplicator(3),
        };
      }
      if (
        diceThrowMessage.includes('Ça passe très juste') ||
                diceThrowMessage.includes('Ça passe juste')
      ) {
        return {
          slots: 4,
          multiplicator: 1,
        };
      }
      if (
        diceThrowMessage.includes('Ça passe!')
      ) {
        return {
          slots: this.randomizeNumberOfSlots(10),
          multiplicator: this.randomizeMultiplicator(0),
        };
      }
      if (
        diceThrowMessage.includes('Ça passe largement!')
      ) {
        return {
          slots: this.randomizeNumberOfSlots(50),
          multiplicator: this.randomizeMultiplicator(1),
        };
      }
    } else {
      if (
        diceThrowMessage.includes('échec critique') ||
                diceThrowMessage.includes('échec absolu')
      ) {
        return {
          slots: 0,
          multiplicator: 0,
        };
      } else {
        return {
          slots: 2,
          multiplicator: 1,
        };
      }
    }
    return 'error';
  },
  randomizeNumberOfSlots: function(condition) {
    const randomNumber = Math.floor(Math.random() * 100);
    return randomNumber <= condition ? 5 : 4;
  },
  randomizeMultiplicator: function(stateIndex) {
    let multiplicator = 0;
    const randomNumber = Math.floor(Math.random() * 100);
    switch (stateIndex) {
      case 0:
        if (randomNumber <= 25) {
          multiplicator = 1;
        } else {
          multiplicator = 2;
        }
        break;
      case 1:
        if (randomNumber <= 10) {
          multiplicator = 2;
        } else if (randomNumber <= 70) {
          multiplicator = 3;
        } else {
          multiplicator = 4;
        }
        break;
      case 2:
        if (randomNumber <= 25) {
          multiplicator = 5;
        } else if (randomNumber <= 75) {
          multiplicator = 6;
        } else {
          multiplicator = 7;
        }
        break;
      case 3:
        if (randomNumber <= 25) {
          multiplicator = 10;
        } else if (randomNumber <= 75) {
          multiplicator = 12;
        } else {
          multiplicator = 14;
        }
        break;
    }
    return multiplicator;
  },
  calculateTotalPotencyValue: function(potencyArray) {
    let totalPotency = 0;
    for (let i = 0; i < potencyArray.length; i++) {
      totalPotency = totalPotency + potencyArray[i].value;
    }
    return totalPotency;
  },
  getPokéblocArray: function(numberOfPokeblocFlavor, potencyArray, rarity) {
    const pokéblocArray = [];

    if (rarity === 'commun') {
      if (
        this.isPokéblocRainbow(potencyArray)
      ) {
        return {
          color: 'rainbow',
          level: this.getRainbowPokeblocLevel(potencyArray),
        };
      }

      const numberOfMajorFlavors = this.getNumberOfDifferentCommonFlavors(
          numberOfPokeblocFlavor,
          potencyArray,
      );

      for (let i = 0; i <= numberOfMajorFlavors - 1; i++) {
        pokéblocArray[pokéblocArray.length] = this.getPokeblocColorAndLevel(potencyArray[i]);
      }
    }
    if (rarity === 'rare') {
      const numberOfMajorFlavors = this.getNumberOfDifferentRareFlavors(
          numberOfPokeblocFlavor,
          potencyArray,
      );

      for (let i = 0; i <= numberOfMajorFlavors - 1; i++) {
        pokéblocArray[pokéblocArray.length] = this.getPokeblocColorAndLevel(potencyArray[i]);
      }
    }

    return pokéblocArray;
  },
  isPokéblocRainbow: function(sortedPotency) {
    return sortedPotency[0].value >= 2 &&
            sortedPotency[1].value >= 2 &&
            sortedPotency[2].value >= 2 &&
            sortedPotency[3].value >= 2;
  },
  getRainbowPokeblocLevel: function(sortedPotency) {
    if (
      sortedPotency[0].value >= 4 &&
            sortedPotency[1].value >= 4 &&
            sortedPotency[2].value >= 4 &&
            sortedPotency[3].value >= 4
    ) {
      return 1;
    }
    if (
      sortedPotency[0].value >= 2 &&
            sortedPotency[1].value >= 2 &&
            sortedPotency[2].value >= 2 &&
            sortedPotency[3].value >= 2
    ) {
      return 0;
    }
  },
  getPokeblocColorAndLevel: function(potency) {
    if (potency.value >= 4) {
      return {
        color: this.getPokeblocColorFromFlavorName(potency.name),
        level: 1,
      };
    }
    return {
      color: this.getPokeblocColorFromFlavorName(potency.name),
      level: 0,
    };
  },
  getPokeblocColorFromFlavorName: function(flavorName) {
    switch (flavorName) {
      case 'dry':
        return 'blue';
      case 'spicy':
        return 'red';
      case 'sweet':
        return 'pink';
      case 'bitter':
        return 'green';
      case 'sour':
        return 'yellow';
      case 'umami':
        return 'orange';
      case 'salty':
        return 'indigo';
      case 'astringent':
        return 'purple';
    }
  },
  getNumberOfDifferentCommonFlavors: function(numberOfPokeblocFlavor, potencyArray) {
    const maximumPotency = potencyArray[0].value;

    if (potencyArray[0].value === 0) {
      return 0;
    }
    if (numberOfPokeblocFlavor === 1) {
      return 1;
    }
    if (numberOfPokeblocFlavor === 4) {
      if ((maximumPotency / 2) <= potencyArray[3].value) {
        return 4;
      }
    }
    if (numberOfPokeblocFlavor === 2) {
      if ((maximumPotency / 2) <= potencyArray[1].value) {
        return 2;
      }
    }
    if (potencyArray[1].value === 0) {
      return 1;
    } else if ((maximumPotency / 2) <= potencyArray[2].value) {
      return 3;
    } else if ((maximumPotency / 2) <= potencyArray[1].value) {
      return 2;
    }
    return 1;
  },
  getNumberOfDifferentRareFlavors: function(numberOfPokeblocFlavor, potencyArray) {
    const maximumPotency = potencyArray[0].value;

    if (potencyArray[0].value === 0) {
      return 0;
    }
    if (numberOfPokeblocFlavor === 1) {
      return 1;
    }
    if (potencyArray[1].value === 0) {
      return 1;
    } else if ((maximumPotency / 2) <= potencyArray[2].value) {
      return 3;
    } else if ((maximumPotency / 2) <= potencyArray[1].value) {
      return 2;
    }
    return 1;
  },
  getRepartitionOfMultiplicators: function(
      communPokéblocArray,
      rarePokéblocArray,
      pokeblocMultiplicator,
      slots,
  ) {
    const communLength = communPokéblocArray.length !== undefined ?
      communPokéblocArray.length :
      1;
    const rareLength = rarePokéblocArray.length !== undefined ?
      rarePokéblocArray.length :
      1;

    switch (slots) {
      case 2:
        switch (communLength + rareLength) {
          case 1:
            return {
              0: pokeblocMultiplicator * 2,
            };
          case 2:
            return {
              0: pokeblocMultiplicator,
              1: pokeblocMultiplicator,
            };
        }
        break;
      case 4:
        switch (communLength + rareLength) {
          case 1:
            return {
              0: pokeblocMultiplicator * 4,
            };
          case 2:
            return {
              0: pokeblocMultiplicator * 2,
              1: pokeblocMultiplicator * 2,
            };
          case 3:
            return {
              0: pokeblocMultiplicator * 2,
              1: pokeblocMultiplicator,
              2: pokeblocMultiplicator,
            };
          case 4:
            return {
              0: pokeblocMultiplicator,
              1: pokeblocMultiplicator,
              2: pokeblocMultiplicator,
              3: pokeblocMultiplicator,
            };
        }
        break;
      case 5:
        switch (communLength + rareLength) {
          case 1:
            return {
              0: pokeblocMultiplicator * 5,
            };
          case 2:
            return {
              0: pokeblocMultiplicator * 3,
              1: pokeblocMultiplicator * 2,
            };
          case 3:
            return {
              0: pokeblocMultiplicator * 2,
              1: pokeblocMultiplicator * 2,
              2: pokeblocMultiplicator,
            };
          case 4:
            return {
              0: pokeblocMultiplicator * 2,
              1: pokeblocMultiplicator,
              2: pokeblocMultiplicator,
              3: pokeblocMultiplicator,
            };
          case 5:
            return {
              0: pokeblocMultiplicator,
              1: pokeblocMultiplicator,
              2: pokeblocMultiplicator,
              3: pokeblocMultiplicator,
              4: pokeblocMultiplicator,
            };
        }
        break;
    }
  },
  formatPokéblocNameFromColorAndLevel: function(color, level) {
    const concatenateName = color + level;
    switch (concatenateName) {
      case 'purple0':
        return 'Violet';
      case 'purple1':
        return 'Violet concentrés';
      case 'green0':
        return 'Vert';
      case 'green1':
        return 'Vert concentrés';
      case 'red0':
        return 'Rouge';
      case 'red1':
        return 'Rouge concentrés';
      case 'pink0':
        return 'Rose';
      case 'pink1':
        return 'Rose concentrés';
      case 'orange0':
        return 'Orange';
      case 'orange1':
        return 'Orange concentrés';
      case 'yellow0':
        return 'Jaune';
      case 'yellow1':
        return 'Jaune concentrés';
      case 'indigo0':
        return 'Indigo';
      case 'indigo1':
        return 'Indigo concentrés';
      case 'blue0':
        return 'Bleu';
      case 'blue1':
        return 'Bleu concentrés';
      case 'rainbow0':
        return 'Arc-en-ciel';
      case 'rainbow1':
        return 'Arc-en-ciel concentrés';
    }
  },
  paginateBerryData: function(pageNumber) {
    const allBerryData = itemsData.getAllItemsPerCategory('Baie');
    return allBerryData.slice((pageNumber - 1) * 10, pageNumber * 10);
  },
  paginateBerryDataPerPotencyAttribute: function(pageNumber, intPotency) {
    const allBerryData = itemsData.getAllItemsPerCategory('Baie');
    allBerryData.sort(function(a, b) {
      return b.berryData.flavors[intPotency].potency - a.berryData.flavors[intPotency].potency;
    });
    return allBerryData.slice((pageNumber - 1) * 10, pageNumber * 10);
  },
  switchIntPotency: function(potencyName) {
    switch (potencyName) {
      case 'épicé':
        return 0;

      case 'sec':
        return 1;

      case 'sucré':
        return 2;

      case 'amère':
        return 3;

      case 'acide':
        return 4;

      case 'umami':
        return 5;

      case 'salé':
        return 6;

      case 'astringent':
        return 7;

      default:
        return Math.floor(Math.random() * 7);
    }
  },
};
