const traitData = require('../Data/traitData');

module.exports = {
  generate: function(numberOfTrait) {
    const positiveTraits = traitData.getAllPositiveTraits();
    const negativeTraits = traitData.getAllNegativeTraits();
    let pokemonTraits = '\n';

    if (numberOfTrait > 0) {
      for (let i = 0; i < numberOfTrait - 1; i++) {
        const randNumber = Math.floor(Math.random() * positiveTraits.length);
        pokemonTraits = pokemonTraits + '- ' + positiveTraits[randNumber] + '\n';
      }

      const randNumber = Math.floor(Math.random() * negativeTraits.length);
      pokemonTraits = pokemonTraits + '- ' + negativeTraits[randNumber];

      return pokemonTraits;
    }
  },
};
