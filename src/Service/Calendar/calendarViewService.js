const moment = require('moment');

const emojiService = require('../emojiService');

const calendarData = require('../../Data/calendarData');
const frenchTranslation = require('../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  iconCalendarDescription: function(dayType) {
    let returnValue = tr.calendar.iconCalendarError;

    if (!isNaN(parseInt(dayType))) {
      dayType = parseInt(dayType);
      switch (dayType) {
        case 0:
          returnValue = tr.calendar.iconCalendarDescription.null;
          break;
        case 1:
          returnValue = tr.calendar.iconCalendarDescription.session;
          break;
        case 2:
          returnValue = tr.calendar.iconCalendarDescription.offSession;
          break;
        case 3:
          returnValue = tr.calendar.iconCalendarDescription.offSeason;
          break;
        case 4:
          returnValue = tr.calendar.iconCalendarDescription.event;
          break;
        case 5:
          returnValue = tr.calendar.iconCalendarDescription.offSessionEvent;
          break;
        case 6:
          returnValue = tr.calendar.iconCalendarDescription.sessionEvent;
          break;
        case 7:
          returnValue = tr.calendar.iconCalendarDescription.sessionOffSession;
          break;
        case 8:
          returnValue = tr.calendar.iconCalendarDescription.todo;
          break;
        default:
          returnValue = tr.calendar.iconCalendarDescription.null;
          break;
      }
    } else {
      switch (dayType) {
        case 'null':
          returnValue = tr.calendar.iconCalendarDescription.null;
          break;
        case 'Session':
          returnValue = tr.calendar.iconCalendarDescription.session;
          break;
        case 'Off-Session':
          returnValue = tr.calendar.iconCalendarDescription.offSession;
          break;
        case 'Off-Season':
          returnValue = tr.calendar.iconCalendarDescription.offSeason;
          break;
        case 'Event':
          returnValue = tr.calendar.iconCalendarDescription.event;
          break;
        case 'Off-Session+Event':
          returnValue = tr.calendar.iconCalendarDescription.offSessionEvent;
          break;
        case 'Session+Event':
          returnValue = tr.calendar.iconCalendarDescription.sessionEvent;
          break;
        case 'Session+Off-Session':
          returnValue = tr.calendar.iconCalendarDescription.sessionOffSession;
          break;
        case 'TODO':
          returnValue = tr.calendar.iconCalendarDescription.todo;
          break;
        default:
          returnValue = tr.calendar.iconCalendarDescription.null;
          break;
      }
    }

    return returnValue;
  },
  iconCalendarExplanation: function(client) {
    return tr.calendar.iconCalendarExplanation
        .replace(
            '{EMOJI_NULL}',
            emojiService.getCalendarEmojiPerInt(
                calendarData.switchDayTypeStringToIntValue('null'),
                client,
            ),
        )
        .replace(
            '{EMOJI_SESSION}',
            emojiService.getCalendarEmojiPerInt(
                calendarData.switchDayTypeStringToIntValue('Session'),
                client,
            ),
        )
        .replace(
            '{EMOJI_EVENT}',
            emojiService.getCalendarEmojiPerInt(
                calendarData.switchDayTypeStringToIntValue('Event'),
                client,
            ),
        )
        .replace(
            '{EMOJI_OFFSESSION}',
            emojiService.getCalendarEmojiPerInt(
                calendarData.switchDayTypeStringToIntValue('Off-Session'),
                client,
            ),
        )
        .replace(
            '{EMOJI_OFFSAISON}',
            emojiService.getCalendarEmojiPerInt(
                calendarData.switchDayTypeStringToIntValue('Off-Season'),
                client,
            ),
        )
        .replace(
            '{EMOJI_OFFSESSIONEVENT}',
            emojiService.getCalendarEmojiPerInt(
                calendarData.switchDayTypeStringToIntValue('Off-Session+Event'),
                client,
            ),
        )
        .replace(
            '{EMOJI_SESSIONEVENT}',
            emojiService.getCalendarEmojiPerInt(
                calendarData.switchDayTypeStringToIntValue('Session+Event'),
                client,
            ),
        )
        .replace(
            '{EMOJI_SESSIONOFFSESSION}',
            emojiService.getCalendarEmojiPerInt(
                calendarData.switchDayTypeStringToIntValue('Session+Off-Session'),
                client,
            ),
        )
        .replace(
            '{EMOJI_TODO}',
            emojiService.getCalendarEmojiPerInt(
                calendarData.switchDayTypeStringToIntValue('TODO'),
                client,
            ),
        );
  },
  displayDayEventsOrder: function(dayEventsOrder, client) {
    let displayData = '';

    if (dayEventsOrder.events.length > 0) {
      for (let i = 0; i < dayEventsOrder.events.length; i++) {
        displayData = displayData + tr.calendar.modifyOrderPreview.eventLine
            .replace(
                '{ORDER}',
                dayEventsOrder.events[i].order,
            ).replace(
                '{ID}',
                dayEventsOrder.events[i].id,
            ).replace(
                '{EVENTTITLE}',
                dayEventsOrder.events[i].title,
            ).replace(
                '{EMOJITYPE}',
                emojiService.getCriticalityEmojiPerLevel(
                    parseInt(dayEventsOrder.events[i].level),
                    client,
                ),
            );
      }
    }

    return displayData;
  },
  displayDayMonthData: function(getMonthDaysCalendarData, client) {
    let displayData = '';
    const startDate = moment(moment(getMonthDaysCalendarData[0].date, 'DD/MM/YYYY')
        .subtract(1, 'months')
        .endOf('month').set({hour: 0, minute: 0, second: 0, millisecond: 1})).add(1, 'days');

    const maxDate = moment(moment(getMonthDaysCalendarData[0].date, 'DD/MM/YYYY')).endOf('month');

    if (getMonthDaysCalendarData.length > 0) {
      while (startDate <= maxDate) {
        let isDayFound = false;
        for (let i = 0; i < getMonthDaysCalendarData.length; i++) {
          if (moment(getMonthDaysCalendarData[i].date, 'DD/MM/YYYY').isSame(startDate, 'day')) {
            displayData = displayData + tr.calendar.viewMonthDays.enteredDayLine
                .replace(
                    '{DAY}',
                    getMonthDaysCalendarData[i].date,
                ).replace(
                    '{EMOJITYPE}',
                    emojiService.getCalendarEmojiPerInt(
                        calendarData.switchDayTypeStringToIntValue(getMonthDaysCalendarData[i].type),
                        client,
                    ),
                ).replace(
                    '{EVENTNUMBER}',
                    getMonthDaysCalendarData[i].events.length,
                );

            isDayFound = true;
          }
        }

        if (!isDayFound) {
          displayData = displayData + tr.calendar.viewMonthDays.notEnteredDayLine
              .replace(
                  '{DAY}',
                  startDate.locale('fr').format('DD/MM/YYYY'),
              ).replace(
                  '{EMOJITYPE}',
                  emojiService.getCalendarEmojiPerInt(0, client),
              );
        }

        startDate.add(1, 'days');
      }
    }

    return displayData;
  },
  getEventsFromDate: function(dayCalendarData, client) {
    const fields = [];

    for (let i = 0; i < dayCalendarData.events.length; i++) {
      fields.push(
          {
            name:
                tr.calendar.dayCalendarTemplate.eventTitleLine
                    .replace(
                        '{EMOJI}',
                        emojiService.getCriticalityEmojiPerLevel(parseInt(dayCalendarData.events[i].level), client),
                    )
                    .replace(
                        '{TITLE}',
                        dayCalendarData.events[i].title,
                    )
                    .replace(
                        '{ID}',
                        dayCalendarData.events[i].id,
                    ),
            value: dayCalendarData.events[i].description,
          },
      );
    }
    return fields;
  },
};
