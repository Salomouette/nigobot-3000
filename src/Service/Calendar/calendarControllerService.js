const lodash = require('lodash');
const moment = require('moment');

const calendarData = require('../../Data/calendarData');
const botService = require('../botService');

const frenchTranslation = require('../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
const frenchCommandTranslation = require('../../Data/frenchCommandTranslationData');
const commandTr = frenchCommandTranslation.getFrenchCommandTranslations();
module.exports = {
  formatAddEventPeriodCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 1:
        formatedInput = callInput.split('--Début=')[1];
        formatedInput = formatedInput.split('--')[0];
        break;
      case 2:
        formatedInput = callInput.split('--Fin=')[1];
        formatedInput = formatedInput.split('--')[0];
        break;
      case 3:
        formatedInput = callInput.split('--Titre=')[1];
        formatedInput = formatedInput.split('--')[0];
        break;
      case 4:
        formatedInput = callInput.split('--Description=')[1];
        formatedInput = formatedInput.split('--')[0];
        break;
      case 5:
        formatedInput = callInput.split('--Criticité=')[1];
        break;

      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/(\r\n|\n|\r)/gm, '') : formatedInput;
    }
    return null;
  },
  formatAddPeriodCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 1:
        formatedInput = callInput.split('--Début=')[1];
        formatedInput = formatedInput.split('--')[0];
        break;
      case 2:
        formatedInput = callInput.split('--Fin=')[1];
        formatedInput = formatedInput.split('--')[0];
        break;
      case 3:
        formatedInput = callInput.split('--Type=')[1];
        formatedInput = formatedInput.split('--')[0];
        break;
      case 4:
        formatedInput = callInput.split('--Titre=')[1];
        formatedInput = formatedInput.split('--')[0];
        break;
      case 5:
        formatedInput = callInput.split('--Description=')[1];
        formatedInput = formatedInput.split('--')[0];
        break;
      case 6:
        formatedInput = callInput.split('--Criticité=')[1];
        break;

      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/(\r\n|\n|\r)/gm, '') : formatedInput;
    }
    return null;
  },
  formatAddEventCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 1:
        formatedInput = callInput.split('--Date=')[1];
        formatedInput = formatedInput.split('--Titre=')[0];
        break;
      case 2:
        formatedInput = callInput.split('--Titre=')[1];
        formatedInput = formatedInput.split('--Description=')[0];
        break;
      case 3:
        formatedInput = callInput.split('--Description=')[1];
        formatedInput = formatedInput.split('--Criticité=')[0];
        break;
      case 4:
        formatedInput = callInput.split('--Criticité=')[1];
        break;

      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/(\r\n|\n|\r)/gm, '') : formatedInput;
    }
    return null;
  },
  formatDayCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 1:
        formatedInput = callInput.split('--Date=')[1];
        formatedInput = formatedInput.split('--Type=')[0];
        break;
      case 2:
        formatedInput = callInput.split('--Type=')[1];
        break;
      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/(\r\n|\n|\r)/gm, '') : formatedInput;
    }
    return null;
  },
  modifyOrderEventsDate: function(formatedInput) {
    const allCalendarData = calendarData.getAllCalendarData();
    let day = '';

    // Data are preformated and securised
    for (let i = 0; i < allCalendarData.calendar.length; i++) {
      if (allCalendarData.calendar[i].date === formatedInput.date) {
        for (let j = 0; j < allCalendarData.calendar[i].events.length; j++) {
          for (let k = 0; k < formatedInput.order.length; k++) {
            if (allCalendarData.calendar[i].events[j].id === formatedInput.order[k].id) {
              allCalendarData.calendar[i].events[j].order = formatedInput.order[k].order;
              day = allCalendarData.calendar[i];
            }
          }
        }
      }
    }

    botService.jsonWriteFile(allCalendarData, 'calendar');
    return day;
  },
  formatModifyOrderCommandInput: function(callInput) {
    let inputs = {
      date: '',
      order: [],
    };
    let error = '';

    const dateCommandLineBlank = botService.removeSpaceAndEnterKey(
        commandTr.calendar.modifyOrderTemplate.dayLine.replace('{DAY}', ''),
    );
    let date = callInput.split(dateCommandLineBlank)[1];
    date = botService.removeSpaceAndEnterKey(date.split('--')[0]);
    const dayCalendarData = calendarData.getDayCalendarData(date);

    const argsNumber = callInput.split('--').length - 2;
    // Check if date exist
    if (typeof dayCalendarData === 'object') {
      // Check if number of args is equal to number of events
      if (argsNumber === dayCalendarData.events.length) {
        for (let i = 1; i < (argsNumber + 1); i++) {
          const argLine = botService.removeSpaceAndEnterKey(
              commandTr.calendar.modifyOrderTemplate.eventLine.replace('{ORDER}', i).replace('{ID}', ''),
          );

          // Check if command args are exact
          if (callInput.split(argLine).length === 2) {
            let idInput = callInput.split(argLine)[1];
            idInput = parseInt(botService.removeSpaceAndEnterKey(
                idInput.split('--')[0],
            ));

            // Check if ID is between 0 and number of events
            if (idInput > 0 && idInput <= dayCalendarData.events.length) {
              // Check for dupes ID in arguments
              if (lodash.find(inputs.order, {id: idInput}) === undefined) {
                inputs.date = dayCalendarData.date;
                inputs.order.push(
                    {
                      order: i,
                      id: idInput,
                    },
                );
              } else {
                error = tr.error.invalidOrBadArgument;
              }
            } else {
              error = tr.error.invalidOrBadArgument;
            }
          } else {
            error = tr.error.invalidOrBadArgument;
          }
        }
      } else {
        error = tr.error.invalidOrBadArgument;
      }
    } else {
      error = tr.error.invalidOrBadArgument;
    }

    if (error.length !== 0) {
      inputs = error;
    }

    return inputs;
  },
  formatModifyDayCommandInput: function(inputCommandPlaceNumber, callInput) {
    let formatedInput = '';
    inputCommandPlaceNumber = parseInt(inputCommandPlaceNumber);
    switch (inputCommandPlaceNumber) {
      case 1:
        formatedInput = callInput.split('--Date=')[1];
        formatedInput = formatedInput.split('--Id=')[0];
        break;
      case 2:
        formatedInput = callInput.split('--Id=')[1];
        break;
      default:
        formatedInput = 'error';
        break;
    }
    if (formatedInput !== undefined) {
      return formatedInput.length !== 0 ? formatedInput.replace(/(\r\n|\n|\r)/gm, '') : formatedInput;
    }
    return null;
  },
  modifyEventsDayOrderGenerateCommand: function(day) {
    const dayCalendarData = calendarData.displayEventsDaysOrderCalendarData(day);
    let returnString = '';

    if (typeof dayCalendarData === 'string' && dayCalendarData.length > 0) {
      returnString = dayCalendarData;
    } else {
      if (dayCalendarData.events.length > 0) {
        returnString = 'nb!calendrier modifierOrdreJour \n';
        returnString = returnString + commandTr.calendar.modifyOrderTemplate.dayLine
            .replace(
                '{DAY}',
                dayCalendarData.date,
            );
        for (let i = 0; i < dayCalendarData.events.length; i++) {
          returnString = returnString + commandTr.calendar.modifyOrderTemplate.eventLine
              .replace(
                  '{ORDER}',
                  dayCalendarData.events[i].order,
              )
              .replace(
                  '{ID}',
                  dayCalendarData.events[i].id,
              );
        }
      }
    }

    return (returnString);
  },
  modifyEventDateGenerateCommand: function(day, id) {
    const dayCalendarData = calendarData.getEventFromDayAndIdCalendarData(day, parseInt(id));
    let returnString = '';

    // Check if calendarData is not empty
    if (typeof dayCalendarData === 'string' && dayCalendarData.length > 0) {
      returnString = dayCalendarData;
    } else {
      if (typeof dayCalendarData.title === 'string') {
        returnString = 'nb!calendrier modifierEvenement \n';
        returnString = returnString + '--Date=' + day + '\n';
        returnString = returnString + '--Id=' + id + '\n';
        returnString = returnString + '--Criticité=' + dayCalendarData.level + '\n';
        returnString = returnString + '--Titre=' + dayCalendarData.title + '\n';
        returnString = returnString + '--Description=' + dayCalendarData.description + '\n';
      } else {
        returnString = tr.error.invalidDayOrId;
      }
    }

    return returnString;
  },
  getDataAndModifyDateCommand: function(input) {
    const allCalendarData = calendarData.getAllCalendarData();
    let returnDay = 'invalidDate';

    if (
      input.includes('--Date=') &&
        input.includes('--Id=') &&
        input.includes('--Criticité=') &&
        input.includes('--Titre=') &&
        input.includes('--Description=')
    ) {
      const date = botService.removeSpaceAndEnterKey((input.split('--Date=')[1]).split('--Id=')[0]);
      const id = parseInt(botService.removeSpaceAndEnterKey((input.split('--Id=')[1]).split('--Criticité=')[0]));
      const level = botService.removeSpaceAndEnterKey((input.split('--Criticité=')[1]).split('--Titre=')[0]);
      const title = (input.split('--Titre=')[1]).split('--Description=')[0];
      const description = input.split('--Description=')[1];

      // check if json is not empty
      if (allCalendarData.calendar.length > 0) {
        for (let i = 0; i < allCalendarData.calendar.length; i++) {
          if (allCalendarData.calendar[i].date === date) {
            for (let j = 0; j < allCalendarData.calendar[i].events.length; j++) {
              if (allCalendarData.calendar[i].events[j].id === id) {
                allCalendarData.calendar[i].events[j].level = level;
                allCalendarData.calendar[i].events[j].title = title;
                allCalendarData.calendar[i].events[j].description = description;

                returnDay = allCalendarData.calendar[i].events[j];
                returnDay.date = date;
              }
            }
          }
        }
      } else {
        returnDay = 'calendarNotInitialized';
      }
    } else {
      returnDay = 'missingArgs';
    }


    botService.jsonWriteFile(allCalendarData, 'calendar');
    return returnDay;
  },
  addEventPeriod: function(enteredStartDate, enteredEndDate, title, description, level) {
    const allCalendarData = calendarData.getAllCalendarData();
    const start = moment(enteredStartDate, 'DD/MM/YYYY');
    const end = moment(enteredEndDate, 'DD/MM/YYYY');
    const incrementDate = start;
    let nbDaySaved = 0;

    while (incrementDate <= end) {
      let isDayFound = false;
      for (let i = 0; i < allCalendarData.calendar.length; i++) {
        if (
          (incrementDate.isAfter(start) || incrementDate.isSame(start)) &&
            (incrementDate.isBefore(end) || incrementDate.isSame(end))
        ) {
          if (moment(allCalendarData.calendar[i].date, 'DD/MM/YYYY').isSame(incrementDate, 'day')) {
            isDayFound = true;
            allCalendarData.calendar[i].events.push(
                {
                  id: allCalendarData.calendar[i].events.length + 1,
                  level: level,
                  title: title,
                  description: description,
                  order: allCalendarData.calendar[i].events.length + 1,
                },
            );
          }
        }
      }

      if (!isDayFound) {
        allCalendarData.calendar.push(
            {
              date: incrementDate.format('DD/MM/YYYY'),
              type: 'TODO',
              actualDate: false,
              events: [
                {
                  id: 1,
                  level: level,
                  title: title,
                  description: description,
                  order: 1,
                },
              ],
            },
        );
      }

      nbDaySaved++;
      incrementDate.add(1, 'days');
    }

    botService.jsonWriteFile(allCalendarData, 'calendar');
    return nbDaySaved;
  },
  addPeriod: function(enteredStartDate, enteredEndDate, dayType, title, description, level) {
    const allCalendarData = calendarData.getAllCalendarData();
    enteredStartDate = moment(enteredStartDate, 'DD/MM/YYYY');
    enteredEndDate = moment(enteredEndDate, 'DD/MM/YYYY');
    const incrementDate = enteredStartDate;
    let nbDaySaved = 0;
    let nbDayNotSaved = 0;

    while (incrementDate <= enteredEndDate) {
      let isDayFound = false;
      for (let i = 0; i < allCalendarData.calendar.length; i++) {
        if (
          (incrementDate.isAfter(enteredStartDate) || incrementDate.isSame(enteredStartDate)) &&
            (incrementDate.isBefore(enteredEndDate) || incrementDate.isSame(enteredEndDate))
        ) {
          if (moment(allCalendarData.calendar[i].date, 'DD/MM/YYYY').isSame(incrementDate, 'day')) {
            isDayFound = true;
          }
        }
      }

      if (!isDayFound) {
        nbDaySaved++;
        allCalendarData.calendar.push(
            {
              date: incrementDate.format('DD/MM/YYYY'),
              type: dayType,
              actualDate: false,
              events: [
                {
                  id: 1,
                  level: level,
                  title: title,
                  description: description,
                  order: 1,
                },
              ],
            },
        );
      } else {
        nbDayNotSaved++;
      }

      incrementDate.add(1, 'days');
    }

    botService.jsonWriteFile(allCalendarData, 'calendar');
    return {
      nbDaySaved: nbDaySaved,
      nbDayNotSaved: nbDayNotSaved,
    };
  },
  addEventAtDate: function(enteredDate, title, description, level) {
    const allCalendarData = calendarData.getAllCalendarData();
    let day = '';

    for (let i = 0; i < allCalendarData.calendar.length; i++) {
      if (allCalendarData.calendar[i].date === enteredDate) {
        day = allCalendarData.calendar[i];
      }
    }

    if (!day) {
      allCalendarData.calendar.push(
          {
            date: enteredDate,
            type: 'TODO',
            actualDate: false,
            events: [],
          },
      );
      day = allCalendarData.calendar[allCalendarData.calendar.length - 1];
    }

    day.events.push(
        {
          id: parseInt(day.events.length) + 1,
          level: level,
          title: title,
          description: description,
          order: parseInt(day.events.length) + 1,
        },
    );

    botService.jsonWriteFile(allCalendarData, 'calendar');
  },
  changeTypeOfDate: function(enteredDate, typeOfDate) {
    const allCalendarData = calendarData.getAllCalendarData();
    let typeOfDayChanged = false;

    // check if json is not empty
    if (allCalendarData.calendar.length > 0) {
      for (let i = 0; i < allCalendarData.calendar.length; i++) {
        if (allCalendarData.calendar[i].date === enteredDate) {
          allCalendarData.calendar[i].type = typeOfDate;
          typeOfDayChanged = true;
        }
      }
    } else {
      typeOfDayChanged = tr.error.emptyCalendar;
    }
    botService.jsonWriteFile(allCalendarData, 'calendar');
    return typeOfDayChanged;
  },
  addMissingDates: function(enteredDate, allCalendarData) {
    const oldestDate = moment(calendarData.getEndsDate('end', allCalendarData), 'DD/MM/YYYY');
    const newsestDate = moment(calendarData.getEndsDate('start', allCalendarData), 'DD/MM/YYYY');
    const incrementDate = oldestDate;
    let nbDayTODO = 0;

    if (oldestDate !== tr.error.emptyCalendar && newsestDate !== tr.error.emptyCalendar) {
      while (incrementDate <= newsestDate) {
        let isDayFound = false;
        for (let i = 0; i < allCalendarData.calendar.length; i++) {
          // Check if between period ends
          if (
            (incrementDate.isAfter(oldestDate) || incrementDate.isSame(oldestDate)) &&
              (incrementDate.isBefore(newsestDate) || incrementDate.isSame(newsestDate))
          ) {
            // Check if day exist in data
            if (moment(allCalendarData.calendar[i].date, 'DD/MM/YYYY').isSame(incrementDate, 'day')) {
              isDayFound = true;
            }
          }
        }

        if (!isDayFound) {
          nbDayTODO++;
          allCalendarData.calendar.push(
              {
                date: incrementDate.format('DD/MM/YYYY'),
                type: calendarData.switchDayTypeIntToStringValue(8),
                actualDate: false,
                events: [],
              },
          );
        }
        incrementDate.add(1, 'days');
      }
    }

    return {
      data: allCalendarData,
      nbDayTODO: nbDayTODO,
    };
  },
  // Return an object with:
  // - data: allCalendarData
  // - nbDayTODO: Number of days added in TODO Status
  // - dayStatus: String that defines if the current day is in the past or not.
  changeCalendarRPGDate: function(enteredDate, typeOfDate) {
    const allCalendarData = calendarData.getAllCalendarData();
    let returnString = 'newDay';

    for (let i = 0; i < allCalendarData.calendar.length; i++) {
      // Check if date already entered
      if (allCalendarData.calendar[i].date === enteredDate) {
        allCalendarData.calendar[i].actualDate = true;
        returnString = 'oldDay';
      } else {
        allCalendarData.calendar[i].actualDate = false;
      }
    }

    if (returnString === 'newDay') {
      allCalendarData.calendar.push(
          {
            date: enteredDate,
            type: typeOfDate,
            actualDate: true,
            events: [],
          },
      );
    }

    const missingDatesReturn = this.addMissingDates(enteredDate, allCalendarData);
    botService.jsonWriteFile(missingDatesReturn.data, 'calendar');
    missingDatesReturn.dayStatus = returnString;
    return missingDatesReturn;
  },
};
