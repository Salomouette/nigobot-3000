const craftControllerService = require('../Service/Craft/craftControllerService');
const messageService = require('../Service/messageService');

const diceThrowValidateTemplate = require('../Assets/EmbedMessage/Dice/diceThrowValidateTemplate');
const craftHelpTemplate = require('../Assets/EmbedMessage/Craft/craftHelpTemplate');
const craftCreationCommandTemplate = require('../Assets/EmbedMessage/Craft/craftCreationCommandTemplate');
const craftModuleRecipeShowTemplate = require('../Assets/EmbedMessage/Craft/craftModuleRecipeShowTemplate');
const craftViewDiscoveredRecipesTemplate = require('../Assets/EmbedMessage/Craft/craftViewDiscoveredRecipesTemplate');

const logService = require('../Service/logService');

module.exports = {
  craftController: function(msg, guild, client) {
    if (msg.content.split(' ')[1] === 'aide') {
      messageService.sendPrivateMessage(msg, craftHelpTemplate.craftHelpTemplate(), true);

      logService.sendLogMessage(
          guild,
          'craft:help',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'craftTemplate') {
      messageService.sendEmbedMessage(msg.channel, craftCreationCommandTemplate.craftCreationCommandTemplate(), true);

      logService.sendLogMessage(
          guild,
          'craft:creationTemplate',
          msg.author.username,
      );
    }
    if ((msg.content.split(' ')[1]).includes('fabriquer')) {
      const componentInput1 = craftControllerService.formatCraftCommandInputCreate(
          1,
          msg.content,
      );
      const componentInput2 = craftControllerService.formatCraftCommandInputCreate(
          2,
          msg.content,
      );
      const componentInput3 = craftControllerService.formatCraftCommandInputCreate(
          3,
          msg.content,
      );
      const componentInput4 = craftControllerService.formatCraftCommandInputCreate(
          4,
          msg.content,
      );

      const caracteristicInput = craftControllerService.formatCraftCommandInputCreate(
          5,
          msg.content,
      );

      let skillInput = 0;
      if (msg.content.includes('NivCompétence')) {
        skillInput = craftControllerService.formatCraftCommandInputCreate(
            6,
            msg.content,
        );
      }

      let modificatorInput = 0;
      if (msg.content.includes('Modificateur')) {
        modificatorInput = craftControllerService.formatCraftCommandInputCreate(
            7,
            msg.content,
        );
      }

      const templateDiceResult = diceThrowValidateTemplate.diceThrowValidateTemplate(
          caracteristicInput,
          skillInput,
          modificatorInput,
          msg.author.id,
      );
      messageService.sendEmbedMessageWithImages(
          msg.channel,
          templateDiceResult.embedMessage,
          templateDiceResult.images,
          true,
      );

      const foundRecipe = craftControllerService.findCompatibleRecipe(
          componentInput1,
          componentInput2,
          componentInput3,
          componentInput4,
          templateDiceResult.embedMessage.description.includes('Votre jet est valide!'),
          skillInput,
      );

      // Send message
      const embedCraftResult = craftControllerService.switchCraftResultView(foundRecipe, msg);

      logService.sendLogMessage(
          guild,
          'craft:creation',
          msg.author.username,
          [
            {
              fieldName: 'Nom composant 1',
              value: componentInput1,
            },
            {
              fieldName: 'Nom composant 2',
              value: componentInput2,
            },
            {
              fieldName: 'Nom composant 3',
              value: componentInput3,
            },
            {
              fieldName: 'Nom composant 4',
              value: componentInput4,
            },
            {
              fieldName: 'Caractérisrique',
              value: caracteristicInput,
            },
            {
              fieldName: 'Compétence',
              value: skillInput.length > 0 ? skillInput : 'Non renseigné',
            },
            {
              fieldName: 'Modificateur',
              value: modificatorInput.length > 0 ? modificatorInput : 'Non renseigné',
            },
          ],
          embedCraftResult.embedMessage,
      );
    }
    if (msg.content.split(' ')[1] === 'découvert') {
      messageService.sendEmbedMessage(
          msg.channel,
          craftViewDiscoveredRecipesTemplate.craftViewDiscoveredRecipesTemplate(client),
          true,
      );

      logService.sendLogMessage(
          guild,
          'craft:discoveredRecipes',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'voir') {
      const callInput = msg.content.split('voir ')[1];
      const embed = craftModuleRecipeShowTemplate.craftModuleRecipeShowTemplate(callInput);
      messageService.sendEmbedMessageWithImages(msg.channel, embed.embedMessage, embed.images, true);

      logService.sendLogMessage(
          guild,
          'craft:creation',
          msg.author.username,
          [
            {
              fieldName: 'Nom recette',
              value: callInput,
            },
          ],
          embed.embedMessage,
      );
    }
  },
};
