const logService = require('../Service/logService');
const trainerControllerService = require('../Service/Trainer/trainerControllerService');
const messageService = require('../Service/messageService');

const trainersGeneratorTemplate = require('../Assets/EmbedMessage/Trainers/trainersGeneratorTemplate');
const trainersHelpTemplate = require('../Assets/EmbedMessage/Trainers/trainersHelpTemplate');
const trainersListTemplate = require('../Assets/EmbedMessage/Trainers/trainersListTemplate');
const pastTrainersListTemplate = require('../Assets/EmbedMessage/Trainers/pastTrainersListTemplate');
const pastTrainersTemplate = require('../Assets/EmbedMessage/Trainers/pastTrainersTemplate');
module.exports = {
  trainerController: function(msg, guild, client) {
    if (msg.content.split(' ')[1] === 'aide') {
      messageService.sendPrivateMessage(msg, trainersHelpTemplate.trainersHelpTemplate(guild, client), true);

      logService.sendLogMessage(
          guild,
          'trainers:help',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'générer') {
      const trainer = trainerControllerService.formatTrainerCommandInput(1, msg.content);
      const nbPokémon = trainerControllerService.formatTrainerCommandInput(2, msg.content);
      const classe = trainerControllerService.formatTrainerCommandInput(3, msg.content);
      const badges = trainerControllerService.formatTrainerCommandInput(4, msg.content);
      const master = trainerControllerService.formatTrainerCommandInput(5, msg.content);
      const biome = 'None';
      const embedTemplate = trainersGeneratorTemplate.trainersGeneratorTemplate(
          trainer,
          classe,
          nbPokémon,
          badges,
          master,
          biome,
      );
      messageService.sendEmbedMessageWithImages(msg.channel, embedTemplate.embedMessage, embedTemplate.images, true);

      logService.sendLogMessage(
          guild,
          'trainers:generate',
          msg.author.username,
          [
            {
              fieldName: 'Type',
              value: trainer,
            },
            {
              fieldName: 'Nombre de Pokémon',
              value: nbPokémon,
            },
            {
              fieldName: 'Classe',
              value: classe,
            },
            {
              fieldName: 'Avancée',
              value: badges,
            },
            {
              fieldName: 'Catégorie',
              value: master,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'générerParBiome') {
      const trainer = 'None';
      const nbPokémon = trainerControllerService.formatTrainerCommandInput(2, msg.content);
      const classe = trainerControllerService.formatTrainerCommandInput(3, msg.content);
      const badges = trainerControllerService.formatTrainerCommandInput(4, msg.content);
      const master = trainerControllerService.formatTrainerCommandInput(5, msg.content);
      const biome = trainerControllerService.formatTrainerCommandInput(6, msg.content);
      const embedTemplate = trainersGeneratorTemplate.trainersGeneratorTemplate(
          trainer,
          classe,
          nbPokémon,
          badges,
          master,
          biome,
      );
      messageService.sendEmbedMessageWithImages(msg.channel, embedTemplate.embedMessage, embedTemplate.images, true);

      logService.sendLogMessage(
          guild,
          'trainers:generate',
          msg.author.username,
          [
            {
              fieldName: 'Biome',
              value: biome,
            },
            {
              fieldName: 'Nombre de Pokémon',
              value: nbPokémon,
            },
            {
              fieldName: 'Classe',
              value: classe,
            },
            {
              fieldName: 'Avancée',
              value: badges,
            },
            {
              fieldName: 'Catégorie',
              value: master,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'listeClasseDresseur') {
      messageService.sendEmbedMessage(
          msg.channel,
          trainersListTemplate.trainersListTemplate(guild, client),
          true,
      );

      logService.sendLogMessage(
          guild,
          'trainers:list',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'memoryDex') {
      messageService.sendEmbedMessage(
          msg.channel,
          pastTrainersListTemplate.pastTrainersListTemplate(guild, client),
          true,
      );

      logService.sendLogMessage(
          guild,
          'trainers:list',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'voirDresseur') {
      const id = msg.content.split(' ')[2];
      const embedTemplate = pastTrainersTemplate.pastTrainersTemplate(id);
      messageService.sendEmbedMessageWithImages(msg.channel, embedTemplate.embedMessage, embedTemplate.images, true);

      logService.sendLogMessage(
          guild,
          'trainers:list',
          msg.author.username,
      );
    }
  },

};
