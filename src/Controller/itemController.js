const itemControllerService = require('../Service/Item/itemControllerService');
const logService = require('../Service/logService');
const messageService = require('../Service/messageService');
const playerService = require('../Service/playerService');

const blankTemplate = require('../Assets/EmbedMessage/blankTemplate');
const itemHelpTemplate = require('../Assets/EmbedMessage/Item/itemHelpTemplate');
const itemViewTemplate = require('../Assets/EmbedMessage/Item/itemViewTemplate');
const itemDefaultTemplate = require('../Assets/EmbedMessage/Item/itemDefaultTemplate');
const itemGenerateFoundItemTemplate = require('../Assets/EmbedMessage/Item/itemGenerateFoundItemTemplate');

const frenchTranslation = require('../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  itemController: function(msg, guild, client) {
    if (msg.content.split(' ')[1] === 'aide') {
      messageService.sendPrivateMessage(msg, itemHelpTemplate.itemHelpTemplate(), true);

      logService.sendLogMessage(
          guild,
          'item:help',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'voir') {
      const callInput = msg.content.split('voir ')[1];
      const itemName = itemControllerService.formatCommandInput(callInput);
      const embedsMessages = itemViewTemplate.itemViewTemplate(itemName, client);

      for (let i = 0; i < embedsMessages.length; i++) {
        messageService.sendEmbedMessageWithImages(
            msg.channel,
            embedsMessages[i].embedMessage,
            embedsMessages[i].images,
            true,
        );
      }

      logService.sendLogMessage(
          guild,
          'item:view',
          msg.author.username,
          [
            {
              fieldName: 'Nom Objet',
              value: itemName,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'voirSousCatégorie') {
      let callInput = msg.content.split('voirSousCatégorie ')[1];
      callInput = itemControllerService.formatCommandInput(callInput);
      const subCategoryItemsNames = itemControllerService.formatSubCategoryView(callInput);
      messageService.sendEmbedMessage(
          msg.channel,
          itemDefaultTemplate.itemDefaultTemplate(subCategoryItemsNames),
          true,
      );

      logService.sendLogMessage(
          guild,
          'item:subcategory',
          msg.author.username,
          [
            {
              fieldName: 'Nom Sous Catégorie',
              value: callInput,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'voirCatégorie') {
      let callInput = msg.content.split('voirCatégorie ')[1];
      callInput = itemControllerService.formatCommandInput(callInput);
      const subCategories = itemControllerService.formatSubCategoriesView(callInput);
      messageService.sendEmbedMessage(msg.channel, itemDefaultTemplate.itemDefaultTemplate(subCategories), true);

      logService.sendLogMessage(
          guild,
          'item:category',
          msg.author.username,
          [
            {
              fieldName: 'Nom Catégorie',
              value: callInput,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'voirCatégories') {
      const categories = itemControllerService.formatCategoriesView();
      messageService.sendEmbedMessage(msg.channel, itemDefaultTemplate.itemDefaultTemplate(categories), true);

      logService.sendLogMessage(
          guild,
          'item:categories',
          msg.author.username,
      );
    }

    if (playerService.checkIfAdminUser(msg.author.id)) {
      if (msg.content.split(' ')[1] === 'générerObjetTrouvé') {
        const playerID = msg.content.split('--IDJoueur=')[1];
        messageService.sendEmbedMessage(
            msg.channel,
            itemGenerateFoundItemTemplate.itemGenerateFoundItemTemplate(playerID, client),
            true,
        );

        logService.sendLogMessage(
            guild,
            'biome:biomes',
            msg.author.username,
        );
      }
    } else {
      messageService.sendEmbedMessage(
          msg.channel,
          blankTemplate.blankTemplate(
              tr.error.notAdminUser,
              'system',
          ),
          true,
      );
    }
  },
};
