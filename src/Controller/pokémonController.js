const pokémonControllerService = require('../Service/Pokémon/pokémonControllerService');
const pokémonViewService = require('../Service/Pokémon/pokémonViewService');
const logService = require('../Service/logService');
const playerService = require('../Service/playerService');
const messageService = require('../Service/messageService');

const pokémonData = require('../Data/pokémonData');

const pokémonHelpTemplate = require('../Assets/EmbedMessage/Pokémon/pokémonHelpTemplate');
const pokémonPokédexTemplate = require('../Assets/EmbedMessage/Pokémon/pokémonPokédexTemplate');
const pokémonGenerateTemplate = require('../Assets/EmbedMessage/Pokémon/pokémonGenerateTemplate');
const pokémonSheetTemplate = require('../Assets/EmbedMessage/Pokémon/pokémonSheetTemplate');
const playerPokémonListTemplate = require('../Assets/EmbedMessage/Pokémon/playerPokémonListTemplate');
const pokémonLevelUpTemplate = require('../Assets/EmbedMessage/Pokémon/pokémonLevelAndFriendshipUpTemplate');
const pokémonMoveChangeTemplate = require('../Assets/EmbedMessage/Pokémon/pokémonMoveChangeTemplate');
const pokémonNameChangeTemplate = require('../Assets/EmbedMessage/Pokémon/pokémonNameChangeTemplate');
const pokémonEvolutionTemplate = require('../Assets/EmbedMessage/Pokémon/pokémonEvolutionTemplate');
const notAdminTemplate = require('../Assets/EmbedMessage/notAdminTemplate');
const pokémonCatchTemplate = require('../Assets/EmbedMessage/Pokémon/pokémonCatchTemplate');
const trainersPokémonHelpTemplate = require('../Assets/EmbedMessage/Pokémon/trainersPokémonHelpTemplate');
const trainersPokémonAdminHelpTemplate = require('../Assets/EmbedMessage/Pokémon/trainersPokémonAdminHelpTemplate');
const pokémonNotesChangeTemplate = require('../Assets/EmbedMessage/Pokémon/pokémonNotesChangeTemplate');
module.exports = {
  pokémonController: function(msg, guild, client) {
    if (msg.content.split(' ')[1] === 'aide') {
      messageService.sendPrivateMessage(msg, trainersPokémonHelpTemplate.trainersPokémonHelpTemplate(), true);
      messageService.sendPrivateMessage(msg, pokémonHelpTemplate.pokémonHelpTemplate(), true);

      if (playerService.checkIfAdminUser(msg.author.id)) {
        messageService.sendPrivateMessage(
            msg,
            trainersPokémonAdminHelpTemplate.trainersPokémonAdminHelpTemplate(),
            true,
        );

        logService.sendLogMessage(
            guild,
            'pokémon:trainersHelpAdmin',
            msg.author.username,
        );
      }

      logService.sendLogMessage(
          guild,
          'pokémon:help',
          msg.author.username,
      );
      logService.sendLogMessage(
          guild,
          'pokémon:trainersHelp',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'pokédex') {
      const pokémonName = pokémonControllerService.formatPokedexCommandInput(1, msg.content);
      const pokémonForm = pokémonControllerService.formatPokedexCommandInput(2, msg.content);
      const pokémonForm2 = pokémonControllerService.formatPokedexCommandInput(3, msg.content);
      const pokémon = pokémonData.getPokémonFromPokémonNameandForm(
          pokémonName,
          pokémonForm,
          pokémonForm2,
      );
      const embed = pokémonPokédexTemplate.pokémonPokédexTemplate(pokémon, client);
      messageService.sendEmbedMessageWithImages(msg.channel, embed.embedMessage, embed.images, true);

      logService.sendLogMessage(
          guild,
          'pokémon:view',
          msg.author.username,
          [
            {
              fieldName: 'Nom Pokémon',
              value: pokémonName,
            },
            {
              fieldName: 'Forme Pokémon',
              value: pokémonForm,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'générer') {
      const biome =
          msg.content.includes('--Biome') ?
          pokémonControllerService.formatGenerateCommandInput(1, msg.content) :
          '';
      const rarity =
          msg.content.includes('--Biome') ?
              pokémonControllerService.formatGenerateCommandInput(2, msg.content) :
              '';
      const pokémonName =
          msg.content.includes('--Pokémon') ?
              pokémonControllerService.formatGenerateCommandInput(6, msg.content) :
              '';
      const pokémonForm =
          msg.content.includes('--Pokémon') ?
              pokémonControllerService.formatGenerateCommandInput(7, msg.content) :
              '';
      const shine = pokémonControllerService.formatGenerateCommandInput(3, msg.content);
      const alpha = pokémonControllerService.formatGenerateCommandInput(4, msg.content);
      const size = pokémonControllerService.formatGenerateCommandInput(5, msg.content);
      const tera = pokémonControllerService.formatGenerateCommandInput(8, msg.content);
      const titan = pokémonControllerService.formatGenerateCommandInput(9, msg.content);
      const antique = pokémonControllerService.formatGenerateCommandInput(10, msg.content);
      const pokémon = msg.content.includes('--Pokémon') ?
        pokémonData.getPokémonFromPokémonNameandForm(
            pokémonName,
            pokémonForm,
            null,
        ) :
        '';
      const templateResult = pokémonGenerateTemplate.pokémonGenerateTemplate(
          biome,
          rarity,
          pokémon,
          size,
          shine,
          alpha,
          tera,
          titan,
          antique,
          client,
      );
      messageService.sendEmbedMessageWithImages(msg.channel, templateResult.embedMessage, templateResult.images, true);

      logService.sendLogMessage(
          guild,
          'pokémon:generation',
          msg.author.username,
          [
            {
              fieldName: 'Biome',
              value: biome,
            },
            {
              fieldName: 'Rareté',
              value: rarity,
            },
            {
              fieldName: 'Pokémon',
              value: pokémonName,
            },
            {
              fieldName: 'Forme',
              value: pokémonForm,
            },
            {
              fieldName: 'Charme Chroma',
              value: shine,
            },
            {
              fieldName: 'Charme Alpha',
              value: alpha,
            },
            {
              fieldName: 'Charme Taille',
              value: size,
            },
            {
              fieldName: 'Charme Tera',
              value: tera,
            },
            {
              fieldName: 'Charme Titan',
              value: titan,
            },
            {
              fieldName: 'Charme Antique',
              value: antique,
            },
          ],
          templateResult.embedMessage,
      );
    }
    if (msg.content.split(' ')[1] === 'voirMonPokémon') {
      const callInput = msg.content.split(' ')[2];
      const trainerPokémon = pokémonControllerService.getPlayerPokémonByID(
          msg.author.id,
          callInput,
      );
      const pokémonFoundItem = pokémonControllerService.getPlayerPokémonFoundItemsByIds(
          msg.author.id,
          callInput,
      );

      const templateResult = pokémonSheetTemplate.pokémonSheetTemplate(trainerPokémon, pokémonFoundItem, client);
      messageService.sendEmbedMessageWithImages(msg.channel, templateResult.embedMessage, templateResult.images, true);

      logService.sendLogMessage(
          guild,
          'pokémon:seePokémon',
          msg.author.username,
          [
            {
              fieldName: 'Id Joueur',
              value: msg.author.id,
            },
            {
              fieldName: 'Id Pokémon',
              value: callInput,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'voirMesPokémon') {
      const trainerPokémon = pokémonViewService.getTrainersAllPokémon(msg);
      const templateResult = playerPokémonListTemplate.playerPokémonListTemplate(trainerPokémon, msg);
      messageService.sendPrivateMessage(msg, templateResult, true);

      logService.sendLogMessage(
          guild,
          'pokémon:seePokémonPlayer',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'checkPokémonOfPlayer') {
      if (playerService.checkIfAdminUser(msg.author.id)) {
        const idInput = msg.content.split(' ')[2];
        const pokemonInput = msg.content.split(' ')[3];
        const trainerPokémon = pokémonControllerService.getPlayerPokémonByID(
            idInput,
            pokemonInput,
        );
        const pokémonFoundItem = pokémonControllerService.getPlayerPokémonFoundItemsByIds(
            idInput,
            pokemonInput,
        );

        const templateResult = pokémonSheetTemplate.pokémonSheetTemplate(trainerPokémon, pokémonFoundItem, client);
        messageService.sendEmbedMessageWithImages(
            msg.channel,
            templateResult.embedMessage,
            templateResult.images,
            true,
        );

        logService.sendLogMessage(
            guild,
            'pokémon:seePokémon',
            msg.author.username,
            [
              {
                fieldName: 'Id Joueur',
                value: idInput,
              },
              {
                fieldName: 'Id Pokémon',
                value: pokemonInput,
              },
            ],
        );
      } else {
        const templateResult = notAdminTemplate.notAdminTemplate();
        messageService.sendEmbedMessage(msg.channel, templateResult, true);
      }
    }

    if (msg.content.split(' ')[1] === 'addLevel') {
      msg.channel.sendTyping();
      if (playerService.checkIfAdminUser(msg.author.id)) {
        const under20 = pokémonControllerService.formatLevelCommandInput(0, msg.content);
        const under40 = pokémonControllerService.formatLevelCommandInput(1, msg.content);
        const under60 = pokémonControllerService.formatLevelCommandInput(2, msg.content);
        const under80 = pokémonControllerService.formatLevelCommandInput(3, msg.content);
        const under100 = pokémonControllerService.formatLevelCommandInput(4, msg.content);

        const returnJsonEmbeds = pokémonLevelUpTemplate.pokémonLevelAndFriendshipUpTemplate(
            'level',
            under20,
            under40,
            under60,
            under80,
            under100,
        );

        pokémonControllerService.sendMessagesLevelAndFriendshipUpdates(client, returnJsonEmbeds, msg);

        logService.sendLogMessage(
            guild,
            'pokémon:addLevel',
            msg.author.username,
            [
              {
                fieldName: 'Sous 20',
                value: under20,
              },
              {
                fieldName: 'Sous 40',
                value: under40,
              },
              {
                fieldName: 'Sous 60',
                value: under60,
              },
              {
                fieldName: 'Sous 80',
                value: under80,
              },
              {
                fieldName: 'Sous 100',
                value: under100,
              },
            ],
        );
      } else {
        const templateResult = notAdminTemplate.notAdminTemplate();
        messageService.sendEmbedMessage(msg.channel, templateResult, true);
      }
    }

    if (msg.content.split(' ')[1] === 'addFriendship') {
      msg.channel.sendTyping();
      if (playerService.checkIfAdminUser(msg.author.id)) {
        const returnJsonEmbeds = pokémonLevelUpTemplate.pokémonLevelAndFriendshipUpTemplate('friendship');

        pokémonControllerService.sendMessagesLevelAndFriendshipUpdates(client, returnJsonEmbeds, msg);

        logService.sendLogMessage(
            guild,
            'pokémon:addFriendship',
            msg.author.username,
            [
              {
                fieldName: 'Changer amitié',
                value: 'Changement amitié',
              },
            ],
        );
      } else {
        const templateResult = notAdminTemplate.notAdminTemplate();
        messageService.sendEmbedMessage(msg.channel, templateResult, true);
      }
    }

    if (msg.content.split(' ')[1] === 'givePokémonToPlayer') {
      msg.channel.sendTyping();
      if (playerService.checkIfAdminUser(msg.author.id)) {
        const idTrainerInput = pokémonControllerService.formatCatchCommandInput(0, msg.content);
        const pokemonIdInput = pokémonControllerService.formatCatchCommandInput(1, msg.content);
        const levelInput = pokémonControllerService.formatCatchCommandInput(2, msg.content);
        const pokeballInput = pokémonControllerService.formatCatchCommandInput(3, msg.content);

        const templateResult = pokémonCatchTemplate.pokémonCatchTemplate(
            idTrainerInput,
            pokemonIdInput,
            levelInput,
            pokeballInput,
        );

        messageService.sendEmbedMessageWithImages(
            msg.channel,
            templateResult.embedMessage,
            templateResult.images,
            true,
        );

        logService.sendLogMessage(
            guild,
            'pokémon:givePokémonToPlayer',
            msg.author.username,
            [
              {
                fieldName: 'Changer amitié',
                value: 'Changement amitié',
              },
            ],
            templateResult,
        );

        logService.sendLogMessage(
            guild,
            'pokémon:givePokémonToPlayer',
            msg.author.username,
            [
              {
                fieldName: 'Id Joueur',
                value: idTrainerInput,
              },
              {
                fieldName: 'Id Pokémon',
                value: pokemonIdInput,
              },
              {
                fieldName: 'Niveau PKMN',
                value: levelInput,
              },
              {
                fieldName: 'Ball',
                value: pokeballInput,
              },
            ],
        );
      } else {
        const templateResult = notAdminTemplate.notAdminTemplate();
        messageService.sendEmbedMessage(msg.channel, templateResult, true);
      }
    }
    if (msg.content.split(' ')[1] === 'changerCapacités') {
      const pokémonID = pokémonControllerService.formatMoveCommandInput(0, msg.content);
      const move1 = pokémonControllerService.formatMoveCommandInput(1, msg.content);
      const move2 = pokémonControllerService.formatMoveCommandInput(2, msg.content);
      const move3 = pokémonControllerService.formatMoveCommandInput(3, msg.content);
      const move4 = pokémonControllerService.formatMoveCommandInput(4, msg.content);

      const templateResult = pokémonMoveChangeTemplate.pokémonMoveChangeTemplate(
          msg.author.id,
          pokémonID,
          move1,
          move2,
          move3,
          move4,
      );
      messageService.sendEmbedMessage(msg.channel, templateResult, true);

      logService.sendLogMessage(
          guild,
          'pokémon:givePokémonToPlayer',
          msg.author.username,
          [
            {
              fieldName: 'Id Joueur',
              value: msg.author.id,
            },
            {
              fieldName: 'Id Pokémon',
              value: pokémonID,
            },
            {
              fieldName: 'Capa 1',
              value: move1,
            },
            {
              fieldName: 'Capa 2',
              value: move2,
            },
            {
              fieldName: 'Capa 3',
              value: move3,
            },
            {
              fieldName: 'Capa 4',
              value: move4,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'changeMove') {
      msg.channel.sendTyping();
      if (playerService.checkIfAdminUser(msg.author.id)) {
        const trainerID = pokémonControllerService.formatMoveCommandInput(5, msg.content);
        const pokémonID = pokémonControllerService.formatMoveCommandInput(0, msg.content);

        const move1 = pokémonControllerService.formatMoveCommandInput(1, msg.content);
        const move2 = pokémonControllerService.formatMoveCommandInput(2, msg.content);
        const move3 = pokémonControllerService.formatMoveCommandInput(3, msg.content);
        const move4 = pokémonControllerService.formatMoveCommandInput(4, msg.content);

        const templateResult = pokémonMoveChangeTemplate.pokémonMoveChangeTemplate(
            trainerID,
            pokémonID,
            move1,
            move2,
            move3,
            move4,
        );
        messageService.sendEmbedMessage(msg.channel, templateResult, true);

        logService.sendLogMessage(
            guild,
            'pokémon:givePokémonToPlayer',
            msg.author.username,
            [
              {
                fieldName: 'Id Joueur',
                value: trainerID,
              },
              {
                fieldName: 'Id Pokémon',
                value: pokémonID,
              },
              {
                fieldName: 'Capa 1',
                value: move1,
              },
              {
                fieldName: 'Capa 2',
                value: move2,
              },
              {
                fieldName: 'Capa 3',
                value: move3,
              },
              {
                fieldName: 'Capa 4',
                value: move4,
              },
            ],
        );
      } else {
        const templateResult = notAdminTemplate.notAdminTemplate();
        messageService.sendEmbedMessage(msg.channel, templateResult, true);
      }
    }

    if (msg.content.split(' ')[1] === 'changerSurnom') {
      const pokémonID = pokémonControllerService.formatNameCommandInput(0, msg.content);
      const nickname = pokémonControllerService.formatNameCommandInput(1, msg.content);
      const templateResult = pokémonNameChangeTemplate.pokémonNameChangeTemplate(msg.author.id, pokémonID, nickname);

      messageService.sendEmbedMessage(msg.channel, templateResult, true);

      logService.sendLogMessage(
          guild,
          'pokémon:changeNickname',
          msg.author.username,
          [
            {
              fieldName: 'Id Pokémon',
              value: pokémonID,
            },
            {
              fieldName: 'Surnom',
              value: nickname,
            },
          ],
      );
    }

    if (msg.content.split(' ')[1] === 'changeNickname') {
      msg.channel.sendTyping();

      if (playerService.checkIfAdminUser(msg.author.id)) {
        const trainerID = pokémonControllerService.formatNameCommandInput(2, msg.content);
        const pokémonID = pokémonControllerService.formatNameCommandInput(0, msg.content);
        const nickname = pokémonControllerService.formatNameCommandInput(1, msg.content);

        const templateResult = pokémonNameChangeTemplate.pokémonNameChangeTemplate(trainerID, pokémonID, nickname);

        messageService.sendEmbedMessage(msg.channel, templateResult, true);

        logService.sendLogMessage(
            guild,
            'pokémon:changeNickname',
            msg.author.username,
            [
              {
                fieldName: 'Id Pokémon',
                value: pokémonID,
              },
              {
                fieldName: 'Surnom',
                value: nickname,
              },
            ],
        );
      } else {
        const templateResult = notAdminTemplate.notAdminTemplate();
        messageService.sendEmbedMessage(msg.channel, templateResult, true);
      }
    }

    if (msg.content.split(' ')[1] === 'evolveOneInto') {
      msg.channel.sendTyping();

      if (playerService.checkIfAdminUser(msg.author.id)) {
        const trainerID = pokémonControllerService.formatEvolutionCommandInput(2, msg.content);
        const pokémonID = pokémonControllerService.formatEvolutionCommandInput(0, msg.content);
        const pokémonToEvInto = pokémonControllerService.formatEvolutionCommandInput(1, msg.content);
        const pokémonFormToEvInto = pokémonControllerService.formatEvolutionCommandInput(3, msg.content);

        const templateResult = pokémonEvolutionTemplate.pokémonEvolutionTemplate(trainerID,
            pokémonID,
            pokémonToEvInto,
            pokémonFormToEvInto);

        messageService.sendEmbedMessageWithImages(
            msg.channel,
            templateResult.embedMessage,
            templateResult.images,
            true,
        );


        logService.sendLogMessage(
            guild,
            'pokémon:evolveOneInto',
            msg.author.username,
            [
              {
                fieldName: 'Id Dresseur',
                value: trainerID,
              },
              {
                fieldName: 'Id Pokémon',
                value: pokémonID,
              },
              {
                fieldName: 'Évolution demandée',
                value: pokémonToEvInto,
              },
              {
                fieldName: 'Forme',
                value: pokémonFormToEvInto,
              },
            ],
        );
      } else {
        const templateResult = notAdminTemplate.notAdminTemplate();
        messageService.sendEmbedMessage(msg.channel, templateResult, true);
      }
    }

    if (msg.content.split(' ')[1] === 'modèleModifierNotes') {
      const callInput = msg.content.split(' ')[2];
      const templateResult = pokémonNotesChangeTemplate.pokémonNotesChangeTemplate(msg.author.id,
          callInput,
          true);
      messageService.sendEmbedMessage(
          msg.channel,
          templateResult,
          true,
      );
    }

    if (msg.content.split(' ')[1] === 'modifierNotes') {
      const trainerID = msg.author.id;
      const pokemonID = pokémonControllerService.formatNotesCommandInput(1, msg.content);
      const pokemonNotes = [
        pokémonControllerService.formatNotesCommandInput(2, msg.content),
        pokémonControllerService.formatNotesCommandInput(3, msg.content),
        pokémonControllerService.formatNotesCommandInput(4, msg.content),
        pokémonControllerService.formatNotesCommandInput(5, msg.content),
        pokémonControllerService.formatNotesCommandInput(6, msg.content),
        pokémonControllerService.formatNotesCommandInput(7, msg.content),
        pokémonControllerService.formatNotesCommandInput(8, msg.content),
        pokémonControllerService.formatNotesCommandInput(9, msg.content),
        pokémonControllerService.formatNotesCommandInput(10, msg.content),
      ];

      const templateResult = pokémonNotesChangeTemplate.pokémonNotesChangeTemplate(trainerID,
          pokemonID,
          false,
          pokemonNotes);
      messageService.sendEmbedMessage(
          msg.channel,
          templateResult,
          true,
      );

      logService.sendLogMessage(
          guild,
          'pokémon:changeNotes',
          msg.author.username,
          [
            {
              fieldName: 'Id Pokémon',
              value: pokemonID,
            },
            {
              fieldName: 'Notes',
              value: pokemonNotes,
            },
          ],
      );
    }
  },
};
