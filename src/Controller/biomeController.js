const biomeControllerService = require('../Service/Biome/biomeControllerService');
const biomeViewService = require('../Service/Biome/biomeViewService');
const logService = require('../Service/logService');
const messageService = require('../Service/messageService');

const biomeViewTemplate = require('../Assets/EmbedMessage/Biome/biomeViewTemplate');
const biomesViewTemplate = require('../Assets/EmbedMessage/Biome/biomesViewTemplate');
const biomeHelpTemplate = require('../Assets/EmbedMessage/Biome/biomeHelpTemplate');
module.exports = {
  biomeController: function(msg, guild, client) {
    if (msg.content.split(' ')[1] === 'aide') {
      messageService.sendPrivateMessage(msg, biomeHelpTemplate.biomeHelpTemplate(), true);

      logService.sendLogMessage(
          guild,
          'biome:help',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'biomes') {
      messageService.sendEmbedMessage(
          msg.channel,
          biomesViewTemplate.biomesViewTemplate(biomeViewService.displayBiomes()),
          true,
      );

      logService.sendLogMessage(
          guild,
          'biome:biomes',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[msg.content.split(' ').length - 1] === 'voir') {
      const callInput = biomeControllerService.getCallInputBiomeView(msg);
      const templateResult = biomeViewTemplate.biomeViewTemplate(callInput);
      messageService.sendEmbedMessageWithImages(msg.channel, templateResult.embedMessage, templateResult.images, true);

      logService.sendLogMessage(
          guild,
          'biome:biome',
          msg.author.username,
          [
            {
              fieldName: 'biome',
              value: callInput,
            },
          ],
      );
    }
  },
};
