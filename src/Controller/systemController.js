const logService = require('../Service/logService');
const systemService = require('../Service/systemService');
const messageService = require('../Service/messageService');
const playerService = require('../Service/playerService');

const systemHelpTemplate = require('../Assets/EmbedMessage/System/systemHelpTemplate');
const blankTemplate = require('../Assets/EmbedMessage/blankTemplate');

const frenchTranslation = require('../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  systemController: function(msg, guild, client) {
    if (msg.content.split(' ')[1] === 'aide') {
      messageService.sendPrivateMessage(msg, systemHelpTemplate.systemHelpTemplate(), true);

      logService.sendLogMessage(
          guild,
          'system:help',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'setBiomeCategory') {
      const inputBiome = msg.content.split('setBiomeCategory ')[1];

      messageService.sendEmbedMessage(
          msg.channel,
          blankTemplate.blankTemplate(
              systemService.setBiomeCategory(inputBiome),
              'system',
          ),
          true,
      );
    }

    if (playerService.checkIfAdminUser(msg.author.id)) {
      if (msg.content.split(' ')[1] === 'setPeriod') {
        const stringPeriod = msg.content.split('setPeriod ')[1];

        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(
                systemService.setPeriod(stringPeriod),
                'system',
            ),
            true,
        );
      }
      if (msg.content.split(' ')[1] === 'startDatabases') {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(
                systemService.startDatabases(msg),
                'system',
            ),
            true,
        );

        logService.sendLogMessage(
            guild,
            'system:startDatabases',
            msg.author.username,
        );
      }
      if (msg.content.split(' ')[1] === 'dump') {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(
                systemService.dumpData(msg),
                'system',
            ),
            true,
        );

        logService.sendLogMessage(
            guild,
            'system:dump',
            msg.author.username,
        );
      }
      if (msg.content.split(' ')[1] === 'removeAllPlayers') {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(
                systemService.removeAllPlayers(msg),
                'system',
            ),
            true,
        );

        logService.sendLogMessage(
            guild,
            'system:dumpPlayers',
            msg.author.username,
        );
      }
      if (msg.content.split(' ')[1] === 'removePlayer') {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(
                systemService.removePlayer(msg, client),
                'system',
            ),
            true,
        );

        logService.sendLogMessage(
            guild,
            'system:addPlayer',
            msg.author.username,
            [
              {
                fieldName: 'text',
                value: returnMessage.text,
              },
              {
                fieldName: 'ID',
                value: returnMessage.id,
              },
              {
                fieldName: 'username',
                value: returnMessage.username,
              },
            ],
        );
      }
      if (msg.content.split(' ')[1] === 'addPlayer') {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(
                systemService.addPlayer(msg, client),
                'system',
            ),
            true,
        );


        logService.sendLogMessage(
            guild,
            'system:addPlayer',
            msg.author.username,
            [
              {
                fieldName: 'text',
                value: returnMessage.text,
              },
              {
                fieldName: 'ID',
                value: returnMessage.id,
              },
              {
                fieldName: 'username',
                value: returnMessage.username,
              },
            ],
        );
      }
      if (msg.content.split(' ')[1] === 'addLogChannelId') {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(
                systemService.addLogChannelId(msg, client),
                'system',
            ),
            true,
        );

        logService.sendLogMessage(
            guild,
            'system:addLogChannelId',
            msg.author.username,
            [
              {
                fieldName: 'text',
                value: returnMessage.text,
              },
              {
                fieldName: 'ID',
                value: returnMessage.id,
              },
              {
                fieldName: 'logChannelId',
                value: returnMessage.logChannelId,
              },
              {
                fieldName: 'username',
                value: returnMessage.username,
              },
            ],
        );
      }

      // Setup Emoji servers
      if (msg.content.split(' ')[1] === 'setupEmojiServer1') {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(
                systemService.uploadEmojiInDiscordServer(msg, 0, 50),
                'system',
            ),
            true,
        );

        logService.sendLogMessage(
            guild,
            'system:setupEmojiServer1',
            msg.author.username,
        );
      }

      if (msg.content.split(' ')[1] === 'setupEmojiServer2') {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(
                systemService.uploadEmojiInDiscordServer(msg, 50, 100),
                'system',
            ),
            true,
        );

        logService.sendLogMessage(
            guild,
            'system:setupEmojiServer2',
            msg.author.username,
        );
      }

      if (msg.content.split(' ')[1] === 'setupEmojiServer3') {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(
                systemService.uploadEmojiInDiscordServer(msg, 100, 150),
                'system',
            ),
            true,
        );

        logService.sendLogMessage(
            guild,
            'system:setupEmojiServer3',
            msg.author.username,
        );
      }

      if (msg.content.split(' ')[1] === 'setupEmojiServer4') {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(
                systemService.uploadEmojiInDiscordServer(msg, 150, 200),
                'system',
            ),
            true,
        );

        logService.sendLogMessage(
            guild,
            'system:setupEmojiServer4',
            msg.author.username,
        );
      }

      if (msg.content.split(' ')[1] === 'setupEmojiServer5') {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(
                systemService.uploadEmojiInDiscordServer(msg, 200, 250),
                'system',
            ),
            true,
        );

        logService.sendLogMessage(
            guild,
            'system:setupEmojiServer5',
            msg.author.username,
        );
      }

      if (msg.content.split(' ')[1] === 'setupEmojiServer6') {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(
                systemService.uploadEmojiInDiscordServer(msg, 250, -1),
                'system',
            ),
            true,
        );

        logService.sendLogMessage(
            guild,
            'system:setupEmojiServer6',
            msg.author.username,
        );
      }

      if (msg.content.split(' ')[1] === 'deleteAllEmojiInServer') {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(
                systemService.deleteAllEmojisInGuild(msg),
                'system',
            ),
            true,
        );

        logService.sendLogMessage(
            guild,
            'system:deleteAllEmojiInServer',
            msg.author.username,
        );
      }
    } else {
      messageService.sendEmbedMessage(
          msg.channel,
          blankTemplate.blankTemplate(
              tr.error.notAdminUser,
              'system',
          ),
          true,
      );
    }
  },
};
