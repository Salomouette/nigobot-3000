const logService = require('../Service/logService');
const calendarControllerService = require('../Service/Calendar/calendarControllerService');
const messageService = require('../Service/messageService');

const calendarHelpTemplate = require('../Assets/EmbedMessage/Calendar/calendarHelpTemplate');
const dayCalendarTemplate = require('../Assets/EmbedMessage/Calendar/dayCalendarTemplate');
const monthDaysCalendarTemplate = require('../Assets/EmbedMessage/Calendar/viewMonthDaysTemplate');
const setTodayCalendarTemplate = require('../Assets/EmbedMessage/Calendar/setTodayCalendarTemplate');
const changeTypeDateTemplate = require('../Assets/EmbedMessage/Calendar/changeDateTypeTemplate');
const addEventLogTemplate = require('../Assets/EmbedMessage/Calendar/addEventLogTemplate');
const addEventPeriodLogTemplate = require('../Assets/EmbedMessage/Calendar/addEventPeriodLogTemplate');
const addEventPeriodOutput = require('../Assets/EmbedMessage/Calendar/addEventPeriodOutput');
const addPeriodLogTemplate = require('../Assets/EmbedMessage/Calendar/addPeriodLogTemplate');
const addPeriodOutputTemplate = require('../Assets/EmbedMessage/Calendar/addPeriodOutputTemplate');
const modifyDayTemplate = require('../Assets/EmbedMessage/Calendar/modifyDayTemplate');
const modifyOrderPreviewTemplate = require('../Assets/EmbedMessage/Calendar/modifyOrderPreviewTemplate');
const blankTemplate = require('../Assets/EmbedMessage/blankTemplate');

const calendarData = require('../Data/calendarData');
const frenchTranslation = require('../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  calendarController: async function(msg, guild, client) {
    if (msg.content.split(' ')[1] === 'aide') {
      messageService.sendPrivateMessage(msg, calendarHelpTemplate.calendarHelpTemplate(client), true);

      logService.sendLogMessage(
          guild,
          'calendar:help',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'voirMois') {
      const month = msg.content.split(' ')[2];
      messageService.sendEmbedMessage(
          msg.channel,
          monthDaysCalendarTemplate.monthDaysCalendarTemplate(month, client),
          true,
      );

      logService.sendLogMessage(
          guild,
          'calendar:viewMonth',
          msg.author.username,
          [
            {
              fieldName: 'Mois demandé',
              value: month,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'voirJour') {
      const day = msg.content.split(' ')[2];
      const embedsMessages = dayCalendarTemplate.dayCalendarTemplate(day, client);

      for (let i = 0; i < embedsMessages.length; i++) {
        messageService.sendEmbedMessageWithImages(
            msg.channel,
            embedsMessages[i].embedMessage,
            embedsMessages[i].images,
            true,
        );
      }

      logService.sendLogMessage(
          guild,
          'calendar:viewDate',
          msg.author.username,
          [
            {
              fieldName: 'Jour demandé',
              value: day,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'jourActuel') {
      const embedsMessages = dayCalendarTemplate.dayCalendarTemplate(calendarData.getTodayDate(), client);

      for (let i = 0; i < embedsMessages.length; i++) {
        messageService.sendEmbedMessageWithImages(
            msg.channel,
            embedsMessages[i].embedMessage, embedsMessages[i].images,
            true,
        );
      }

      logService.sendLogMessage(
          guild,
          'calendar:viewActualDay',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'définirJour') {
      const day = calendarControllerService.formatDayCommandInput(1, msg.content);
      const typeOfDay = calendarControllerService.formatDayCommandInput(2, msg.content);
      const changeCalendarRPGDateData = calendarControllerService.changeCalendarRPGDate(day, typeOfDay);
      const embed = setTodayCalendarTemplate.setTodayCalendarTemplate(day, changeCalendarRPGDateData.dayStatus);

      if (changeCalendarRPGDateData.nbDayTODO >= 1) {
        const TODOTemplate = blankTemplate.blankTemplate(
            tr.calendar.TODODaysBlankTemplate.line
                .replace(
                    '{NBTODODAYS}',
                    changeCalendarRPGDateData.nbDayTODO,
                ),
            'calendarTodo',
        );

        messageService.sendEmbedMessage(msg.channel, TODOTemplate, true);
      }

      messageService.sendEmbedMessage(msg.channel, embed, true);

      logService.sendLogMessage(
          guild,
          'calendar:defineDay',
          msg.author.username,
          [
            {
              fieldName: 'Jour in JDR défini',
              value: day,
            },
          ],
          embed,
      );
    }
    if (msg.content.split(' ')[1] === 'changerTypeJour') {
      const day = calendarControllerService.formatDayCommandInput(1, msg.content);
      const typeOfDay = calendarControllerService.formatDayCommandInput(2, msg.content);
      const dayChanged = calendarControllerService.changeTypeOfDate(day, typeOfDay);
      const embed = changeTypeDateTemplate.changeTypeDateTemplate(day, dayChanged);

      messageService.sendEmbedMessage(msg.channel, embed, true);

      logService.sendLogMessage(
          guild,
          'calendar:changeDateType',
          msg.author.username,
          [
            {
              fieldName: 'Jour in JDR défini',
              value: day,
            },
            {
              fieldName: 'Type de jour',
              value: typeOfDay,
            },
          ],
          embed,
      );
    }
    if (msg.content.split(' ')[1] === 'ajoutEvenementPériode') {
      const inputStart = calendarControllerService.formatAddEventPeriodCommandInput(1, msg.content);
      const inputEnd = calendarControllerService.formatAddEventPeriodCommandInput(2, msg.content);
      const inputTitle = calendarControllerService.formatAddEventPeriodCommandInput(3, msg.content);
      const inputDescription = calendarControllerService.formatAddEventPeriodCommandInput(4, msg.content);
      const inputCriticality = calendarControllerService.formatAddEventPeriodCommandInput(5, msg.content);

      const addEventPeriodReturn = calendarControllerService.addEventPeriod(
          inputStart,
          inputEnd,
          inputTitle,
          inputDescription,
          inputCriticality,
      );

      messageService.sendEmbedMessage(
          msg.channel,
          addEventPeriodOutput.addEventPeriodOutput(
              addEventPeriodReturn,
              inputStart,
              inputEnd,
          ),
          true,
      );

      logService.sendLogMessage(
          guild,
          'calendar:addEventPeriod',
          msg.author.username,
          [],
          addEventPeriodLogTemplate.addEventPeriodLogTemplate(
              inputStart,
              inputEnd,
              false,
              inputTitle,
              inputDescription,
              inputCriticality,
          ),
      );
    }
    if (msg.content.split(' ')[1] === 'ajoutPériode') {
      const inputStart = calendarControllerService.formatAddPeriodCommandInput(1, msg.content);
      const inputEnd = calendarControllerService.formatAddPeriodCommandInput(2, msg.content);
      const inputType = calendarControllerService.formatAddPeriodCommandInput(3, msg.content);
      const inputTitle = calendarControllerService.formatAddPeriodCommandInput(4, msg.content);
      const inputDescription = calendarControllerService.formatAddPeriodCommandInput(5, msg.content);
      const inputCriticality = calendarControllerService.formatAddPeriodCommandInput(6, msg.content);

      const addPeriodReturn = calendarControllerService.addPeriod(
          inputStart,
          inputEnd,
          inputType,
          inputTitle,
          inputDescription,
          inputCriticality,
      );

      messageService.sendEmbedMessage(
          msg.channel,
          addPeriodOutputTemplate.addPeriodOutputTemplate(
              addPeriodReturn,
              inputStart,
              inputEnd,
          ),
          true,
      );

      logService.sendLogMessage(
          guild,
          'calendar:addPeriod',
          msg.author.username,
          [],
          addPeriodLogTemplate.addPeriodLogTemplate(
              inputStart,
              inputEnd,
              inputType,
              inputTitle,
              inputDescription,
              inputCriticality,
          ),
      );
    }
    if (msg.content.split(' ')[1] === 'ajoutEvenement') {
      const inputDate = calendarControllerService.formatAddEventCommandInput(1, msg.content);
      const inputTitle = calendarControllerService.formatAddEventCommandInput(2, msg.content);
      const inputDescription = calendarControllerService.formatAddEventCommandInput(3, msg.content);
      const inputCriticality = calendarControllerService.formatAddEventCommandInput(4, msg.content);

      if (inputDescription.length < 1000 && inputTitle.length < 200) {
        calendarControllerService.addEventAtDate(
            inputDate,
            inputTitle,
            inputDescription,
            inputCriticality,
        );

        // Wait for write file
        await new Promise((resolve) => setTimeout(resolve, 300)).then(async function() {
          const embedsMessages = dayCalendarTemplate.dayCalendarTemplate(inputDate, client);

          for (let i = 0; i < embedsMessages.length; i++) {
            messageService.sendEmbedMessageWithImages(
                msg.channel,
                embedsMessages[i].embedMessage, embedsMessages[i].images,
                true,
            );
          }
        });

        const embedLog = addEventLogTemplate.addEventLogTemplate(
            inputDate,
            inputTitle,
            inputDescription,
            inputCriticality,
        );

        logService.sendLogMessage(
            guild,
            'calendar:addEvent',
            msg.author.username,
            [],
            embedLog,
        );
      } else if (inputDescription.length >= 1000 && inputTitle.length >= 200) {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(tr.error.addEventDescTitleTooManyChar, 'error'),
            true,
        );
      } else if (inputDescription.length >= 1000) {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(tr.error.addEventDescTooManyChar, 'error'),
            true,
        );
      } else if (inputTitle.length >= 200) {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(tr.error.addEventTitleTooManyChar, 'error'),
            true,
        );
      }
    }
    if (msg.content.split(' ')[1] === 'modifierOrdreTemplate') {
      const day = msg.content.split(' ')[2];
      const dayEventsOrder = calendarData.displayEventsDaysOrderCalendarData(day);
      const embedMessage = modifyOrderPreviewTemplate.modifyOrderPreviewTemplate(dayEventsOrder, client, true);
      const modifyCommandMessage = calendarControllerService.modifyEventsDayOrderGenerateCommand(day);

      messageService.sendEmbedMessage(msg.channel, embedMessage, true);
      messageService.sendTextMessage(msg, modifyCommandMessage);

      logService.sendLogMessage(
          guild,
          'calendar:modifyOrderTemplate',
          msg.author.username,
          [
            {
              fieldName: 'Jour demandé',
              value: day,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'modifierOrdreJour') {
      const inputs = calendarControllerService.formatModifyOrderCommandInput(msg.content);

      if (typeof inputs !== 'object') {
        messageService.sendEmbedMessage(msg.channel, blankTemplate.blankTemplate(inputs, 'error'), true);
      } else {
        const dayData = calendarControllerService.modifyOrderEventsDate(inputs);
        const embedMessage = modifyOrderPreviewTemplate.modifyOrderPreviewTemplate(dayData, client, false);

        messageService.sendEmbedMessage(msg.channel, embedMessage, true);

        logService.sendLogMessage(
            guild,
            'calendar:modifyOrder',
            msg.author.username,
            [
              {
                fieldName: 'Jour demandé',
                value: inputs.date,
              },
            ],
        );
      }
    }
    if (msg.content.split(' ')[1] === 'modifierEvenementTemplate') {
      const day = calendarControllerService.formatModifyDayCommandInput(1, msg.content);
      const eventId = calendarControllerService.formatModifyDayCommandInput(2, msg.content);

      messageService.sendTextMessage(msg, calendarControllerService.modifyEventDateGenerateCommand(day, eventId));

      logService.sendLogMessage(
          guild,
          'calendar:modifyDateTemplate',
          msg.author.username,
          [
            {
              fieldName: 'Jour demandé',
              value: day,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'modifierEvenement') {
      const inputDescription = calendarControllerService.formatAddEventCommandInput(3, msg.content);
      const inputTitle = calendarControllerService.formatAddEventCommandInput(2, msg.content);
      const input = msg.content.split('modifierEvenement ')[1];

      if (inputDescription.length < 1000 && inputTitle.length < 200) {
        const dayData = calendarControllerService.getDataAndModifyDateCommand(input);
        const embed = modifyDayTemplate.modifyDayTemplate(dayData, client);
        messageService.sendEmbedMessage(msg.channel, embed, true);

        logService.sendLogMessage(
            guild,
            'calendar:modifyEvent',
            msg.author.username,
            [
              {
                fieldName: 'Inputs',
                value: input,
              },
            ],
            embed,
        );
      } else if (inputDescription.length >= 1000 && inputTitle.length >= 200) {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(tr.error.addEventDescTitleTooManyChar, 'error'),
            true,
        );
      } else if (inputDescription.length >= 1000) {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(tr.error.addEventDescTooManyChar, 'error'),
            true,
        );
      } else if (inputTitle.length >= 200) {
        messageService.sendEmbedMessage(
            msg.channel,
            blankTemplate.blankTemplate(tr.error.addEventTitleTooManyChar, 'error'),
            true,
        );
      }
    }
  },
};
