const diceThrowTemplate = require('../Assets/EmbedMessage/Dice/diceThrowTemplate');
const diceThrowValidateTemplate = require('../Assets/EmbedMessage/Dice/diceThrowValidateTemplate');
const diceHelpTemplate = require('../Assets/EmbedMessage/Dice/diceHelpTemplate');
const diceSumTemplate = require('../Assets/EmbedMessage/Dice/diceSumTemplate');

const logService = require('../Service/logService');
const systemService = require('../Service/systemService');
const messageService = require('../Service/messageService');
module.exports = {
  diceController: function(msg, guild, client) {
    if (msg.content.split(' ')[1] === 'aide') {
      messageService.sendPrivateMessage(msg, diceHelpTemplate.diceHelpTemplate(), true);

      logService.sendLogMessage(
          guild,
          'dice:help',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'lancer') {
      const callInput = msg.content.split(' ')[2];
      const templateResult = diceThrowTemplate.diceThrowTemplate(callInput, msg.author.id);
      messageService.replyEmbedMessageWithImages(msg, templateResult.embedMessage, templateResult.images);

      logService.sendLogMessage(
          guild,
          'dice:throw',
          msg.author.username,
          [
            {
              fieldName: 'Dé',
              value: callInput,
            },
          ],
          templateResult.embedMessage,
      );
    }
    if (msg.content.split(' ')[1] === 'moyenne') {
      const callInput = msg.content.split(' ')[2];
      const templateResult = diceSumTemplate.diceSumTemplate(callInput, msg.author.id);
      messageService.replyEmbedMessageWithImages(msg, templateResult.embedMessage, templateResult.images);

      logService.sendLogMessage(
          guild,
          'dice:average',
          msg.author.username,
          [
            {
              fieldName: 'Dé',
              value: callInput,
            },
          ],
          templateResult.embedMessage,
      );
    }
    if (msg.content.split(' ')[1] === 'valider') {
      const characteristicInput = msg.content.split(' ')[2];
      const skillInput = msg.content.split(' ')[3];
      const malusInput = msg.content.split('--modif=')[1];

      const templateResult = diceThrowValidateTemplate.diceThrowValidateTemplate(
          characteristicInput,
          skillInput,
          malusInput,
          msg.author.id,
      );
      messageService.replyEmbedMessageWithImages(msg, templateResult.embedMessage, templateResult.images);

      logService.sendLogMessage(
          guild,
          'dice:validate',
          msg.author.username,
          [
            {
              fieldName: 'Charactéristique',
              value: characteristicInput,
            },
            {
              fieldName: 'Compétence',
              value: skillInput,
            },
            {
              fieldName: 'Modificateur',
              value: malusInput,
            },
          ],
          templateResult.embedMessage,
      );
    }
    if (msg.content.split(' ')[1] === 'privé') {
      const characteristicInput = msg.content.split(' ')[2];
      const skillInput = msg.content.split(' ')[3];
      const malusInput = msg.content.split('--modif=')[1];

      const templateResult = diceThrowValidateTemplate.diceThrowValidateTemplate(
          characteristicInput,
          skillInput,
          malusInput,
          msg.author.id,
      );
      // Wait for writing player.json
      msg.author.send({
        embeds: [templateResult.embedMessage],
        files: templateResult.images,
      },
      ).then(() => {
        systemService.sendMessageToLogServer(msg.author.id, guild, templateResult);

        logService.sendLogMessage(
            guild,
            'dice:private',
            msg.author.username,
            [
              {
                fieldName: 'Caractéristique',
                value: characteristicInput,
              },
              {
                fieldName: 'Compétence',
                value: skillInput,
              },
              {
                fieldName: 'Modificateur',
                value: malusInput,
              },
            ],
            templateResult.embedMessage,
        );
      });
    }
  },
};
