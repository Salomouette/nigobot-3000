const logService = require('../Service/logService');
const playerService = require('../Service/playerService');
const messageService = require('../Service/messageService');

const moneyTemplate = require('../Assets/EmbedMessage/Player/moneyTemplate');
const playerHelpTemplate = require('../Assets/EmbedMessage/Player/playerHelpTemplate');
const throwStatsPlayerTemplate = require('../Assets/EmbedMessage/Player/throwStatsPlayerTemplate');
module.exports = {
  playerController: function(msg, guild, client) {
    if (msg.content.split(' ')[1] === 'aide') {
      messageService.sendPrivateMessage(msg, playerHelpTemplate.playerHelpTemplate(), true);

      logService.sendLogMessage(
          guild,
          'player:help',
          msg.author.username,
      );
    }
    // Admin User Only
    if (msg.content.split(' ')[1] === 'addPlayerMoney') {
      const returnMessage = playerService.addPlayerMoney(msg);
      messageService.sendEmbedMessage(msg.channel, moneyTemplate.moneyTemplate(returnMessage.text), true);

      logService.sendLogMessage(
          guild,
          'player:addPlayerMoney',
          msg.author.username,
          [
            {
              fieldName: 'text',
              value: returnMessage.text,
            },
            {
              fieldName: 'initialMoney',
              value: returnMessage.initialMoney,
            },
            {
              fieldName: 'moneyAdded',
              value: returnMessage.moneyAdded,
            },
            {
              fieldName: 'money',
              value: returnMessage.money,
            },
            {
              fieldName: 'username',
              value: returnMessage.username,
            },
            {
              fieldName: 'idUser',
              value: returnMessage.idUser,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'removePlayerMoney') {
      const returnMessage = playerService.removePlayerMoneyAdmin(msg);
      messageService.sendEmbedMessage(msg.channel, moneyTemplate.moneyTemplate(returnMessage.text), true);

      logService.sendLogMessage(
          guild,
          'player:removePlayerMoneyAdmin',
          msg.author.username,
          [
            {
              fieldName: 'text',
              value: returnMessage.text,
            },
            {
              fieldName: 'initialMoney',
              value: returnMessage.initialMoney,
            },
            {
              fieldName: 'moneyRemoved',
              value: returnMessage.moneyAdded,
            },
            {
              fieldName: 'money',
              value: returnMessage.money,
            },
            {
              fieldName: 'username',
              value: returnMessage.username,
            },
            {
              fieldName: 'idUser',
              value: returnMessage.idUser,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'addAllPlayersMoney') {
      const returnMessage = playerService.addAllPlayersMoney(msg);
      messageService.sendEmbedMessage(msg.channel, moneyTemplate.moneyTemplate(returnMessage.text), true);

      logService.sendLogMessage(
          guild,
          'player:addAllPlayersMoney',
          msg.author.username,
          [
            {
              fieldName: 'text',
              value: returnMessage.text,
            },
            {
              fieldName: 'moneyAdded',
              value: returnMessage.moneyAdded,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'removeAllPlayersMoney') {
      const returnMessage = playerService.removeAllPlayersMoney(msg);
      messageService.sendEmbedMessage(msg.channel, moneyTemplate.moneyTemplate(returnMessage.text), true);

      logService.sendLogMessage(
          guild,
          'player:removeAllPlayersMoney',
          msg.author.username,
          [
            {
              fieldName: 'text',
              value: returnMessage.text,
            },
            {
              fieldName: 'moneyRemoved',
              value: returnMessage.moneyRemoved,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'statLancerPerso') {
      // Async function, it will send be it's own the embed.
      throwStatsPlayerTemplate.throwStatsPlayerTemplate(msg);

      logService.sendLogMessage(
          guild,
          'player:throwStats',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'voirArgent') {
      const returnMessage = playerService.viewPlayerMoneyPlayer(msg);
      messageService.sendEmbedMessage(msg.channel, moneyTemplate.moneyTemplate(returnMessage.text), true);

      logService.sendLogMessage(
          guild,
          'player:seeMoney',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'utiliserArgent') {
      const returnMessage = playerService.removePlayerMoneyPlayer(msg);
      messageService.sendEmbedMessage(msg.channel, moneyTemplate.moneyTemplate(returnMessage.text), true);

      logService.sendLogMessage(
          guild,
          'player:removePlayerMoneyPlayer',
          msg.author.username,
          [
            {
              fieldName: 'text',
              value: returnMessage.text,
            },
            {
              fieldName: 'initialMoney',
              value: returnMessage.initialMoney,
            },
            {
              fieldName: 'moneyRemoved',
              value: returnMessage.moneyRemoved,
            },
            {
              fieldName: 'money',
              value: returnMessage.money,
            },
            {
              fieldName: 'username',
              value: returnMessage.username,
            },
            {
              fieldName: 'idUser',
              value: returnMessage.idUser,
            },
          ],
      );
    }
  },
};
