const systemController = require('./systemController.js');
const playerController = require('./playerController.js');
const diceController = require('./diceController.js');
const biomeController = require('./biomeController.js');
const pokémonController = require('./pokémonController.js');
const berryController = require('./berryController.js');
const funController = require('./funController.js');
const craftController = require('./craftController.js');
const calendarController = require('./calendarController.js');
const trainerController = require('./trainerController.js');
const itemController = require('./itemController.js');
const inventoryController = require('./inventoryController.js');

const botHelpTemplate = require('../Assets/EmbedMessage/Bot/botHelpTemplate');
const loreLinksTemplate = require('../Assets/EmbedMessage/Bot/loreLinksTemplate');

const logService = require('../Service/logService');
const messageService = require('../Service/messageService');

const jsonIntegrityRoutine = require('../Routine/jsonIntegrityRoutine');
const pokémonFoundableItemRoutine = require('../Routine/pokémonFoundableItemRoutine');
const priceRatesRoutine = require('../Routine/priceRatesRoutine');
// Command
module.exports = {
  commandsController: function(client, Jimp) {
    client.on('messageCreate', (msg) => {
      // Routines without delay
      jsonIntegrityRoutine.jsonIntegrityRoutine();

      const guild = client.guilds.cache.get(process.env.DASHBOARD_GUILD_ID);
      let isMessageToBot = false;
      if (!msg.author.bot) {
        if (msg.content === process.env.NIGOBOT_COMMAND_TAG.concat('', 'ping')) {
          messageService.sendTextMessage(msg, 'pong! :table_tennis:');

          logService.sendLogMessage(
              guild,
              'bot:ping',
              msg.author.username,
          );
          isMessageToBot = true;
        }
        if (msg.content === process.env.NIGOBOT_COMMAND_TAG.concat('', 'aide')) {
          messageService.sendPrivateMessage(msg, botHelpTemplate.botHelpTemplate(client), true);

          logService.sendLogMessage(
              guild,
              'bot:help',
              msg.author.username,
          );
          isMessageToBot = true;
        }
        if (msg.content === process.env.NIGOBOT_COMMAND_TAG.concat('', 'lore')) {
          messageService.sendPrivateMessage(msg, loreLinksTemplate.loreLinksTemplate(), true);

          logService.sendLogMessage(
              guild,
              'bot:help',
              msg.author.username,
          );
          isMessageToBot = true;
        }
        // No particular tag
        funController.funController(msg, guild, client);
        if (msg.content.includes(process.env.NIGOBOT_COMMAND_TAG.concat('', 'baie'))) {
          berryController.berryController(msg, guild, client);
          isMessageToBot = true;
        }
        if (msg.content.includes(process.env.NIGOBOT_COMMAND_TAG.concat('', 'biome'))) {
          biomeController.biomeController(msg, guild, client);
          isMessageToBot = true;
        }
        if (msg.content.includes(process.env.NIGOBOT_COMMAND_TAG.concat('', 'calendrier'))) {
          calendarController.calendarController(msg, guild, client);
          isMessageToBot = true;
        }
        if (msg.content.includes(process.env.NIGOBOT_COMMAND_TAG.concat('', 'fabrication'))) {
          craftController.craftController(msg, guild, client);
          isMessageToBot = true;
        }
        if (msg.content.includes(process.env.NIGOBOT_COMMAND_TAG.concat('', 'dé'))) {
          diceController.diceController(msg, guild, client);
          isMessageToBot = true;
        }
        if (msg.content.includes(process.env.NIGOBOT_COMMAND_TAG.concat('', 'objet'))) {
          itemController.itemController(msg, guild, client);
          isMessageToBot = true;
        }
        if (msg.content.includes(process.env.NIGOBOT_COMMAND_TAG.concat('', 'joueur'))) {
          playerController.playerController(msg, guild, client);
          isMessageToBot = true;
        }
        if (msg.content.includes(process.env.NIGOBOT_COMMAND_TAG.concat('', 'pokémon'))) {
          pokémonController.pokémonController(msg, guild, client);
          isMessageToBot = true;
        }
        if (msg.content.includes(process.env.NIGOBOT_COMMAND_TAG.concat('', 'système'))) {
          systemController.systemController(msg, guild, client);
          isMessageToBot = true;
        }
        if (msg.content.includes(process.env.NIGOBOT_COMMAND_TAG.concat('', 'dresseur'))) {
          trainerController.trainerController(msg, guild, client);
          isMessageToBot = true;
        }
        if (msg.content.includes(process.env.NIGOBOT_COMMAND_TAG.concat('', 'inventaire'))) {
          inventoryController.inventoryController(msg, guild, client);
          isMessageToBot = true;
        }

        // Routines with delay
        setTimeout(function() {
          if (!isMessageToBot) {
            priceRatesRoutine.priceRatesRoutine(client, guild);
          }
        }, 3000);
        setTimeout(function() {
          if (!isMessageToBot) {
            pokémonFoundableItemRoutine.pokémonFoundableItemRoutine(client, guild);
          }
        }, 5000);
      }
    });
  },
};
