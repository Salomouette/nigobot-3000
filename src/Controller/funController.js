const blankTemplate = require('../Assets/EmbedMessage/blankTemplate');

const logService = require('../Service/logService');
const playerService = require('../Service/playerService');
const messageService = require('../Service/messageService');

const frenchSwearList = [
  'abruti',
  'andouille',
  'avorton',
  'bête',
  'bâtard',
  'boudin',
  'bouffon',
  'casse-couille',
  'chie',
  'con',
  'connard',
  'couille molle',
  'crétin',
  'crevure',
  'débile',
  'dugland',
  'ducon',
  'emmerde',
  'enculé',
  'empaf',
  'enfoiré',
  'enflure',
  'idiot',
  'gland',
  'gogole',
  'imbécile',
  'incapable',
  'mange-merde',
  'merde',
  'moche',
  'méchant',
  'minable',
  'mongol',
  'nul',
  'raclure',
  'sagouin',
  'salaud',
  'schlag',
  'sous-merde',
  'tebé',
  'tocard',
  'vendu',
  'vilain',
  'fourbe',
  'nigaud',
  'escroc',
  'arnaque',
  'pédé',
  'faible',
  'niais',
  'gamin',
  'raciste',
  'guignol',
  'illettré',
  'tarte',
];

const nigobotResponse = [
  'Mais c\'est pas gentil ça! :c',
  'Mais non je suis pas comme ça... :c',
  'Je fais de mon mieux... :c',
  'Pourquoi être aussi méchant(e) avec moi... :c',
  'C\'est pas gentil du tout! :c',
  'J\'ai ton adresse IP, j\'arrive',
  'C\'est toi qu\'est sauvage!',
  '*Te jette des saucisses dessus*',
  'J\'en aurais les capacités, je te spammerai le script de Bee Movie en message privé.',
  'Parce que tu te crois en sécurité, {PSEUDO} ?',
];
module.exports = {
  funController: function(msg, guild, client) {
    if (msg.content.toLowerCase().includes('nigobot no jutsu')) {
      messageService.sendEmbedMessage(
          msg.channel,
          blankTemplate.blankTemplate(
              ':ninja:',
              'fun',
          ),
      );
    }

    if (msg.content.toLowerCase().includes('nigobot')) {
      if (msg.content.toLowerCase().includes('patate')) {
        msg.channel,
        blankTemplate.blankTemplate(
            '<:patate:755517292774817943>',
            'fun',
        );
      }
    }

    if (msg.content.toLowerCase().includes('nigobot')) {
      if (frenchSwearList.some((v) => msg.content.includes(v))) {
        playerService.addSlurPlayer(msg);
        messageService.replyEmbedMessage(
            msg,
            blankTemplate.blankTemplate(
                nigobotResponse[Math.floor(Math.random() * nigobotResponse.length)]
                    .replace('{PSEUDO}', msg.author.username),
                'fun',
            ),
        );

        logService.sendLogMessage(
            guild,
            'fun:slur',
            msg.author.username,
        );
      }
    }
  },
};
