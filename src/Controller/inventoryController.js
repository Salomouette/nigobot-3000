const logService = require('../Service/logService');
const messageService = require('../Service/messageService');
const playerViewService = require('../Service/Player/playerViewService');

const playerData = require('../Data/playerData');

const inventoryHelpTemplate = require('../Assets/EmbedMessage/Inventory/inventoryHelpTemplate');
const foundItemInventoryTemplate = require('../Assets/EmbedMessage/Inventory/foundItemInventoryTemplate');
const keepFoundItemTemplate = require('../Assets/EmbedMessage/Inventory/keepFoundItemTemplate');
const sellBaseTemplate = require('../Assets/EmbedMessage/Inventory/sellBaseTemplate');

const frenchTranslation = require('../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  inventoryController: function(msg, guild, client) {
    if (msg.content.split(' ')[1] === 'aide') {
      messageService.sendPrivateMessage(msg, inventoryHelpTemplate.inventoryHelpTemplate(), true);

      logService.sendLogMessage(
          guild,
          'inventory:help',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'garderObjetTrouvé') {
      const callInput = msg.content.split('garderObjetTrouvé ')[1];
      const playerDatas = playerData.getPlayerById(msg.author.id);
      const keepFoundItems = playerViewService.keepFoundItems(
          playerDatas.inventory.foundItems,
          msg.author.id,
          callInput,
      );

      const embedMessage = keepFoundItemTemplate.keepFoundItemTemplate(keepFoundItems);

      messageService.sendEmbedMessage(msg.channel, embedMessage, true);

      logService.sendLogMessage(
          guild,
          'inventory:keepItem',
          msg.author.username,
          [
            {
              fieldName: 'Objet',
              value: callInput,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'garderObjetTrouvéSousCatégorie') {
      const callInput = msg.content.split('garderObjetTrouvéSousCatégorie ')[1];
      const playerDatas = playerData.getPlayerById(msg.author.id);
      const keepFoundItems = playerViewService.keepFoundItemsSubCategory(
          playerDatas.inventory.foundItems,
          msg.author.id,
          callInput,
      );

      const embedMessage = keepFoundItemTemplate.keepFoundItemTemplate(keepFoundItems);

      messageService.sendEmbedMessage(msg.channel, embedMessage, true);

      logService.sendLogMessage(
          guild,
          'inventory:keepSubCategory',
          msg.author.username,
          [
            {
              fieldName: 'Sous catégorie',
              value: callInput,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'garderObjetTrouvéCatégorie') {
      const callInput = msg.content.split('garderObjetTrouvéCatégorie ')[1];
      const playerDatas = playerData.getPlayerById(msg.author.id);
      const keepFoundItems = playerViewService.keepFoundItemsCategory(
          playerDatas.inventory.foundItems,
          msg.author.id,
          callInput,
      );

      const embedMessage = keepFoundItemTemplate.keepFoundItemTemplate(keepFoundItems);

      messageService.sendEmbedMessage(msg.channel, embedMessage, true);

      logService.sendLogMessage(
          guild,
          'inventory:keepCategory',
          msg.author.username,
          [
            {
              fieldName: 'Catégorie',
              value: callInput,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'vendreObjetTrouvé') {
      const callInput = msg.content.split('vendreObjetTrouvé ')[1];
      const playerDatas = playerData.getPlayerById(msg.author.id);
      const valueFoundItems = playerViewService.soldFoundItem(
          playerDatas.inventory.foundItems,
          msg.author.id,
          callInput,
      );

      const embedMessage = sellBaseTemplate.sellBaseTemplate(
                valueFoundItems === -1 ?
                    tr.player.cannotSellSpecialItems :
                    tr.player.sellFoundItemDescription
                        .replace('{MONEY}', valueFoundItems)
                        .replace('{ITEMNAME}', callInput),
      );

      messageService.sendEmbedMessage(msg.channel, embedMessage, true);

      logService.sendLogMessage(
          guild,
          'inventory:sellFoundItem',
          msg.author.username,
          [
            {
              fieldName: 'Objet',
              value: callInput,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'vendreObjetTrouvéSousCatégorie') {
      const callInput = msg.content.split('vendreObjetTrouvéSousCatégorie ')[1];
      const playerDatas = playerData.getPlayerById(msg.author.id);
      const valueFoundItems = playerViewService.soldFoundItemSubCategory(
          playerDatas.inventory.foundItems,
          msg.author.id,
          callInput,
      );

      const embedMessage = sellBaseTemplate.sellBaseTemplate(
                valueFoundItems === -1 ?
                    tr.player.cannotSellSpecialItemsCategory :
                    tr.player.sellFoundItemCategoryDescription
                        .replace('{MONEY}', valueFoundItems),
      );

      messageService.sendEmbedMessage(msg.channel, embedMessage, true);

      logService.sendLogMessage(
          guild,
          'inventory:sellFoundItemSubCategory',
          msg.author.username,
          [
            {
              fieldName: 'Sous catégorie',
              value: callInput,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'vendreObjetTrouvéCatégorie') {
      const callInput = msg.content.split('vendreObjetTrouvéCatégorie ')[1];
      const playerDatas = playerData.getPlayerById(msg.author.id);
      const valueFoundItems = playerViewService.soldFoundItemCategory(
          playerDatas.inventory.foundItems,
          msg.author.id,
          callInput,
      );

      const embedMessage = sellBaseTemplate.sellBaseTemplate(
                valueFoundItems === -1 ?
                    tr.player.cannotSellSpecialItemsCategory :
                    tr.player.sellFoundItemCategoryDescription
                        .replace('{MONEY}', valueFoundItems),
      );

      messageService.sendEmbedMessage(msg.channel, embedMessage, true);

      logService.sendLogMessage(
          guild,
          'inventory:sellFoundItemCategory',
          msg.author.username,
          [
            {
              fieldName: 'Catégorie',
              value: callInput,
            },
          ],
      );
    }
    if (msg.content.split(' ')[1] === 'vendreTousObjetsTrouvés') {
      const playerDatas = playerData.getPlayerById(msg.author.id);
      const valueSoldFoundItems = playerViewService.soldAllFoundItems(playerDatas.inventory.foundItems, msg.author.id);

      const embedMessage = sellBaseTemplate.sellBaseTemplate(
          tr.player.sellFoundItemConfirmationDescription .replace('{MONEY}', valueSoldFoundItems),
      );

      messageService.sendEmbedMessage(msg.channel, embedMessage, true);

      logService.sendLogMessage(
          guild,
          'inventory:sellAllFoundItems',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'estimationTousObjetsTrouvés') {
      const playerDatas = playerData.getPlayerById(msg.author.id);
      const valueFoundItems = playerViewService.getValueFoundItems(playerDatas.inventory.foundItems, msg.author.id);

      const embedMessage = sellBaseTemplate.sellBaseTemplate(
          tr.player.sellFoundItemsDescription
              .replace('{MONEY}', valueFoundItems),
      );

      messageService.sendEmbedMessage(msg.channel, embedMessage, true);

      logService.sendLogMessage(
          guild,
          'inventory:estimateAllFoundItems',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'voirObjetsTrouvés') {
      const embedsMessages = foundItemInventoryTemplate.foundItemInventoryTemplate(msg.author.id, client);

      for (let i = 0; i < embedsMessages.length; i++) {
        messageService.sendEmbedMessage(
            msg.channel,
            embedsMessages[i].embedMessage,
            true,
        );
      }

      logService.sendLogMessage(
          guild,
          'inventory:seeFoundItem',
          msg.author.username,
          [
            {
              fieldName: 'Id Joueur',
              value: msg.author.id,
            },
          ],
      );
    }
  },
};
