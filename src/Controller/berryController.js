const berryControllerService = require('../Service/Berry/berryControllerService');
const logService = require('../Service/logService');
const messageService = require('../Service/messageService');

const berryGeneralTableTemplate = require('../Assets/EmbedMessage/Berry/berryGeneralTableTemplate');
const berrySortedTableTemplate = require('../Assets/EmbedMessage/Berry/berrySortedTableTemplate');
const berryHelpTemplate = require('../Assets/EmbedMessage/Berry/berryHelpTemplate');
const berryMixTemplate = require('../Assets/EmbedMessage/Berry/berryMixTemplate');
const berryMixCommandTemplate = require('../Assets/EmbedMessage/Berry/berryMixCommandTemplate');
const diceThrowValidateTemplate = require('../Assets/EmbedMessage/Dice/diceThrowValidateTemplate');
module.exports = {
  berryController: function(msg, guild, client) {
    if (msg.content.split(' ')[1] === 'aide') {
      messageService.sendPrivateMessage(msg, berryHelpTemplate.berryHelpTemplate(), true);

      logService.sendLogMessage(
          guild,
          'berry:help',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'mixTemplate') {
      messageService.sendEmbedMessage(msg.channel, berryMixCommandTemplate.berryMixCommandTemplate(), true);

      logService.sendLogMessage(
          guild,
          'berry:mixTemplate',
          msg.author.username,
      );
    }
    if (msg.content.split(' ')[1] === 'mix') {
      const berryInput1 = berryControllerService.formatBerryCommandInputMix(
          1,
          msg.content,
      );
      const berryInput2 = berryControllerService.formatBerryCommandInputMix(
          2,
          msg.content,
      );
      const berryInput3 = berryControllerService.formatBerryCommandInputMix(
          3,
          msg.content,
      );
      const berryInput4 = berryControllerService.formatBerryCommandInputMix(
          4,
          msg.content,
      );

      const caracteristicInput = berryControllerService.formatBerryCommandInputMix(
          5,
          msg.content,
      );

      let skillInput = 0;
      if (msg.content.includes('NivCompétence')) {
        skillInput = berryControllerService.formatBerryCommandInputMix(
            6,
            msg.content,
        );
      }

      let modificatorInput = 0;
      if (msg.content.includes('Modificateur')) {
        modificatorInput = berryControllerService.formatBerryCommandInputMix(
            7,
            msg.content,
        );
      }

      const templateDiceResult = diceThrowValidateTemplate.diceThrowValidateTemplate(
          caracteristicInput,
          skillInput,
          modificatorInput,
          msg.author.id,
      );
      messageService.sendEmbedMessageWithImages(
          msg.channel,
          templateDiceResult.embedMessage,
          templateDiceResult.images,
          true,
      );

      const berryMixTemplateResult = berryMixTemplate.berryMixTemplate(
          'Baie ' + berryInput1,
          'Baie ' + berryInput2,
          'Baie ' + berryInput3,
          'Baie ' + berryInput4,
          templateDiceResult,
          client,
      );
      messageService.sendEmbedMessageWithImages(
          msg.channel,
          berryMixTemplateResult.embedMessage,
          berryMixTemplateResult.images,
          true,
      );

      logService.sendLogMessage(
          guild,
          'berry:mix',
          msg.author.username,
          [
            {
              fieldName: 'Nom Baie 1',
              value: berryInput1,
            },
            {
              fieldName: 'Nom Baie 2',
              value: berryInput2,
            },
            {
              fieldName: 'Nom Baie 3',
              value: berryInput3,
            },
            {
              fieldName: 'Nom Baie 4',
              value: berryInput4,
            },
            {
              fieldName: 'Caractérisrique',
              value: caracteristicInput,
            },
            {
              fieldName: 'Compétence',
              value: skillInput.length > 0 ? skillInput : 'Non renseigné',
            },
            {
              fieldName: 'Modificateur',
              value: modificatorInput.length > 0 ? modificatorInput : 'Non renseigné',
            },
          ],
          berryMixTemplateResult.embedMessage,
      );
    }
    if (msg.content.split(' ')[1] === 'tableau') {
      let callInputPage = parseInt(msg.content.split(' ')[2]);
      callInputPage = callInputPage > 0 && callInputPage <= 8 ? callInputPage : 1;

      messageService.sendEmbedMessage(
          msg.channel,
          berryGeneralTableTemplate.berryGeneralTableTemplate(callInputPage),
          true,
      );
    }
    if (msg.content.split(' ')[1] === 'tableauTrié') {
      let callInputPage = parseInt(msg.content.split(' ')[3]);
      callInputPage = callInputPage > 0 && callInputPage <= 8 ? callInputPage : 1;
      let callInputTaste = msg.content.split(' ')[2].toLowerCase();
      callInputTaste = berryControllerService.switchIntPotency(callInputTaste);

      messageService.sendEmbedMessage(
          msg.channel,
          berrySortedTableTemplate.berrySortedTableTemplate(callInputTaste, callInputPage),
          true,
      );
    }
  },
};
