const fs = require('fs');

const botService = require('../Service/botService');

const dumpPath = 'src/Data/jsons/dumps/';
module.exports = {
  jsonIntegrityRoutine: function() {
    this.checkAndUpdateJsonFiles(process.env.PLAYER_DATA, 'calendar');
    this.checkAndUpdateJsonFiles(process.env.PLAYER_POKEMON_DATA, 'playersPokemon');
    this.checkAndUpdateJsonFiles(process.env.GENERATED_POKEMON_DATA, 'generatedPokémon');
    this.checkAndUpdateJsonFiles(process.env.CRAFT_RECIPES_DATA, 'craftRecipes');
    this.checkAndUpdateJsonFiles(process.env.PLAYER_DATA, 'players');
    this.checkAndUpdateJsonFiles(process.env.PAST_TRAINER_DATA, 'pastTrainers');
    this.checkAndUpdateJsonFiles(process.env.SYSTEM_DATA, 'system');
  },
  checkAndUpdateJsonFiles: async function(file, category) {
    if (this.isErrorFile(file)) {
      const newestValidDumpPath = this.findNewestAndCompleteDump(dumpPath + category);
      await new Promise((resolve) => setTimeout(resolve, 1000)).then(async function() {
        if (newestValidDumpPath !== undefined) {
          const json = JSON.parse(fs.readFileSync(newestValidDumpPath, 'utf8'));
          botService.jsonWriteFile(json, category);
        }
      });
    }
  },
  findNewestAndCompleteDump: function(folderPath) {
    if (fs.existsSync(folderPath)) {
      const files = fs.readdirSync(folderPath, {withFileTypes: true})
          .filter((item) => !item.isDirectory())
          .map((item) => item.name)
          .sort();

      for (let i = files.length; i >= 0; i--) {
        const path = botService.formatAbsolutePath(folderPath + '\\' + files[i]);
        if (!this.isErrorFile(path)) {
          return path;
        }
      }
    }
  },
  isErrorFile: function(filePath) {
    try {
      JSON.parse(fs.readFileSync(filePath, 'utf8'));
      return false;
    } catch (err) {
      return true;
    }
  },
};
