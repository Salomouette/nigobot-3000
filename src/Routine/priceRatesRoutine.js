const moment = require('moment');
const Discord = require('discord.js');

const systemData = require('../Data/systemData');
const itemsData = require('../Data/itemsData');
const messageService = require('../Service/messageService');

const frenchTranslation = require('../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  priceRatesRoutine: function(client, guild) {
    const returnValues = this.createPriceRatesData();
    const recalculate = returnValues.recalculate;
    const priceRates = returnValues.priceRates;

    if (recalculate) {
      const pokeBallData = itemsData.getItemFromId(9);

      const embed = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_MONEY_QUERY)
          .setAuthor({
            name: tr.author.money,
            iconURL: process.env.SET_AUTHOR_MONEY_ICON,
          })
          .setTitle(tr.priceRatesRoutine.title)
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          })
          .addFields(
              {
                name: tr.priceRatesRoutine.buyName,
                value: this.checkIfPricesNormal(
                    priceRates.buyRate,
                    process.env.ITEM_BUY_MOY_PRICE_MULTIPLICATOR,
                ) ?
                        tr.priceRatesRoutine.lignRatesNormal
                            .replace('{SELLBUY}', 'd\'achat') :
                        tr.priceRatesRoutine.lignRates
                            .replace('{SELLBUY}', 'd\'achat')
                            .replace(
                                '{DOWNHIGH}',
                                this.generateDOWNHIGHLignEmbed(
                                    priceRates.buyRate,
                                    process.env.ITEM_BUY_MOY_PRICE_MULTIPLICATOR,
                                ),
                            )
                            .replace('{OBJECTNAME}', pokeBallData.name)
                            .replace('{ACTION}', 's\'achète')
                            .replace('{PRICEWITHRATE}', Math.round(pokeBallData.basePrice * priceRates.buyRate))
                            .replace('{NORMALPRICE}', pokeBallData.basePrice),
              },
              {
                name: tr.priceRatesRoutine.sellName,
                value: this.checkIfPricesNormal(
                    priceRates.sellRate,
                    process.env.ITEM_SELL_MOY_PRICE_MULTIPLICATOR,
                ) ?
                            tr.priceRatesRoutine.lignRatesNormal
                                .replace('{SELLBUY}', 'de revente') :
                            tr.priceRatesRoutine.lignRates
                                .replace('{SELLBUY}', 'de revente')
                                .replace(
                                    '{DOWNHIGH}',
                                    this.generateDOWNHIGHLignEmbed(
                                        priceRates.sellRate,
                                        process.env.ITEM_SELL_MOY_PRICE_MULTIPLICATOR,
                                    ),
                                )
                                .replace('{OBJECTNAME}', pokeBallData.name)
                                .replace('{ACTION}', 'se revend')
                                .replace(
                                    '{PRICEWITHRATE}',
                                    Math.round(pokeBallData.basePrice * priceRates.sellRate),
                                )
                                .replace(
                                    '{NORMALPRICE}',
                                    pokeBallData.basePrice * process.env.ITEM_SELL_MOY_PRICE_MULTIPLICATOR,
                                ),
              },
              {
                name: tr.priceRatesRoutine.auctionName,
                value: this.checkIfPricesNormal(
                    priceRates.auctionRate,
                    process.env.ITEM_BUY_MOY_PRICE_MULTIPLICATOR,
                ) ?
                            tr.priceRatesRoutine.lignRatesNormal
                                .replace('{SELLBUY}', 'd\'achat et de revente lors d\'enchères') :
                            tr.priceRatesRoutine.lignRates
                                .replace('{SELLBUY}', 'd\'achat et de revente lors d\'enchères')
                                .replace(
                                    '{DOWNHIGH}',
                                    this.generateDOWNHIGHLignEmbed(
                                        priceRates.auctionRate,
                                        process.env.ITEM_BUY_MOY_PRICE_MULTIPLICATOR,
                                    ),
                                )
                                .replace('{OBJECTNAME}', pokeBallData.name)
                                .replace('{ACTION}', 's\'achète et se revend lors d\'enchères')
                                .replace('{PRICEWITHRATE}', Math.round(pokeBallData.basePrice * priceRates.auctionRate))
                                .replace('{NORMALPRICE}', pokeBallData.basePrice),
              },
          );
      messageService.sendMessageRPGChannel(guild, embed);
    }
  },
  checkIfPricesNormal: function(priceRate, basePriceRates) {
    return Number(priceRate) === Number(basePriceRates);
  },
  generateDOWNHIGHLignEmbed: function(priceRate, basePriceRates) {
    return Number(priceRate) > Number(basePriceRates) ? 'plus haut' : 'plus bas';
  },
  createPriceRatesData: function() {
    let priceRates = systemData.getSystemProperty('priceRates');
    let recalculate = false;

    if (priceRates === undefined) {
      priceRates = {
        sellRate: 0,
        buyRate: 0,
        auctionRate: 0,
        saveDate: moment().unix(),
      };
      recalculate = true;
    } else {
      const today = moment().startOf('day');
      const saveDay = moment.unix(priceRates.saveDate).startOf('day');

      recalculate = !saveDay.isSame(today, 'd');
    }

    if (recalculate) {
      const pricesBuyHigh = this.checkSpecialRates(10);
      const pricesSellHigh = this.checkSpecialRates(15);
      const pricesSellBuyLow = this.checkSpecialRates(20);
      const pricesSellBuyHigh = this.checkSpecialRates(20);

      priceRates.saveDate = moment().unix();

      let min = process.env.ITEM_BUY_MIN_PRICE_MULTIPLICATOR;
      let max = pricesBuyHigh ?
                process.env.ITEM_BUY_EXTRA_MAX_PRICE_MULTIPLICATOR :
                process.env.ITEM_BUY_MAX_PRICE_MULTIPLICATOR;
      priceRates.buyRate = this.generateNormalRandomNumberBetweenTwo(min, max);

      min = process.env.ITEM_SELL_MIN_PRICE_MULTIPLICATOR;
      max = pricesSellHigh ?
                process.env.ITEM_SELL_EXTRA_MAX_PRICE_MULTIPLICATOR :
                process.env.ITEM_SELL_MAX_PRICE_MULTIPLICATOR;
      priceRates.sellRate = this.generateNormalRandomNumberBetweenTwo(min, max);

      min = pricesSellBuyLow ?
                process.env.ITEM_BUY_SELL_EXTRA_MIN_PRICE_AUCTION_MULTIPLICATOR :
                process.env.ITEM_BUY_SELL_MIN_PRICE_AUCTION_MULTIPLICATOR;
      max = pricesSellBuyHigh ?
                process.env.ITEM_BUY_SELL_EXTRA_MAX_PRICE_AUCTION_MULTIPLICATOR :
                process.env.ITEM_BUY_SELL_MAX_PRICE_AUCTION_MULTIPLICATOR;
      priceRates.auctionRate = this.generateNormalRandomNumberBetweenTwo(min, max);

      systemData.setSystemProperty('priceRates', priceRates);
    }

    return {
      recalculate: recalculate,
      priceRates: priceRates,
    };
  },
  checkSpecialRates: function(number) {
    const randNumber = Math.floor(Math.random() * number);
    return randNumber === 0 ? true : false;
  },
  generateNormalRandomNumberBetweenTwo: function(min, max) {
    max = max * 100;
    min = min * 100;

    let u = 0; let v = 0;
    while (u === 0) u = Math.random();
    while (v === 0) v = Math.random();
    let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );

    num = num / 10.0 + 0.5;
    if (num > 1 || num < 0) {
      num = this.generateNormalRandomNumberBetweenTwo(min, max);
    } else {
      num = Math.pow(num, 1);
      num *= max - min;
      num += min;
    }

    return (num/100).toFixed(2);
  },
};
