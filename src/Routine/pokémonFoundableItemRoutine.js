const pokemonFoundItemTemplate = require('../Assets/EmbedMessage/Player/pokemonFoundItemTemplate');

const itemsData = require('../Data/itemsData');
const pokémonData = require('../Data/pokémonData');
const playersData = require('../Data/playerData');
const systemData = require('../Data/systemData');

const emojiService = require('../Service/emojiService');
const botService = require('../Service/botService');
const messageService = require('../Service/messageService');
const logsService = require('../Service/logService');

const pickupAbilityId = 53;

const frenchTranslation = require('../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  pokémonFoundableItemRoutine: function(client, guild) {
    const listPokémonFindAnItem = this.getPokemonFindAnItem(client);
    this.savePlayersFoundItems(listPokémonFindAnItem);
    this.sendFoundItemMessage(listPokémonFindAnItem, client, guild);
  },
  sendFoundItemMessage: function(listPokémonFindAnItem, client, guild) {
    for (let i = 0; i < listPokémonFindAnItem.length; i++) {
      if (listPokémonFindAnItem[i].message.length > 0) {
        const embedMessage = pokemonFoundItemTemplate.pokemonFoundItemTemplate(
            listPokémonFindAnItem[i].title,
            listPokémonFindAnItem[i].message,
        );
        messageService.sendPrivateMessageWithId(client, listPokémonFindAnItem[i].playerId, embedMessage);
        logsService.sendFoundItemLogMessage(guild, embedMessage);
      }
    }
  },
  savePlayersFoundItems: function(listPokémonFindAnItem) {
    const allPlayersData = playersData.getAllPlayers();
    for (let i = 0; i < listPokémonFindAnItem.length; i++) {
      if (listPokémonFindAnItem[i].message.length > 0) {
        for (let j = 0; j < allPlayersData.players.length; j++) {
          if (allPlayersData.players[j].id === listPokémonFindAnItem[i].playerId) {
            listPokémonFindAnItem[i].foundItems.forEach(function(item) {
              for (let k = 0; k < item.quantity; k++) {
                allPlayersData.players[j].inventory.foundItems.push(
                    {
                      name: item.name,
                      id: item.id,
                      pokemonId: item.pokemonId,
                    },
                );
              }
            });
          }
        }
      }
    }
    botService.jsonWriteFile(allPlayersData, 'players');
  },
  getPokemonFindAnItem: function(client) {
    const allPlayersPokémonData = pokémonData.getAllPlayersPokémon();
    const jsonMessageAllPlayers = [];
    let nbItemFound = 0;

    for (let i = 0; i < allPlayersPokémonData.playersPokémon.length; i++) {
      let message = '';
      const foundItems = [];
      for (let j = 0; j < allPlayersPokémonData.playersPokémon[i].playerPokémon.length; j++) {
        const playerPokemonFoundItemRatio = this.getPlayerPokemonFoundItemRatio(
            allPlayersPokémonData.playersPokémon[i].playerPokémon.length,
            allPlayersPokémonData.playersPokémon[i].playerPokémon[j].friendship,
        );
        const pokemonAbilityID = allPlayersPokémonData.playersPokémon[i].playerPokémon[j].ability;
        if (this.checkIfPokemonFoundItem(pokemonAbilityID == pickupAbilityId, playerPokemonFoundItemRatio)) {
          const isCriticQuantityFoundItem = this.isCriticQuantityFoundItem();
          const foundItemRarity = this.getFoundItemRarity();
          let foundItemData = this.getFoundItem(
              foundItemRarity,
              allPlayersPokémonData.playersPokémon[i].playerPokémon[j].genId,
              allPlayersPokémonData.playersPokémon[i].id,
              allPlayersPokémonData.playersPokémon[i].playerPokémon[j].friendship,
          );

          const foundItemDataRarity = foundItemData.rarity;
          const foundItemQuantity = isCriticQuantityFoundItem ? foundItemData.quantity*2 : foundItemData.quantity;
          foundItemData = foundItemData.foundItemData;

          if (foundItemData !== undefined) {
            foundItems.push(
                {
                  name: foundItemData.name,
                  id: foundItemData.id,
                  pokemonId: allPlayersPokémonData.playersPokémon[i].playerPokémon[j].genId,
                  quantity: foundItemQuantity,
                },
            );

            message += isCriticQuantityFoundItem ? '**WOAW!** ' : '';
            message += tr.pokémonFoundableItemRoutine.ligne
                .replace(
                    '{POKÉMON}',
                            allPlayersPokémonData.playersPokémon[i].playerPokémon[j].surname.length >= 1 ?
                                allPlayersPokémonData.playersPokémon[i].playerPokémon[j].surname :
                                allPlayersPokémonData.playersPokémon[i].playerPokémon[j].name,
                )
                .replace(
                    '{OBJET}',
                    foundItemData.name,
                )
                .replace(
                    '{EMOJIITEM}',
                    emojiService.getFoundItemEmojiPerName(foundItemData.foundableData.buddyFoundableEmoji, client),
                )
                .replace(
                    '{RARITY}',
                    this.getRarityName(foundItemDataRarity),
                )
                .replace(
                    '{EMOJIRARITY}',
                    emojiService.getFoundItemRarityEmojiPerNameOrNumber(foundItemDataRarity, client),
                )
                .replace(
                    '{QUANTITY}',
                    foundItemQuantity,
                )
                .replace(
                    '{PLURAL}',
                    foundItemQuantity > 1 ? 's' : '',
                );
            nbItemFound++;
          }
        }
      }
      jsonMessageAllPlayers.push(
          {
            playerId: allPlayersPokémonData.playersPokémon[i].id,
            title: nbItemFound > 1 ?
                allPlayersPokémonData.playersPokémon[i].username + tr.pokémonFoundableItemRoutine.titleMultiple :
                allPlayersPokémonData.playersPokémon[i].username + tr.pokémonFoundableItemRoutine.titleSingle,
            message: message,
            foundItems: foundItems,
          },
      );
    }
    return jsonMessageAllPlayers;
  },
  getPlayerPokemonFoundItemRatio: function(playerNumberOfPokemon, pokemonFriendship) {
    const max = parseInt(process.env.FOUND_ITEM_MAXIMAL_RATIO);
    const min = parseInt(process.env.FOUND_ITEM_MINIMAL_RATIO);
    const randNumber = Math.floor(Math.random() * (max - min + 1)) + min;
    const bonusCatchNBPokemon = Math.round(playerNumberOfPokemon/process.env.FOUND_ITEM_BONUS_NUMBER_POKEMON);
    const bonusPokemonFriendship = Math.round(pokemonFriendship/2);

    const returnValue = (playerNumberOfPokemon * (randNumber - bonusPokemonFriendship)) - bonusCatchNBPokemon;
    return returnValue < 1 ? 1 : returnValue;
  },
  checkIfPokemonFoundItem: function(isPickupAbility, playerPokemonFoundItemRatio) {
    const ratio = isPickupAbility ? Math.floor(parseInt(playerPokemonFoundItemRatio)/2.5) : playerPokemonFoundItemRatio;
    const randNumber = Math.floor(Math.random() * ratio);
    return randNumber === 0 ? true : false;
  },
  getFoundItemRarity: function() {
    const randNumberNormal = Math.floor(Math.random() * 100) + 1;
    const randNumberRari = Math.floor(Math.random() * 1000) + 1;
    let returnValue = 'co';

    if (randNumberRari !== 1) {
      if (randNumberNormal >= 51 && randNumberNormal <= 80) {
        returnValue = 'unco';
      } else if (randNumberNormal >= 81 && randNumberNormal <= 96) {
        returnValue = 'inha';
      } else if (randNumberNormal >= 96 && randNumberNormal <= 100) {
        returnValue = 'rare';
      }
    } else {
      returnValue = 'rari';
    }

    return returnValue;
  },
  getRarityName: function(rarity) {
    let returnValue = 'commun';

    switch (rarity) {
      case 'unco':
        returnValue = 'peu commun';
        break;
      case 'inha':
        returnValue = 'inhabituel';
        break;
      case 'rare':
        returnValue = 'rare';
        break;
      case 'rari':
        returnValue = 'rarissime';
        break;
      case 'special':
        returnValue = 'spécial';
        break;
    }

    return returnValue;
  },
  getFoundItem: function(foundItemRarity, pokémonId, playerId, pokemonFriendship) {
    const randNumber = Math.floor(Math.random() * process.env.FOUND_SPECIAL_RATIO);
    // Generation outside (Player found item) don't provide those ids
    const specialItemFound = pokémonId.toString().length > 0 && playerId.toString().length > 0 && randNumber === 0 ?
            true :
            false;
    let listFoundableItems = [];

    if (!specialItemFound) {
      listFoundableItems = itemsData.getAllFoundableItemsItemsDataByRarityByCriteria(
          foundItemRarity,
          systemData.getSystemProperty('actualBiomeCategory'),
          true,
      );
    } else {
      const nbSpecialFoundItems = this.getNumberSpecialFoundItems(playerId, pokémonId);
      listFoundableItems.push(itemsData.getItemFromName('Feuille Argentée'));

      if (nbSpecialFoundItems.nbSilverLeafFound >= 5) {
        listFoundableItems.push(itemsData.getItemFromName('Feuille Dorée'));
      }
      if (nbSpecialFoundItems.nbGoldLeafFound >= 3) {
        listFoundableItems.push(itemsData.getItemFromName('Petit Bouquet'));
      }
    }

    const randIndex = Math.floor(Math.random() * (listFoundableItems.length));
    return {
      foundItemData: listFoundableItems[randIndex],
      rarity: specialItemFound ? 'special' : foundItemRarity,
      quantity: this.getQuantityFoundItem(listFoundableItems[randIndex], pokemonFriendship, foundItemRarity),
    };
  },
  isCriticQuantityFoundItem: function() {
    let returnValue = false;
    const randNumber = Math.floor(Math.random() * 100) + 1;

    if (randNumber == 1) {
      returnValue = true;
    }

    return returnValue;
  },
  getQuantityFoundItem: function(foundItemData, pokemonFriendship, foundItemRarity) {
    let itemQuantity = 1;

    if (foundItemData.foundableData.foundableQuantity !== undefined) {
      const randNumber = Math.floor(Math.random() * 100) + 1;

      for (let i = 0; i < foundItemData.foundableData.foundableQuantity.length; i++) {
        if (randNumber >= foundItemData.foundableData.foundableQuantity[i].minRange &&
            randNumber <= foundItemData.foundableData.foundableQuantity[i].maxRange
        ) {
          itemQuantity = foundItemData.foundableData.foundableQuantity[i].quantity;
        }
      }
    }

    if (foundItemData.foundableData.quantityFriendship !== undefined) {
      if (foundItemData.foundableData.quantityFriendship) {
        const bonusPokemonFriendship = pokemonFriendship >= 10 ? Math.round(pokemonFriendship/10) : 0;
        itemQuantity += bonusPokemonFriendship;
      }
    } else {
      if (foundItemRarity !== 'rari') {
        let bonusPokemonFriendship = pokemonFriendship >= 10 ? Math.round((pokemonFriendship/10)) : 1;
        bonusPokemonFriendship = bonusPokemonFriendship == 3 ? 2 : bonusPokemonFriendship;
        itemQuantity = bonusPokemonFriendship;
      }
    }

    return itemQuantity;
  },
  getNumberSpecialFoundItems: function(playerId, pokémonId) {
    const pokémonFoundItem = this.getPlayerPokémonFoundItemsByIds(playerId, pokémonId);
    let nbBouquetFound = 0;
    let nbSilverLeafFound = 0;
    let nbGoldLeafFound = 0;

    for (let i = 0; i < pokémonFoundItem.length; i++) {
      const itemData = itemsData.getItemFromId(pokémonFoundItem[i]);

      if (itemData.name === 'Feuille Dorée') {
        nbGoldLeafFound++;
      } else if (itemData.name === 'Feuille Argentée') {
        nbSilverLeafFound++;
      } else if (itemData.name === 'Petit Bouquet') {
        nbBouquetFound++;
      }
    }

    return {
      nbBouquetFound: nbBouquetFound,
      nbSilverLeafFound: nbSilverLeafFound,
      nbGoldLeafFound: nbGoldLeafFound,
    };
  },
  getPlayerPokémonFoundItemsByIds: function(playerId, pokémonID) {
    pokémonID = pokémonID.toString();
    playerId = playerId.toString();
    const allPlayers = playersData.getAllPlayers();
    const jsonOutput = [];

    for (let i = 0; i < allPlayers.players.length; i++) {
      if (allPlayers.players[i] != null) {
        if (allPlayers.players[i].id === playerId) {
          for (let j = 0; j < allPlayers.players[i].inventory.foundItems.length; j++) {
            if (allPlayers.players[i].inventory.foundItems[j].pokemonId == pokémonID) {
              jsonOutput.push(allPlayers.players[i].inventory.foundItems[j].id);
            }
          }
        }
      }
    }

    return jsonOutput;
  },
};
