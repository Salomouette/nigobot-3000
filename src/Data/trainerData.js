const fs = require('fs');

module.exports = {
  getAllTrainer: function() {
    return JSON.parse(fs.readFileSync(process.env.TRAINER_DATA, 'utf8'));
  },
  getTrainerData: function(trainer) {
    const trainerData = this.getAllTrainer();
    let output = '';
    for (let i = 0; i < trainerData.trainers.length; i++) {
      if (trainerData.trainers[i].className === trainer) {
        output = trainerData.trainers[i];
      }
    }

    return output;
  },
  getTrainersByBiome: function(biome) {
    const trainerData = this.getAllTrainer();
    const output = [];
    for (let i = 0; i < trainerData.trainers.length; i++) {
      for (let j = 0; j < trainerData.trainers[i].biome.length; j++) {
        if (trainerData.trainers[i].biome[j].name === biome) {
          output.push(trainerData.trainers[i].className);
        }
      }
    }
    return output[Math.floor(Math.random() * output.length)];
  },
};
