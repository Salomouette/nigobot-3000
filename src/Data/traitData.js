module.exports = {
  getAllPositiveTraits: function() {
    return positiveTraits;
  },
  getAllNegativeTraits: function() {
    return negativeTraits;
  },
};

const positiveTraits = [
  'Adore manger',
  'Fier de sa puissance',
  'Corps robuste',
  'Extrêmement curieux',
  'Très volontaire',
  'Aime courir',
  'S\'assoupit souvent',
  'Coquin',
  'Attentif aux sons',
  'Percévérant',
  'Très astucieux',
  'Fuit rapidement',
  'Aime travailler en équipe',
  'Bonne endurance',
  'Aime combattre',
  'Aime la neige',
  'Aime la pluie',
  'Aime le soleil',
];

const negativeTraits = [
  'S\'assoupit souvent',
  'Maladroit',
  'Peur des humains',
  'Peur des Pokémon',
  'A mauvais caractère',
  'Très obstiné',
  'Un peu idiot',
  'Boudeur',
  'Colérique',
  'N\'aime pas travailler en équipe',
  'N\'aime pas la neige',
  'N\'aime pas la pluie',
  'N\'aime pas le soleil',
  'N\'aime pas le vent',
  'Fugeur',
];
