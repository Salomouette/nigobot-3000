const fs = require('fs');

module.exports = {
  getAllBiomeData: function() {
    return JSON.parse(fs.readFileSync(process.env.BIOME_DATA, 'utf8'));
  },
  getRequestedBiomeData: function(requestBiome, requestData) {
    const biomeData = this.getAllBiomeData();
    for (let i = 0; i < biomeData.biomes.length; i++) {
      if (biomeData.biomes[i].name === requestBiome) {
        if (requestData === 'description') {
          return biomeData.biomes[i].description;
        }
        if (requestData === 'type') {
          return biomeData.biomes[i].type;
        } else if (requestData === 'preview') {
          return biomeData.biomes[i].files[0].preview;
        } else if (requestData === 'icon') {
          return biomeData.biomes[i].files[0].icon;
        } else {
          return null;
        }
      }
    }
  },
  getAllBiomePerType: function(requestBiomeType) {
    const biomeData = this.getAllBiomeData();
    const biome = [];
    for (let i = 0; i < biomeData.biomes.length; i++) {
      if (biomeData.biomes[i].type === requestBiomeType) {
        biome[biome.length] = biomeData.biomes[i];
      }
    }
    return biome;
  },
  getBiomeIDFromName: function(requestBiomeName) {
    const biomeData = this.getAllBiomeData();
    let returnValue = '';
    for (let i = 0; i < biomeData.biomes.length; i++) {
      if (biomeData.biomes[i].name === requestBiomeName) {
        returnValue = biomeData.biomes[i].id;
      }
    }
    return returnValue;
  },
  getBiomeNameFromID: function(requestBiomeId) {
    const biomeData = this.getAllBiomeData();
    let returnValue = '';
    for (let i = 0; i < biomeData.biomes.length; i++) {
      if (biomeData.biomes[i].id === requestBiomeId) {
        returnValue = biomeData.biomes[i].name;
      }
    }
    return returnValue;
  },
};
