const fs = require('fs');

const botService = require('../Service/botService');

const biomeData = require('../Data/biomeData');

const types = [
  'Plante',
  'Feu',
  'Eau',
  'Acier',
  'Dragon',
  'Électrik',
  'Fée',
  'Glace',
  'Insecte',
  'Normal',
  'Poison',
  'Psy',
  'Roche',
  'Sol',
  'Spectre',
  'Ténèbres',
  'Vol',
  'Fungus',
];

const weather = [
  'le soleil',
  'quand il fait nuageux',
  'quand il fait couvert',
  'quand il y a du brouillard',
  'la pluie',
  'les orages',
  'la grêle',
  'la neige',
  'le vent',
  'la pluie acide',
  'le ciel noir',
  'le blizzard',
  'la tempête de sable',
  'les étoiles filantes',
];

module.exports = {
  types, weather,
  getAllPokemon: function() {
    return JSON.parse(fs.readFileSync(process.env.POKEMON_DATA, 'utf8'));
  },
  getAllAbilities: function() {
    return JSON.parse(fs.readFileSync(process.env.ABILITIES_DATA, 'utf8'));
  },
  getAllPlayersPokémon: function() {
    return JSON.parse(fs.readFileSync(process.env.PLAYER_POKEMON_DATA, 'utf8'));
  },
  getAllBalls: function() {
    return JSON.parse(fs.readFileSync(process.env.POKEBALL_DATA, 'utf-8'));
  },
  getAllTraits: function() {
    return JSON.parse(fs.readFileSync(process.env.TRAITS_DATA, 'utf-8'));
  },
  getPokémonFromPokémonNameandForm: function(pokémonName, formName, formName2) {
    const dex = this.getAllPokemon();
    pokémonName = pokémonName.charAt(0).toUpperCase() + pokémonName.slice(1);

    if (formName !== null) {
      formName = botService.removeDiacritics(formName);
      formName = formName.charAt(0).toUpperCase() + formName.slice(1).toLowerCase();
    }
    if (formName2 !== null) {
      formName2 = botService.removeDiacritics(formName2);
      formName2 = formName2.charAt(0).toUpperCase() + formName2.slice(1).toLowerCase();
    }

    const isFormAlolan = formName === 'Alola';
    const isFormGalarian = formName === 'Galar';
    const isFormHisuian = formName === 'Hisui';
    const isFormPaldean = formName === 'Paldea';
    const isParadoxe = formName === 'Paradoxe';
    const isMega = formName === 'Mega';
    const isGiga = formName === 'Giga';
    const isPrimo = formName === 'Primo';
    const hasForms = [
      'Pikachu',
      'Mélofée',
      'Pichu',
      'Cheniti',
      'Cheniselle',
      'Sancoki',
      'Tritosor',
      'Motisma',
      'Dialga',
      'Palkia',
      'Giratina',
      'Shaymin',
      'Darumacho',
      'Kyurem',
      'Boréas',
      'Fulguris',
      'Démétéros',
      'Amovénus',
      'Prismillon',
      'Zygarde',
      'Hoopa',
      'Plumeline',
      'Lougaroc',
      'Necrozma',
      'Zacian',
      'Zamazenta',
      'Shifours',
      'Sylveroy',
      'Mordudor',
      'Tauros',
    ];
    const isMonWithForms = hasForms.includes(pokémonName);

    let returnPokémon = '';

    if (isFormAlolan) {
      for (let i = 0; i < dex['pokédex'].length; i++) {
        if (
          dex['pokédex'][i].name.includes(pokémonName) === true &&
            dex['pokédex'][i].alolan === true
        ) {
          returnPokémon = dex['pokédex'][i];
          break;
        }
      }
    } else if (isFormGalarian) {
      if (formName2 !== null && (pokémonName === 'Darumacho')) {
        for (let i = 0; i < dex['pokédex'].length; i++) {
          if (
            dex['pokédex'][i].name.includes(pokémonName) === true &&
              dex['pokédex'][i].galarian === true && dex['pokédex'][i].name.includes(formName2)
          ) {
            returnPokémon = dex['pokédex'][i];
            break;
          }
        }
      } else {
        for (let i = 0; i < dex['pokédex'].length; i++) {
          if (
            dex['pokédex'][i].name.includes(pokémonName) === true &&
            dex['pokédex'][i].galarian === true
          ) {
            returnPokémon = dex['pokédex'][i];
            break;
          }
        }
      }
    } else if (isFormHisuian) {
      for (let i = 0; i < dex['pokédex'].length; i++) {
        if (
          dex['pokédex'][i].name.includes(pokémonName) === true &&
            dex['pokédex'][i].hisuian === true
        ) {
          returnPokémon = dex['pokédex'][i];
          break;
        }
      }
    } else if (isParadoxe) {
      for (let i = 0; i < dex['pokédex'].length; i++) {
        if (
          dex['pokédex'][i].name.includes(pokémonName) === true &&
            dex['pokédex'][i].paradoxe === true
        ) {
          returnPokémon = dex['pokédex'][i];
          break;
        }
      }
    } else if (isFormPaldean) {
      if (formName2 !== null && (pokémonName === 'Tauros')) {
        for (let i = 0; i < dex['pokédex'].length; i++) {
          if (dex['pokédex'][i].name.includes(formName2) === true) {
            returnPokémon = dex['pokédex'][i];
            break;
          }
        }
      } else {
        for (let i = 0; i < dex['pokédex'].length; i++) {
          if (
            dex['pokédex'][i].name.includes(pokémonName) === true &&
              dex['pokédex'][i].paldean === true
          ) {
            returnPokémon = dex['pokédex'][i];
            break;
          }
        }
      }
    } else if (isMega) {
      if (formName2 !== null && (pokémonName === 'Dracaufeu' || pokémonName === 'Mewtwo')) {
        for (let i = 0; i < dex['pokédex'].length; i++) {
          if (
            dex['pokédex'][i].name.includes(pokémonName) === true &&
              dex['pokédex'][i].mega === true && dex['pokédex'][i].name.includes(formName2)
          ) {
            returnPokémon = dex['pokédex'][i];
            break;
          }
        }
      } else {
        for (let i = 0; i < dex['pokédex'].length; i++) {
          if (
            dex['pokédex'][i].name.includes(pokémonName) === true &&
            dex['pokédex'][i].mega === true
          ) {
            returnPokémon = dex['pokédex'][i];
            break;
          }
        }
      }
    } else if (isGiga) {
      if (formName2 !== null && pokémonName === 'Shifours') {
        for (let i = 0; i < dex['pokédex'].length; i++) {
          if (
            dex['pokédex'][i].name.includes(pokémonName) === true &&
              dex['pokédex'][i].giga === true && dex['pokédex'][i].name.includes(formName2)
          ) {
            returnPokémon = dex['pokédex'][i];
            break;
          }
        }
      } else {
        for (let i = 0; i < dex['pokédex'].length; i++) {
          if (
            dex['pokédex'][i].name.includes(pokémonName) === true &&
            dex['pokédex'][i].giga === true
          ) {
            returnPokémon = dex['pokédex'][i];
            break;
          }
        }
      }
    } else if (isPrimo) {
      for (let i = 0; i < dex['pokédex'].length; i++) {
        if (
          dex['pokédex'][i].name.includes(pokémonName) === true &&
            dex['pokédex'][i].primo === true
        ) {
          returnPokémon = dex['pokédex'][i];
          break;
        }
      }
    } else if (isMonWithForms) {
      if (formName === null) {
        for (let i = 0; i < dex['pokédex'].length; i++) {
          if (
            dex['pokédex'][i].name.includes(pokémonName)
          ) {
            returnPokémon = dex['pokédex'][i];
            break;
          }
        }
      } else {
        for (let i = 0; i < dex['pokédex'].length; i++) {
          if (
            dex['pokédex'][i].name.includes(pokémonName) &&
          dex['pokédex'][i].name.includes(formName)
          ) {
            returnPokémon = dex['pokédex'][i];
            break;
          }
        }
      }
    } else {
      for (let i = 0; i < dex['pokédex'].length; i++) {
        if (
          dex['pokédex'][i].name.includes(pokémonName) === true &&
            dex['pokédex'][i].name.length === pokémonName.length
        ) {
          returnPokémon = dex['pokédex'][i];
          break;
        }
      }
    }
    return returnPokémon;
  },
  getAllPokémonFromBiomeAndRarity: function(biomeId, rarity) {
    const dex = this.getAllPokemon();
    const output = {pokémon: []};

    for (let i = 0; i < dex['pokédex'].length; i++) {
      for (let j = 0; j < dex['pokédex'][i].biome.length; j++) {
        if (dex['pokédex'][i].biome[j] !== null) {
          if (dex['pokédex'][i].biome[j].id === biomeId &&
              dex['pokédex'][i].biome[j].rarity === rarity
          ) {
            output.pokémon.push(dex['pokédex'][i]);
          }
        }
      }
    }

    return output;
  },
  getAllPokémonFromBiome: function(biome) {
    const dex = this.getAllPokemon();
    const biomeId = biomeData.getBiomeIDFromName(biome);
    const output = {pokémon: []};

    for (let i = 0; i < dex['pokédex'].length; i++) {
      for (let j = 0; j < dex['pokédex'][i].biome.length; j++) {
        if (dex['pokédex'][i].biome[j] !== null) {
          if (dex['pokédex'][i].biome[j].id === biomeId) {
            output.pokémon.push(dex['pokédex'][i]);
          }
        }
      }
    }

    return output;
  },
};
