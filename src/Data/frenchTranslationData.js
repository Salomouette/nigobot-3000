const fs = require('fs');

module.exports = {
  getFrenchTranslations: function() {
    return JSON.parse(fs.readFileSync(process.env.FRENCH_TRANSLATION_DATA, 'utf8'));
  },
};
