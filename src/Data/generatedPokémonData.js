const fs = require('fs');

const botService = require('../Service/botService');

module.exports = {
  getAllGeneratedPokémon: function() {
    return JSON.parse(fs.readFileSync(process.env.GENERATED_POKEMON_DATA, 'utf8'));
  },
  getTodayDate: function() {
    const today = this.getAllCalendarData();
    let day = '';
    for (let i = 0; i < today.calendar.length; i++) {
      if (today.calendar[i].actualDate === true) {
        day = today.calendar[i].date;
      }
    }

    return day;
  },
  getAllCalendarData: function() {
    return JSON.parse(fs.readFileSync(process.env.CALENDAR_DATA, 'utf8'));
  },
  addAndSaveGeneratedPokemonFile: function(pokemonData) {
    const todayCalendarData = this.getTodayDate();
    const generatedPokémon = this.getAllGeneratedPokémon();
    generatedPokémon.generatedPokémon.push({
      name: pokemonData.name,
      imagePath: pokemonData.image,
      sex: pokemonData.sex,
      catchDate: todayCalendarData,
      ball: 'Poké-Ball',
      ability: pokemonData.ability,
      teraType: pokemonData.tera,
      surname: '',
      level: 1,
      friendship: pokemonData.friendship,
      genId: pokemonData.genId,
      food: pokemonData.food,
      shiny: pokemonData.shiny,
      alpha: pokemonData.alpha,
      titan: pokemonData.titan,
      antique: pokemonData.antique,
      height: pokemonData.height,
      weight: pokemonData.weight,
      moves: {
        move1: '',
        move2: '',
        move3: '',
        move4: '',
      },
      contest: pokemonData.contest,
      notes: pokemonData.traits,
      editableNotes: pokemonData.notes,
    });
    botService.jsonWriteFile(generatedPokémon, 'generatedPokémon');
  },
};
