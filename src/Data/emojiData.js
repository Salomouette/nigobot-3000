const fs = require('fs');

module.exports = {
  getAllEmojisData: function() {
    return JSON.parse(fs.readFileSync(process.env.EMOJIS_DATA, 'utf8'));
  },
  getEmojiFromName: function(name) {
    const emojis = this.getAllEmojisData();
    let output = null;

    for (let i = 0; i < emojis.emojis.length; i++) {
      if (emojis.emojis[i].name === name) {
        output = emojis.emojis[i];
      }
    }

    return output;
  },
};
