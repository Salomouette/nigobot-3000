const fs = require('fs');
const botService = require('../Service/botService');

module.exports = {
  getPastTrainerData: function() {
    return JSON.parse(fs.readFileSync(process.env.PAST_TRAINER_DATA, 'utf8'));
  },
  addPastTrainerData: function(
      name,
      type,
      age,
      gender,
      classe,
      category,
      faction,
      path,
      team,
      level,
      collectible,
      nbCollectible,
  ) {
    const trainers = this.getPastTrainerData();
    const id = Math.floor(Math.random() * 1000000);
    trainers.pastTrainers.push(
        {
          id: id,
          name: name,
          type: type,
          age: age,
          gender: gender,
          classe: classe,
          category: category,
          collectible: collectible,
          nbCollectible: nbCollectible,
          faction: faction,
          path: path,
          pokémon: [],
        },
    );
    const trainerMon = trainers.pastTrainers[trainers.pastTrainers.length - 1];

    for (let i = 0; i < team.length; i++) {
      trainerMon.pokémon.push(
          {
            species: team[i],
            level: level[i],
          },
      );
    }
    botService.jsonWriteFile(trainers, 'pastTrainers');
  },
};
