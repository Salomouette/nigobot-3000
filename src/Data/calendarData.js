const fs = require('fs');
const lodash = require('lodash');
const moment = require('moment');

const frenchTranslation = require('../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  switchDayTypeStringToIntValue: function(string) {
    let formatedInput = '';

    switch (string) {
      case 'null':
        formatedInput = 0;
        break;
      case 'Session':
        formatedInput = 1;
        break;
      case 'Off-Session':
        formatedInput = 2;
        break;
      case 'Off-Season':
        formatedInput = 3;
        break;
      case 'Event':
        formatedInput = 4;
        break;
      case 'Off-Session+Event':
        formatedInput = 5;
        break;
      case 'Session+Event':
        formatedInput = 6;
        break;
      case 'Session+Off-Session':
        formatedInput = 7;
        break;
      case 'TODO':
        formatedInput = 8;
        break;
      default:
        formatedInput = 0;
        break;
    }

    return formatedInput;
  },
  switchDayTypeIntToStringValue: function(int) {
    let formatedInput = '';

    int = parseInt(int);
    switch (int) {
      case 0:
        formatedInput = 'null';
        break;
      case 1:
        formatedInput = 'Session';
        break;
      case 2:
        formatedInput = 'Off-Session';
        break;
      case 3:
        formatedInput = 'Off-Season';
        break;
      case 4:
        formatedInput = 'Event';
        break;
      case 5:
        formatedInput = 'Off-Session+Event';
        break;
      case 6:
        formatedInput = 'Session+Event';
        break;
      case 7:
        formatedInput = 'Session+Off-Session';
        break;
      case 8:
        formatedInput = 'TODO';
        break;
      default:
        formatedInput = 'null';
        break;
    }

    return formatedInput;
  },
  getAllCalendarData: function() {
    return JSON.parse(fs.readFileSync(process.env.CALENDAR_DATA, 'utf8'));
  },
  getEventFromDayAndIdCalendarData: function(date, id) {
    const dates = this.getAllCalendarData();
    let output = '';

    if (dates.calendar.length > 0) {
      for (let i = 0; i < dates.calendar.length; i++) {
        if (dates.calendar[i].date === date) {
          for (let j = 0; j < dates.calendar[i].events.length; j++) {
            if ( dates.calendar[i].events[j].id === id) {
              output = dates.calendar[i].events[j];
            }
          }
        }
      }
    } else {
      output = tr.error.emptyCalendar;
    }

    return output;
  },
  getEndsDate: function(end, allCalendarData) {
    let endDate = allCalendarData.calendar[0];
    let output = '';

    if (allCalendarData.calendar.length > 0) {
      for (let i = 0; i < allCalendarData.calendar.length; i++) {
        const date = moment(allCalendarData.calendar[i].date, 'DD/MM/YYYY');

        switch (end) {
          case 'start':
            // Check for small isolated dates that can be time travel
            if (date.isAfter(moment(endDate.date, 'DD/MM/YYYY')) && !this.isDateIsolated(date, allCalendarData)) {
              endDate = allCalendarData.calendar[i];
            }
            break;
          case 'end':
            // Check for small isolated dates that can be time travel
            if (date.isBefore(moment(endDate.date, 'DD/MM/YYYY')) && !this.isDateIsolated(date, allCalendarData)) {
              endDate = allCalendarData.calendar[i];
            }
            break;
        }
      }
    } else {
      output = tr.error.emptyCalendar;
    }


    output = output.length > 0 ? output : endDate.date;

    return output;
  },
  isDateIsolated: function(date, allCalendarData) {
    const minDate = moment(moment(date, 'DD/MM/YYYY')).subtract(15, 'days');
    const maxDate = moment(moment(date, 'DD/MM/YYYY')).add(15, 'days');
    const dates = this.getDatesBetweenTwoDates(minDate, maxDate, allCalendarData);

    return dates.length <= 2;
  },
  displayEventsDaysOrderCalendarData: function(date) {
    const dayCalendarData = this.getDayCalendarData(date);
    dayCalendarData.events = lodash.orderBy(dayCalendarData.events, 'order', 'asc');
    return dayCalendarData;
  },
  getDatesBetweenTwoDates: function(minDate, maxDate, allCalendarData) {
    const output = [];

    for (let i = 0; i < allCalendarData.calendar.length; i++) {
      const calendarDate = moment(allCalendarData.calendar[i].date, 'DD/MM/YYYY');
      if (calendarDate.isAfter(minDate) && calendarDate.isBefore(maxDate)) {
        output.push(allCalendarData.calendar[i]);
      }
    }

    return output;
  },
  displayMonthDaysCalendarData: function(date) {
    const minDate = moment(moment('01/' + date, 'DD/MM/YYYY')).subtract(1, 'months').endOf('month');
    const maxDate = moment(moment('01/' + date, 'DD/MM/YYYY')).endOf('month');
    const allCalendarData = this.getAllCalendarData();

    let output = this.getDatesBetweenTwoDates(minDate, maxDate, allCalendarData);
    output = lodash.orderBy(output, 'date', 'asc');

    return output;
  },
  getDayCalendarData: function(date) {
    const dates = this.getAllCalendarData();
    let output = '';

    if (dates.calendar.length > 0) {
      for (let i = 0; i < dates.calendar.length; i++) {
        if (dates.calendar[i].date === date) {
          output = dates.calendar[i];
        }
      }

      output.events = lodash.orderBy(output.events, 'order', 'desc');
    } else {
      output = tr.error.emptyCalendar;
    }

    return output;
  },
  getTodayDate: function() {
    const dates = this.getAllCalendarData();
    let day = '';

    if (dates.calendar.length > 0) {
      for (let i = 0; i < dates.calendar.length; i++) {
        if (dates.calendar[i].actualDate === true) {
          day = dates.calendar[i].date;
        }
      }
    } else {
      day = tr.error.emptyCalendar;
    }

    return day;
  },
};
