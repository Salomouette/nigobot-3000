const fs = require('fs');

const botService = require('../Service/botService');
module.exports = {
  getAllBiomeCategoriesData: function() {
    return JSON.parse(fs.readFileSync(process.env.BIOME_CATEGORIES_DATA, 'utf8'));
  },
  getBiomeCategoryByName: function(name) {
    name = (botService.removeDiacritics(name)).toLowerCase();
    const biomeCategories = this.getAllBiomeCategoriesData();
    let output = null;

    for (let i = 0; i < biomeCategories.biomeCategories.length; i++) {
      const biomeCategoryName = (botService.removeDiacritics(biomeCategories.biomeCategories[i].name)).toLowerCase();
      if (biomeCategoryName === name) {
        output = biomeCategories.biomeCategories[i];
      }
    }

    return output;
  },
  getBiomeCategoryByID: function(id) {
    const biomeCategories = this.getAllBiomeCategoriesData();
    let output = null;

    for (let i = 0; i < biomeCategories.biomeCategories.length; i++) {
      if (biomeCategories.biomeCategories[i].id.toString() === id.toString()) {
        output = biomeCategories.biomeCategories[i];
      }
    }

    return output;
  },
};
