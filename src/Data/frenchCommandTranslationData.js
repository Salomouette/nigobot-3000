const fs = require('fs');

module.exports = {
  getFrenchCommandTranslations: function() {
    return JSON.parse(fs.readFileSync(process.env.FRENCH_COMMAND_TRANSLATION_DATA, 'utf8'));
  },
};
