const fs = require('fs');

const botService = require('../Service/botService');
module.exports = {
  getAllItemsData: function() {
    return JSON.parse(fs.readFileSync(process.env.ITEMS_DATA, 'utf8'));
  },
  findCategoryNameFromRequestedCategory: function(requestCategory) {
    requestCategory = (botService.removeDiacritics(requestCategory)).toLowerCase();
    const items = this.getAllItemsData();
    let category = '';

    for (let i = 0; i < items.items.length; i++) {
      const itemSubcategory = (botService.removeDiacritics(items.items[i].category)).toLowerCase();
      if (itemSubcategory === requestCategory) {
        category = items.items[i].category;
      }
    }

    return category;
  },
  findSubCategoryNameFromRequestedSubCategory: function(requestSubCategory) {
    requestSubCategory = (botService.removeDiacritics(requestSubCategory)).toLowerCase();
    const items = this.getAllItemsData();
    let subCategory = '';

    for (let i = 0; i < items.items.length; i++) {
      const itemSubcategory = (botService.removeDiacritics(items.items[i].subCategory)).toLowerCase();
      if (itemSubcategory === requestSubCategory) {
        subCategory = items.items[i].subCategory;
      }
    }

    return subCategory;
  },
  getItemFromId: function(id) {
    const items = this.getAllItemsData();
    let output = null;

    for (let i = 0; i < items.items.length; i++) {
      if (items.items[i].id == id) {
        output = items.items[i];
      }
    }

    return output;
  },
  getItemFromName: function(name) {
    name = (botService.removeDiacritics(name)).toLowerCase();
    const items = this.getAllItemsData();
    let output = null;

    for (let i = 0; i < items.items.length; i++) {
      const itemName = (botService.removeDiacritics(items.items[i].name)).toLowerCase();
      if (itemName === name) {
        output = items.items[i];
      }
    }

    return output;
  },
  getAllItemsPerCategory: function(requestCategory) {
    requestCategory = (botService.removeDiacritics(requestCategory)).toLowerCase();
    const items = this.getAllItemsData();
    const categoryItems = [];

    for (let i = 0; i < items.items.length; i++) {
      const itemSubcategory = (botService.removeDiacritics(items.items[i].category)).toLowerCase();
      if (itemSubcategory === requestCategory) {
        categoryItems.push(items.items[i]);
      }
    }

    return categoryItems;
  },
  getAllItemsNamePerSubCategory: function(requestSubCategory) {
    requestSubCategory = (botService.removeDiacritics(requestSubCategory)).toLowerCase();
    const items = this.getAllItemsData();
    const subCategoryItems = [];

    for (let i = 0; i < items.items.length; i++) {
      const itemSubcategory = (botService.removeDiacritics(items.items[i].subCategory)).toLowerCase();
      if (itemSubcategory === requestSubCategory) {
        subCategoryItems.push(items.items[i].name);
      }
    }

    return subCategoryItems;
  },
  getAllSubCategoriesPerCategory: function(requestCategory) {
    requestCategory = (botService.removeDiacritics(requestCategory)).toLowerCase();
    const items = this.getAllItemsData();
    const subCategories = [];

    for (let i = 0; i < items.items.length; i++) {
      const itemCategory = (botService.removeDiacritics(items.items[i].category)).toLowerCase();
      if (itemCategory === requestCategory) {
        if (!subCategories.includes(items.items[i].subCategory)) {
          subCategories.push(items.items[i].subCategory);
        }
      }
    }

    return subCategories;
  },
  getAllCategoriesPerCategory: function() {
    const items = this.getAllItemsData();
    const categories = [];

    for (let i = 0; i < items.items.length; i++) {
      if (!categories.includes(items.items[i].category)) {
        categories.push(items.items[i].category);
      }
    }

    return categories;
  },
  getAllFoundableItemsItemsDataByRarityByCriteria: function(rarity, biomeCategoryId, buddyFound) {
    const items = this.getAllItemsData();
    const foundableItems = [];

    for (let i = 0; i < items.items.length; i++) {
      if (items.items[i].foundableData !== undefined) {
        for (let j = 0; j < items.items[i].foundableData.foundableLocation.length; j++) {
          if (
            items.items[i].foundableData.foundableLocation[j].biomeCategoryId == biomeCategoryId &&
              items.items[i].foundableData.foundableLocation[j].rarity == rarity &&
              items.items[i].foundableData.buddyFound == buddyFound
          ) {
            foundableItems.push(items.items[i]);
          }
        }
      }
    }

    return foundableItems;
  },
};
