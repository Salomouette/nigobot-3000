const fs = require('fs');
const lodash = require('lodash');

module.exports = {
  getAllPlayers: function() {
    return JSON.parse(fs.readFileSync(process.env.PLAYER_DATA, 'utf8'));
  },
  getDiceThrowsForGroupTimePeriod: function(userID) {
    const players = this.getAllPlayers();
    let returnJson = null;
    let periodLoop = 0;

    for (let i = 0; i < players.players.length; i++) {
      if (players.players[i] != null) {
        if (players.players[i].throws !== undefined) {
          if (players.players[i].throws.length > 0) {
            if (players.players[i].id !== userID) {
              const throwsLastIndex = players.players[i].throws.length - 1;
              let timePeriod = players.players[i].throws[throwsLastIndex].timePeriod;

              if (periodLoop === 0) {
                returnJson = {
                  periods: [
                    {
                      periodName: timePeriod,
                      throws: [],
                    },
                  ],
                };
              }

              for (let j = throwsLastIndex; j >= 0; j--) {
                const periodKey = lodash.findKey(
                    returnJson.periods,
                    {periodName: players.players[i].throws[j].timePeriod},
                );
                // Only ten last time period
                if (periodLoop < 10) {
                  if (periodKey !== undefined) {
                    returnJson.periods[periodKey].throws.push(players.players[i].throws[j]);
                  } else {
                    periodLoop++;
                    if (periodLoop < 10) {
                      timePeriod = players.players[i].throws[j].timePeriod;
                      returnJson.periods.push(
                          {
                            periodName: timePeriod,
                            throws: [],
                          },
                      );
                      returnJson.periods[periodLoop].throws.push(players.players[i].throws[j]);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    return returnJson;
  },
  getDiceThrowsForPlayerTimePeriod: function(userID) {
    const players = this.getAllPlayers();
    let returnJson = null;

    for (let i = 0; i < players.players.length; i++) {
      if (players.players[i] != null) {
        if (players.players[i].throws !== undefined) {
          if (players.players[i].throws.length > 0) {
            if (players.players[i].id === userID) {
              const throwsLastIndex = players.players[i].throws.length - 1;
              let timePeriod = players.players[i].throws[throwsLastIndex].timePeriod;
              returnJson = {
                periods: [
                  {
                    periodName: timePeriod,
                    throws: [],
                  },
                ],
              };
              let periodLoop = 0;
              for (let j = throwsLastIndex; j >= 0; j--) {
                // Only ten last time period
                if (periodLoop < 10) {
                  if (players.players[i].throws[j].timePeriod === timePeriod) {
                    returnJson.periods[periodLoop].throws.push(players.players[i].throws[j]);
                  } else {
                    periodLoop++;
                    if (periodLoop < 10) {
                      timePeriod = players.players[i].throws[j].timePeriod;
                      returnJson.periods.push(
                          {
                            periodName: timePeriod,
                            throws: [],
                          },
                      );
                      returnJson.periods[periodLoop].throws.push(players.players[i].throws[j]);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    return returnJson;
  },
  getDiceThrowsForPlayerFilterThrowValidation: function(userID, throwValidation) {
    const players = this.getAllPlayers();
    const diceThrows = [];

    for (let i = 0; i < players.players.length; i++) {
      if (players.players[i] != null) {
        if (players.players[i].id === userID) {
          if (players.players[i].throws !== undefined) {
            for (let j = 0; j < players.players[i].throws.length; j++) {
              if (players.players[i].throws[j].throwValidation === throwValidation) {
                diceThrows.push(players.players[i].throws[j]);
              }
            }
          }
        }
      }
    }

    return diceThrows;
  },
  getDiceThrowsForGroupMinusPlayerFilterThrowValidation: function(userID, throwValidation) {
    const players = this.getAllPlayers();
    const diceThrows = [];

    for (let i = 0; i < players.players.length; i++) {
      if (players.players[i] != null) {
        if (players.players[i].id !== userID) {
          if (players.players[i].throws !== undefined) {
            for (let j = 0; j < players.players[i].throws.length; j++) {
              if (players.players[i].throws[j].throwValidation === throwValidation) {
                diceThrows.push(players.players[i].throws[j]);
              }
            }
          }
        }
      }
    }

    return diceThrows;
  },
  getPlayerById: function(playerId) {
    const allPlayers = this.getAllPlayers();
    let returnValue = '';

    for (let i = 0; i < allPlayers.players.length; i++) {
      if (allPlayers.players[i] != null) {
        if (allPlayers.players[i].id === playerId.toString()) {
          returnValue = allPlayers.players[i];
        }
      }
    }

    return returnValue;
  },
};
