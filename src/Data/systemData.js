const fs = require('fs');

const botService = require('../Service/botService');
module.exports = {
  getSystemProperty: function(property) {
    const json = JSON.parse(fs.readFileSync(process.env.SYSTEM_DATA, 'utf8'));
    return json[property];
  },
  setSystemProperty: function(property, value) {
    const json = JSON.parse(fs.readFileSync(process.env.SYSTEM_DATA, 'utf8'));
    json[property]= value;
    botService.jsonWriteFile(json, 'system');
  },
};
