module.exports = {
  getCharacteristicConstraintByLevel: function(characteristicLevel) {
    let returnValue = null;
    switch (parseInt(characteristicLevel)) {
      case 0: {
        returnValue = parseInt(process.env.CHARACTERISTIC_POINT_0);
        break;
      }
      case 1: {
        returnValue = parseInt(process.env.CHARACTERISTIC_POINT_1);
        break;
      }
      case 2: {
        returnValue = parseInt(process.env.CHARACTERISTIC_POINT_2);
        break;
      }
      case 3: {
        returnValue = parseInt(process.env.CHARACTERISTIC_POINT_3);
        break;
      }
      case 4: {
        returnValue = parseInt(process.env.CHARACTERISTIC_POINT_4);
        break;
      }
      case 5: {
        returnValue = parseInt(process.env.CHARACTERISTIC_POINT_5);
        break;
      }
      case 6: {
        returnValue = parseInt(process.env.CHARACTERISTIC_POINT_6);
        break;
      }
    }

    return returnValue;
  },
  getSkillConstraintByLevel: function(skillLevel) {
    let returnValue = null;

    switch (parseInt(skillLevel)) {
      case 0: {
        returnValue = parseInt(process.env.SKILL_LEVEL_0);
        break;
      }
      case 1: {
        returnValue = parseInt(process.env.SKILL_LEVEL_1);
        break;
      }
      case 2: {
        returnValue = parseInt(process.env.SKILL_LEVEL_2);
        break;
      }
      case 3: {
        returnValue = parseInt(process.env.SKILL_LEVEL_3);
        break;
      }
      case 4: {
        returnValue = parseInt(process.env.SKILL_LEVEL_4);
        break;
      }
    }

    return returnValue;
  },
};
