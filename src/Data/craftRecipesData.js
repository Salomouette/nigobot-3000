const fs = require('fs');
const botService = require('../Service/botService');

module.exports = {
  getAllCraftRecipes: function() {
    return JSON.parse(fs.readFileSync(process.env.CRAFT_RECIPES_DATA, 'utf8'));
  },
  setCraftRecipeDiscovered: function(id) {
    const recipes = this.getAllCraftRecipes();
    recipes.recipes[id].discovered = true;

    botService.jsonWriteFile(recipes, 'craftRecipes');
  },
  getCraftRecipesFromNameAndDiscovered: function(name) {
    const recipes = this.getAllCraftRecipes();
    const output = [];

    for (let i = 0; i < recipes.recipes.length; i++) {
      if (recipes.recipes[i].name === name) {
        if (recipes.recipes[i].discovered) {
          output.push(recipes.recipes[i]);
        }
      }
    }

    return output;
  },
  getDiscoveredCraftModuleRecipeFromName: function(name) {
    const recipes = this.getAllCraftRecipes();
    const output = '';

    for (let i = 0; i < recipes.recipes.length; i++) {
      if (recipes.recipes[i].name === name && recipes.recipes[i].discovered) {
        return recipes.recipes[i];
      }
    }

    return output;
  },
  getCraftRecipeDiscovered: function(nameType) {
    const recipes = this.getAllCraftRecipes();
    const output = [];

    for (let i = 0; i < recipes.recipes.length; i++) {
      if (recipes.recipes[i].type === nameType && recipes.recipes[i].discovered === true) {
        output.push(recipes.recipes[i]);
      }
    }

    return output;
  },
};
