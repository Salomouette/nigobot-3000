const path = require('path');
const fs = require('fs');
const moment = require('moment');
const lodash = require('lodash');
const pokémonData = require('../Data/pokémonData');
require('dotenv').config({path: path.resolve(__dirname, '../..', '.env')});

const allgeneratedPokemonData = JSON.parse(fs.readFileSync('../Data/jsons/dynamicData/generatedPokemon.json', 'utf8'));
const allPlayersPokemonData = JSON.parse(fs.readFileSync('../Data/jsons/dynamicData/playersPokémon.json', 'utf8'));
const pokedex = JSON.parse(fs.readFileSync('../Data/jsons/staticData/pokémon.json', 'utf8'));

// Migration des données Players Pokémon afin d'ajouter le type Téracristal
try {
  for (let i = 0; i < allPlayersPokemonData.playersPokémon.length; i++) {
    for (let j = 0; j < allPlayersPokemonData.playersPokémon[i].playerPokémon.length; j++) {
      if (allPlayersPokemonData.playersPokémon[i].playerPokémon[j].teraType == undefined) {
        const type = searchInTheDex(allPlayersPokemonData.playersPokémon[i].playerPokémon[j].name);
        const teraType = generateTera(type);

        allPlayersPokemonData.playersPokémon[i].playerPokémon[j].teraType = teraType;
      }
    }
  }
} catch (err) {
  console.log('Une erreur est survenue:' + err);
} finally {
  fs.writeFile( '../Data/jsons/dumps/playersPokemon/' + moment().unix() + '.json',
      fs.readFileSync('../Data/jsons/dynamicData/playersPokémon.json', 'utf8'),
      function() {},
  );
  fs.writeFile( '../Data/jsons/dynamicData/playersPokémon.json',
      JSON.stringify(allPlayersPokemonData),
      function() {},
  );
  console.log('Migration playersPokémon Terminé');
}

// Migration des données Generated Pokémon afin d'ajouter le type Téracristal
try {
  for (let i = 0; i < allgeneratedPokemonData.generatedPokémon.length; i++) {
    if (allgeneratedPokemonData.generatedPokémon[i].teraType == undefined) {
      const type = searchInTheDex(allgeneratedPokemonData.generatedPokémon[i].name);
      const teraType = generateTera(type);

      allgeneratedPokemonData.generatedPokémon[i].teraType = teraType;
    }
  }
} catch (err) {
  console.log('Une erreur est survenue:' + err);
} finally {
  fs.writeFile( '../Data/jsons/dumps/generatedPokemon/' + moment().unix() + '.json',
      fs.readFileSync('../Data/jsons/dynamicData/generatedPokemon.json', 'utf8'),
      function() {},
  );
  fs.writeFile( '../Data/jsons/dynamicData/generatedPokemon.json',
      JSON.stringify(allgeneratedPokemonData),
      function() {},
  );
  console.log('Migration generatedPokemon Terminé');
}

// dans deux try différents, pour que le second puisse opérer quand même si le premier marche po

// fonctions
function generateTera(generatePokemonTypes) {
  let teraType = '';
  const rand = Math.floor(Math.random()*process.env.TERA_CHANCE);
  if (rand == 0) {
    let possibleTypes = Array.from(pokémonData.types);

    possibleTypes = lodash.remove(possibleTypes, function(n) {
      return n !== generatePokemonTypes.first && n !== generatePokemonTypes.second;
    });

    teraType = possibleTypes[Math.floor(Math.random()*possibleTypes.length)];
  } else {
    const chooseBetweenTwo = generatePokemonTypes.second === undefined ? 0 : Math.floor(Math.random()*2);
    teraType = chooseBetweenTwo === 0 ? generatePokemonTypes.first : generatePokemonTypes.second;
  }

  return teraType;
}

function searchInTheDex(name) {
  let type = '';
  for (let k = 0; k < pokedex.pokédex.length; k++) {
    if (pokedex.pokédex[k].name == name) {
      type = pokedex.pokédex[k].type;
      break;
    }
  }

  return type;
}
