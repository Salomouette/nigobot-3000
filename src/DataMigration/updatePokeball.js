const path = require('path');
const fs = require('fs');
const moment = require('moment');
require('dotenv').config({path: path.resolve(__dirname, '../..', '.env')});

const allPlayersPokemonData = JSON.parse(fs.readFileSync('../Data/jsons/dynamicData/playersPokémon.json', 'utf8'));
const balls = JSON.parse(fs.readFileSync('../Data/jsons/staticData/balls.json', 'utf8'));

try {
  for (let i = 0; i < allPlayersPokemonData.playersPokémon.length; i++) {
    for (let j = 0; j < allPlayersPokemonData.playersPokémon[i].playerPokémon.length; j++) {
      const pokemon = allPlayersPokemonData.playersPokémon[i].playerPokémon[j];
      pokemon.ball = giveThisPokémonABall(pokemon.ball);
    }
  }
} catch (err) {
  console.log('Une erreur est survenue:' + err);
} finally {
  fs.writeFile( '../Data/jsons/dumps/playersPokemon/' + moment().unix() + '.json',
      fs.readFileSync('../Data/jsons/dynamicData/playersPokémon.json', 'utf8'),
      function() {},
  );
  fs.writeFile( '../Data/jsons/dynamicData/playersPokémon.json',
      JSON.stringify(allPlayersPokemonData),
      function() {},
  );
  console.log('L\'ensemble des Pokéball des joueurs a été mise à jour ! Prenez gare aux Étrange Ball !');
}

function giveThisPokémonABall(pokemonBall) {
  pokemonBall = toutcorriger(pokemonBall);
  for (let k = 0; k < balls.balls.length; k++) {
    if (toutcorriger(balls.balls[k].name) === pokemonBall || toutcorriger(balls.balls[k].id) === pokemonBall) {
      return balls.balls[k].id;
    }
  }
  // Si la Pokéball n'est pas trouvé, ID de l'Étrange Ball
  return 27;
}

function toutcorriger(string) {
  return string.toString().toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '')
      .replace(/([^\w]+|\s+)/g, '')
      .replace(/\-\-+/g, '')
      .replace(/(^-+|-+$)/g, '');
}
