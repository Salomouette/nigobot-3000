const path = require('path');
const fs = require('fs');
const lodash = require('lodash');
const moment = require('moment');
require('dotenv').config({path: path.resolve(__dirname, '../..', '.env')});

const allCalendarData = JSON.parse(fs.readFileSync('../Data/jsons/dynamicData/calendar.json', 'utf8'));
try {
  for (let i = 0; i < allCalendarData.calendar.length; i++) {
    allCalendarData.calendar[i].events = lodash.orderBy(allCalendarData.calendar[i].events, 'level', 'desc');
    for (let j = 0; j < allCalendarData.calendar[i].events.length; j++) {
      allCalendarData.calendar[i].events[j].order = allCalendarData.calendar[i].events[j].id;
    }
  }
} catch (err) {
  console.log('Une erreur est survenue:' + err);
} finally {
  fs.writeFile( '../Data/jsons/dumps/calendar/' + moment().unix() + '.json',
      fs.readFileSync('../Data/jsons/dynamicData/calendar.json', 'utf8'),
      function() {},
  );
  fs.writeFile( '../Data/jsons/dynamicData/calendar.json',
      JSON.stringify(allCalendarData),
      function() {},
  );
  console.log('Terminé');
}
