const path = require('path');
const fs = require('fs');
const moment = require('moment');
require('dotenv').config({path: path.resolve(__dirname, '../..', '.env')});

const allPlayerData = JSON.parse(fs.readFileSync('../Data/jsons/dynamicData/players.json', 'utf8'));
try {
  for (let i = 0; i < allPlayerData.players.length; i++) {
    allPlayerData.players[i].inventory = {};
    allPlayerData.players[i].inventory.foundItems = [];
  }
} catch (err) {
  console.log('Une erreur est survenue:' + err);
} finally {
  fs.writeFile( '../Data/jsons/dumps/players/' + moment().unix() + '.json',
      fs.readFileSync('../Data/jsons/dynamicData/players.json', 'utf8'),
      function() {},
  );
  fs.writeFile( '../Data/jsons/dynamicData/players.json',
      JSON.stringify(allPlayerData),
      function() {},
  );
  console.log('Terminé');
}
