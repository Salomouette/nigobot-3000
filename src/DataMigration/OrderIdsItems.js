const path = require('path');
const fs = require('fs');
require('dotenv').config({path: path.resolve(__dirname, '../..', '.env')});

const allItemDatas = JSON.parse(fs.readFileSync('../Data/jsons/staticData/items.json', 'utf8'));
try {
  for (let i = 0; i < allItemDatas.items.length; i++) {
    allItemDatas.items[i].id = i;
  }
} catch (err) {
  console.log('Une erreur est survenue:' + err);
} finally {
  fs.writeFile( '../Data/jsons/staticData/items.json',
      JSON.stringify(allItemDatas),
      function() {},
  );
  console.log('Terminé');
}
