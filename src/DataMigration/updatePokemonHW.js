const path = require('path');
const fs = require('fs');
const moment = require('moment');
require('dotenv').config({path: path.resolve(__dirname, '../..', '.env')});

const allgeneratedPokemonData = JSON.parse(fs.readFileSync('../Data/jsons/dynamicData/generatedPokemon.json', 'utf8'));
const allPlayersPokemonData = JSON.parse(fs.readFileSync('../Data/jsons/dynamicData/playersPokémon.json', 'utf8'));
const pokedex = JSON.parse(fs.readFileSync('../Data/jsons/staticData/pokémon.json', 'utf8'));
try {
  for (let i = 0; i < allPlayersPokemonData.playersPokémon.length; i++) {
    for (let j = 0; j < allPlayersPokemonData.playersPokémon[i].playerPokémon.length; j++) {
      const pokemon = allPlayersPokemonData.playersPokémon[i].playerPokémon[j];
      if (pokemon.weight.multiplicateur == undefined || pokemon.height.multiplicateur == undefined) {
        const {baseHeight, baseWeight} = searchInTheDex(pokemon.name);
        const weight = pokemon.weight.number;
        const height = pokemon.height.number;
        pokemon.weight = {multiplicateur: weight/baseWeight, emoji: pokemon.weight.emoji};
        pokemon.height = {multiplicateur: height/baseHeight, emoji: pokemon.height.emoji};
      }
    }
  }
} catch (err) {
  console.log('Une erreur est survenue:' + err);
} finally {
  fs.writeFile( '../Data/jsons/dumps/playersPokemon/' + moment().unix() + '.json',
      fs.readFileSync('../Data/jsons/dynamicData/playersPokémon.json', 'utf8'),
      function() {},
  );
  fs.writeFile( '../Data/jsons/dynamicData/playersPokémon.json',
      JSON.stringify(allPlayersPokemonData),
      function() {},
  );
  console.log('Update des données taille poids terminée !');
}

try {
  for (let i = 0; i < allgeneratedPokemonData.generatedPokémon.length; i++) {
    const pokemon = allgeneratedPokemonData.generatedPokémon[i];
    if (pokemon.weight.multiplicateur == undefined || pokemon.height.multiplicateur == undefined) {
      const {baseHeight, baseWeight} = searchInTheDex(pokemon.name);
      const weight = pokemon.weight.number;
      const height = pokemon.height.number;
      pokemon.weight = {multiplicateur: weight/baseWeight, emoji: pokemon.weight.emoji};
      pokemon.height = {multiplicateur: height/baseHeight, emoji: pokemon.height.emoji};
    }
  }
} catch (err) {
  console.log('Une erreur est survenue:' + err);
} finally {
  fs.writeFile( '../Data/jsons/dumps/generatedPokemon/' + moment().unix() + '.json',
      fs.readFileSync('../Data/jsons/dynamicData/generatedPokemon.json', 'utf8'),
      function() {},
  );
  fs.writeFile( '../Data/jsons/dynamicData/generatedPokemon.json',
      JSON.stringify(allgeneratedPokemonData),
      function() {},
  );
  console.log('Migration generatedPokemon Terminé');
}

function searchInTheDex(name) {
  let height = 0;
  let weight = 0;
  for (let k = 0; k < pokedex.pokédex.length; k++) {
    if (pokedex.pokédex[k].name == name) {
      height = pokedex.pokédex[k].height;
      weight = pokedex.pokédex[k].weight;
      break;
    }
  }

  return {baseHeight: height, baseWeight: weight};
}
