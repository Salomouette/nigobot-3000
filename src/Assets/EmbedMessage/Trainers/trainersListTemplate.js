const Discord = require('discord.js');

const trainerData = require('../../../Data/trainerData');
module.exports = {
  trainersListTemplate: function() {
    const trainers = trainerData.getAllTrainer();
    const embed = new Discord.MessageEmbed()
        .setColor(process.env.COLOR_BATTLE_QUERY)
        .setTitle('Liste des dresseurs')
        .setAuthor({
          name: 'Partie Combat',
          iconURL: process.env.SET_AUTHOR_BATTLE_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });

    let output = '';
    for (let i = 0; i < trainers.trainers.length; i++) {
      output = output + '- ' + trainers.trainers[i].className + '\n';
    }

    embed.addFields(
        {
          name: 'Liste des classes de dresseurs',
          value: output,
        },
    );
    return embed;
  },
};
