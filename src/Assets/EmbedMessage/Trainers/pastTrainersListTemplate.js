const Discord = require('discord.js');

const pastTrainersData = require('../../../Data/pastTrainersData');
module.exports = {
  pastTrainersListTemplate: function() {
    const pastTrainers = pastTrainersData.getPastTrainerData();
    const embed = new Discord.MessageEmbed()
        .setColor(process.env.COLOR_BATTLE_QUERY)
        .setTitle('Liste des dresseurs déjà affrontés')
        .setAuthor({
          name: 'Partie Combat',
          iconURL: process.env.SET_AUTHOR_BATTLE_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });

    let output = '';
    for (let i = 0; i < pastTrainers.pastTrainers.length; i++) {
      output = output + '- ' +
            pastTrainers.pastTrainers[i].type +
            ' ' +
            pastTrainers.pastTrainers[i].name +
            ', ' +
            pastTrainers.pastTrainers[i].classe +
            ' ' +
            pastTrainers.pastTrainers[i].category +
            ', ID: ' +
            pastTrainers.pastTrainers[i].id +
            '\n';
    }
    embed.addFields(
        {
          name: 'Liste des dresseurs',
          value: output,
        },
    );
    return embed;
  },
};
