const Discord = require('discord.js');
const botService = require('../../../Service/botService');
const trainerData = require('../../../Data/trainerData');
const trainerViewService = require('../../../Service/Trainer/trainerViewService');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const pastTrainersData = require('../../../Data/pastTrainersData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  trainersGeneratorTemplate: function(trainer, classe, nbPokémon, badges, categorie, biome) {
    const mastersEquiv = ['masters', 'master', 'Masters', 'Master', 'm', 'M'];
    const embed = new Discord.MessageEmbed();
    let image = '';
    if (trainer === 'None') {
      trainer = trainerData.getTrainersByBiome(biome);
    }

    let trainerDatas = '';
    if (trainer !== undefined) {
      trainerDatas = trainerData.getTrainerData(trainer);
    }

    if (trainerDatas !== '') {
      // #region Quick Fixes
      if (badges > 8) {
        badges = 8;
      }
      // default collectible value
      if (badges == null || badges < 0) {
        badges = 0;
      }

      // if false default category value
      if (mastersEquiv.includes(categorie)) {
        categorie = 'Master';
      } else {
        categorie = 'Normal';
      }
      // #endregion

      // #region Trainer's Identity elements
      const age = Math.floor(Math.random() * (trainerDatas.ageRange.max - trainerDatas.ageRange.min)) +
          trainerDatas.ageRange.min;
      const faction = trainerDatas.possibleFaction[Math.floor(Math.random() *
          (trainerDatas.possibleFaction.length))].name;
      const gender = trainerViewService.getTrainerGender(
          trainerDatas.miscInformations.male,
          trainerDatas.miscInformations.female,
          tr.trainers.gender,
      );
      const path = trainerViewService.getTrainerAppearance(
          gender,
          trainerDatas.miscInformations.maleMugshot,
          trainerDatas.miscInformations.femaleMugshot,
          tr.trainers.gender,
      );
      const randomName = trainerViewService.giveTrainerName(gender, tr.trainers.gender);
      image = botService.generatePathTrainerMugshot(path);
      classe = trainerViewService.setClass(classe, gender, trainer, tr.trainers);
      const typeCollectible = trainerViewService.setCollectibles(classe, tr.trainers);
      const title = trainerViewService.setTitle(randomName, trainer, gender, tr.trainers);
      // #endregion

      // Pokémon of the Trainer
      const {team: pokémon, niv: level} =
          trainerViewService.getTrainerMonData(trainerDatas.predilectionType, classe, nbPokémon, badges, categorie);

      embed
          .setAuthor({
            name: tr.author.trainers,
            iconURL: process.env.SET_AUTHOR_BATTLE_ICON,
          })
          .setTitle(title)
          .setColor(process.env.COLOR_BATTLE_QUERY)
          .addFields(
              {
                name: 'Âge',
                value: age + ' ans',
                inline: true,
              },
              {
                name: 'Faction',
                value: faction,
                inline: true,
              },
              {
                name: 'Genre',
                value: gender,
                inline: true,
              },
              {
                name: typeCollectible,
                value: badges,
                inline: true,
              },
              {
                name: 'Classe',
                value: classe,
                inline: true,
              },
              {
                name: 'Catégorie',
                value: categorie,
                inline: true,
              }
              ,
              {
                name: '\u200b',
                value: '\u200b',
                inline: false,
              },
          )
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          });
      for (let i = 0; i < pokémon.length; i++) {
        embed.addFields(
            {
              name: pokémon[i],
              value: tr.trainers.level + ' ' + level[i],
              inline: true,
            },
        );
      }

      pastTrainersData.addPastTrainerData(
          randomName,
          trainer,
          age,
          gender,
          classe,
          categorie,
          faction,
          image,
          pokémon,
          level,
          typeCollectible,
          badges,
      );

      if (image.length > 0) {
        embed.setThumbnail('attachment://' + botService.getFilename(image));
        return {
          embedMessage: embed,
          images: [image],
        };
      } else {
        return {
          embedMessage: embed,
          images: '',
        };
      }
      // #endregion
      // Error Embed Message
    } else {
      embed.setTitle(tr.error.errorTitle)
          .setColor(process.env.COLOR_BATTLE_QUERY)
          .setAuthor({
            name: tr.author.trainers,
            iconURL: process.env.SET_AUTHOR_BATTLE_ICON,
          })
          .addFields(
              {
                name: tr.error.errorGenerateTrainer.fieldTitle,
                value: tr.error.errorGenerateTrainer.fieldContent,
              })
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          })
          .setTimestamp();

      return {
        embedMessage: embed,
        images: '',
      };
    }
  },
};
