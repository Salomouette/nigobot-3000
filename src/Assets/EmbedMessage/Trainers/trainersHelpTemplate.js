const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();

module.exports = {
  trainersHelpTemplate: function() {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_HELP_QUERY)
        .setTitle(tr.help.trainers.helpCommandTitle)
        .addFields(
            {
              name: tr.help.trainers.generateByClass.fieldTitle,
              value: tr.help.trainers.generateByClass.fieldContent,
            },
            {
              name: tr.help.trainers.generateByBiome.fieldTitle,
              value: tr.help.trainers.generateByBiome.fieldContent,
            }, {
              name: tr.help.trainers.memoryDex.fieldTitle,
              value: tr.help.trainers.memoryDex.fieldContent,
            },
            {
              name: tr.help.trainers.viewPastTrainers.fieldTitle,
              value: tr.help.trainers.viewPastTrainers.fieldContent,
            },
            {
              name: tr.help.trainers.values.fieldTitle,
              value: tr.help.trainers.values.fieldContent,
            },
        )
        .setAuthor({
          name: tr.author.trainers,
          iconURL: process.env.SET_AUTHOR_HELP_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
