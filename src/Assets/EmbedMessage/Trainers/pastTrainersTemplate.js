const Discord = require('discord.js');
const botService = require('../../../Service/botService');
const pastTrainerViewService = require('../../../Service/Trainer/pastTrainerViewService');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  pastTrainersTemplate: function(id) {
    const embed = new Discord.MessageEmbed();
    id = parseInt(id);
    const trainer = pastTrainerViewService.getPastTrainerById(id);
    const name = trainer.name;
    const type = trainer.type;
    const age = trainer.age;
    const gender = trainer.gender;
    const classe = trainer.classe;
    const category = trainer.category;
    const faction = trainer.faction;
    const imagePath = trainer.path;
    const pokémon = trainer.pokémon;
    const collectible = trainer.collectible;
    const nbCollectible = trainer.nbCollectible;

    embed
        .setAuthor({
          name: tr.author.trainers,
          iconURL: process.env.SET_AUTHOR_BATTLE_ICON,
        })
        .setTitle(type + ' ' + name)
        .setColor(process.env.COLOR_BATTLE_QUERY)
        .addFields(
            {
              name: 'Âge',
              value: age + ' ans',
              inline: true,
            },
            {
              name: 'Faction',
              value: faction,
              inline: true,
            },
            {
              name: 'Genre',
              value: gender,
              inline: true,
            },
            {
              name: collectible,
              value: nbCollectible,
              inline: true,
            },
            {
              name: 'Classe',
              value: classe,
              inline: true,
            },
            {
              name: 'Catégorie',
              value: category,
              inline: true,
            }
            ,
            {
              name: '\u200b',
              value: '\u200b',
              inline: false,
            },
        )
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
    for (let i = 0; i < pokémon.length; i++) {
      embed.addFields(
          {
            name: pokémon[i].species,
            value: tr.trainers.level + ' ' + pokémon[i].level,
            inline: true,
          },
      );
    }

    if (imagePath.length > 0) {
      embed.setImage('attachment://' + botService.getFilename(imagePath));
      return {
        embedMessage: embed,
        images: [imagePath],
      };
    } else {
      return {
        embedMessage: embed,
        images: '',
      };
    }
  },
};
