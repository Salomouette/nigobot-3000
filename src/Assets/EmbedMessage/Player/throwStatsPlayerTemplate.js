const playerStatsViewTemplate = require('../../../Service/Player/Stats/playerStatsViewService');
const privateMessageAlertTemplate = require('../../EmbedMessage/privateMessageAlertTemplate');

const botService = require('../../../Service/botService');
const playerService = require('../../../Service/playerService');
const messageService = require('../../../Service/messageService');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const Discord = require('discord.js');
const tr = frenchTranslation.getFrenchTranslations();

const diagramImagePath = 'src/Assets/Temp/throwDiagramPlayer.png';
module.exports = {
  throwStatsPlayerTemplate: async function(msg) {
    if (playerService.checkIfUserAlreadyAdded(msg.author.id)) {
      playerStatsViewTemplate.generateThrowDiagramPlayerImage(msg.author.id);
      messageService.sendEmbedMessage(msg.channel, privateMessageAlertTemplate.privateMessageAlertTemplate(), true);

      await new Promise((resolve) => setTimeout(resolve, 10000)).then(async function() {
        const fields = playerStatsViewTemplate.generateThrowStatsPlayerFields(msg.author.id);
        let embedMessage = '';

        if (fields.length > 0) {
          embedMessage = new Discord.MessageEmbed()
              .setColor(process.env.COLOR_PLAYER_QUERY)
              .setTitle(tr.player.stats.title)
              .setAuthor({
                name: tr.author.player,
                iconURL: process.env.SET_AUTHOR_PLAYER_ICON,
              })
              .setTimestamp()
              .setFooter({
                text: process.env.NIGOBOT_NAME,
                iconURL: process.env.NIGOBOT_AVATAR_LINK,
              })
              .setImage('attachment://' + botService.getFilename(diagramImagePath));

          for (let i = 0; i < fields.length; i++) {
            embedMessage.addFields(fields[i]);
          }
        } else {
          embedMessage = new Discord.MessageEmbed()
              .setColor(process.env.COLOR_PLAYER_QUERY)
              .setTitle(tr.player.stats.title)
              .setDescription(tr.player.stats.noData)
              .setAuthor({
                name: tr.author.player,
                iconURL: process.env.SET_AUTHOR_PLAYER_ICON,
              })
              .setTimestamp()
              .setFooter({
                text: process.env.NIGOBOT_NAME,
                iconURL: process.env.NIGOBOT_AVATAR_LINK,
              });
        }
        messageService.sendEmbedMessageWithImages(msg.author, embedMessage, [diagramImagePath], false);
      });
    } else {
      const embedMessage = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_PLAYER_QUERY)
          .setTitle(tr.error.errorTitle)
          .setDescription(tr.error.userNotPlayer)
          .setAuthor({
            name: tr.author.player,
            iconURL: process.env.SET_AUTHOR_PLAYER_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          });

      messageService.sendEmbedMessage(msg.channel, embedMessage, true);
    }
  },
};
