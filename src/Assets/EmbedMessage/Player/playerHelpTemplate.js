const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  playerHelpTemplate: function() {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_HELP_QUERY)
        .setTitle(tr.help.player.helpCommandTitle)
        .addFields(
            {
              name: tr.help.player.money.money.fieldTitle,
              value: tr.help.player.money.money.fieldContent,
            },
            {
              name: tr.help.player.money.useMoney.fieldTitle,
              value: tr.help.player.money.useMoney.fieldContent,
            },
            {
              name: tr.help.player.money.foundItem.fieldTitle,
              value: tr.help.player.money.foundItem.fieldContent,
            },
            {
              name: tr.help.player.money.throwStats.fieldTitle,
              value: tr.help.player.money.throwStats.fieldContent,
            },
            {
              name: tr.help.player.money.viewMoney.fieldTitle,
              value: tr.help.player.money.viewMoney.fieldContent,
            },
            {
              name: tr.help.player.money.addPlayerMoney.fieldTitle,
              value: tr.help.player.money.addPlayerMoney.fieldContent,
            },
            {
              name: tr.help.player.money.removePlayerMoney.fieldTitle,
              value: tr.help.player.money.removePlayerMoney.fieldContent,
            },
            {
              name: tr.help.player.money.addAllPlayersMoney.fieldTitle,
              value: tr.help.player.money.addAllPlayersMoney.fieldContent,
            },
            {
              name: tr.help.player.money.removeAllPlayersMoney.fieldTitle,
              value: tr.help.player.money.removeAllPlayersMoney.fieldContent,
            },
        )
        .setAuthor({
          name: tr.author.player,
          iconURL: process.env.SET_AUTHOR_HELP_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
