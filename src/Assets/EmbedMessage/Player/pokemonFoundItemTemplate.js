const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  pokemonFoundItemTemplate: function(title, message) {
    const embedMessage = new Discord.MessageEmbed()
        .setColor(process.env.COLOR_POKEMON_QUERY)
        .setTitle(title)
        .setAuthor({
          name: tr.author.pokémon,
          iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        })
        .setDescription(message);

    return embedMessage;
  },
};
