const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  systemHelpTemplate: function() {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_HELP_QUERY)
        .setTitle(tr.help.system.helpCommandTitle)
        .addFields(
            {
              name: tr.help.system.dump.fieldTitle,
              value: tr.help.system.dump.fieldContent,
            },
            {
              name: tr.help.system.addPlayer.fieldTitle,
              value: tr.help.system.addPlayer.fieldContent,
            },
            {
              name: tr.help.system.removePlayer.fieldTitle,
              value: tr.help.system.removePlayer.fieldContent,
            },
            {
              name: tr.help.system.removeAllPlayers.fieldTitle,
              value: tr.help.system.removeAllPlayers.fieldContent,
            },
            {
              name: tr.help.system.addLogChannelId.fieldTitle,
              value: tr.help.system.addLogChannelId.fieldContent,
            },
            {
              name: tr.help.system.initializeMoney.fieldTitle,
              value: tr.help.system.initializeMoney.fieldContent,
            },
            {
              name: tr.help.system.setPeriod.fieldTitle,
              value: tr.help.system.setPeriod.fieldContent,
            },
            {
              name: tr.help.system.setBiomeCategory.fieldTitle,
              value: tr.help.system.setBiomeCategory.fieldContent,
            },
        )
        .setAuthor({
          name: tr.author.system,
          iconURL: process.env.SET_AUTHOR_HELP_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
