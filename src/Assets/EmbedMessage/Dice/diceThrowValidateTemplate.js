const Discord = require('discord.js');

const diceViewService = require('../../../Service/Dice/diceViewService');
const botService = require('../../../Service/botService');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  diceThrowValidateTemplate: function(
      characteristicInput,
      skillInput,
      malusInput,
      userID,
  ) {
    const description = diceViewService.generateDiceThrowValidateDescription(
        characteristicInput,
        skillInput,
        malusInput,
        userID,
    );
    let resultsInputs = null;
    let diceImage = null;

    const embedMessage = new Discord.MessageEmbed()
        .setColor(process.env.COLOR_DICE_QUERY)
        .setTitle(tr.dice.diceThrown)
        .setDescription(description)
        .setAuthor({
          name: tr.author.dice,
          iconURL: process.env.SET_AUTHOR_DICE_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });

    if (!(description.toString()).includes(tr.error.errorInvalidArguments)) {
      resultsInputs = diceViewService.generateInputsThrowResult(description);
      diceImage = botService.generateFileAbsolutePath(resultsInputs.imagePath);
      embedMessage
          .setAuthor({
            name: tr.author.dice,
            iconURL: resultsInputs.embedAuthorIcon,
          })
          .setColor(resultsInputs.colorEmbed);
    }

    if (diceImage.length > 0) {
      embedMessage.setThumbnail('attachment://' + botService.getFilename(diceImage));
      return {
        embedMessage: embedMessage,
        images: [diceImage],
      };
    } else {
      return {
        embedMessage: embedMessage,
        images: '',
      };
    }
  },
};
