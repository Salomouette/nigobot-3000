const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  diceHelpTemplate: function() {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_HELP_QUERY)
        .setTitle(tr.help.dice.helpCommandTitle)
        .addFields(
            {
              name: tr.help.dice.randomDices.fieldTitle,
              value: tr.help.dice.randomDices.fieldContent,
            },
            {
              name: tr.help.dice.sumRandomDices.fieldTitle,
              value: tr.help.dice.sumRandomDices.fieldContent,
            },
            {
              name: tr.help.dice.validateThrowDices.fieldTitle,
              value: tr.help.dice.validateThrowDices.fieldContent,
            },
            {
              name: tr.help.dice.privateValidateThrowDices.fieldTitle,
              value: tr.help.dice.privateValidateThrowDices.fieldContent,
            },
        )
        .setAuthor({
          name: tr.author.dice,
          iconURL: process.env.SET_AUTHOR_HELP_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
