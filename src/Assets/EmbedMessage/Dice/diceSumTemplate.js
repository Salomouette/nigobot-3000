const Discord = require('discord.js');

const diceViewService = require('../../../Service/Dice/diceViewService');
const botService = require('../../../Service/botService');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  diceSumTemplate: function(callInput, userID) {
    const diceImage = botService.generateFileAbsolutePath('/src/Assets/nigobot-3000-images/Dice/dices.gif');
    const description = diceViewService.generateDiceSumDescription(callInput, userID);
    const embedMessage = new Discord.MessageEmbed()
        .setColor(process.env.COLOR_DICE_QUERY)
        .setTitle(tr.dice.diceThrown)
        .setDescription(description)
        .setThumbnail('attachment://' + botService.getFilename(diceImage))
        .setAuthor({
          name: tr.author.dice,
          iconURL: process.env.SET_AUTHOR_DICE_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });

    if (diceImage.length > 0) {
      embedMessage.setThumbnail('attachment://' + botService.getFilename(diceImage));
      return {
        embedMessage: embedMessage,
        images: [diceImage],
      };
    } else {
      return {
        embedMessage: embedMessage,
        images: '',
      };
    }
  },
};
