const Discord = require('discord.js');

const itemsData = require('../../../Data/itemsData');

const botService = require('../../../Service/botService');
const berryViewService = require('../../../Service/Berry/berryViewService');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  berryMixTemplate: function(
      baieInput1,
      baieInput2,
      baieInput3,
      baieInput4,
      templateDiceResult,
      client,
  ) {
    let embedMessage = '';
    let image = '';

    let isValidInputInformation =
        itemsData.getItemFromName(baieInput1).length !== 0 &&
        itemsData.getItemFromName(baieInput2).length !== 0 &&
        itemsData.getItemFromName(baieInput3).length !== 0 &&
        itemsData.getItemFromName(baieInput4).length !== 0;

    isValidInputInformation = isValidInputInformation ?
      !JSON.stringify(templateDiceResult).includes(tr.error.errorInvalidArguments) :
      isValidInputInformation;

    if (isValidInputInformation) {
      image = botService.generateFileAbsolutePath('/src/Assets/nigobot-3000-images/MixeurBaie.png');
      const description = berryViewService.generateBerryMixDescription(
          baieInput1,
          baieInput2,
          baieInput3,
          baieInput4,
          templateDiceResult,
          client,
      );

      embedMessage = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_FRUIT_QUERY)
          .setTitle(tr.berry.berryMixTemplate.title)
          .setDescription(description)
          .setAuthor({
            name: tr.author.berry,
            iconURL: process.env.SET_AUTHOR_FRUIT_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          });

      embedMessage.setThumbnail('attachment://' + botService.getFilename(image));
    } else {
      embedMessage = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_FRUIT_QUERY)
          .setTitle(tr.error.errorTitle)
          .setDescription(tr.error.errorInvalidArguments)
          .setAuthor({
            name: tr.author.berry,
            iconURL: process.env.SET_AUTHOR_FRUIT_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          });
    }

    return {
      embedMessage: embedMessage,
      images: [image],
    };
  },
};
