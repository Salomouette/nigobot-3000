const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  berryHelpTemplate: function() {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_HELP_QUERY)
        .setTitle('Liste des commandes concernant la partie **Baie**')
        .addFields(
            {
              name: tr.help.berry.mixTemplate.fieldTitle,
              value: tr.help.berry.mixTemplate.fieldContent,
            },
            {
              name: tr.help.berry.mixBerries.fieldTitle,
              value: tr.help.berry.mixBerries.fieldContent,
            },
            {
              name: tr.help.berry.generalTable.fieldTitle,
              value: tr.help.berry.generalTable.fieldContent,
            },
            {
              name: tr.help.berry.sortedTable.fieldTitle,
              value: tr.help.berry.sortedTable.fieldContent,
            },
        )
        .setAuthor({
          name: tr.author.berry,
          iconURL: process.env.SET_AUTHOR_HELP_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
