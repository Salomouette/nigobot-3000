const Discord = require('discord.js');
const berryViewService = require('../../../Service/Berry/berryViewService');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  berryGeneralTableTemplate: function(intPage) {
    const description = berryViewService.generateBerryTableDescription(intPage);

    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_FRUIT_QUERY)
        .setDescription(description)
        .setAuthor({
          name: tr.author.berry,
          iconURL: process.env.SET_AUTHOR_FRUIT_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
