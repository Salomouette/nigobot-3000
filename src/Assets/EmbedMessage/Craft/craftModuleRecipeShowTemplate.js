const Discord = require('discord.js');

const botService = require('../../../Service/botService');
const craftViewService = require('../../../Service/Craft/craftViewService');

const itemsData = require('../../../Data/itemsData');
const craftRecipesData = require('../../../Data/craftRecipesData');
const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  craftModuleRecipeShowTemplate: function(recipeName) {
    const recipe = craftRecipesData.getDiscoveredCraftModuleRecipeFromName(recipeName);
    let embedMessage = '';
    let image = '';

    if (recipe.length !== 0) {
      embedMessage = new Discord.MessageEmbed()
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          });
      // Recipe is discovered by group
      if (recipe.discovered) {
        if (recipe.type === 'Module') {
          image = botService.generatePathCraftModuleImages(recipe.level, recipe.imageType);
          const fields = craftViewService.craftModuleRecipeShowFields(recipe);
          embedMessage = embedMessage
              .setColor(process.env.COLOR_BOOK_QUERY)
              .setTitle(tr.craft.craftModuleRecipeShowTemplate.title)
              .addFields(
                  fields.description,
                  fields.group,
                  fields.components,
                  fields.level,
                  fields.sellPrice,
                  fields.illegal,
              )
              .setAuthor({
                name: tr.author.craft,
                iconURL: process.env.SET_AUTHOR_BOOK_ICON,
              });
        } else {
          const itemData = itemsData.getItemFromName(recipeName);
          const fields = craftViewService.craftRecipeShowFields(recipe, itemData);
          image = botService.generatePathItemsImages(itemData.imageFilePath);

          embedMessage = embedMessage
              .setColor(process.env.COLOR_BOOK_QUERY)
              .setTitle(tr.craft.craftModuleRecipeShowTemplate.title)
              .addFields(
                  fields.description,
                  fields.group,
                  fields.components,
                  fields.level,
                  fields.sellPrice,
                  fields.illegal,
              )
              .setAuthor({
                name: tr.author.craft,
                iconURL: process.env.SET_AUTHOR_BOOK_ICON,
              });
        }
      }
      // Not found or not discovered
    } else {
      embedMessage = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_TOOLS_QUERY)
          .setTitle(tr.error.errorTitle)
          .setDescription(tr.error.errorModuleNotFound)
          .setAuthor({
            name: tr.author.craft,
            iconURL: process.env.SET_AUTHOR_TOOLS_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          });
    }

    if (image.length > 0) {
      embedMessage.setThumbnail('attachment://' + botService.getFilename(image));
      return {
        embedMessage: embedMessage,
        images: [image],
      };
    } else {
      return {
        embedMessage: embedMessage,
        images: '',
      };
    }
  },
};
