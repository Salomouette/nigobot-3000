const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  craftCreationCommandTemplate: function() {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_TOOLS_QUERY)
        .setTitle(tr.craft.craftCreationCommandTemplate.title)
        .setDescription(tr.craft.craftCreationCommandTemplate.description)
        .setAuthor({
          name: tr.author.craft,
          iconURL: process.env.SET_AUTHOR_TOOLS_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
