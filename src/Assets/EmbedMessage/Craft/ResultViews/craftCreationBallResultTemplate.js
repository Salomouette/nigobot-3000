const Discord = require('discord.js');

const botService = require('../../../../Service/botService');
const craftViewService = require('../../../../Service/Craft/craftViewService');

const itemData = require('../../../../Data/itemsData');
const frenchTranslation = require('../../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  craftCreationBallResultTemplate: function(foundRecipe) {
    const item = itemData.getItemFromName(foundRecipe.name);
    const fields = craftViewService.craftMedecineCreationResultFields(foundRecipe, item);
    const recipeImage = botService.generatePathItemsImages(item.imageFilePath);

    const embedMessage = new Discord.MessageEmbed()
        .setTitle(tr.craft.craftCreationMedecineResultTemplate.title)
        .addFields(
            fields.head,
            fields.description,
            fields.components,
            fields.type,
            fields.sellPrice,
            fields.illegal,
        )
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        })
        .setAuthor({
          name: tr.author.craft,
          iconURL: process.env.SET_AUTHOR_POKEBALL_ICON,
        })
        .setColor(process.env.COLOR_POKEBALL_QUERY);

    if (recipeImage.length > 0) {
      embedMessage.setThumbnail('attachment://' + botService.getFilename(recipeImage));
      return {
        embedMessage: embedMessage,
        images: [recipeImage],
      };
    } else {
      return {
        embedMessage: embedMessage,
        images: '',
      };
    }
  },
};
