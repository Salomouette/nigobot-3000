const Discord = require('discord.js');

const botService = require('../../../../Service/botService');
const craftViewService = require('../../../../Service/Craft/craftViewService');

const frenchTranslation = require('../../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  craftCreationMealResultTemplate: function(foundRecipe) {
    const recipeImage = botService.generatePathCraftMealImages(foundRecipe.level, foundRecipe.imageType);
    const fields = craftViewService.craftModuleCreationResultFields(foundRecipe);

    const embedMessage = new Discord.MessageEmbed()
        .setColor(process.env.COLOR_FOOD_QUERY)
        .setTitle(tr.craft.craftCreationMealResultTemplate.title)
        .addFields(
            fields.head,
            fields.description,
            fields.components,
            fields.sellPrice,
        )
        .setAuthor(tr.author.craft, process.env.SET_AUTHOR_FOOD_ICON)
        .setAuthor({
          name: tr.author.craft,
          iconURL: process.env.SET_AUTHOR_FOOD_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });

    if (recipeImage.length > 0) {
      embedMessage.setThumbnail('attachment://' + botService.getFilename(recipeImage));
      return {
        embedMessage: embedMessage,
        images: [recipeImage],
      };
    } else {
      return {
        embedMessage: embedMessage,
        images: '',
      };
    }
  },
};
