const Discord = require('discord.js');

const craftViewService = require('../../../Service/Craft/craftViewService');
const emojiService = require('../../../Service/emojiService');

const craftRecipesData = require('../../../Data/craftRecipesData');
const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  craftViewDiscoveredRecipesTemplate: function(client) {
    let recipeModule = craftRecipesData.getCraftRecipeDiscovered('Module');
    recipeModule = craftViewService.sortDiscoveredRecipe(recipeModule);
    const moduleFields = craftViewService.generateDiscoveredRecipeFieldValue(recipeModule);

    let recipeElectronic = craftRecipesData.getCraftRecipeDiscovered('Électronique');
    recipeElectronic = craftViewService.sortDiscoveredRecipe(recipeElectronic);
    const electroniFields = craftViewService.generateDiscoveredRecipeFieldValue(recipeElectronic);

    let recipeChemistry = craftRecipesData.getCraftRecipeDiscovered('Chemical');
    recipeChemistry = craftViewService.sortDiscoveredRecipe(recipeChemistry);
    const chemistryFields = craftViewService.generateDiscoveredRecipeFieldValue(recipeChemistry);

    let recipePokeball = craftRecipesData.getCraftRecipeDiscovered('Balls');
    recipePokeball = craftViewService.sortDiscoveredRecipe(recipePokeball);
    const pokeballFields = craftViewService.generateDiscoveredRecipeFieldValue(recipePokeball);

    let recipeMeal = craftRecipesData.getCraftRecipeDiscovered('Meal');
    recipeMeal = craftViewService.sortDiscoveredRecipe(recipeMeal);
    const mealFields = craftViewService.generateDiscoveredRecipeFieldValue(recipeMeal);

    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_BOOK_QUERY)
        .setTitle(tr.craft.craftViewDiscoveredRecipesTemplate.title)
        .addFields(
            {
              name: tr.craft.craftViewDiscoveredRecipesTemplate.module +
                  emojiService.getBotPartEmojiPerName('Tools', client),
              value: moduleFields.length > 0 ?
                  moduleFields :
                  tr.craft.craftViewDiscoveredRecipesTemplate.noRecipesDiscovered,
            },
            {
              name: tr.craft.craftViewDiscoveredRecipesTemplate.electronic +
                  emojiService.getBotPartEmojiPerName('Tools', client),
              value: electroniFields.length > 0 ?
                  electroniFields :
                  tr.craft.craftViewDiscoveredRecipesTemplate.noRecipesDiscovered,
            },
            {
              name: tr.craft.craftViewDiscoveredRecipesTemplate.chemistry +
                  emojiService.getBotPartEmojiPerName('Cauldron', client),
              value: chemistryFields.length > 0 ?
                  chemistryFields :
                  tr.craft.craftViewDiscoveredRecipesTemplate.noRecipesDiscovered,
            },
            {
              name: tr.craft.craftViewDiscoveredRecipesTemplate.balls +
                  emojiService.getBotPartEmojiPerName('Pokeball', client),
              value: pokeballFields.length > 0 ?
                  pokeballFields :
                  tr.craft.craftViewDiscoveredRecipesTemplate.noRecipesDiscovered,
            },
            {
              name: tr.craft.craftViewDiscoveredRecipesTemplate.meal +
                  emojiService.getBotPartEmojiPerName('Food', client),
              value: mealFields.length > 0 ?
                  mealFields :
                  tr.craft.craftViewDiscoveredRecipesTemplate.noRecipesDiscovered,
            },
        )
        .setAuthor({
          name: tr.author.craft,
          iconURL: process.env.SET_AUTHOR_BOOK_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
