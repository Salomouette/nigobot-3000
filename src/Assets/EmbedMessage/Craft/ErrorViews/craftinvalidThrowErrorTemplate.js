const Discord = require('discord.js');

const frenchTranslation = require('../../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  craftinvalidThrowErrorTemplate: function() {
    const embed = new Discord.MessageEmbed()
        .setColor(process.env.COLOR_TOOLS_QUERY)
        .setTitle(tr.craft.craftCreationModuleResultTemplate.title)
        .setDescription(tr.craft.invalidThrowDescription)
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });

    return {
      embedMessage: embed,
      images: '',
    };
  },
};
