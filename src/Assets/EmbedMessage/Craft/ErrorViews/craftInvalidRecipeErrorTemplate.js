const Discord = require('discord.js');

const frenchTranslation = require('../../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  craftInvalidRecipeErrorTemplate: function() {
    const embed = new Discord.MessageEmbed()
        .setColor(process.env.COLOR_TOOLS_QUERY)
        .setTitle(tr.craft.craftCreationModuleResultTemplate.title)
        .setAuthor({
          name: tr.author.craft,
          iconURL: process.env.SET_AUTHOR_,
        })
        .setDescription(tr.craft.invalidRecipeDescription)
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });


    return {
      embedMessage: embed,
      images: '',
    };
  },
};
