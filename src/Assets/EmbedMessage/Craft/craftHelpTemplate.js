const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  craftHelpTemplate: function() {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_HELP_QUERY)
        .setTitle(tr.help.craft.helpCommandTitle)
        .addFields(
            {
              name: tr.help.craft.viewCraftRecipe.fieldTitle,
              value: tr.help.craft.viewCraftRecipe.fieldContent,
            },
            {
              name: tr.help.craft.viewDiscoveredRecipe.fieldTitle,
              value: tr.help.craft.viewDiscoveredRecipe.fieldContent,
            },
            {
              name: tr.help.craft.craftTemplate.fieldTitle,
              value: tr.help.craft.craftTemplate.fieldContent,
            },
            {
              name: tr.help.craft.craftCommand.fieldTitle,
              value: tr.help.craft.craftCommand.fieldContent,
            },
        )
        .setAuthor({
          name: tr.author.craft,
          iconURL: process.env.SET_AUTHOR_HELP_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
