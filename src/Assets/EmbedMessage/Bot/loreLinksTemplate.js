const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  loreLinksTemplate: function() {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_SCROLL_QUERY)
        .setTitle(tr.lore.title)
        .setDescription(
            tr.lore.link +
                process.env.LORE_FOLDER_LINK +
                tr.lore.description,
        )
        .addFields(
            {
              name: tr.lore.fields.S1Resume,
              value: process.env.LORE_FOLDER_LINK,
            },
            {
              name: tr.lore.fields.Draconids,
              value: process.env.LORE_DRACONIDS,
            },
            {
              name: tr.lore.fields.Darkrai,
              value: process.env.LORE_DARKRAI,
            },
            {
              name: tr.lore.fields.Teams,
              value: process.env.LORE_TEAMS,
            },
            {
              name: tr.lore.fields.Blue,
              value: process.env.LORE_A0,
            },
            {
              name: tr.lore.fields.S2NPC,
              value: process.env.LORE_S2_NPC,
            },
            {
              name: tr.lore.fields.S3NPC,
              value: process.env.LORE_S3_NPC,
            },
        )
        .setAuthor({
          name: tr.author.lore,
          iconURL: process.env.SET_AUTHOR_SCROLLS_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
