const Discord = require('discord.js');

const emojiService = require('../../../Service/emojiService');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  botHelpTemplate: function(client) {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_HELP_QUERY)
        .setTitle(tr.help.main.helpCommandTitle)
        .setDescription(
            tr.help.main.descriptionTitle +
            emojiService.getBotPartEmojiPerName('Cog', client) +
            '\n' +
            tr.help.main.description,
        )
        .addFields(
            {
              name: tr.help.main.fields.playerPart.fieldTitle +
                  emojiService.getBotPartEmojiPerName('Player', client) + '\n',
              value: tr.help.main.fields.playerPart.fieldContent,
            },
            {
              name: tr.help.main.fields.dicePart.fieldTitle +
                  emojiService.getBotPartEmojiPerName('Dice', client) + '\n',
              value: tr.help.main.fields.dicePart.fieldContent,
            },
            {
              name: tr.help.main.fields.biomePart.fieldTitle +
                  emojiService.getBotPartEmojiPerName('Earth', client) + '\n',
              value: tr.help.main.fields.biomePart.fieldContent,
            },
            {
              name: tr.help.main.fields.pokemonPart.fieldTitle +
                  emojiService.getBotPartEmojiPerName('Pokemon', client) + '\n',
              value: tr.help.main.fields.pokemonPart.fieldContent,
            },
            {
              name: tr.help.main.fields.pokemonTrainerPart.fieldTitle +
                  emojiService.getBotPartEmojiPerName('Pokemon', client) + '\n',
              value: tr.help.main.fields.pokemonTrainerPart.fieldContent,
            },
            {
              name: tr.help.main.fields.berryPart.fieldTitle +
                  emojiService.getBotPartEmojiPerName('Fruit', client) + '\n',
              value: tr.help.main.fields.berryPart.fieldContent,
            },
            {
              name:
              tr.help.main.fields.craftPart.fieldTitle +
              emojiService.getBotPartEmojiPerName('Tools', client) +
              emojiService.getBotPartEmojiPerName('Food', client) +
              emojiService.getBotPartEmojiPerName('Cauldron', client) +
              emojiService.getBotPartEmojiPerName('Pokeball', client) +
              '\n',
              value: tr.help.main.fields.craftPart.fieldContent,
            },
            {
              name: tr.help.main.fields.calendarPart.fieldTitle +
                  emojiService.getBotPartEmojiPerName('Calendar', client) + '\n',
              value: tr.help.main.fields.calendarPart.fieldContent,
            },
            {
              name: tr.help.main.fields.trainerPart.fieldTitle +
                  emojiService.getBotPartEmojiPerName('Battle', client) + '\n',
              value: tr.help.main.fields.trainerPart.fieldContent,
            },
            {
              name: tr.help.main.fields.itemPart.fieldTitle +
                  emojiService.getBotPartEmojiPerName('Item', client) + '\n',
              value: tr.help.main.fields.itemPart.fieldContent,
            },
        )
        .setAuthor({
          name: tr.author.help,
          iconURL: process.env.SET_AUTHOR_HELP_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
