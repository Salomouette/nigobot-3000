const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  biomesViewTemplate: function(description) {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_EARTH_QUERY)
        .setTitle(tr.help.biome.biomesView.fieldTitle)
        .setDescription(description)
        .setAuthor({
          name: tr.author.biome,
          iconURL: process.env.SET_AUTHOR_EARTH_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
