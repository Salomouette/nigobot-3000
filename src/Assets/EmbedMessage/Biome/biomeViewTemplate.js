const Discord = require('discord.js');

const botService = require('../../../Service/botService');
const biomeViewService = require('../../../Service/Biome/biomeViewService');

const biomeData = require('../../../Data/biomeData');
const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  biomeViewTemplate: function(biomeName) {
    const biomePreviewImagePath = botService.generatePathBiomeImages(
        biomeData.getRequestedBiomeData(biomeName, 'preview'),
        'preview',
    );
    const biomeIconImagePath = botService.generatePathBiomeImages(
        biomeData.getRequestedBiomeData(biomeName, 'icon'),
        'icon',
        biomeData.getRequestedBiomeData(biomeName, 'type'),
    );
    const biomePokémon = biomeViewService.biomeActionHandler('view', biomeName);

    let embedMessage = null;

    if (biomePokémon.includes('d\'arguments')) {
      embedMessage = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_EARTH_QUERY)
          .setTitle(tr.error.errorTitle)
          .setDescription(biomePokémon)
          .setAuthor({
            name: tr.author.biome,
            iconURL: process.env.SET_AUTHOR_EARTH_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          });
    } else {
      embedMessage = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_EARTH_QUERY)
          .setTitle('Pokémon du biome ' + biomeName)
          .addFields(
              {
                name: 'Description',
                value: biomeData.getRequestedBiomeData(biomeName, 'description'),
              },
          )
          .setAuthor({
            name: tr.author.biome,
            iconURL: process.env.SET_AUTHOR_EARTH_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          });
      if (!biomePokémon.includes('Biome invalide')) {
        embedMessage.setDescription(biomePokémon);
      }
    }

    if (biomePreviewImagePath !== undefined && biomeIconImagePath !== undefined) {
      embedMessage
          .setImage('attachment://' + botService.getFilename(biomePreviewImagePath))
          .setThumbnail('attachment://' + botService.getFilename(biomeIconImagePath));
      return {
        embedMessage: embedMessage,
        images: [biomePreviewImagePath, biomeIconImagePath],
      };
    } else {
      return {
        embedMessage: embedMessage,
        images: '',
      };
    }
  },
};
