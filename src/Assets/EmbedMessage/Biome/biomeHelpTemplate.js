const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  biomeHelpTemplate: function() {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_HELP_QUERY)
        .setTitle(tr.help.biome.helpCommandTitle)
        .addFields(
            {
              name: tr.help.biome.biomesView.fieldTitle,
              value: tr.help.biome.biomesView.fieldContent,
            },
            {
              name: tr.help.biome.biomeView.fieldTitle,
              value: tr.help.biome.biomeView.fieldContent,
            },
        )
        .setAuthor({
          name: tr.author.biome,
          iconURL: process.env.SET_AUTHOR_HELP_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
