const Discord = require('discord.js');

const frenchTranslation = require('../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  blankTemplate: function(text, messagePart) {
    let embedMessage = new Discord.MessageEmbed().setDescription(text);

    switch (messagePart) {
      case ('calendarTodo'):
        embedMessage = embedMessage
            .setColor(process.env.COLOR_CALENDAR_QUERY)
            .setAuthor({
              name: tr.author.calendar,
              iconURL: process.env.set_author_calendar_icon,
            })
            .setTimestamp()
            .setFooter({
              text: process.env.NIGOBOT_NAME,
              iconURL: process.env.NIGOBOT_AVATAR_LINK,
            });
        break;
      case ('error'):
        embedMessage = embedMessage
            .setColor(process.env.COLOR_ALERT_QUERY)
            .setAuthor({
              name: tr.author.error,
              iconURL: process.env.SET_AUTHOR_Alert_ICON,
            })
            .setTimestamp()
            .setFooter({
              text: process.env.NIGOBOT_NAME,
              iconURL: process.env.NIGOBOT_AVATAR_LINK,
            });
        break;
      case ('fun'):
        embedMessage = embedMessage
            .setColor(process.env.COLOR_FUN_QUERY)
            .setAuthor({
              name: tr.author.fun,
              iconURL: process.env.SET_AUTHOR_FUN_ICON,
            })
            .setTimestamp()
            .setFooter({
              text: process.env.NIGOBOT_NAME,
              iconURL: process.env.NIGOBOT_AVATAR_LINK,
            });
        break;
      case ('system'):
        embedMessage = embedMessage
            .setColor(process.env.COLOR_COG_QUERY)
            .setAuthor({
              name: tr.author.system,
              iconURL: process.env.SET_AUTHOR_COG_ICON,
            })
            .setTimestamp()
            .setFooter({
              text: process.env.NIGOBOT_NAME,
              iconURL: process.env.NIGOBOT_AVATAR_LINK,
            });
        break;
    }

    return embedMessage;
  },
};
