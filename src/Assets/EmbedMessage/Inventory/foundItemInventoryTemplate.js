const Discord = require('discord.js');

const playerViewService = require('../../../Service/Player/playerViewService');
const viewService = require('../../../Service/viewService');
const emojiService = require('../../../Service/emojiService');

const playerData = require('../../../Data/playerData');
const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  foundItemInventoryTemplate: function(playerId, client) {
    const playerDatas = playerData.getPlayerById(playerId);
    const nbPlayerFoundItems = playerViewService.getArrayNumberFoundItems(playerDatas.inventory.foundItems);
    const embeds = [];
    let description = '';
    let embed = '';

    // Normal Case
    if (nbPlayerFoundItems.length > 0) {
      for (let i = 0; i < nbPlayerFoundItems.length; i++) {
        const newLine = '- ' +
                    viewService.formatToString(nbPlayerFoundItems[i].name) +
                    ' ' + emojiService.getFoundItemEmojiPerName(nbPlayerFoundItems[i].emojiName, client) +
                    ': ' + viewService.formatToString(nbPlayerFoundItems[i].quantity) +
                    ' trouvé(s)\n';

        if ((description.length + newLine.length) >= 4000) {
          embed = new Discord.MessageEmbed()
              .setColor(process.env.COLOR_BAG_QUERY)
              .setTitle(tr.inventory.foundItem)
              .setAuthor({
                name: tr.author.inventory,
                iconURL: process.env.SET_AUTHOR_BAG_ICON,
              })
              .setTimestamp()
              .setFooter({
                text: process.env.NIGOBOT_NAME,
                iconURL: process.env.NIGOBOT_AVATAR_LINK,
              })
              .setDescription(description);

          embeds.push({
            embedMessage: embed,
          });

          description = '';
        }
        description += newLine;
      }

      embed = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_BAG_QUERY)
          .setTitle(tr.player.foundItemTitle)
          .setAuthor({
            name: tr.author.inventory,
            iconURL: process.env.SET_AUTHOR_BAG_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          })
          .setDescription(description);

      embeds.push({
        embedMessage: embed,
      });
    } else {
      embed = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_BAG_QUERY)
          .setTitle(tr.inventory.foundItem)
          .setAuthor({
            name: tr.author.inventory,
            iconURL: process.env.SET_AUTHOR_BAG_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          })
          .setDescription(tr.inventory.noItemFound);

      embeds.push({
        embedMessage: embed,
      });
    }

    return embeds;
  },
};
