const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  inventoryHelpTemplate: function() {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_HELP_QUERY)
        .setTitle(tr.help.inventory.helpCommandTitle)
        .addFields(
            {
              name: tr.help.inventory.foundItem.fieldTitle,
              value: tr.help.inventory.foundItem.fieldContent,
            },
            {
              name: tr.help.inventory.keepItem.fieldTitle,
              value: tr.help.inventory.keepItem.fieldContent,
            },
            {
              name: tr.help.inventory.keepItemSubCategory.fieldTitle,
              value: tr.help.inventory.keepItemSubCategory.fieldContent,
            },
            {
              name: tr.help.inventory.keepItemCategory.fieldTitle,
              value: tr.help.inventory.keepItemCategory.fieldContent,
            },
            {
              name: tr.help.inventory.sellItem.fieldTitle,
              value: tr.help.inventory.sellItem.fieldContent,
            },
            {
              name: tr.help.inventory.sellItemSubCategory.fieldTitle,
              value: tr.help.inventory.sellItemSubCategory.fieldContent,
            },
            {
              name: tr.help.inventory.sellItemCategory.fieldTitle,
              value: tr.help.inventory.sellItemCategory.fieldContent,
            },
            {
              name: tr.help.inventory.sellAllFoundItems.fieldTitle,
              value: tr.help.inventory.sellAllFoundItems.fieldContent,
            },
            {
              name: tr.help.inventory.estimateAllFoundItems.fieldTitle,
              value: tr.help.inventory.estimateAllFoundItems.fieldContent,
            },
            {
              name: tr.help.inventory.values.fieldTitle,
              value: tr.help.inventory.values.fieldContent,
            },
        )
        .setAuthor({
          name: tr.author.inventory,
          iconURL: process.env.SET_AUTHOR_HELP_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
