const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  sellBaseTemplate: function(description) {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_MONEY_QUERY)
        .setTitle(tr.player.sellFoundItemTitle)
        .setAuthor({
          name: tr.author.money,
          iconURL: process.env.SET_AUTHOR_MONEY_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        })
        .setDescription(description);
  },
};
