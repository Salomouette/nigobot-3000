const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  keepFoundItemTemplate: function(description) {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_BAG_QUERY)
        .setTitle(tr.player.keepFoundItemTitle)
        .setAuthor({
          name: tr.author.inventory,
          iconURL: process.env.SET_AUTHOR_BAG_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        })
        .setDescription(description);
  },
};
