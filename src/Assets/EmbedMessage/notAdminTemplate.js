const Discord = require('discord.js');

module.exports = {
  notAdminTemplate: function() {
    let embedMessage = null;

    embedMessage = new Discord.MessageEmbed().setColor(process.env.COLOR_POKEMON_QUERY)
        .setTitle('ERREUR')
        .addFields({
          name: 'ERREUR',
          value: 'Vous n\'êtes pas l\'admin.',
        });

    return embedMessage;
  },
};
