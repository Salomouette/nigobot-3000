const Discord = require('discord.js');

const pokémonFoundableItemRoutine = require('../../../Routine/pokémonFoundableItemRoutine');

const emojiService = require('../../../Service/emojiService');
const itemViewService = require('../../../Service/Item/itemViewService');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  itemGenerateFoundItemTemplate: function(playerId, client) {
    let title = '';
    let description = '';
    if (typeof playerId !== 'undefined') {
      const foundItemRarity = pokémonFoundableItemRoutine.getFoundItemRarity();
      let foundItemData = pokémonFoundableItemRoutine.getFoundItem(foundItemRarity, '', '');

      const foundItemDataRarity = foundItemData.rarity;
      foundItemData = foundItemData.foundItemData;

      itemViewService.savePlayerFoundItem(
          playerId,
          {
            name: foundItemData.name,
            id: foundItemData.id,
            pokemonId: 'player',
          },
      );

      description = tr.item.lignFoundItem
          .replace(
              '{OBJET}',
              foundItemData.name,
          )
          .replace(
              '{EMOJI}',
              emojiService.getFoundItemEmojiPerName(foundItemData.foundableData.buddyFoundableEmoji, client),
          )
          .replace(
              '{RARITY}',
              pokémonFoundableItemRoutine.getRarityName(foundItemDataRarity),
          );
      title = tr.item.youFoundItem;
    } else {
      description = tr.item.missingIdPlayer;
      title = tr.author.error;
    }
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_ITEM_QUERY)
        .setTitle(title)
        .setDescription(description)
        .setAuthor({
          name: tr.author.item,
          iconURL: process.env.SET_AUTHOR_ITEM_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
