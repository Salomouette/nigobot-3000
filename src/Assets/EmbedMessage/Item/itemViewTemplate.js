const Discord = require('discord.js');

const botService = require('../../../Service/botService');
const viewService = require('../../../Service/viewService');
const itemViewService = require('../../../Service/Item/itemViewService');
const berryViewService = require('../../../Service/Berry/berryViewService');
const emojiService = require('../../../Service/emojiService');

const itemsData = require('../../../Data/itemsData');
const craftRecipesData = require('../../../Data/craftRecipesData');
const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  itemViewTemplate: function(name, client) {
    const item = itemsData.getItemFromName(name);
    const itemRecipes = craftRecipesData.getCraftRecipesFromNameAndDiscovered(name);
    const fieldRecipes = itemViewService.itemViewRecipeFormat(itemRecipes);
    const embeds = [];
    let embedMessageBerries = '';
    let embedMessageFoundableDate = '';
    let embedMessage = '';
    let image = '';

    if (item !== null) {
      image = botService.generatePathItemsImages(item.imageFilePath);
      const foundItemEmoji = typeof item.foundableData !== 'undefined' ?
            emojiService.getFoundItemEmojiPerName(item.foundableData.buddyFoundableEmoji, client) :
            '';

      embedMessage = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_BOOK_QUERY)
          .setTitle(
              viewService.formatUnderscoreAndBoldText('[' + item.category + '] ' + item.name) + ' ' + foundItemEmoji,
          )
          .addFields(
              {
                name: viewService.formatUnderscoreAndBoldText(tr.item.itemViewTemplate.description),
                value: viewService.formatToString(item.description),
              },
              {
                name: viewService.formatToString(tr.item.itemViewTemplate.subCategory),
                value: viewService.formatToString(item.subCategory),
                inline: true,
              },
              {
                name: viewService.formatToString(tr.item.itemViewTemplate.craftable),
                value: viewService.formatToString(item.manufacturable ? 'Oui' : 'Non'),
                inline: true,
              },
              {
                name: viewService.formatToString(tr.item.itemViewTemplate.illegal),
                value: viewService.formatToString(item.illegal ? 'Oui' : 'Non'),
                inline: true,
              },
          )
          .setAuthor({
            name: tr.author.item,
            iconURL: process.env.SET_AUTHOR_BOOK_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          });

      for (let i = 0; i < fieldRecipes.length; i++) {
        embedMessage.addFields(fieldRecipes[i]);
      }

      // Fluctuating price items
      if (item.fluctuatingPrice) {
        const minBuyPrice = parseFloat(item.basePrice) * parseFloat(
            process.env.ITEM_BUY_MIN_PRICE_MULTIPLICATOR,
        );
        const maxBuyPrice = parseFloat(item.basePrice) * parseFloat(
            process.env.ITEM_BUY_EXTRA_MAX_PRICE_MULTIPLICATOR,
        );
        const minSellPrice = parseFloat(item.basePrice) * parseFloat(
            process.env.ITEM_SELL_MIN_PRICE_MULTIPLICATOR,
        );
        const moySellPrice = parseFloat(item.basePrice) * parseFloat(
            process.env.ITEM_SELL_MOY_PRICE_MULTIPLICATOR,
        );
        const maxSellPrice = parseFloat(item.basePrice) * parseFloat(
            process.env.ITEM_SELL_EXTRA_MAX_PRICE_MULTIPLICATOR,
        );

        embedMessage = embedMessage.addFields(
            {
              name: viewService.formatUnderscoreAndBoldText(tr.item.itemViewTemplate.priceName),
              value: viewService.formatToString(tr.item.itemViewTemplate.priceValue),
            },
        );

        // If items is not available, show only sell prices
        if (item.storeAvailable) {
          embedMessage = embedMessage.addFields(
              {
                name: viewService.formatBoldText(tr.item.itemViewTemplate.prices.buyBaseDown),
                value: viewService.formatToString(minBuyPrice),
                inline: true,
              },
              {
                name: viewService.formatBoldText(tr.item.itemViewTemplate.prices.buyBase),
                value: viewService.formatToString(item.basePrice),
                inline: true,
              },
              {
                name: viewService.formatBoldText(tr.item.itemViewTemplate.prices.buyBaseUp),
                value: viewService.formatToString(maxBuyPrice),
                inline: true,
              },
          );
        }

        embedMessage = embedMessage.addFields(
            {
              name: viewService.formatBoldText(tr.item.itemViewTemplate.prices.sellBaseDown),
              value: viewService.formatToString(minSellPrice),
              inline: true,
            },
            {
              name: viewService.formatBoldText(tr.item.itemViewTemplate.prices.sellBase),
              value: viewService.formatToString(moySellPrice),
              inline: true,
            },
            {
              name: viewService.formatBoldText(tr.item.itemViewTemplate.prices.sellBaseUp),
              value: viewService.formatToString(maxSellPrice),
              inline: true,
            },
        );
      }
      // Base Item always available in stores
      if (item.alwaysAvailable) {
        const moySellPrice = parseFloat(item.basePrice) * parseFloat(process.env.ITEM_SELL_MOY_PRICE_MULTIPLICATOR);
        embedMessage = embedMessage.addFields(
            {
              name: viewService.formatUnderscoreAndBoldText(tr.item.itemViewTemplate.priceName),
              value: viewService.formatToString(tr.item.itemViewTemplate.priceAlwaysAvailableValue),
            },
            {
              name: viewService.formatBoldText(tr.item.itemViewTemplate.prices.buy),
              value: viewService.formatToString(item.basePrice),
              inline: true,
            },
            {
              name: viewService.formatBoldText(tr.item.itemViewTemplate.prices.sell),
              value: viewService.formatToString(moySellPrice),
              inline: true,
            },
        );
      }
      // Item not available in stores
      if (!item.storeAvailable) {
        embedMessage = embedMessage.addFields(
            {
              name: viewService.formatBoldText(tr.item.itemViewTemplate.unavailableName),
              value: viewService.formatToString(tr.item.itemViewTemplate.unavailableValue),
            },
        );
      }

      // Auction (Online and physical)
      const minBuySellPriceAuction = parseFloat(item.basePrice) * parseFloat(
          process.env.ITEM_BUY_SELL_EXTRA_MIN_PRICE_AUCTION_MULTIPLICATOR,
      );
      const maxBuySellPriceAuction = parseFloat(item.basePrice) * parseFloat(
          process.env.ITEM_BUY_SELL_EXTRA_MAX_PRICE_AUCTION_MULTIPLICATOR,
      );
      embedMessage = embedMessage.addFields(
          {
            name: viewService.formatBoldText(tr.item.itemViewTemplate.prices.auctionName),
            value: viewService.formatToString(tr.item.itemViewTemplate.prices.auctionValue),
          },
          {
            name: viewService.formatBoldText(tr.item.itemViewTemplate.prices.auctionDown),
            value: viewService.formatToString(minBuySellPriceAuction),
            inline: true,
          },
          {
            name: viewService.formatBoldText(tr.item.itemViewTemplate.prices.auctionHigh),
            value: viewService.formatToString(maxBuySellPriceAuction),
            inline: true,
          },
      );

      // Berries
      if (typeof item.berryData !== 'undefined') {
        embedMessageBerries = new Discord.MessageEmbed()
            .setColor(process.env.COLOR_BOOK_QUERY)
            .setTitle(
                viewService.formatUnderscoreAndBoldText('[' + item.category + '] ' + item.name) + ' ' + foundItemEmoji,
            )
            .addFields(
                {
                  name: viewService.formatToString(tr.berry.berryViewTemplate.firmness),
                  value: viewService.formatToString(item.berryData.firmness),
                  inline: true,
                },
                {
                  name: viewService.formatToString(tr.berry.berryViewTemplate.size),
                  value: viewService.formatToString(item.berryData.size + tr.berry.berryViewTemplate.cm),
                  inline: true,
                },
                {
                  name: viewService.formatUnderscoreAndBoldText(tr.berry.berryViewTemplate.tastes),
                  value: viewService.formatToString(tr.berry.berryViewTemplate.tastesDescription),
                },
                {
                  name: viewService.formatBoldText(tr.berry.berryViewTemplate.astringency),
                  value: berryViewService.generateBerryPotencyEmoji(
                      item.berryData.flavors[7].potency,
                      client,
                  ),
                  inline: true,
                },
                {
                  name: viewService.formatBoldText(tr.berry.berryViewTemplate.salty),
                  value: berryViewService.generateBerryPotencyEmoji(
                      item.berryData.flavors[6].potency,
                      client,
                  ),
                  inline: true,
                },
                {
                  name: viewService.formatBoldText(tr.berry.berryViewTemplate.umami),
                  value: berryViewService.generateBerryPotencyEmoji(
                      item.berryData.flavors[5].potency,
                      client,
                  ),
                  inline: true,
                },
                {
                  name: viewService.formatBoldText(tr.berry.berryViewTemplate.spicy),
                  value: berryViewService.generateBerryPotencyEmoji(
                      item.berryData.flavors[0].potency,
                      client,
                  ),
                  inline: true,
                },
                {
                  name: viewService.formatBoldText(tr.berry.berryViewTemplate.dry),
                  value: berryViewService.generateBerryPotencyEmoji(
                      item.berryData.flavors[1].potency,
                      client,
                  ),
                  inline: true,
                },
                {
                  name: viewService.formatBoldText(tr.berry.berryViewTemplate.sweet),
                  value: berryViewService.generateBerryPotencyEmoji(
                      item.berryData.flavors[2].potency,
                      client,
                  ),
                  inline: true,
                },
                {
                  name: viewService.formatBoldText(tr.berry.berryViewTemplate.bitter),
                  value: berryViewService.generateBerryPotencyEmoji(
                      item.berryData.flavors[3].potency,
                      client,
                  ),
                  inline: true,
                },
                {
                  name: viewService.formatBoldText(tr.berry.berryViewTemplate.sour),
                  value: berryViewService.generateBerryPotencyEmoji(
                      item.berryData.flavors[4].potency,
                      client,
                  ),
                  inline: true,
                },
            )
            .setAuthor({
              name: tr.author.item,
              iconURL: process.env.SET_AUTHOR_BOOK_ICON,
            })
            .setTimestamp()
            .setFooter({
              text: process.env.NIGOBOT_NAME,
              iconURL: process.env.NIGOBOT_AVATAR_LINK,
            });
      }

      // Objet trouvé
      if (typeof item.foundableData !== 'undefined') {
        embedMessageFoundableDate = new Discord.MessageEmbed()
            .setColor(process.env.COLOR_BOOK_QUERY)
            .setTitle(
                viewService.formatUnderscoreAndBoldText('[' + item.category + '] ' + item.name) + ' ' + foundItemEmoji,
            )
            .addFields(
                {
                  name: viewService.formatUnderscoreAndBoldText(tr.item.itemViewTemplate.foundableItem.infoName),
                  value: viewService.formatToString(tr.item.itemViewTemplate.foundableItem.infoValue),
                },
                {
                  name: viewService.formatBoldText(tr.item.itemViewTemplate.foundableItem.location),
                  value: viewService.formatToString(
                      itemViewService.itemViewFoundableLocationFormat(item.foundableData.foundableLocation, client),
                  ),
                },
                {
                  name: viewService.formatToString(tr.item.itemViewTemplate.foundableItem.foundableByPartner),
                  value: viewService.formatToString(
                    item.foundableData.buddyFound ?
                        tr.item.itemViewTemplate.foundableItem.foundableByPokémon :
                        tr.item.itemViewTemplate.foundableItem.notFoundableByPokémon,
                  ),
                })
            .setAuthor({
              name: tr.author.item,
              iconURL: process.env.SET_AUTHOR_BOOK_ICON,
            })
            .setTimestamp()
            .setFooter({
              text: process.env.NIGOBOT_NAME,
              iconURL: process.env.NIGOBOT_AVATAR_LINK,
            });
      }
    } else {
      embedMessage = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_BOOK_QUERY)
          .setTitle(viewService.formatToString(tr.error.errorTitle))
          .setDescription(viewService.formatToString(tr.error.errorItemNotFound))
          .setAuthor({
            name: tr.author.item,
            iconURL: process.env.SET_AUTHOR_BOOK_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          });
    }


    if (image.length > 0) {
      embedMessage.setThumbnail('attachment://' + botService.getFilename(image));
      embeds.push({
        embedMessage: embedMessage,
        images: [image],
      });

      if (embedMessageBerries.length > 0) {
        embedMessageBerries.setThumbnail('attachment://' + botService.getFilename(image));
        embeds.push({
          embedMessage: embedMessageBerries,
          images: [image],
        });
      }
      if (embedMessageFoundableDate.length > 0) {
        embedMessageFoundableDate.setThumbnail('attachment://' + botService.getFilename(image));
        embeds.push({
          embedMessage: embedMessageFoundableDate,
          images: [image],
        });
      }

      return embeds;
    } else {
      embeds.push({
        embedMessage: embedMessage,
        images: [image],
      });

      if (embedMessageBerries.length > 0) {
        embeds.push({
          embedMessage: embedMessageBerries,
          images: [image],
        });
      }
      if (embedMessageFoundableDate.length > 0) {
        embeds.push({
          embedMessage: embedMessageFoundableDate,
          images: [image],
        });
      }

      return embeds;
    }
  },
};
