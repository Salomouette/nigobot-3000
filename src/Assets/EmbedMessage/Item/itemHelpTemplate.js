const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  itemHelpTemplate: function() {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_HELP_QUERY)
        .setTitle(tr.help.item.helpCommandTitle)
        .addFields(
            {
              name: tr.help.item.viewItem.fieldTitle,
              value: tr.help.item.viewItem.fieldContent,
            },
            {
              name: tr.help.item.viewSubCategory.fieldTitle,
              value: tr.help.item.viewSubCategory.fieldContent,
            },
            {
              name: tr.help.item.viewCategory.fieldTitle,
              value: tr.help.item.viewCategory.fieldContent,
            },
            {
              name: tr.help.item.viewCategories.fieldTitle,
              value: tr.help.item.viewCategories.fieldContent,
            },
        )
        .setAuthor({
          name: tr.author.pokémon,
          iconURL: process.env.SET_AUTHOR_HELP_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
