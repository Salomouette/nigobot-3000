const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  itemDefaultTemplate: function(text) {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_ITEM_QUERY)
        .setTitle(tr.item.titleItemDefaultTemplate)
        .setDescription(text)
        .setAuthor({
          name: tr.author.pokémon,
          iconURL: process.env.SET_AUTHOR_ITEM_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
