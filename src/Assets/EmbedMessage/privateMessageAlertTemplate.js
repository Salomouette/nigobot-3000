const Discord = require('discord.js');

const frenchTranslation = require('../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  privateMessageAlertTemplate: function() {
    return new Discord.MessageEmbed()
        .setDescription(tr.alert.privateMessage)
        .setColor(process.env.COLOR_ALERT_QUERY)
        .setAuthor({
          name: tr.author.alert,
          iconURL: process.env.SET_AUTHOR_ALERT_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
