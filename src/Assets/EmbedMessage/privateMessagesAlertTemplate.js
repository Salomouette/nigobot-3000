const Discord = require('discord.js');

const frenchTranslation = require('../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  privateMessagesAlertTemplate: function() {
    return new Discord.MessageEmbed()
        .setDescription(tr.alert.privateMessages)
        .setColor(process.env.COLOR_ALERT_QUERY)
        .setAuthor(tr.author.alert, process.env.SET_AUTHOR_ALERT_ICON)
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
