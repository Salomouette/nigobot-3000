const Discord = require('discord.js');
const frenchTranslation = require('../../../Data/frenchTranslationData');

const botService = require('../../../Service/botService');
const pokémonControllerService = require('../../../Service/Pokémon/pokémonControllerService');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  pokémonCatchTemplate: function(idTrainerInput, pokemonIdInput, levelInput, pokeballInput) {
    // TODO add commande lieu
    const catchMon = pokémonControllerService.addNewPokémon(
        idTrainerInput,
        pokemonIdInput,
        levelInput,
        pokeballInput,
        'Inconnu',
    );
    let embedMessage = '';

    if (catchMon !== null) {
      embedMessage = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_POKEMON_QUERY)
          .setTitle('Et hop ! ' + catchMon.trainer + ' a attrapé un ' + catchMon.pokémon.name + ' !');

      embedMessage
          .setAuthor({
            name: tr.author.pokémon,
            iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          });
    } else {
      embedMessage = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_POKEMON_QUERY)
          .setTitle('L\'ID de dresseur ou du Pokémon n\'existe pas.')
          .setDescription('*Réssayez*');
    }

    // TODO Pokemon Image
    // if (catchMon && catchMon.pokémon.imagePath.length > 0) {
    if (false) {
      embedMessage.setThumbnail('attachment://' + botService.getFilename(catchMon.pokémon.imagePath));
      return {
        embedMessage: embedMessage,
        images: [catchMon.pokémon.imagePath],
      };
    } else {
      return {
        embedMessage: embedMessage,
        images: '',
      };
    }
  },
};
