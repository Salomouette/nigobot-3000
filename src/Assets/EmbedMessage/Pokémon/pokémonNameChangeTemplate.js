const Discord = require('discord.js');
const frenchTranslation = require('../../../Data/frenchTranslationData');
const pokémonControllerService = require('../../../Service/Pokémon/pokémonControllerService');

const tr = frenchTranslation.getFrenchTranslations();

module.exports = {
  pokémonNameChangeTemplate: function(idTrainer, idPokémon, nickname) {
    const embedMessage = new Discord.MessageEmbed();

    const result = pokémonControllerService.changeNameOfAPokémon(idTrainer, idPokémon, nickname);

    embedMessage.setColor(process.env.COLOR_POKEMON_QUERY)
        .setTitle(tr.playersPokémon.nicknameChange.titleEmbed)
        .setAuthor({
          name: tr.author.pokémon,
          iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        })
        .addFields({
          name: result.title,
          value: result.field,
        })
        .setThumbnail('https://cdn.discordapp.com/attachments/927292878806483076/963504696658645032/unknown.png');

    return embedMessage;
  },
};
