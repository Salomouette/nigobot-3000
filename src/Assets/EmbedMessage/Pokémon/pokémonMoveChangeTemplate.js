const Discord = require('discord.js');
const frenchTranslation = require('../../../Data/frenchTranslationData');
const pokémonControllerService = require('../../../Service/Pokémon/pokémonControllerService');

const tr = frenchTranslation.getFrenchTranslations();

module.exports = {
  pokémonMoveChangeTemplate: function(trainerID, pokémonID, move1, move2, move3, move4) {
    const embedMessage = new Discord.MessageEmbed();

    // TODO
    const {title: resultTitle, returnValue: resultField} =
        pokémonControllerService.changeMoveOfAPokémon(trainerID, pokémonID, move1, move2, move3, move4);

    embedMessage.setColor(process.env.COLOR_POKEMON_QUERY)
        .setTitle(tr.playersPokémon.moveChange.titleEmbed)
        .setAuthor({
          name: tr.author.pokémon,
          iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        })
        .addFields({
          name: resultTitle,
          value: resultField,
        },
        )
        .setThumbnail('https://cdn.discordapp.com/attachments/927292878806483076/963504696658645032/unknown.png');

    return embedMessage;
  },
};
