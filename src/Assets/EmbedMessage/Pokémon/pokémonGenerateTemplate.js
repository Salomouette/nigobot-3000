const Discord = require('discord.js');
const moment = require('moment');

const botService = require('../../../Service/botService');
const pokémonControllerService = require('../../../Service/Pokémon/pokémonControllerService');
const pokémonViewService = require('../../../Service/Pokémon/pokémonViewService');

const generatedPokémonData = require('../../../Data/generatedPokémonData');
const frenchTranslation = require('../../../Data/frenchTranslationData');
const emojiService = require('../../../Service/emojiService');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  pokémonGenerateTemplate: function(biomeName, rarity, pokémonEntered,
      size, shine, alpha,
      teraModifier, titanModifier, antiqueModifier, client) {
    let embedMessage = '';
    let image = '';
    shine = shine !== null ? shine.toUpperCase() : '';
    alpha = alpha !== null ? alpha.toUpperCase() : '';
    size = size !== null ? size.toUpperCase() : '';
    teraModifier = teraModifier !== null ? teraModifier.toUpperCase() : '';

    const pokémon = pokémonEntered === '' ?
        pokémonViewService.pokémonGeneratorHandler(biomeName, rarity) :
        pokémonEntered;

    if (pokémon !== undefined) {
      const pokémonSex = pokémonControllerService.generatePokémonSex(pokémon);
      const shiny = pokémonViewService.isPokémonChroma(shine);
      let antique = false;
      if (pokémon.traits !== undefined && pokémon.traits['antique'] !== undefined) {
        antique = pokémonViewService.isPokémonAntique(pokémon.traits['antique'], antiqueModifier);
      }
      const titanCharacteristic = pokémonViewService.isPokémonTitan(titanModifier);
      const alphaCharacteristic = titanCharacteristic ? false: pokémonViewService.isPokémonAlpha(alpha);
      const tera = pokémonViewService.doesPokémonHasTera(teraModifier);
      const teraType = pokémonControllerService.generatePokémonTeraType(pokémon, tera);
      const teraBoss = tera ? pokémonViewService.isTeraPokémonABoss(teraModifier) : false;
      const pokebloc = pokémonControllerService.generatePokémonFavouriteBloc();
      const pokemonOutput = {};
      image = botService.generatePathPokémonImages(pokémon, pokémonSex);
      const ability = pokémonControllerService.generatePokémonAbility(pokémon.id, pokémon.abilities, pokémonSex, image);
      const traits = pokémonControllerService.generatePokémonTraits();
      const notes = pokémonControllerService.generatePokémonNotes(traits.length);
      const contest = pokémonControllerService.generatePokémonCondition();
      const shownTraits = pokémonViewService.getPlayerPokémonTraits(traits, notes);
      // Output saved for catching

      pokemonOutput.name = pokémon.name;
      pokemonOutput.image = image;
      pokemonOutput.sex = pokémonSex;
      pokemonOutput.friendship = 1;
      pokemonOutput.genId = moment().unix();
      pokemonOutput.food = pokebloc;
      pokemonOutput.tera = teraType;
      pokemonOutput.shiny = shiny;
      pokemonOutput.alpha = alphaCharacteristic;
      pokemonOutput.titan = titanCharacteristic;
      pokemonOutput.ability = ability.id;
      pokemonOutput.traits = traits;
      pokemonOutput.notes = notes;
      pokemonOutput.antique = antique;
      pokemonOutput.contest = contest;
      pokemonOutput.dangerousness = pokémon.dangerousness;
      if (typeof pokémon === 'string') {
        if (pokémon.includes('Biome invalide')) {
          embedMessage = new Discord.MessageEmbed()
              .setColor(process.env.COLOR_POKEMON_QUERY)
              .setTitle(tr.error.errorTitle)
              .setDescription(pokémon)
              .setAuthor(tr.author.biome, process.env.SET_AUTHOR_POKEMON_ICON)
              .setTimestamp()
              .setFooter({
                text: process.env.NIGOBOT_NAME,
                iconURL: process.env.NIGOBOT_AVATAR_LINK,
              });
        }
      } else {
        embedMessage = new Discord.MessageEmbed()
            .setColor(process.env.COLOR_POKEMON_QUERY)
            .setDescription(
                tr.pokémon.pokemonGenerateTemplate.pokemonFoundStart +
                pokémon.name +
                tr.pokémon.pokemonGenerateTemplate.pokemonFoundEnd +
                tr.pokémon.pokemonGenerateTemplate.pokemonID +
                pokemonOutput.genId + '!',
            )
            .addFields(
                {
                  name: tr.pokémon.pokemonGenerateTemplate.sexTitle,
                  value: tr.pokémon.pokemonGenerateTemplate.sexDescription + pokémonSex + '**.',
                },
                {
                  name: tr.pokémon.pokemonGenerateTemplate.abilityTitle,
                  value: tr.pokémon.pokemonGenerateTemplate.abilityDescription.replace('{TALENT}', ability.name),
                },
                {
                  name: tr.pokémon.pokemonGenerateTemplate.dangerousnessTitle,
                  value: pokémonViewService.generateDangerousnessDescription(pokemonOutput.dangerousness, client),
                },
            );


        if (pokémon.height !== undefined && pokémon.weight !== undefined) {
          const pokémonHeightFactor = pokémonViewService.generateFactorPokémon(size);
          const pokémonHeight = pokémonViewService.generateHeightPokémon(
              pokémon.height,
              alphaCharacteristic,
              titanCharacteristic,
              pokémonHeightFactor);

          const pokémonWeightFactor = pokémonViewService.generateFactorPokémon(size);
          const pokémonWeight = pokémonViewService.generateWeightPokémon(
              pokémonHeight,
              pokémon.height,
              pokémon.weight,
              alphaCharacteristic,
              titanCharacteristic,
              pokémonWeightFactor,
          );
          const emojiHeight = pokémonViewService.generateHeightWeightEmoji(
              pokémonHeightFactor,
              client,
          );
          const emojiWeight = pokémonViewService.generateHeightWeightEmoji(
              pokémonWeightFactor,
              client,
          );
          pokemonOutput.height = {multiplicateur: pokémonHeight/pokémon.height, emoji: emojiHeight.name};
          pokemonOutput.weight = {multiplicateur: pokémonWeight/pokémon.weight, emoji: emojiWeight.name};

          embedMessage
              .addFields(
                  {
                    name: tr.pokémon.pokemonGenerateTemplate.heightTitle,
                    value:
                        emojiHeight.emoji +
                        ' - ' +
                        (pokémonHeight).toFixed(2) +
                        'm',
                    inline: true,
                  },
                  {
                    name: tr.pokémon.pokemonGenerateTemplate.weightTitle,
                    value:
                        emojiWeight.emoji +
                        ' - ' +
                        (pokémonWeight).toFixed(2) +
                        'kg',
                    inline: true,
                  },
              )
              .setAuthor({
                name: tr.author.biome,
                iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
              })
              .setTimestamp()
              .setFooter({
                text: process.env.NIGOBOT_NAME,
                iconURL: process.env.NIGOBOT_AVATAR_LINK,
              });
        }

        if (alphaCharacteristic) {
          if (teraBoss) {
            embedMessage.setTitle(emojiService.getCharacteristicEmojiPerName('Tera', client) +
              tr.pokémon.pokemonGenerateTemplate.alphaTera +
                emojiService.getCharacteristicEmojiPerName('Alpha', client));
          } else {
            embedMessage.setTitle(tr.pokémon.pokemonGenerateTemplate.alpha +
              emojiService.getCharacteristicEmojiPerName('Alpha', client));
          }
        } else if (titanCharacteristic) {
          if (teraBoss) {
            embedMessage.setTitle(emojiService.getCharacteristicEmojiPerName('Tera', client) +
              tr.pokémon.pokemonGenerateTemplate.titanTera +
                emojiService.getCharacteristicEmojiPerName('A_Titan', client));
          } else {
            embedMessage.setTitle(tr.pokémon.pokemonGenerateTemplate.titan +
              emojiService.getCharacteristicEmojiPerName('A_Titan', client));
          }
        } else if (teraBoss) {
          embedMessage.setTitle(
              emojiService.getCharacteristicEmojiPerName('Tera', client) +
            tr.pokémon.pokemonGenerateTemplate.tera);
        } else {
          embedMessage.setTitle(tr.pokémon.pokemonGenerateTemplate.surprise);
        }

        if (shiny) {
          const emojiShiny = emojiService.getCharacteristicEmojiPerName('Shiny', client);
          embedMessage.addFields(
              {
                name: tr.pokémon.pokemonGenerateTemplate.shinyTitle,
                value: emojiShiny + tr.pokémon.pokemonGenerateTemplate.shiny + emojiShiny,
              },
          );
        }

        if (tera) {
          if (teraBoss) {
            embedMessage.addFields(
                {
                  name: tr.pokémon.pokemonGenerateTemplate.teraTitle,
                  value: emojiService.getTeraIconEmojiPerType(teraType, client) +
                  ' ' +
                  tr.pokémon.pokemonGenerateTemplate.teraType[teraType]+tr.pokémon.pokemonGenerateTemplate.teraEnd,
                },
            );
          } else {
            embedMessage.addFields(
                {
                  name: tr.pokémon.pokemonGenerateTemplate.teraTitle,
                  value: emojiService.getTeraIconEmojiPerType(teraType, client) +
                  ' ' +
                  tr.pokémon.pokemonGenerateTemplate.teraTypeNotBoss,
                },
            );
          }
        }

        if (antique) {
          const emojiAntique = emojiService.getCharacteristicEmojiPerName('Antique', client);
          embedMessage.addFields(
              {
                name: tr.pokémon.pokemonGenerateTemplate.antiqueTitle,
                value: emojiAntique + tr.pokémon.pokemonGenerateTemplate.antiqueDescription,
              },
          );
        }

        embedMessage.addFields(
            {
              name: tr.pokémon.pokemonGenerateTemplate.traitsNotesTitle,
              value: tr.pokémon.pokemonGenerateTemplate.traitsNotesDescription + shownTraits,
            },
        );

        if (pokemonOutput !== null) {
          generatedPokémonData.addAndSaveGeneratedPokemonFile(pokemonOutput);
        }
      }
    } else {
      embedMessage = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_POKEMON_QUERY)
          .setTitle(tr.error.errorTitle)
          .setDescription(tr.error.errorPokemonBiomeGeneration)
          .setAuthor({
            name: tr.author.biome,
            iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          });
    }

    // TODO Pokemon Image
    // if (catchMon && catchMon.pokémon.imagePath.length > 0) {
    if (false) {
      embedMessage.setThumbnail('attachment://' + botService.getFilename(image));
      return {
        embedMessage: embedMessage,
        images: [image],
      };
    } else {
      return {
        embedMessage: embedMessage,
        images: '',
      };
    }
  },
};
