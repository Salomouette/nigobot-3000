const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  trainersPokémonAdminHelpTemplate: function() {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_HELP_QUERY)
        .setTitle(tr.help.trainersPokémonAdmin.helpCommandTitle)
        .addFields(
            {
              name: tr.help.trainersPokémonAdmin.seePokémonPage.fieldTitle,
              value: tr.help.trainersPokémonAdmin.seePokémonPage.fieldContent,
            },
            {
              name: tr.help.trainersPokémonAdmin.addLevelPage.fieldTitle,
              value: tr.help.trainersPokémonAdmin.addLevelPage.fieldContent,
            },
            {
              name: tr.help.trainersPokémonAdmin.addFriendshipPage.fieldTitle,
              value: tr.help.trainersPokémonAdmin.addFriendshipPage.fieldContent,
            },
            {
              name: tr.help.trainersPokémonAdmin.givePokémonToPlayerPage.fieldTitle,
              value: tr.help.trainersPokémonAdmin.givePokémonToPlayerPage.fieldContent,
            },
            {
              name: tr.help.trainersPokémonAdmin.changeMovePage.fieldTitle,
              value: tr.help.trainersPokémonAdmin.changeMovePage.fieldContent,
            },
            {
              name: tr.help.trainersPokémonAdmin.changeNicknamePage.fieldTitle,
              value: tr.help.trainersPokémonAdmin.changeNicknamePage.fieldContent,
            },
            {
              name: tr.help.trainersPokémonAdmin.values.fieldTitle,
              value: tr.help.trainersPokémonAdmin.values.fieldContent,
            },
        )
        .setAuthor({
          name: tr.author.pokémon,
          iconURL: process.env.SET_AUTHOR_HELP_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
