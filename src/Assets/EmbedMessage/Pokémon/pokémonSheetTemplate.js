const Discord = require('discord.js');
const moment = require('moment');

const botService = require('../../../Service/botService');
const emojiService = require('../../../Service/emojiService');
const pokémonViewService = require('../../../Service/Pokémon/pokémonViewService');
const viewService = require('../../../Service/viewService');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  pokémonSheetTemplate: function(pokémon, pokémonFoundItem, client) {
    let embedMessage = '';
    if (pokémon === 0 || pokémon === -1) {
      embedMessage = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_POKEMON_QUERY)
          .setAuthor({
            name: tr.author.pokémon,
            iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
          });

      if (pokémon === 0) {
        embedMessage.setTitle(tr.playersPokémon.pokémonSheetTemplate.error.title1)
            .setDescription(tr.playersPokémon.pokémonSheetTemplate.error.field1);
      }

      if (pokémon === -1) {
        embedMessage.setTitle(tr.playersPokémon.pokémonSheetTemplate.error.title2)
            .setDescription(tr.playersPokémon.pokémonSheetTemplate.error.field2);
      }
    } else {
      const name = pokémonViewService.getPlayerPokémonNames(
          pokémon.name,
          pokémon.surname,
          pokémon.alpha,
          pokémon.shiny,
          pokémon.sex,
          pokémon.titan,
          pokémon.antique,
          client,
      );
      const ability = pokémonViewService.getPlayerPokémonAbility(pokémon.ability);
      const moves = pokémonViewService.getPlayerPokémonMoves(pokémon.moves);
      const traits = pokémonViewService.getPlayerPokémonTraits(pokémon.notes, pokémon.editableNotes);
      const friendship = pokémonViewService.getPlayerPokémonFriendship(pokémon.friendship);
      const nbSpecialFoundItems = pokémonViewService.getNumberSpecialFoundItems(pokémonFoundItem);
      const height = pokémonViewService.viewHeightWeight(true, pokémon.height.multiplicateur, pokémon.name);
      const weight = pokémonViewService.viewHeightWeight(false, pokémon.weight.multiplicateur, pokémon.name);
      const ball = pokémonViewService.getPokeball(pokémon.ball, client);
      const typeOfBall = pokémonViewService.geStatusOrBall(pokémon.ball);

      embedMessage = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_POKEMON_QUERY)
          .setTitle(name + tr.playersPokémon.pokémonSheetTemplate.id.replace('{IDPOKE}', pokémon.genId))
          .addFields(
              {
                name: viewService.formatUnderscoreAndBoldText(tr.playersPokémon.pokémonSheetTemplate.level),
                value: pokémon.level.toString(),
                inline: true,
              },
              {
                name: viewService.formatUnderscoreAndBoldText(tr.playersPokémon.pokémonSheetTemplate.sex),
                value: pokémon.sex,
                inline: true,
              },
              {
                name: typeOfBall ?
                viewService.formatUnderscoreAndBoldText(tr.playersPokémon.pokémonSheetTemplate.status):
                viewService.formatUnderscoreAndBoldText(tr.playersPokémon.pokémonSheetTemplate.catchBall),
                value: ball,
                inline: true,
              },
          );

      if (typeof pokémon.teraType != 'undefined' && pokémon.teraType.length > 0) {
        embedMessage.addFields( {
          name: viewService.formatUnderscoreAndBoldText(tr.playersPokémon.pokémonSheetTemplate.teraType),
          value: emojiService.getTeraIconEmojiPerType(pokémon.teraType, client) + ' ' + pokémon.teraType,
        });
      }

      embedMessage.addFields(
          {
            name: viewService.formatUnderscoreAndBoldText( tr.playersPokémon.pokémonSheetTemplate.date.title),
            value: tr.playersPokémon.pokémonSheetTemplate.date.field.replace(
                '{DATE}',
                moment(pokémon.catchDate, 'DD/MM/YYYY').locale('fr').format('dddd Do MMMM YYYY'),
            ),
          },
          {
            name: viewService.formatUnderscoreAndBoldText(tr.playersPokémon.pokémonSheetTemplate.size.title),
            value: emojiService.getHeightWeightEmojiPerName(
                pokémon.height.emoji, client) + tr.playersPokémon.pokémonSheetTemplate.size.field.replace(
                '{HEIGHT}',
                height.toFixed(2),
            ),
            inline: true,
          },
          {
            name: viewService.formatUnderscoreAndBoldText(tr.playersPokémon.pokémonSheetTemplate.weight.title),
            value: emojiService.getHeightWeightEmojiPerName(
                pokémon.weight.emoji, client) +
                    tr.playersPokémon.pokémonSheetTemplate.weight.field.replace(
                        '{WEIGHT}',
                        weight.toFixed(2),
                    ),
            inline: true,
          })
          .addFields(
              {
                name: viewService.formatUnderscoreAndBoldText(tr.playersPokémon.pokémonSheetTemplate.friendship),
                value: friendship + ' - ' + pokémon.friendship + '/15',
              },
              {
                name: viewService.formatUnderscoreAndBoldText(tr.playersPokémon.pokémonSheetTemplate.ability),
                value: ability,
                inline: true,
              },
              {
                name: viewService.formatUnderscoreAndBoldText(tr.playersPokémon.pokémonSheetTemplate.moves),
                value: moves,
                inline: true,
              },
              {
                name: viewService.formatUnderscoreAndBoldText(tr.playersPokémon.pokémonSheetTemplate.pokebloc.title),
                value: tr.playersPokémon.pokémonSheetTemplate.pokebloc.field.replace(
                    '{FOOD}',
                    pokémonViewService.getTrainerPokémonFood(pokémon.food)) +
                      emojiService.getPokeblocEmojiPerColorAndLevelValue(pokémon.food, 0, client),
              },
          );

      embedMessage.addFields(
          {
            name: viewService.formatUnderscoreAndBoldText(tr.playersPokémon.pokémonSheetTemplate.condition.title),
            value: tr.playersPokémon.pokémonSheetTemplate.condition.beauty +
                  emojiService.getPokeblocEmojiPerColorAndLevelValue(
                      'blue',
                      0,
                      client,
                  ) + ' ' +
                  pokémon.contest.beauty +
                  '/10\n' +
                  tr.playersPokémon.pokémonSheetTemplate.condition.cuteness +
                  emojiService.getPokeblocEmojiPerColorAndLevelValue(
                      'pink',
                      0,
                      client,
                  ) + ' ' +
                  pokémon.contest.cuteness +
                  '/10\n' +
                  tr.playersPokémon.pokémonSheetTemplate.condition.cleverness +
                  emojiService.getPokeblocEmojiPerColorAndLevelValue(
                      'green',
                      0,
                      client,
                  ) + ' ' +
                  pokémon.contest.cleverness +
                  '/10\n' +
                  tr.playersPokémon.pokémonSheetTemplate.condition.toughness +
                  emojiService.getPokeblocEmojiPerColorAndLevelValue(
                      'yellow',
                      0,
                      client,
                  ) + ' ' +
                  pokémon.contest.toughness +
                  '/10\n' +
                  tr.playersPokémon.pokémonSheetTemplate.condition.coolness +
                  emojiService.getPokeblocEmojiPerColorAndLevelValue(
                      'red',
                      0,
                      client,
                  ) + ' ' +
                  pokémon.contest.coolness +
                  '/10\n' +
                  tr.playersPokémon.pokémonSheetTemplate.condition.liveliness +
                  emojiService.getPokeblocEmojiPerColorAndLevelValue(
                      'indigo',
                      0,
                      client,
                  ) + ' ' +
                  pokémon.contest.liveliness +
                  '/10\n' +
                  tr.playersPokémon.pokémonSheetTemplate.condition.atypical +
                  emojiService.getPokeblocEmojiPerColorAndLevelValue(
                      'purple',
                      0,
                      client,
                  ) + ' ' +
                  pokémon.contest.atypical +
                  '/10\n' +
                  tr.playersPokémon.pokémonSheetTemplate.condition.natural +
                  emojiService.getPokeblocEmojiPerColorAndLevelValue(
                      'orange',
                      0,
                      client,
                  ) + ' ' +
                  pokémon.contest.natural +
                  '/10\n',
            inline: true,
          },
      );
      embedMessage.addFields(
          {
            name: viewService.formatUnderscoreAndBoldText(tr.playersPokémon.pokémonSheetTemplate.notes),
            value: traits,
            inline: true,
          },
          {
            name: viewService.formatUnderscoreAndBoldText(
                tr.playersPokémon.pokémonSheetTemplate.foundItem.lastItemsLine,
            ),
            value: pokémonFoundItem.length == 0 ? tr.playersPokémon.pokémonSheetTemplate.foundItem.none :
            viewService.formatToString(
                pokémonViewService.getPlayerPokémonLastFoundItems(
                    pokémonFoundItem,
                    client,
                ),
            ),
          },
          {
            name: viewService.formatUnderscoreAndBoldText(
                tr.playersPokémon.pokémonSheetTemplate.foundItem.specialItemsLine,
            ),
            value: tr.playersPokémon.pokémonSheetTemplate.foundItem.specialItemsLineValue,
          },
          {
            name: viewService.formatBoldText(tr.playersPokémon.pokémonSheetTemplate.foundItem.silverLeaf) +
                  emojiService.getFoundItemEmojiPerName('silverLeaf', client),
            value: nbSpecialFoundItems.nbSilverLeafFound + ' trouvée(s)',
            inline: true,
          },
          {
            name: viewService.formatBoldText(tr.playersPokémon.pokémonSheetTemplate.foundItem.goldLeaf) +
                  emojiService.getFoundItemEmojiPerName('goldLeaf', client),
            value: nbSpecialFoundItems.nbGoldLeafFound + ' trouvée(s)',
            inline: true,
          },
          {
            name: viewService.formatBoldText(tr.playersPokémon.pokémonSheetTemplate.foundItem.bouquet) +
                  emojiService.getFoundItemEmojiPerName('bouquet', client),
            value: nbSpecialFoundItems.nbBouquetFound + ' offert(s)',
            inline: true,
          },
      ).setAuthor({
        name: tr.author.pokémon,
        iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
      })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          });
    }

    // TODO Pokemon Image
    // if (catchMon && catchMon.pokémon.imagePath.length > 0) {
    if (false) {
      embedMessage.setThumbnail('attachment://' + botService.getFilename(pokémon.imagePath));
      return {
        embedMessage: embedMessage,
        images: [pokémon.imagePath],
      };
    } else {
      return {
        embedMessage: embedMessage,
        images: '',
      };
    }
  },
};
