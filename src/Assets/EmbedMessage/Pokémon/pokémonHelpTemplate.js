const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  pokémonHelpTemplate: function() {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_HELP_QUERY)
        .setTitle(tr.help.pokémon.helpCommandTitle)
        .addFields(
            {
              name: tr.help.pokémon.pokedexPage.fieldTitle,
              value: tr.help.pokémon.pokedexPage.fieldContent,
            },
            {
              name: tr.help.pokémon.genererPage.fieldTitle,
              value: tr.help.pokémon.genererPage.fieldContent,
            },
            {
              name: tr.help.pokémon.generer2Page.fieldTitle,
              value: tr.help.pokémon.generer2Page.fieldContent,
            },
            {
              name: tr.help.pokémon.values.fieldTitle,
              value: tr.help.pokémon.values.fieldContent,
            },
        )
        .setAuthor({
          name: tr.author.pokémon,
          iconURL: process.env.SET_AUTHOR_HELP_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
