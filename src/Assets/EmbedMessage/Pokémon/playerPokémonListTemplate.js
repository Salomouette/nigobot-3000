const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  playerPokémonListTemplate: function(pokémon, msg) {
    let embedMessage = null;
    if (pokémon !== 0 || pokémon !== -1) {
      embedMessage = new Discord.MessageEmbed().setColor(process.env.COLOR_POKEMON_QUERY)
          .setAuthor({
            name: tr.author.pokémon,
            iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
          })
          .setTitle(tr.playersPokémon.allTrainerPokémon.title)
          .setDescription(tr.playersPokémon.allTrainerPokémon.trainer.replace(
              '{PLAYER}',
              msg.author.username,
          ) + '\n\n' + pokémon);
    } else {
      embedMessage = new Discord.MessageEmbed().setColor(process.env.COLOR_POKEMON_QUERY)
          .setAuthor({
            name: tr.author.pokémon,
            iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
          });

      if (pokémon === 0) {
        embedMessage.setTitle(tr.playersPokémon.allTrainerPokémon.title)
            .setDescription('Il n\'y a aucun dresseur initialisé.', 'Prévenez l\'Admin.');
      }

      if (pokémon === -1) {
        embedMessage.setTitle(tr.playersPokémon.allTrainerPokémon.title)
            .setDescription('Vous n\'êtes pas dans les fichiers du jeu.', 'Prévenez l\'Admin.');
      }
    }
    return embedMessage;
  },

  allPlayerPokémonListTemplate: function(trainer, pokémon, msg) {
    let embedMessage = null;
    embedMessage = new Discord.MessageEmbed().setColor(process.env.COLOR_POKEMON_QUERY)
        .setTitle(tr.playersPokémon.allTrainerPokémon.title)
        .setAuthor({
          name: tr.author.pokémon,
          iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
        });

    if (trainer !== 0 && pokémon !== 0) {
      for (let i = 0; i < trainer.length; i++) {
        embedMessage.setDescription(trainer[i], pokémon[i]);
      }
    } else {
      embedMessage.setDescription('Il n\'y a aucun dresseur initialisé.', 'Vérifiez les fichiers internes.');
    }

    return embedMessage;
  },
};
