const Discord = require('discord.js');

const pokémonControllerService = require('../../../Service/Pokémon/pokémonControllerService');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  pokémonLevelAndFriendshipUpTemplate: function(mode, under20, under40, under60, under80, under100) {
    const returnJsonEmbeds = [];
    let returnUpdateFunctionData = [];
    let embed = '';
    let embedTitle = '';

    switch (mode) {
      case 'level':
        returnUpdateFunctionData = pokémonControllerService.addLevelPokémon(
            under20,
            under40,
            under60,
            under80,
            under100,
        );
        embedTitle = tr.playersPokémon.levelAndFriendship.title.level;
        break;
      case 'friendship':
        returnUpdateFunctionData = pokémonControllerService.addFriendshipPokémon();
        embedTitle = tr.playersPokémon.levelAndFriendship.title.friendship;
        break;
    }

    if (returnUpdateFunctionData.length !== 0) {
      for (let i = 0; i < returnUpdateFunctionData.length; i++) {
        let increment = 0;
        embed = new Discord.MessageEmbed()
            .setTitle(embedTitle)
            .setColor(process.env.COLOR_POKEMON_QUERY)
            .setAuthor({
              name: tr.author.pokémon,
              iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
            })
            .setTimestamp()
            .setFooter({
              text: process.env.NIGOBOT_NAME,
              iconURL: process.env.NIGOBOT_AVATAR_LINK,
            });

        let description = returnUpdateFunctionData[i].trainerLine;
        for (let j = 0; j < returnUpdateFunctionData[i].pokémonLines.length; j++) {
          if (increment === 30) {
            embed.setDescription(description);
            returnJsonEmbeds.push(
                {
                  trainerId: returnUpdateFunctionData[i].trainerId,
                  embed: embed,
                },
            );

            embed = new Discord.MessageEmbed()
                .setTitle(embedTitle)
                .setColor(process.env.COLOR_POKEMON_QUERY)
                .setAuthor({
                  name: tr.author.pokémon,
                  iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
                })
                .setTimestamp()
                .setFooter({
                  text: process.env.NIGOBOT_NAME,
                  iconURL: process.env.NIGOBOT_AVATAR_LINK,
                });

            description = returnUpdateFunctionData[i].trainerLine;
            increment = 0;
          }

          description = description + returnUpdateFunctionData[i].pokémonLines[j];
          increment++;
        }
        embed.setDescription('');
        embed.setDescription(description);
        returnJsonEmbeds.push(
            {
              trainerId: returnUpdateFunctionData[i].trainerId,
              embed: embed,
            },
        );
      }
      return returnJsonEmbeds;
    } else {
      return new Discord.MessageEmbed()
          .setTitle(embedTitle)
          .setColor(process.env.COLOR_POKEMON_QUERY)
          .setAuthor({
            name: tr.author.pokémon,
            iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          })
          .addFields(
              {
                name: 'Aucun dresseur initialisé',
                value: 'Veuillez vérifier dans les données du Nigobot s\'il n\'y a pas de problème.',
              },
          );
    }
  },
};
