const Discord = require('discord.js');
const frenchTranslation = require('../../../Data/frenchTranslationData');
const pokémonControllerService = require('../../../Service/Pokémon/pokémonControllerService');

const tr = frenchTranslation.getFrenchTranslations();

module.exports = {
  pokémonNotesChangeTemplate: function(idTrainer, idPokémon, template, notesToAdd = undefined) {
    const embedMessage = new Discord.MessageEmbed();

    if (template) {
      const result = idPokémon != undefined ?
    pokémonControllerService.changeNoteTemplate(idTrainer, idPokémon) :
    tr.playersPokémon.noteChange.template.noID;
      embedMessage.setColor(process.env.COLOR_POKEMON_QUERY)
          .setTitle(tr.playersPokémon.noteChange.titleEmbed)
          .setAuthor({
            name: tr.author.pokémon,
            iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          })
          .addFields({
            name: tr.playersPokémon.noteChange.template.titleField,
            value: result,
          });
    } else {
      if (notesToAdd != undefined) {
        const result = pokémonControllerService.changeNote(idTrainer, idPokémon, notesToAdd);
        embedMessage.setColor(process.env.COLOR_POKEMON_QUERY)
            .setTitle(tr.playersPokémon.noteChange.titleEmbed)
            .setAuthor({
              name: tr.author.pokémon,
              iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
            })
            .setTimestamp()
            .setFooter({
              text: process.env.NIGOBOT_NAME,
              iconURL: process.env.NIGOBOT_AVATAR_LINK,
            })
            .addFields({
              name: tr.playersPokémon.noteChange.template.titleField,
              value: result,
            });
      }
    }
    return embedMessage;
  },
};
