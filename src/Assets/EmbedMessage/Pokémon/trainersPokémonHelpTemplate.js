const Discord = require('discord.js');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  trainersPokémonHelpTemplate: function() {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_HELP_QUERY)
        .setTitle(tr.help.trainersPokémon.helpCommandTitle)
        .addFields(
            {
              name: tr.help.trainersPokémon.seeAllPokémonPage.fieldTitle,
              value: tr.help.trainersPokémon.seeAllPokémonPage.fieldContent,
            },
            {
              name: tr.help.trainersPokémon.seePokémonPage.fieldTitle,
              value: tr.help.trainersPokémon.seePokémonPage.fieldContent,
            },
            {
              name: tr.help.trainersPokémon.changeMovePage.fieldTitle,
              value: tr.help.trainersPokémon.changeMovePage.fieldContent,
            },
            {
              name: tr.help.trainersPokémon.changeNicknamePage.fieldTitle,
              value: tr.help.trainersPokémon.changeNicknamePage.fieldContent,
            },
            {
              name: tr.help.trainersPokémon.changeNotesPage.fieldTitle,
              value: tr.help.trainersPokémon.changeNotesPage.fieldContent,
            },
            {
              name: tr.help.trainersPokémon.templateChangeNotesPage.fieldTitle,
              value: tr.help.trainersPokémon.templateChangeNotesPage.fieldContent,
            },
            {
              name: tr.help.trainersPokémon.values.fieldTitle,
              value: tr.help.trainersPokémon.values.fieldContent,
            },
        )
        .setAuthor({
          name: tr.author.pokémon,
          iconURL: process.env.SET_AUTHOR_HELP_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
