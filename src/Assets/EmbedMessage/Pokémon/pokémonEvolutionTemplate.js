const Discord = require('discord.js');
const frenchTranslation = require('../../../Data/frenchTranslationData');
const pokémonControllerService = require('../../../Service/Pokémon/pokémonControllerService');
const botService = require('../../../Service/botService');
const tr = frenchTranslation.getFrenchTranslations();

module.exports = {
  pokémonEvolutionTemplate: function(idTrainer, idPokémon, evolution, form) {
    const embedMessage = new Discord.MessageEmbed();
    const result = pokémonControllerService.evolveAPokémon(idTrainer, idPokémon, evolution, form);
    let title = '';
    let field = '';
    switch (result.error) {
      default:
        const surname = result.oldPokemonSurname == '' ? result.oldPokemon : result.oldPokemonSurname;

        title = tr.playersPokémon.pokemonEvolutionTemplate.evolution.title.replace('{{NOMPOKEMON}}', surname);
        field = tr.playersPokémon.pokemonEvolutionTemplate.evolution.field.replace('{{PSEUDOJOUEUR}}', result.trainer)
            .replace('{{BASEPOKEMON}}', surname)
            .replace('{{NEWPOKEMON}}', result.newPokemon);
        break;
      case 1:
        title = tr.playersPokémon.pokemonEvolutionTemplate.noTrainer.title;
        field = tr.playersPokémon.pokemonEvolutionTemplate.noTrainer.field.replace('{{IDJOUEUR}}', idTrainer);
        break;
      case 2:
        title = tr.playersPokémon.pokemonEvolutionTemplate.noPokemon.title;
        field = tr.playersPokémon.pokemonEvolutionTemplate.noPokemon.field.replace('{{PSEUDOJOUEUR}}', result.trainer)
            .replace('{{IDPOKEMON}}', idPokémon);
        break;
      case 3:
        title = tr.playersPokémon.pokemonEvolutionTemplate.notKnownPokemon.title;
        field = tr.playersPokémon.pokemonEvolutionTemplate.notKnownPokemon.field.replace('{{NOMPOKEMON}}', evolution);
        break;
    }
    embedMessage.setColor(process.env.COLOR_POKEMON_QUERY)
        .setTitle(tr.playersPokémon.pokemonEvolutionTemplate.titleEmbed)
        .setAuthor({
          name: tr.author.pokémon,
          iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        })
        .addFields({
          name: title,
          value: field,
        });

    // TODO Pokemon Image
    // if (catchMon && catchMon.pokémon.imagePath.length > 0) {
    if (false) {
      embedMessage.setThumbnail('attachment://' + botService.getFilename(result.imagePath));
      return {
        embedMessage: embedMessage,
        images: [result.imagePath],
      };
    } else {
      return {
        embedMessage: embedMessage,
        images: '',
      };
    }
  },
};
