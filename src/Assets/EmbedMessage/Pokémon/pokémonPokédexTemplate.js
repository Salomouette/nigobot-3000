const Discord = require('discord.js');

const botService = require('../../../Service/botService');
const pokémonViewService = require('../../../Service/Pokémon/pokémonViewService');
const emojiService = require('../../../Service/emojiService');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  pokémonPokédexTemplate: function(pokémon, client) {
    const livingBiome = pokémonViewService.generateLivingBiomePokémonPokédexData(pokémon);
    let embedMessage = '';
    let image = '';

    if (livingBiome !== undefined) {
      image = botService.generatePathPokémonImages(pokémon);
      embedMessage = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_BOOK_QUERY)
          .setTitle('[' + pokémon.id + '] ' + pokémon.name)
          .addFields(
              {
                name: tr.pokémon.pokémonPokédexTemplate.livingZone,
                value: livingBiome,
              },
              {
                name: tr.pokémon.pokémonPokédexTemplate.types,
                value: pokémonViewService.generateTypesPokémonPokédexData(pokémon, client),
              },
              {
                name: tr.pokémon.pokémonPokédexTemplate.description,
                value: pokémon.description.length !== 0 ?
                    pokémon.description :
                    tr.pokémon.pokémonPokédexTemplate.noDescription,
              },
              {
                name: tr.pokémon.pokémonPokédexTemplate.favoriteFoodName,
                value: pokémonViewService.generateFieldFavoriteFood(pokémon, client),
              },
              {
                name: tr.pokémon.pokémonPokédexTemplate.sexRepartition,
                value: pokémonViewService.generateFieldSexRepartition(pokémon, client),
              },
              {
                name: tr.pokémon.pokémonPokédexTemplate.dangerousnessTitle,
                value: pokémonViewService.generateDangerousnessDescription(pokémon.dangerousness, client),
              },
              {
                name: tr.pokémon.pokémonPokédexTemplate.talents.title + pokémon.name,
                value: pokémonViewService.getAllAbilities(pokémon.abilities, client),
              },
              {
                name: tr.pokémon.pokémonPokédexTemplate.weight,
                value: pokémonViewService.generateDisplayWeightFieldValue(pokémon, client),
                inline: true,
              },
              {
                name: tr.pokémon.pokémonPokédexTemplate.height,
                value: pokémonViewService.generateDisplayHeightFieldValue(pokémon, client),
                inline: true,
              },
          )
          .setAuthor({
            name: tr.author.pokémon,
            iconURL: process.env.SET_AUTHOR_BOOK_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          });
      if (pokémon['pokéAssist'] !== null && pokémon.fieldMove !== null) {
        embedMessage.addFields(
            {
              name: tr.pokémon.pokémonPokédexTemplate.browser,
              value: tr.pokémon.pokémonPokédexTemplate.pokémonRangerData,
            },
            {
              name: tr.pokémon.pokémonPokédexTemplate.pokéAssist,
              value: pokémon['pokéAssist'] +
                  ' ' +
                  emojiService.getPokémonAssistIconEmojiPerType(pokémon['pokéAssist'], client),
              inline: true,
            },
            {
              name: tr.pokémon.pokémonPokédexTemplate.fieldsMove,
              value: pokémonViewService.generateFieldMovesPokémonPokédexData(pokémon, client),
              inline: true,
            },
        );
      }

      if (pokémon.traits !== undefined && pokémon.traits !== null) {
        let traits = '';
        const emojiAntique = emojiService.getCharacteristicEmojiPerName('Antique', client);
        if (pokémon.traits['antique'] !== undefined && pokémon.traits['antique'] !== null) {
          traits += pokémon.traits['antique'] < 100 ?
          tr.pokémon.pokémonPokédexTemplate.traits.antique.notGuaranteed.replace(
              '{PERCENT}', pokémon.traits['antique']) :
          tr.pokémon.pokémonPokédexTemplate.traits.antique.guaranteed;
        }
        embedMessage.addFields( {
          name: tr.pokémon.pokémonPokédexTemplate.traits['title'],
          value: emojiAntique + traits,
        });
      }

      if (pokémon.credit !== undefined) {
        embedMessage.addFields(
            {
              name: tr.pokémon.pokémonPokédexTemplate.credit,
              value: tr.pokémon.pokémonPokédexTemplate.artistCredit
                  .replace('{ARTIST}', pokémon.credit.artist)
                  .replace('{LINK}', pokémon.credit.link)
              ,
            },
        );
      }
    } else {
      embedMessage = new Discord.MessageEmbed()
          .setColor(process.env.COLOR_POKEMON_QUERY)
          .setTitle(tr.error.errorTitle)
          .setDescription(tr.error.errorInvalidArguments)
          .setAuthor({
            name: tr.author.pokémon,
            iconURL: process.env.SET_AUTHOR_POKEMON_ICON,
          })
          .setTimestamp()
          .setFooter({
            text: process.env.NIGOBOT_NAME,
            iconURL: process.env.NIGOBOT_AVATAR_LINK,
          });
    }

    if (image.length > 0) {
      embedMessage.setThumbnail('attachment://' + botService.getFilename(image));
      return {
        embedMessage: embedMessage,
        images: [image],
      };
    } else {
      return {
        embedMessage: embedMessage,
        images: '',
      };
    }
  },
};
