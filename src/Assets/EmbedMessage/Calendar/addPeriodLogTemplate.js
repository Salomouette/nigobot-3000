const Discord = require('discord.js');
const moment = require('moment');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  addPeriodLogTemplate: function(
      inputStartDate,
      inputEndDate,
      inputType,
      inputTitle,
      inputDescription,
      inputCriticality,
  ) {
    const title = tr.calendar.addPeriodLogTemplate.title
        .replace(
            '{START}',
            moment(inputStartDate, 'DD/MM/YYYY').locale('fr').format('dddd Do MMMM YYYY'),
        ).replace(
            '{END}',
            moment(inputEndDate, 'DD/MM/YYYY').locale('fr').format('dddd Do MMMM YYYY'),
        );

    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_CALENDAR_QUERY)
        .setAuthor({
          name: tr.author.calendar,
          iconURL: process.env.SET_AUTHOR_CALENDAR_ICON,
        })
        .setTitle(title)
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        })
        .addFields(
            {
              name: tr.calendar.addPeriodLogTemplate.fieldDayType,
              value: inputType,
            },
            {
              name: tr.calendar.addPeriodLogTemplate.fieldLevel,
              value: inputCriticality,
            },
            {
              name: tr.calendar.addPeriodLogTemplate.fieldTitle,
              value: inputTitle,
            },
            {
              name: tr.calendar.modifyDayTemplate.fieldDescription,
              value: inputDescription,
            },
        );
  },
};
