const Discord = require('discord.js');
const moment = require('moment');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  changeTypeDateTemplate: function(setDay, dayChanged) {
    const embed = new Discord.MessageEmbed()
        .setColor(process.env.COLOR_CALENDAR_QUERY)
        .setAuthor({
          name: tr.author.calendar,
          iconURL: process.env.SET_AUTHOR_CALENDAR_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });

    switch (typeof dayChanged) {
      case ('string'):
        embed.setTitle(tr.error.errorTitle)
            .setDescription(dayChanged);
        break;
      case ('boolean'):
        if (dayChanged) {
          embed.setTitle(tr.calendar.changeTypeDateTemplate.changeDate +
              moment(setDay, 'DD/MM/YYYY').locale('fr').format('dddd Do MMMM YYYY'));
        } else {
          embed.setTitle(tr.error.errorTitle)
              .setDescription(tr.error.errorChangeTypeDate);
        }
        break;
    }

    return embed;
  },
};
