const Discord = require('discord.js');
const moment = require('moment');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  addPeriodOutputTemplate: function(addPeriodReturn, enteredStartDate, enteredEndDate) {
    const embed = new Discord.MessageEmbed()
        .setColor(process.env.COLOR_CALENDAR_QUERY)
        .setAuthor({
          name: tr.author.calendar,
          iconURL: process.env.SET_AUTHOR_CALENDAR_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });

    const title = tr.calendar.addPeriodOutputTemplate.title
        .replace(
            '{START}',
            moment(enteredStartDate, 'DD/MM/YYYY').locale('fr').format('dddd Do MMMM YYYY'),
        ).replace(
            '{END}',
            moment(enteredEndDate, 'DD/MM/YYYY').locale('fr').format('dddd Do MMMM YYYY'),
        );
    let description = ' ';

    if (addPeriodReturn.nbDaySaved > 0) {
      description = tr.calendar.addPeriodOutputTemplate.descriptionNormal
          .replace(
              '{DAYSAVED}',
              addPeriodReturn.nbDaySaved,
          );
      if (addPeriodReturn.nbDayNotSaved > 0) {
        description = description + tr.calendar.addPeriodOutputTemplate.descriptionDayNotSaved
            .replace(
                '{DAYNOTSAVED}',
                addPeriodReturn.nbDayNotSaved,
            );
      }
    } else {
      description = tr.calendar.addPeriodOutputTemplate.decriptionNoDaySaved;
    }

    embed
        .setTitle(title)
        .setDescription(description);

    return embed;
  },
};
