const Discord = require('discord.js');
const moment = require('moment');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  setTodayCalendarTemplate: function(setDay, returnString) {
    const returnEmbed = new Discord.MessageEmbed()
        .setTitle(
            tr.calendar.setTodayCalendarTemplate.actualDate +
            moment(setDay, 'DD/MM/YYYY').locale('fr').format('dddd Do MMMM YYYY'),
        )
        .setColor(process.env.COLOR_CALENDAR_QUERY)
        .setAuthor({
          name: tr.author.calendar,
          iconURL: process.env.SET_AUTHOR_CALENDAR_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
    switch (returnString) {
      case 'newDay': {
        returnEmbed.addFields(
            {
              name: tr.calendar.setTodayCalendarTemplate.fieldNewName,
              value: tr.calendar.setTodayCalendarTemplate.fieldValue,
            },
        );
        break;
      }
      case 'oldDay': {
        returnEmbed .addFields(
            {
              name: tr.calendar.setTodayCalendarTemplate.fieldOldName,
              value: tr.calendar.setTodayCalendarTemplate.fieldValue,
            },
        );
      }
    }
    return returnEmbed;
  },
};
