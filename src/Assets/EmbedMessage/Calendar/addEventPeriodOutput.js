const Discord = require('discord.js');
const moment = require('moment');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  addEventPeriodOutput: function(nbDaySaved, enteredStartDate, enteredEndDate) {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_CALENDAR_QUERY)
        .setAuthor({
          name: tr.author.calendar,
          iconURL: process.env.SET_AUTHOR_CALENDAR_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        })
        .setTitle(tr.calendar.addEventPeriodOutputTemplate.title
            .replace(
                '{START}',
                moment(enteredStartDate, 'DD/MM/YYYY').locale('fr').format('dddd Do MMMM YYYY'),
            ).replace(
                '{END}',
                moment(enteredEndDate, 'DD/MM/YYYY').locale('fr').format('dddd Do MMMM YYYY'),
            ),
        )
        .setDescription(
            tr.calendar.addEventPeriodOutputTemplate.description
                .replace(
                    '{DAYSAVED}',
                    nbDaySaved,
                ),
        );
  },
};
