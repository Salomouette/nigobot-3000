const Discord = require('discord.js');
const moment = require('moment');

const botService = require('../../../Service/botService');
const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();

const calendarData = require('../../../Data/calendarData');

const calendarViewService = require('../../../Service/Calendar/calendarViewService');
module.exports = {
  dayCalendarTemplate: function(day, client) {
    const dayCalendarData = calendarData.getDayCalendarData(day);
    const embeds = [];
    let image = '';

    let embed = new Discord.MessageEmbed()
        .setColor(process.env.COLOR_CALENDAR_QUERY)
        .setAuthor({
          name: tr.author.calendar,
          iconURL: process.env.SET_AUTHOR_CALENDAR_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });

    // If date don't exist
    if (dayCalendarData.length === 0) {
      embed.setTitle(tr.error.errorTitle);
      embed.setDescription(tr.error.invalidDay);
    }
    // Check for string error for empty calendar Data
    if (dayCalendarData.length !== 0 && typeof dayCalendarData === 'string') {
      embed.setTitle(tr.error.errorTitle);
      embed.setDescription(dayCalendarData);
    }
    // Normal Case
    if (typeof dayCalendarData === 'object') {
      const date = moment(day, 'DD/MM/YYYY').locale('fr').format('dddd Do MMMM YYYY');
      image = botService.generatePathCalendarIcon(dayCalendarData.type);
      const dayEvents = calendarViewService.getEventsFromDate(dayCalendarData, client);
      embed.setTitle('Le ' + date + ' [1]')
          .setDescription(calendarViewService.iconCalendarDescription(dayCalendarData.type));

      // Check if day have events
      if (dayEvents.length !== 0) {
        let charCounter = 0;
        let embedCounter = 2;
        let fieldCounter = 0;
        for (let i = 0; i < dayEvents.length; i++) {
          charCounter = charCounter + dayEvents[i].name.length + dayEvents[i].value.length;
          if (charCounter >= 5000 || fieldCounter >= 25) {
            charCounter = dayEvents[i].name.length + dayEvents[i].value.length;

            if (image && image.length > 0) {
              embed.setThumbnail('attachment://' + botService.getFilename(image));
              embeds.push({
                embedMessage: embed,
                images: [image],
              });
            } else {
              embeds.push({
                embedMessage: embed,
                images: '',
              });
            }

            embed = new Discord.MessageEmbed()
                .setColor(process.env.COLOR_CALENDAR_QUERY)
                .setAuthor({
                  name: tr.author.calendar,
                  iconURL: process.env.SET_AUTHOR_CALENDAR_ICON,
                })
                .setTimestamp()
                .setFooter({
                  text: process.env.NIGOBOT_NAME,
                  iconURL: process.env.NIGOBOT_AVATAR_LINK,
                })
                .setTitle('Le ' + date + ' [' + embedCounter + ']')
                .setDescription(calendarViewService.iconCalendarDescription(dayCalendarData.type));

            embedCounter++;
          }
          embed.addFields(dayEvents[i]);
          fieldCounter++;
        }
      } else {
        embed.addFields(
            {
              name: tr.calendar.dayCalendarTemplate.fieldName,
              value: tr.calendar.dayCalendarTemplate.fieldValue,
            },
        );
      }
    }

    if (image && image.length > 0) {
      embed.setThumbnail('attachment://' + botService.getFilename(image));
      embeds.push({
        embedMessage: embed,
        images: [image],
      });
    } else {
      embeds.push({
        embedMessage: embed,
        images: '',
      });
    }

    return embeds;
  },
};
