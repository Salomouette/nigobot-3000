const Discord = require('discord.js');
const moment = require('moment');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  modifyDayTemplate: function(dayCalendarData, client) {
    const embed = new Discord.MessageEmbed();

    switch (dayCalendarData) {
      case 'invalidDate':
        embed.setColor(process.env.COLOR_CALENDAR_QUERY)
            .setAuthor({
              name: tr.author.calendar,
              iconURL: process.env.SET_AUTHOR_CALENDAR_ICON,
            })
            .setTitle(tr.error.errorTitle)
            .setDescription(tr.error.invalidDayOrId)
            .setTimestamp()
            .setFooter({
              text: process.env.NIGOBOT_NAME,
              iconURL: process.env.NIGOBOT_AVATAR_LINK,
            });
        break;
      case 'missingArgs':
        embed.setColor(process.env.COLOR_CALENDAR_QUERY)
            .setAuthor({
              name: tr.author.calendar,
              iconURL: process.env.SET_AUTHOR_CALENDAR_ICON,
            })
            .setTitle(tr.error.errorTitle)
            .setDescription(tr.error.invalidOrBadArgument)
            .setTimestamp()
            .setFooter({
              text: process.env.NIGOBOT_NAME,
              iconURL: process.env.NIGOBOT_AVATAR_LINK,
            });
        break;
      case 'calendarNotInitialized':
        embed.setColor(process.env.COLOR_CALENDAR_QUERY)
            .setAuthor({
              name: tr.author.calendar,
              iconURL: process.env.SET_AUTHOR_CALENDAR_ICON,
            })
            .setTitle(tr.error.errorTitle)
            .setDescription(tr.error.emptyCalendar)
            .setTimestamp()
            .setFooter({
              text: process.env.NIGOBOT_NAME,
              iconURL: process.env.NIGOBOT_AVATAR_LINK,
            });
        break;
      default:
        const date = moment(dayCalendarData.date, 'DD/MM/YYYY').locale('fr').format('dddd Do MMMM YYYY');
        let description = (tr.calendar.modifyDayTemplate.description);
        description = description.replace('{EVENTID}', dayCalendarData.id);
        description = description.replace('{DATE}', date);

        embed.setColor(process.env.COLOR_CALENDAR_QUERY)
            .setAuthor({
              name: tr.author.calendar,
              iconURL: process.env.SET_AUTHOR_CALENDAR_ICON,
            })
            .setTitle(tr.calendar.modifyDayTemplate.title)
            .setDescription(description)
            .setTimestamp()
            .setFooter({
              text: process.env.NIGOBOT_NAME,
              iconURL: process.env.NIGOBOT_AVATAR_LINK,
            })
            .addFields(
                {
                  name: tr.calendar.modifyDayTemplate.fieldLevel,
                  value: dayCalendarData.level,
                },
                {
                  name: tr.calendar.modifyDayTemplate.fieldTitle,
                  value: dayCalendarData.title,
                },
                {
                  name: tr.calendar.modifyDayTemplate.fieldDescription,
                  value: dayCalendarData.description,
                },
            );
    }

    return embed;
  },
};
