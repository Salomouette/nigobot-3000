const Discord = require('discord.js');

const calendarViewService = require('../../../Service/Calendar/calendarViewService');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  calendarHelpTemplate: function(client) {
    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_CALENDAR_QUERY)
        .setTitle(tr.help.calendar.helpCommandTitle)
        .setDescription(calendarViewService.iconCalendarExplanation(client))
        .addFields(
            {
              name: tr.help.calendar.viewActualDate.fieldTitle,
              value: tr.help.calendar.viewActualDate.fieldContent,
            },
            {
              name: tr.help.calendar.viewMonthDays.fieldTitle,
              value: tr.help.calendar.viewMonthDays.fieldContent,
            },
            {
              name: tr.help.calendar.viewDateEvents.fieldTitle,
              value: tr.help.calendar.viewDateEvents.fieldContent,
            },
            {
              name: tr.help.calendar.changeDay.fieldTitle,
              value: tr.help.calendar.changeDay.fieldContent,
            },
            {
              name: tr.help.calendar.addEvent.fieldTitle,
              value: tr.help.calendar.addEvent.fieldContent,
            },
            {
              name: tr.help.calendar.changeTypeDay.fieldTitle,
              value: tr.help.calendar.changeTypeDay.fieldContent,
            },
            {
              name: tr.help.calendar.addPeriod.fieldTitle,
              value: tr.help.calendar.addPeriod.fieldContent,
            },
            {
              name: tr.help.calendar.addEventPeriod.fieldTitle,
              value: tr.help.calendar.addEventPeriod.fieldContent,
            },
            {
              name: tr.help.calendar.modifyDayCommand.fieldTitle,
              value: tr.help.calendar.modifyDayCommand.fieldContent,
            },
            {
              name: tr.help.calendar.modifyOrderCommand.fieldTitle,
              value: tr.help.calendar.modifyOrderCommand.fieldContent,
            },
            {
              name: tr.help.calendar.values.fieldTitle,
              value: tr.help.calendar.values.fieldContent,
            },
        )
        .setAuthor({
          name: tr.author.calendar,
          iconURL: process.env.SET_AUTHOR_HELP_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });
  },
};
