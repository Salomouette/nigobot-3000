const Discord = require('discord.js');
const moment = require('moment');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();

const calendarData = require('../../../Data/calendarData');

const calendarViewService = require('../../../Service/Calendar/calendarViewService');
module.exports = {
  monthDaysCalendarTemplate: function(month, client) {
    const date = moment(moment('01/' + month, 'DD/MM/YYYY'));
    const monthCalendarData = calendarData.displayMonthDaysCalendarData(month);

    const embed = new Discord.MessageEmbed()
        .setColor(process.env.COLOR_CALENDAR_QUERY)
        .setAuthor({
          name: tr.author.calendar,
          iconURL: process.env.SET_AUTHOR_CALENDAR_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });

    if (monthCalendarData.length === 0) {
      embed.setTitle(tr.error.errorTitle);
      embed.setDescription(tr.error.emptyCalendarForSelectedPeriod);
    } else {
      const displayDayMonthData = calendarViewService.displayDayMonthData(monthCalendarData, client);
      embed.setTitle(
          tr.calendar.viewMonthDays.title
              .replace(
                  '{MONTH}',
                  date.locale('fr').format('MMMM'),
              ).replace(
                  '{YEAR}',
                  date.locale('fr').format('YYYY'),
              ),
      );
      embed.setDescription(displayDayMonthData);
    }

    return embed;
  },
};
