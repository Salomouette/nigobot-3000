const Discord = require('discord.js');
const moment = require('moment');
const lodash = require('lodash');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();

const calendarViewService = require('../../../Service/Calendar/calendarViewService');
module.exports = {
  modifyOrderPreviewTemplate: function(dayEventsOrder, client, displayInfo) {
    displayInfo = displayInfo ? displayInfo : false;
    dayEventsOrder.events = lodash.orderBy(dayEventsOrder.events, 'order', 'asc');

    const embed = new Discord.MessageEmbed()
        .setColor(process.env.COLOR_CALENDAR_QUERY)
        .setAuthor({
          name: tr.author.calendar,
          iconURL: process.env.SET_AUTHOR_CALENDAR_ICON,
        })
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        });


    if (dayEventsOrder.events.length === 0) {
      embed.setTitle(tr.error.errorTitle);
      embed.setDescription(tr.error.dayNoEvent);
    } else {
      const displayDayEventsOrder = calendarViewService.displayDayEventsOrder(dayEventsOrder, client);

      embed.setTitle(
          tr.calendar.modifyOrderPreview.title
              .replace(
                  '{DATE}',
                  moment(dayEventsOrder.date, 'DD/MM/YYYY').locale('fr').format('dddd Do MMMM YYYY'),
              ),
      );

      if (displayInfo) {
        embed.addFields(
            {
              name: tr.calendar.modifyOrderPreview.nameField,
              value: tr.calendar.modifyOrderPreview.valueField,
            },
        );
      }
      embed.setDescription(displayDayEventsOrder);
    }

    return embed;
  },
};
