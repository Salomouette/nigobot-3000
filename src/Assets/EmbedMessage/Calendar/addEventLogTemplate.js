const Discord = require('discord.js');
const moment = require('moment');

const frenchTranslation = require('../../../Data/frenchTranslationData');
const tr = frenchTranslation.getFrenchTranslations();
module.exports = {
  addEventLogTemplate: function(inputDate, inputTitle, inputDescription, inputCriticality) {
    const title = tr.calendar.addEventLogTemplate.title
        .replace(
            '{DATE}',
            moment(inputDate, 'DD/MM/YYYY').locale('fr').format('dddd Do MMMM YYYY'),
        );

    return new Discord.MessageEmbed()
        .setColor(process.env.COLOR_CALENDAR_QUERY)
        .setAuthor({
          name: tr.author.calendar,
          iconURL: process.env.SET_AUTHOR_CALENDAR_ICON,
        })
        .setTitle(title)
        .setTimestamp()
        .setFooter({
          text: process.env.NIGOBOT_NAME,
          iconURL: process.env.NIGOBOT_AVATAR_LINK,
        })
        .addFields(
            {
              name: tr.calendar.modifyDayTemplate.fieldLevel,
              value: inputCriticality,
            },
            {
              name: tr.calendar.modifyDayTemplate.fieldTitle,
              value: inputTitle,
            },
            {
              name: tr.calendar.modifyDayTemplate.fieldDescription,
              value: inputDescription,
            },
        );
  },
};
