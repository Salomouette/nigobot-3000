# Nigobot 3000
## Installation
### Basic
Make sure node is installed and version above or equal as 14.15.4. If not install Node.js.

Clone the repository in the folder you want to work with.

Run npm install (Install it if not installed on your environment) for package installation at the root of the project.
```
npm install
```

All assets image are on a sub repository (https://bitbucket.org/Salomouette/nigobot-3000-images/src/master/).
They are saved in the main git repo as submodule. Run the two command bellow to initialise this folder.
```
git submodule init
git submodule update
```
Be sure to commit changes in the correct repo when you add images to this folder.
### Create a bot account
To work and see what you change on the bot, you will need to create a discord application in the dev portal of Discord:
```
https://discordapp.com/developers/applications/ 
```
Create the child bot with a relevant name (Like Nigobot 3001). Open the app and on the bot part in the left, click on 'Add Bot' and accept. You can see the child bot token here, it will be needed for your child bot to boot.

### env file
Add the yaml env in a .env file. You need to change 6 parameters:
DISCORD_TOKEN, add the token obtain in the previous part.
ABSOLUTE_PATH, add the absolute path were is save the repository.
NIGOBOT_COMMAND_TAG, the tag needed for bot's response. For the Nigobot it's 'nb!', but you can have something else.
ADMIN_ID, add your account ID here.
For the two missing ones see below.

### Create dynamic data json folder
The bot need some json files that will be read, update and write. Use this command to initialize them:
```
nb!système startDatabases
```
### Setup log discord server
You need to create a discord server how can serve to log infos. 
After you create it you will need to create two channels, one for our personal logs and one for general logs. 
When it's done copy and paste the discord id in DASHBOARD_GUILD_ID, do the same for the general log channel in DASHBOARD_LOGS_GENERAL_CHANNEL.
Create another one if you want to separate logs of found item message, if not use the same ID for DASHBOARD_LOGS_FOUND_ITEM_CHANNEL.

### Setup emojis servers
The bot needs 4 emojis server to run, create 4 blank servers and add your bot in those. 
Be carefull to add the admin permission for your bot in these servers, or it will not be permitted.
You have to wait around 10 minutes to launch each commands because of discord API limitation for this type of usage.

First command in the first server (Emoji 1 to 50)
```
nb!système setupEmojiServer1
```
Second command in the second server (Emoji 51 to 100)
```
nb!système setupEmojiServer2
```
Third command in the third server (Emoji 101 to 150)
```
nb!système setupEmojiServer3
```
4th command in the 4th server (Emoji 151 and 200)
```
nb!système setupEmojiServer4
```
If one of the command fail during uploading all the emojis, you can delete all emoji in server with
```
nb!système deleteAllEmojiInServer
```
Be extremely carefull with this command! **Make sure to never use it in servers that are NOT emoji bank for your bot**.

### Setup basic user
Add a player user (yourself):
```
nb!système addPlayer X
```
Add the id of your private log channel create before with the following command:
```
nb!système addLogChannelId yourId yourPrivateLogChannelId
```
Initialize money management for your user:
```
nb!système initializeMoney
```

#Eslint
Eslint is configured for style errors and mistakes. Run it after working on a part/feature. Run this command to check every files of the project.
```
npx eslint .
```
If tou want it to autocorrect small issues:
```
npx eslint . --fix
```