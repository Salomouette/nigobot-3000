// Import
require('dotenv').config();
// const Discord = require('discord.js');
const {Client, Intents} = require('discord.js');
const commands = require('./src/Controller/botController.js');
const Jimp = require('jimp');

const bot = new Client({
  partials: ['CHANNEL'],
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MEMBERS,
    Intents.FLAGS.GUILD_MESSAGES,
    Intents.FLAGS.DIRECT_MESSAGES,
    Intents.FLAGS.DIRECT_MESSAGE_TYPING,
    Intents.FLAGS.GUILD_EMOJIS_AND_STICKERS,
  ],
});

bot.on('ready', () => {
  console.log(`Logged in as ${bot.user.tag}!`);

  bot.user.setActivity(process.env.NIGOBOT_ACTIVITY_NAME, {
    type: process.env.NIGOBOT_ACTIVITY,
  });
});
bot.login(process.env.DISCORD_TOKEN);

// Commands
commands.commandsController(bot, Jimp);
