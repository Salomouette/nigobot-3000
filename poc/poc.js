const request = require('request')

if (msg.content.toString().includes('nb!test')) {
  const nb = msg.content.split(' ')[1]
  const variationMin = -10
  const variationMax = 10

  Jimp.read('src/Assets/Image/Pokémon/Normal/' + nb + '.png')
    .then(image => {
      image.resize(image.bitmap.width / 4, Jimp.AUTO)
      image.color([
        { apply: 'hue', params: [variationMin] }
      ])
      image.write('TEST1.png')
      msg.channel.send('-' + variationMin + '  de nuance', { files: ['C:\\Users\\Salom\\PhpstormProjects\\nigobot-3000\\TEST1.png'] })
    })
  msg.channel.send('Normal', { files: ['C:\\Users\\Salom\\PhpstormProjects\\nigobot-3000\\src\\Assets\\Image\\Pokémon\\Normal\\' + nb + '.png'] })
  Jimp.read('src/Assets/Image/Pokémon/Normal/' + nb + '.png')
    .then(image => {
      image.resize(image.bitmap.width / 4, Jimp.AUTO)
      image.color([
        { apply: 'hue', params: [variationMax] }
      ])
      image.write('TEST2.png')
      msg.channel.send('+' + variationMax + ' de nuance', { files: ['C:\\Users\\Salom\\PhpstormProjects\\nigobot-3000\\TEST2.png'] })
    })
}

if (msg.content === process.env.NIGOBOT_COMMAND_TAG.concat('', 'api')) {
  request('http://127.0.0.1:8000/item/1', function (error, response, body) {
    msg.reply(body)
  })
  request('http://127.0.0.1:8000/item/2', function (error, response, body) {
    msg.reply(body)
  })
}
